require("./runtime.js");require("./vendor.js");module.exports =
(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["bundle"],{

/***/ "../$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "gr-app",
            template: "<page-router-outlet></page-router-outlet>"
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("nativescript-theme-core/css/core.light.css", () => __webpack_require__("../node_modules/nativescript-dev-webpack/css2json-loader.js?useForImports!../node_modules/nativescript-theme-core/css/core.light.css"));module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"import","import":"\"nativescript-theme-core/css/core.light.css\""},{"type":"rule","selectors":[".btn-primary"],"declarations":[{"type":"declaration","property":"height","value":"50"},{"type":"declaration","property":"background-color","value":"rgb(26, 91, 213)"},{"type":"declaration","property":"border-radius","value":"5"},{"type":"declaration","property":"font-size","value":"20"},{"type":"declaration","property":"font-weight","value":"600"}]},{"type":"rule","selectors":[".btn-second"],"declarations":[{"type":"declaration","property":"height","value":"35"},{"type":"declaration","property":"background-color","value":"rgb(13, 100, 172)"},{"type":"declaration","property":"color","value":"whitesmoke"},{"type":"declaration","property":"border-radius","value":"5"},{"type":"declaration","property":"font-size","value":"12"},{"type":"declaration","property":"font-weight","value":"600"},{"type":"declaration","property":"width","value":"105"}]},{"type":"rule","selectors":[".btn-primary:disabled"],"declarations":[{"type":"declaration","property":"opacity","value":"0.5"}]},{"type":"rule","selectors":[".btn-second:disabled"],"declarations":[{"type":"declaration","property":"opacity","value":"0.5"}]},{"type":"rule","selectors":[".card"],"declarations":[{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"margin","value":"12"},{"type":"declaration","property":"padding","value":"10"},{"type":"declaration","property":"background-color","value":"#E3E9F8"},{"type":"declaration","property":"color","value":"#131636"},{"type":"declaration","property":"height","value":"86%"}]}],"parsingErrors":[]}};;
    if (true) {
        module.hot.accept();
        module.hot.dispose(() => {
            global.hmrRefresh({ type: 'style', path: './app.css' });
        })
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/forms/index.js");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/nativescript-angular/http-client/index.js");
/* harmony import */ var nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/nativescript.module.js");
/* harmony import */ var nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./app.routing.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./app.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("./login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("./register/register.component.ts");
/* harmony import */ var _register2_register_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("./register2/register.component.ts");
/* harmony import */ var _student_student_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("./student/student.component.ts");
/* harmony import */ var _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__("./teacher/teacher.component.ts");
/* harmony import */ var _session_code_session_code_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__("./session_code/session_code.component.ts");
/* harmony import */ var _identification_identification_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__("./identification/identification.component.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__("./shared/user/authenticate.service.ts");
/* harmony import */ var _helpers_index__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__("./helpers/index.ts");

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3__["NativeScriptModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1__["NativeScriptFormsModule"],
                nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2__["NativeScriptHttpClientModule"],
                nativescript_angular_router__WEBPACK_IMPORTED_MODULE_4__["NativeScriptRouterModule"],
                nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3__["NativeScriptModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1__["NativeScriptFormsModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2__["NativeScriptHttpClientModule"]
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"],
                _register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"],
                _register2_register_component__WEBPACK_IMPORTED_MODULE_9__["Register2Component"],
                _student_student_component__WEBPACK_IMPORTED_MODULE_10__["StudentComponent"],
                _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_11__["TeacherComponent"],
                _session_code_session_code_component__WEBPACK_IMPORTED_MODULE_12__["Session_codeComponent"],
                _identification_identification_component__WEBPACK_IMPORTED_MODULE_13__["IdentificationComponent"]
            ],
            providers: [
                _shared_user_user_service__WEBPACK_IMPORTED_MODULE_14__["UserService"],
                _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_15__["AuthenticateService"],
                _helpers_index__WEBPACK_IMPORTED_MODULE_16__["JwtInterceptorProvider"]
                //ErrorInterceptorProvider
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./register/register.component.ts");
/* harmony import */ var _register2_register_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./register2/register.component.ts");
/* harmony import */ var _student_student_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./student/student.component.ts");
/* harmony import */ var _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./teacher/teacher.component.ts");
/* harmony import */ var _session_code_session_code_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("./session_code/session_code.component.ts");
/* harmony import */ var _identification_identification_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("./identification/identification.component.ts");









var routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "login", component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: "register", component: _register_register_component__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"] },
    { path: "register2", component: _register2_register_component__WEBPACK_IMPORTED_MODULE_4__["Register2Component"] },
    { path: "student", component: _student_student_component__WEBPACK_IMPORTED_MODULE_5__["StudentComponent"] },
    { path: "teacher", component: _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_6__["TeacherComponent"] },
    { path: "session_code", component: _session_code_session_code_component__WEBPACK_IMPORTED_MODULE_7__["Session_codeComponent"] },
    { path: "identification", component: _identification_identification_component__WEBPACK_IMPORTED_MODULE_8__["IdentificationComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forRoot(routes)],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./helpers/error.interceptor.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return ErrorInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptorProvider", function() { return ErrorInterceptorProvider; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/rxjs/add/observable/throw.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/rxjs/add/operator/catch.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4__);





var ErrorInterceptor = /** @class */ (function () {
    function ErrorInterceptor() {
    }
    ErrorInterceptor.prototype.intercept = function (request, next) {
        // extract error message from http body if an error occurs
        return next.handle(request).catch(function (errorResponse) {
            return rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(errorResponse.error);
        });
    };
    ErrorInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], ErrorInterceptor);
    return ErrorInterceptor;
}());

var ErrorInterceptorProvider = {
    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HTTP_INTERCEPTORS"],
    useClass: ErrorInterceptor,
    multi: true,
};


/***/ }),

/***/ "./helpers/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _error_interceptor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./helpers/error.interceptor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return _error_interceptor__WEBPACK_IMPORTED_MODULE_0__["ErrorInterceptor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptorProvider", function() { return _error_interceptor__WEBPACK_IMPORTED_MODULE_0__["ErrorInterceptorProvider"]; });

/* harmony import */ var _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./helpers/jwt.interceptor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__["JwtInterceptor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptorProvider", function() { return _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__["JwtInterceptorProvider"]; });





/***/ }),

/***/ "./helpers/jwt.interceptor.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptorProvider", function() { return JwtInterceptorProvider; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/common/fesm5/http.js");


var JwtInterceptor = /** @class */ (function () {
    function JwtInterceptor() {
    }
    JwtInterceptor.prototype.intercept = function (request, next) {
        // add authorization header with jwt token if available
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + currentUser.token
                }
            });
        }
        return next.handle(request);
    };
    JwtInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], JwtInterceptor);
    return JwtInterceptor;
}());

var JwtInterceptorProvider = {
    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HTTP_INTERCEPTORS"],
    useClass: JwtInterceptor,
    multi: true,
};


/***/ }),

/***/ "./identification/identification.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n\tvertical-align: center;\r\n\tfont-size: 20;\r\n\tmargin: 15;\r\n}\r\n\r\n.header {\r\n\thorizontal-align: center;\r\n\tfont-size: 25;\r\n\tfont-weight: 600;\r\n\tmargin-bottom: 20;\r\n\ttext-align: center;\r\n\tcolor: rgb(38, 8, 99);\r\n  }"

/***/ }),

/***/ "./identification/identification.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Student Identification\" class=\"action-bar\">\r\n</ActionBar>\r\n\r\n<StackLayout>\r\n    <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\r\n    <Label  class=\"header\" text=\"Registro de asistencia en clase\"\r\n    horizontalAlignment=\"center\" class=\"header\"></Label>\r\n\r\n    <ScrollView class=\"page\">\r\n        <StackLayout class=\"home-panel\">\r\n            <GridLayout rows=\"80 80 80 80 80\">\r\n                <Button  row=\"0\" text=\"Lector QR\"  background-color:rgb(76, 91, 213) (tap)=\"scanBarcode()\"></Button>\r\n                <Button  row=\"1\" text=\"Lector NFC\" background-color:rgb(76, 91, 213) (tap)=\"mensajeReadNFC()\" (tap)=\"ReadNFC()\"></Button>\r\n                <Button  row=\"2\" text=\"Manual\" background-color:rgb(76, 91, 213) (tap)=\"mostrarmensaje()\"></Button>\r\n                <Button  row=\"3\"text=\"Prueba Update\" background-color:rgb(200, 91, 213) (tap)=\"update()\"></Button>\r\n                <Button  row=\"4\"text=\"Cerrar Sesión\" class=\"btn btn-primary m-t-20\" (tap)=\"logout()\"></Button>\r\n            </GridLayout>\r\n        </StackLayout>\r\n    </ScrollView>\r\n</StackLayout>"

/***/ }),

/***/ "./identification/identification.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdentificationComponent", function() { return IdentificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("../node_modules/nativescript-barcodescanner/barcodescanner.js");
/* harmony import */ var nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var nativescript_nfc__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-nfc/nfc.js");
/* harmony import */ var nativescript_nfc__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_nfc__WEBPACK_IMPORTED_MODULE_6__);







var IdentificationComponent = /** @class */ (function () {
    function IdentificationComponent(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.nfc = new nativescript_nfc__WEBPACK_IMPORTED_MODULE_6__["Nfc"]();
        this.currentUser = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_2__["User"]();
        this.page.actionBarHidden = true;
        //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = this.userService.currentUser();
    }
    IdentificationComponent.prototype.ngOnInit = function () {
    };
    IdentificationComponent.prototype.back = function () {
        this.routerExtensions.back();
    };
    IdentificationComponent.prototype.scanBarcode = function () {
        var _this = this;
        new nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_5__["BarcodeScanner"]().scan({
            formats: "QR_CODE, EAN_13",
            cancelLabel: "EXIT. Also, try the volume buttons!",
            cancelLabelBackgroundColor: "#333333",
            message: "Use the volume buttons for extra light",
            showFlipCameraButton: true,
            preferFrontCamera: false,
            showTorchButton: true,
            beepOnScan: true,
            fullScreen: true,
            torchOn: false,
            closeCallback: function () { console.log("Scanner closed"); },
            resultDisplayDuration: 500,
            openSettingsIfPermissionWasPreviouslyDenied: true,
            presentInRootViewController: true // iOS-only; If you're sure you're not presenting the (non embedded) scanner in a modal, or are experiencing issues with fi. the navigationbar, set this to 'true' and see if it works better for your app (default false).
        }).then(function (result) {
            _this.mensaje = result.text,
                _this.mensaje = _this.mensaje.replace(/&#34;/gi, "\""),
                _this.function1();
            //alert({
            //title: "Scan result",
            //message: "Format: " + result.format + ",\nValue: " + result.text,
            //okButtonText: "OK"
            //});
        }, function (errorMessage) {
            console.log("No scan. " + errorMessage);
        });
        //if(this.valid_QR==false)
        alert("QR no válido");
    };
    IdentificationComponent.prototype.function1 = function () {
        if (this.saveJSON())
            this.update(this.currentUser);
        else
            alert("QR no válido");
    };
    IdentificationComponent.prototype.saveJSON = function () {
        // /mensaje/gi sirve para que reemplace todos los valores encontrados, porque sin esto solo reemplaza el primero
        try {
            var json = JSON.parse(this.mensaje);
            if (json.clase && json.asiento) {
                this.currentUser.class = json.clase;
                this.currentUser.seat = json.asiento;
                //localStorage.setItem("currentUser",JSON.stringify(this.currentUser));
                console.log("Mensaje de Prueba");
                return true;
            }
            else {
                this.valid_QR = false;
                alert("QR no válido");
                console.log("QR no válido 1");
                return false;
            }
        }
        catch (_a) {
            this.valid_QR = false;
            alert("QR no válido");
            console.log("QR no válido 1");
            return false;
        }
    };
    IdentificationComponent.prototype.mostrarValores = function () {
        console.log(JSON.stringify(this.currentUser));
        alert(JSON.stringify(this.currentUser));
        //alert("La clase leída del qr es: " + this.currentUser.class + "\n El asiento leído del qr es: " +  this.currentUser.seat
        //+ "\n El id de la sesión  es: " +  this.currentUser._id + this.currentUser.username );
    };
    IdentificationComponent.prototype.update = function (currentUser) {
        var _this = this;
        this.userService.setUser(this.currentUser);
        //localStorage.setItem("currentUser",JSON.stringify(currentUser));
        console.log(this.currentUser.seat);
        console.log(JSON.stringify(this.currentUser));
        this.userService.update(this.currentUser).subscribe(function (data) {
            alert("Registrado en la clase con éxito");
            _this.routerExtensions.navigate(["/student"]);
        }, function (error) {
            alert("Error de registro en clase");
        });
    };
    IdentificationComponent.prototype.mensajeReadNFC = function () {
        alert("Por favor acerque un chip NFC");
    };
    IdentificationComponent.prototype.ReadNFC = function () {
        var _this = this;
        this.nfc.setOnNdefDiscoveredListener(function (data) {
            // data.message is an array of records, so:
            if (data.message) {
                //for (let m in data.message) {
                var record = data.message[0];
                _this.mensaje = record.payloadAsString;
                _this.mensaje = _this.mensaje.replace(/&#34;/gi, "\""),
                    _this.saveJSON(),
                    _this.update(_this.currentUser);
                //alert(
                //"\n data.message: " + record.payloadAsString
                //)         
            }
        }, {
            // iOS-specific options
            stopAfterFirstRead: true,
            scanHint: "Scan a tag"
        }).then(function () {
            console.log("OnNdefDiscovered listener added");
        });
    };
    IdentificationComponent.prototype.logout = function () {
        var _this = this;
        this.currentUser = this.userService.currentUser();
        this.userService.logout(this.currentUser._id, this.currentUser).subscribe(function (data) {
            _this.routerExtensions.navigate(['/login']);
        }, function (error) {
            alert("Error al cerrar sesión");
        });
    };
    IdentificationComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"] }
    ]; };
    IdentificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "identification",
            providers: [_shared_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]],
            template: __importDefault(__webpack_require__("./identification/identification.component.html")).default,
            styles: [__importDefault(__webpack_require__("./identification/identification.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4__["Page"], _shared_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"]])
    ], IdentificationComponent);
    return IdentificationComponent;
}());



/***/ }),

/***/ "./login/login.component.css":
/***/ (function(module, exports) {

module.exports = ".page {\r\n  align-items: center;\r\n  flex-direction: column;\r\n}\r\n.form {\r\n  margin-left: 30;\r\n  margin-right: 30;\r\n  flex-grow: 2;\r\n  vertical-align: middle;\r\n}\r\n\r\n.logo {\r\n  margin-bottom: 32;\r\n  font-weight: bold;\r\n}\r\n\r\n.boton{\r\n  width: 200;\r\n  height: 50;\r\n  background-color: rgb(26, 91, 213);\r\n  color: whitesmoke;\r\n  border-radius: 5;\r\n  font-size: 20;\r\n  font-weight: 600;\r\n}\r\n.logo2{\r\n  margin-bottom: 12;\r\n  height: 100;\r\n  horizontal-align: center;\r\n  font-weight: bold;\r\n}\r\n.header {\r\n  horizontal-align: center;\r\n  font-size: 25;\r\n  font-weight: 600;\r\n  margin-bottom: 20;\r\n  text-align: center;\r\n  color: rgb(38, 8, 99);\r\n}\r\n\r\n.input-field {\r\n  margin-bottom: 25;\r\n}\r\n.input {\r\n  font-size: 18;\r\n  placeholder-color: #A8A8A8;\r\n}\r\n.input:disabled {\r\n  background-color: rgb(255, 255, 255);\r\n  opacity: 0.5;\r\n}\r\n\r\n.btn-primary {\r\n  margin: 15 5 15 5;\r\n}\r\n\r\n.btn-second {\r\n  margin: 15 5 15 5;\r\n}\r\n\r\n.login-label {\r\n  horizontal-align: center;\r\n  color: #A8A8A8;\r\n  font-size: 16;\r\n}\r\n.sign-up-label {\r\n  margin-bottom: 20;\r\n}\r\n.bold {\r\n  color: #000000; \r\n}\r\n\r\n"

/***/ }),

/***/ "./login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Login Page\"></ActionBar>\r\n\r\n<FlexboxLayout class=\"page\">\r\n    <StackLayout>\r\n            <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\r\n            <Image class=\"logo2\" src=\"~/images/erisid2.png\"></Image>\r\n    </StackLayout>\r\n\r\n    <StackLayout class=\"form\">\r\n        <Label class=\"header\" text=\"PROYECTO SIDISEC\"></Label>\r\n        <GridLayout columns=\"*,*\" rows=\"60,*,*,*\" width=\"280\" height=\"300\"\r\n            backgroundColor=\"lightgray\" borderColor=\"rgb(15, 5, 59)\" borderwidth=\"3\">\r\n            <Button text=\"Alumnoss\" row=\"0\" col=\"0\" class=\"btn btn-second\" \r\n            (tap)=\"select()\"\r\n          ></Button>\r\n\r\n            <Label row=\"0\" col=\"0\" *ngIf=\"!selection\" backgroundColor=\"white\">\r\n            </Label>\r\n\r\n            <Button text=\"Profesores\" row=\"0\" col=\"1\" class=\"btn btn-second\" (tap)=\"select2()\"></Button>\r\n            <Label row=\"0\" col=\"1\" *ngIf=\"selection\" backgroundColor=\"white\">\r\n            </Label>\r\n\r\n            <TextField row=\" 1\" col=\"0\" colSpan=\"2\" backgroundColor=\"white\"\r\n                width=\"260\" height=\"50\" class=\"input\" hint=\"Email\" \r\n                [isEnabled]=\"!processing\" keyboardType=\"email\"\r\n                autocorrect=\"false\" autocapitalizationType=\"none\"\r\n                [(ngModel)]=\"user.username\">\r\n            </TextField>\r\n\r\n                <TextField #password class=\"input\" row=\"2\" col=\"0\" colSpan=\"2\"\r\n                    backgroundColor=\"white\" width=\"260\" height=\"50\"\r\n                    [isEnabled]=\"!processing\" hint=\"Password\" secure=\"true\"\r\n                    [(ngModel)]=\"user.password\"\r\n                    [returnKeyType]=\"isLoggingIn ? 'done' : 'next'\"\r\n                    (returnPress)=\"focusConfirmPassword()\"></TextField>\r\n            \r\n                 <Button [text]=\"isLoggingIn ? 'Login' : 'Sign Up'\" row=\"3\"\r\n                    col=\"0\" *ngIf=\"selection\" [isEnabled]=\"!processing\"\r\n                    (tap)=\"submit()\" class=\"btn btn-primary m-t-20\"></Button>\r\n                <Button [text]=\"'Login Profesor'\" row=\"3\"\r\n                    col=\"0\" colSpan=\"2\" *ngIf=\"!selection\" [isEnabled]=\"!processing\"\r\n                    class=\"boton\" (tap)=\"loginteacher()\"></Button>\r\n\r\n                <Button [text]=\"'Registro'\" row=\"3\" col=\"1\"\r\n                    class=\"btn btn-primary m-t-20\" *ngIf=\"selection\" (tap)=\"register2()\"></Button>\r\n\r\n\r\n        </GridLayout>\r\n        <Label *ngIf=\"isLoggingIn\" text=\"¿Olvidaste la contraseña?\"\r\n            class=\"login-label\" (tap)=\"forgotPassword()\"></Label>\r\n    </StackLayout>\r\n</FlexboxLayout>"

/***/ }),

/***/ "./login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./shared/user/authenticate.service.ts");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6__);







var LoginComponent = /** @class */ (function () {
    function LoginComponent(page, authenticateService, routerExtensions) {
        this.page = page;
        this.authenticateService = authenticateService;
        this.routerExtensions = routerExtensions;
        this.isLoggingIn = true;
        this.selection = true;
        this.user = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_1__["User"]();
        this.page.actionBarHidden = true;
        //this.user.username="prueba930@gmail.com";
        //this.user.password="prueba930";
    }
    LoginComponent.prototype.select = function () {
        this.selection = true;
    };
    LoginComponent.prototype.select2 = function () {
        this.selection = false;
    };
    LoginComponent.prototype.submit = function () {
        if (this.isLoggingIn) {
            this.login();
        }
        else {
            this.signUp();
        }
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.authenticateService.login(this.user)
            .subscribe(function (response) {
            _this.authenticateService.setUser(response);
            _this.routerExtensions.navigate(["/session_code"]);
            console.log(_this.authenticateService.currentUser());
            console.log(response);
        }, function (exception) {
            if (exception.error && exception.error.description) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception.error.description);
            }
            else {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception.error);
            }
        });
    };
    LoginComponent.prototype.register2 = function () {
        this.routerExtensions.navigate(["/register"]);
    };
    LoginComponent.prototype.loginteacher = function () {
        Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])("Bienvenido");
        this.routerExtensions.navigate(["/teacher"]);
    };
    LoginComponent.prototype.signUp = function () {
        var _this = this;
        this.authenticateService.register(this.user)
            .subscribe(function () {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])("Your account was successfully created.");
            _this.toggleDisplay();
        }, function (exception) {
            if (exception.error && exception.error.description) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception.error.description);
            }
            else {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception);
            }
        });
    };
    LoginComponent.prototype.forgotPassword = function () {
        if (this.user.username)
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])("Se ha mandado un enlace a su correo para restaurar la contraseña");
        else
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])("Por favor introduzca un correo para restaurar la contraseña");
    };
    LoginComponent.prototype.toggleDisplay = function () {
        this.isLoggingIn = !this.isLoggingIn;
    };
    LoginComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5__["Page"] },
        { type: _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticateService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterExtensions"] }
    ]; };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "gr-login",
            providers: [_shared_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]],
            template: __importDefault(__webpack_require__("./login/login.component.html")).default,
            styles: [__importDefault(__webpack_require__("./login/login.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5__["Page"], _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticateService"], nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterExtensions"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-angular/platform.js");
/* harmony import */ var nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app.module.ts");

        let applicationCheckPlatform = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
        if (applicationCheckPlatform.android && !global["__snapshot"]) {
            __webpack_require__("../node_modules/tns-core-modules/ui/frame/frame.js");
__webpack_require__("../node_modules/tns-core-modules/ui/frame/activity.js");
        }

        
            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-angular.js")();
            
            
        if (true) {
            const hmrUpdate = __webpack_require__("../node_modules/nativescript-dev-webpack/hmr/index.js").hmrUpdate;
            global.__initialHmrUpdate = true;
            global.__hmrSyncBackup = global.__onLiveSync;

            global.__onLiveSync = function () {
                hmrUpdate();
            };

            global.hmrRefresh = function({ type, path } = {}) {
                if (global.__initialHmrUpdate) {
                    return;
                }

                setTimeout(() => {
                    global.__hmrSyncBackup({ type, path });
                });
            };

            hmrUpdate().then(() => {
                global.__initialHmrUpdate = false;
            })
        }
        
            
        __webpack_require__("../node_modules/tns-core-modules/bundle-entry-points.js");
        


var options_Generated = {};

if (true) {
    options_Generated = {
        hmrOptions: {
            moduleTypeFactory: function () { return __webpack_require__("./app.module.ts").AppModule; },
            livesyncCallback: function (platformReboot) { setTimeout(platformReboot, 0); }
        }
    };
}

if (true) {
    module["hot"].accept(["./app.module.ts"], function(__WEBPACK_OUTDATED_DEPENDENCIES__) { /* harmony import */ _app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app.module.ts");
(function () {
        global["hmrRefresh"]({});
    })(__WEBPACK_OUTDATED_DEPENDENCIES__); });
}
Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__["platformNativeScriptDynamic"](Object.assign({}, options_Generated)).bootstrapModule(_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]);

    
        
        
    
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"android":{"v8Flags":"--expose_gc","forceLog":true,"markingMode":"none"},"name":"nativescript-template-ng-groceries","version":"3.0.0","main":"main.js"};

/***/ }),

/***/ "./register/register.component.css":
/***/ (function(module, exports) {

module.exports = "\n.header {\n\n  font-size: 25;\n  font-weight: 600;\n  margin-bottom: 20;\n  text-align: center;\n  color: rgb(38, 8, 99);\n}\n\n.home-panel{\n    vertical-align: center; \n    font-size: 20;\n    margin: 15;\n}\n\n.logo {\n    margin-bottom: 32;\n    font-weight: bold;\n  }\n  \n.description-label{\n    margin-bottom: 15;\n}\n\n.form {\n    margin: 20;\n}\n\n.label {\n    margin-right: 5;\n    vertical-align: center;\n}\n\n.input {\n    margin-bottom: 10;\n}\n"

/***/ }),

/***/ "./register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<FlexboxLayout>\n    <ScrollView>\n        <StackLayout>\n                <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\n                <Label text=\"Formulario de Registro\"\n                horizontalAlignment=\"center\" class=\"header\"></Label>\n            <GridLayout rows=\"*, *, *, *, *\" columns=\"83, *\" class=\"form\">\n\n                <Label row=\"0\" col=\"0\" text=\"Nombre:\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"0\" col=\"1\" [(ngModel)]=\"model.firstName\"\n                    hint=\"Escriba su nombre\" class=\"input input-border\" name=\"lastName\" required>\n                </TextField>\n\n                <Label row=\"1\" col=\"0\" text=\"Apellidos\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"1\" col=\"1\" [(ngModel)]=\"model.lastName\"\n                    hint=\"Escriba sus apellidos\" class=\"input input-border\"\n                    keyboardType=\"email\"></TextField>\n\n                <Label row=\"2\" col=\"0\" text=\"Email:\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"2\" col=\"1\" [(ngModel)]=\"model.username\"\n                    hint=\"Escriba su email\" class=\"input input-border\"\n                    keyboardType=\"email\" name=\"username\" required></TextField>\n\n                <Label row=\"3\" col=\"0\" text=\"Matrícula:\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"3\" col=\"1\" [(ngModel)]=\"model.numat\"\n                    hint=\"Escriba su número de matrícula\"\n                    maxLength=\"5\"\n                    class=\"input input-border\" name=\"numat\" required>\n                </TextField>\n                <Label row=\"4\" col=\"0\" text=\"Contraseña:\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"4\" col=\"1\" [type]=\"hide ? 'password' : 'text'\" [(ngModel)]=\"model.password\"\n                    hint=\"Escriba su contraseña\" class=\"input input-border\" name=\"password\" required secure=\"true\">\n                </TextField>\n            </GridLayout>\n\n            <Button text=\"Seleccionar Imagen\"  backgroundcolor=\"green\" row=\"0\" col=\"1\" class=\"btn btn-primary\"\n            (tap)=\"image_picker()\"></Button>\n\n            <GridLayout rows=\"*\" columns=\"*,*\" width=\"310\" height=\"80\">\n                <Button text=\"Registro\" row=\"0\" col=\"0\"\n                    class=\"btn btn-primary\" (tap)=\"onUpload()\">\n                </Button>\n\n                <Button text=\"Volver\" row=\"0\" col=\"1\" class=\"btn btn-primary\"\n                    (tap)=\"back()\">\n                </Button>\n            </GridLayout>\n\n        </StackLayout>\n    </ScrollView>\n</FlexboxLayout>"

/***/ }),

/***/ "./register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-imagepicker/imagepicker.js");
/* harmony import */ var nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_6__);







var fs = __webpack_require__("../node_modules/tns-core-modules/file-system/file-system.js");
//import { AlertService, } from "../shared/user/alert.service";
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.model = {};
        this.loading = false;
        //Campo email
        //email = new FormControl('', [Validators.required, Validators.email]);
        //Campo Contraseña
        this.hide = true;
        this.imageAssets = [];
        //FOTO
        this.selectedFile = null;
        this.page.actionBarHidden = true;
        this.user = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__["User"]();
        this.user.username = "";
        this.user.password = "";
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    /*public image_picker3(){

            let that = this
            let context = imagepicker.create({ mode: "single" });
            context
                .authorize()
                .then(() => {
                    return context.present();
                })
                .then(selection => {
                    const imageAsset = selection.length > 0 ? selection[0] : null;
                    ImageSourceModule.fromAsset(imageAsset).then(
                        savedImage => {
                            let filename = "image" + "-" + new Date().getTime() + ".png";
                            let folder = fs.knownFolders.documents();
                            let path = fs.path.join(folder.path, filename);
                            savedImage.saveToFile(path, "png");
                            var loadedImage = ImageSourceModule.fromFile(path);
                            //loadedImage.filename = filename;
                            //loadedImage.note = "";
                            //that.arrayPictures.unshift(loadedImage);
                            //that.storeData();
                            console.log(loadedImage);

                        },
                        err => {
                            console.log("Failed to load from asset");
                            console.log(err)
                        }
                    );
                })
                .catch(err => {
                    console.log(err);
                });
    
    }*/
    RegisterComponent.prototype.image_picker = function () {
        var _this = this;
        var context = nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_6__["create"]({
            mode: "single" // use "multiple" for multiple selection
        });
        context.authorize().then(function () {
            return context.present();
        }).then(function (selection) {
            _this.imageSrc = JSON.stringify(selection[0]);
            var json = JSON.parse(_this.imageSrc);
            _this.imageSrc = JSON.stringify(json._android);
            console.log("Ruta del archivo seleccionado: " + JSON.stringify(json._android));
        }).catch(function (e) {
            console.log(e);
        });
    };
    RegisterComponent.prototype.register2 = function () {
        this.userService.createphoto2();
        //var file =  "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200225-WA0002.jpg";
        //var url = "https://some.remote.service.com/path";
        //var name = file.substr(file.lastIndexOf("/") + 1);
        //console.log(name);
        //alert("Función no disponible actualmente")
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        //this.loading = true;
        //this.model.src = this.selectedFile.name;
        this.userService.create(this.model)
            .subscribe(function (data) {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Registrado con éxito ");
            _this.routerExtensions.navigate(['/login']);
        }, function (error) {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(error);
            _this.loading = false;
        });
    };
    RegisterComponent.prototype.back = function () {
        this.routerExtensions.navigate(["/login"]);
    };
    /*upload(attachment) {
        console.log(attachment);
          this.userService.createphoto(attachment)
              .subscribe(
                  data => {
                      alert("Subido con éxito ");
                      this.routerExtensions.navigate(['/login']);
                  },
                  error => {
                      alert(error);
                  });
      }*/
    RegisterComponent.prototype.onFileSelected = function (event) {
        this.selectedFile = event.target.files[0];
        console.log(this.selectedFile);
    };
    RegisterComponent.prototype.onUpload = function () {
        var _this = this;
        try {
            console.log("Prueba");
            var file = "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200301-WA0001.jpg";
            var name = file.substr(file.lastIndexOf("/") + 1);
            console.log(name);
            var fd = new FormData();
            //fd.append('image', this.selectedFile, this.selectedFile.name);
            fd.append('imagen', file, name);
            console.log('******************************');
            console.log(fd);
            this.userService.createphoto(fd)
                .subscribe(function (data) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Subida foto con éxito ");
                _this.routerExtensions.navigate(['/login']);
            }, function (error) {
                //this.alertService.error(error);
                console.log(error);
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Error de subida en la imagen");
            });
        }
        catch (e) {
            console.log(e);
        }
    };
    RegisterComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"] }
    ]; };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Register",
            template: __importDefault(__webpack_require__("./register/register.component.html")).default,
            styles: [__importDefault(__webpack_require__("./register/register.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"],
            _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./register2/register.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n\r\n.header {\r\n    horizontal-align: center;\r\n    font-size: 25;\r\n    font-weight: 600;\r\n    margin-bottom: 20;\r\n    text-align: center;\r\n    color: rgb(38, 8, 99);\r\n  }\r\n  \r\n  .home-panel{\r\n      vertical-align: center; \r\n      font-size: 20;\r\n      margin: 15;\r\n  }\r\n  \r\n  .logo {\r\n      margin-bottom: 32;\r\n      font-weight: bold;\r\n    }\r\n    \r\n  .description-label{\r\n      margin-bottom: 15;\r\n  }\r\n  \r\n  .form {\r\n      margin: 20;\r\n  }"

/***/ }),

/***/ "./register2/register.component.html":
/***/ (function(module, exports) {

module.exports = "<Label text=\"Hello\"\r\n                horizontalAlignment=\"center\" class=\"header\"></Label>"

/***/ }),

/***/ "./register2/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Register2Component", function() { return Register2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/user/user.service.ts");




//import { MatSnackBar, MatBottomSheet, MatBottomSheetRef } from '@angular/material';


//import { AlertService, } from "../shared/user/alert.service";
var Register2Component = /** @class */ (function () {
    function Register2Component(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.model = {};
        this.loading = false;
        //Campo email
        //email = new FormControl('', [Validators.required, Validators.email]);
        //Campo Contraseña
        this.hide = true;
        //FOTO
        this.selectedFile = null;
        this.page.actionBarHidden = true;
        this.user = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__["User"]();
        this.user.username = "";
        this.user.password = "";
    }
    Register2Component.prototype.ngOnInit = function () {
    };
    Register2Component.prototype.register2 = function () {
        Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Función no disponible actualmente");
    };
    Register2Component.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"] }
    ]; };
    Register2Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Register2",
            template: __importDefault(__webpack_require__("./register2/register.component.html")).default,
            styles: [__importDefault(__webpack_require__("./register2/register.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"],
            _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"]])
    ], Register2Component);
    return Register2Component;
}());



/***/ }),

/***/ "./session_code/session_code.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n    vertical-align: center; \r\n    font-size: 20;\r\n    margin: 15;\r\n}\r\n\r\n.description-label{\r\n    margin-bottom: 15;\r\n}\r\n\r\n.prueba {\r\n  font-size: 25;\r\n  font-weight: 600;\r\n  margin-bottom: 20;\r\n  text-align: center;\r\n  color:white;\r\n}\r\n\r\n.recuadro {\r\n  font-size: 35;\r\n  font-weight: 800;\r\n  margin-bottom: 20;\r\n  text-align: center;\r\n}"

/***/ }),

/***/ "./session_code/session_code.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Home\">\r\n</ActionBar>\r\n\r\n<GridLayout backgroundColor=\"#0374dd\">\r\n    <ScrollView>\r\n        <StackLayout class=\"home-panel\">\r\n            <!--Add your page content here-->\r\n            <Label textWrap=\"true\" text=\"Escriba su código de sesión\"\r\n                textAlignment=\"center\" class=\"prueba\"\r\n                textFieldBackgroundColor=\"white\"></Label>\r\n            <TextField height=\"30\" maxLength=\"4\" row=\"3\" col=\"1\"\r\n                [(ngModel)]=\"model.code_session\" class=\"recuadro\"\r\n                backgroundColor=\"white\" height=\"80\" width=\"200\">\r\n            </TextField>\r\n            <Button text=\"Continuar\" (tap)=\"insert_code_session()\"></Button>\r\n        </StackLayout>\r\n    </ScrollView>\r\n</GridLayout>"

/***/ }),

/***/ "./session_code/session_code.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Session_codeComponent", function() { return Session_codeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/user/user.service.ts");




//import { MatSnackBar, MatBottomSheet, MatBottomSheetRef } from '@angular/material';


//import { AlertService, } from "../shared/user/alert.service";
var Session_codeComponent = /** @class */ (function () {
    function Session_codeComponent(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.model = {};
        this.loading = false;
        this.currentUser = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__["User"]();
        this.page.actionBarHidden = true;
        //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = this.userService.currentUser();
    }
    Session_codeComponent.prototype.ngOnInit = function () {
    };
    Session_codeComponent.prototype.insert_code_session = function () {
        if (this.model.code_session == '')
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Introduzca un código de sesión válido");
        else {
            this.currentUser.session_code = this.model.code_session;
            console.log(this.model.code_session);
            console.log(this.currentUser.session_code);
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(this.currentUser.session_code);
            this.userService.setUser(this.currentUser);
            this.update(this.currentUser);
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(this.currentUser.session_code);
        }
    };
    Session_codeComponent.prototype.update = function (currentUser) {
        var _this = this;
        this.userService.setUser(this.currentUser);
        //localStorage.setItem("currentUser",JSON.stringify(currentUser));
        console.log(this.currentUser.seat);
        console.log(JSON.stringify(this.currentUser));
        this.userService.update(this.currentUser).subscribe(function (data) {
            _this.routerExtensions.navigate(["/identification"]);
        }, function (error) {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Error al actualizar el código de sessión");
        });
    };
    Session_codeComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"] }
    ]; };
    Session_codeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Session_code",
            template: __importDefault(__webpack_require__("./session_code/session_code.component.html")).default,
            styles: [__importDefault(__webpack_require__("./session_code/session_code.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"], _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"], nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"]])
    ], Session_codeComponent);
    return Session_codeComponent;
}());



/***/ }),

/***/ "./shared/config.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Config", function() { return Config; });
var Config = /** @class */ (function () {
    function Config() {
    }
    // casa //static apiUrl = "http://192.168.1.36:4000";
    // emulador //static apiUrl = "http://10.0.3.2:4000";
    // ae avana wifi //static apiUrl = "http://192.168.35.2:4000";
    // movil // static apiUrl = "http://192.168.43.226:4000";
    // eduroam // static apiUrl = "http://10.150.232.231:4000";
    // avana // static apiUrl = "http://10.99.153.128:4000";
    Config.apiUrl = "http://192.168.35.2:4000";
    Config.appKey = "kid_HyHoT_REf";
    Config.authHeader = "Basic a2lkX0h5SG9UX1JFZjo1MTkxMDJlZWFhMzQ0MzMyODFjN2MyODM3MGQ5OTIzMQ";
    Config.token = "";
    return Config;
}());



/***/ }),

/***/ "./shared/user/authenticate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticateService", function() { return AuthenticateService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/nativescript-localstorage/localstorage.js");
/* harmony import */ var nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/config.ts");






var AuthenticateService = /** @class */ (function () {
    function AuthenticateService(http) {
        this.http = http;
    }
    AuthenticateService.prototype.register = function (user) {
        if (!user.username || !user.password) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])("Please provide both an email address and password.");
        }
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + "user/" + _config__WEBPACK_IMPORTED_MODULE_5__["Config"].appKey, JSON.stringify({
            username: user.username,
            email: user.username,
            password: user.password
        }), { headers: this.getCommonHeaders() }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    AuthenticateService.prototype.login = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/authenticate', ({
            username: user.username,
            password: user.password
        })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    AuthenticateService.prototype.login5 = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/authenticate', { username: user.username, password: user.password })
            .subscribe(function (user32) {
            // login successful if there's a jwt token in the response
            if (user32) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["setItem"]('currentUser', JSON.stringify(user));
            }
            return user32;
        });
    };
    AuthenticateService.prototype.create = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/register', user);
    };
    AuthenticateService.prototype.createphoto = function (file) {
        console.log("**!!FILE!!**");
        console.log(file);
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/upload', file);
    };
    AuthenticateService.prototype.getCommonHeaders = function () {
        return {
            "Content-Type": "application/json",
            "Authorization": _config__WEBPACK_IMPORTED_MODULE_5__["Config"].authHeader
        };
    };
    //logout(_id: string, user: User) {
    // return this.http.put(Config.apiUrl + '/users/logout/' + _id, user);
    //}
    AuthenticateService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error));
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
    };
    //Hemos añadido el {responseType: 'text'} porque daba un error de aprsing cuando leía el valor retornado
    // El método en realidad es  return this.http.put(Config.apiUrl + '/users/' + user._id, user)
    AuthenticateService.prototype.update = function (user) {
        return this.http.put(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/' + user._id, user, { responseType: 'text' });
    };
    AuthenticateService.prototype.logout = function (_id, user) {
        return this.http.put(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/logout/' + _id, user, { responseType: 'text' });
    };
    AuthenticateService.prototype.setUser = function (data) {
        nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["setItem"]("currentUser", JSON.stringify(data));
    };
    AuthenticateService.prototype.currentUser = function () {
        return JSON.parse(nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["getItem"]("currentUser")); //|| null;
    };
    AuthenticateService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    AuthenticateService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthenticateService);
    return AuthenticateService;
}());



/***/ }),

/***/ "./shared/user/user.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./shared/user/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/nativescript-localstorage/localstorage.js");
/* harmony import */ var nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/config.ts");






var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.register = function (user) {
        if (!user.username || !user.password) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])("Please provide both an email address and password.");
        }
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + "user/" + _config__WEBPACK_IMPORTED_MODULE_5__["Config"].appKey, JSON.stringify({
            username: user.username,
            email: user.username,
            password: user.password
        }), { headers: this.getCommonHeaders() }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    UserService.prototype.login = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/authenticate', ({
            username: user.username,
            password: user.password
        })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    UserService.prototype.login5 = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/authenticate', { username: user.username, password: user.password })
            .subscribe(function (user32) {
            // login successful if there's a jwt token in the response
            if (user32) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["setItem"]('currentUser', JSON.stringify(user));
            }
            return user32;
        });
    };
    UserService.prototype.create = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/register', user);
    };
    UserService.prototype.createphoto = function (file) {
        console.log("**!!FILE!!**");
        console.log(file);
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/upload', file);
    };
    UserService.prototype.createphoto2 = function () {
        console.log("**!!FILEe!!**");
        //console.log(file);
        //return this.http.post(Config.apiUrl + '/upload', file);
        //var file =  "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200225-WA0001.jpg";
        var file = "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200301-WA0001.jpg";
        var name = file.substr(file.lastIndexOf("/") + 1);
        var bghttp = __webpack_require__("../node_modules/nativescript-background-http/background-http.js");
        var session = bghttp.session("image-upload");
        var request = {
            url: _config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/upload2',
            method: "POST",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": name
            },
            description: "Uploading " + name,
            //androidDisplayNotificationProgress: false,
            androidAutoClearNotification: true,
        };
        var task = session.uploadFile(file, request);
        return true;
    };
    UserService.prototype.getCommonHeaders2 = function () {
        return {
            "Content-Type": "application/octet-stream",
            "File-Name": name
        };
    };
    UserService.prototype.getCommonHeaders = function () {
        return {
            "Content-Type": "application/json",
            "Authorization": _config__WEBPACK_IMPORTED_MODULE_5__["Config"].authHeader
        };
    };
    //logout(_id: string, user: User) {
    // return this.http.put(Config.apiUrl + '/users/logout/' + _id, user);
    //}
    UserService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error));
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
    };
    //Hemos añadido el {responseType: 'text'} porque daba un error de aprsing cuando leía el valor retornado
    // El método en realidad es  return this.http.put(Config.apiUrl + '/users/' + user._id, user)
    UserService.prototype.update = function (user) {
        return this.http.put(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/' + user._id, user, { responseType: 'text' });
    };
    UserService.prototype.logout = function (_id, user) {
        return this.http.put(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/logout/' + _id, user, { responseType: 'text' });
    };
    UserService.prototype.setUser = function (data) {
        nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["setItem"]("currentUser", JSON.stringify(data));
    };
    UserService.prototype.currentUser = function () {
        return JSON.parse(nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["getItem"]("currentUser")); //|| null;
    };
    UserService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./student/student.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n\tvertical-align: center;\r\n\tfont-size: 20;\r\n\tmargin: 15;\r\n}\r\n\r\n.card {\r\n\tborder-radius: 20;\r\n\tmargin: 12;\r\n    padding: 10;\r\n\tbackground-color: rgb(179, 127, 24);\r\n    color: #362713;\r\n    height: 56%;\r\n}"

/***/ }),

/***/ "./student/student.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Student Main Page\" class=\"action-bar\">\r\n</ActionBar>\r\n\r\n<StackLayout>\r\n    <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\r\n    <Label text=\"Bienvenido a la APP SIDISEC\"></Label>\r\n    <Label text=\"\"></Label>\r\n    <StackLayout>\r\n        <Tabs height=\"2000px\">\r\n            <TabStrip>\r\n                <TabStripItem>\r\n                    <Label text=\"Datos de la clase\"></Label>\r\n                    <Image src=\"res://home\"></Image>\r\n                </TabStripItem>\r\n                <TabStripItem>\r\n                    <Label text=\"Tests disponibles\"></Label>\r\n                    <Image src=\"res://settings\"></Image>\r\n                </TabStripItem>\r\n                <TabStripItem>\r\n                    <Label text=\"Kahoot Game\"></Label>\r\n                    <Image src=\"res://search\"></Image>\r\n                </TabStripItem>\r\n                <TabStripItem>\r\n                    <Label text=\"Ajustes\"></Label>\r\n                    <Image src=\"res://settings\"></Image>\r\n                </TabStripItem>\r\n            </TabStrip>\r\n            <TabContentItem>\r\n                <GridLayout>\r\n                    <StackLayout>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Home Page\" class=\"h2 text-center\">\r\n                        </Label>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Profesor: {{profesor}}\" class=\"h2 text-center\">\r\n                        </Label>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Clase: {{clase}}\"\r\n                            class=\"h2 text-center\"></Label>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Asignatura: {{asignatura}}\"\r\n                            class=\"h2 text-center\">\r\n                        </Label>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Grupo: {{grupo}}\" class=\"h2 text-center\">\r\n                        </Label>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Asiento ocupado: {{asiento_ocupado}}\"\r\n                            class=\"h2 text-center\"></Label>\r\n                    </StackLayout>\r\n                </GridLayout>\r\n            </TabContentItem>\r\n            <TabContentItem>\r\n                <GridLayout>\r\n                    <StackLayout>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Tests Page\" class=\"h2 text-center\">\r\n                        </Label>\r\n                        <Label text=\"\" class=\"text-center\">\r\n                        </Label>\r\n                        <Button text=\"Test 1\" backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\">\r\n                        </Button>\r\n                        <Label text=\"\" class=\"text-center\"></Label>\r\n                        <Button text=\"Test 2\" backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\">\r\n                        </Button>\r\n                        <Label text=\"\" class=\"text-center\"></Label>\r\n                        <Button text=\"Test 3\" backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\">\r\n                        </Button>\r\n                        <Label text=\"\" class=\"text-center\"></Label>\r\n                        <Button text=\"Test 4\" backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\">\r\n                        </Button>\r\n                        <Label text=\"\" class=\"text-center\"></Label>\r\n                        <Button text=\"Test 5\" backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\">\r\n                        </Button>\r\n                    </StackLayout>\r\n                </GridLayout>\r\n            </TabContentItem>\r\n            <TabContentItem>\r\n                <GridLayout>\r\n                    <Label text=\"Search Page\" class=\"h2 text-center\">\r\n                    </Label>\r\n                </GridLayout>\r\n            </TabContentItem>\r\n            <TabContentItem>\r\n                <GridLayout>\r\n                    <StackLayout>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Ajustes\" class=\"h2 text-center\">\r\n                        </Label>\r\n                        <Label text=\"\" class=\"text-center\">\r\n                        </Label>\r\n                        <Button text=\"Salir de la clase\"\r\n                            backgroundColor=\"#35A6E8\" color=\"whitesmoke\">\r\n                        </Button>\r\n                        <Label text=\"\" class=\"text-center\">\r\n                        </Label>\r\n                        <Button text=\"Cerrar sesión\" backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\">\r\n                        </Button>\r\n                    </StackLayout>\r\n                </GridLayout>\r\n            </TabContentItem>\r\n        </Tabs>\r\n    </StackLayout>\r\n    <Button text=\"Cerrar Sesión\" (tap)=\"logout()\"></Button>\r\n</StackLayout>"

/***/ }),

/***/ "./student/student.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentComponent", function() { return StudentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./shared/user/user.service.ts");





var StudentComponent = /** @class */ (function () {
    function StudentComponent(userService, routerExtensions, page) {
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.currentUser = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_3__["User"]();
        this.profesor = "Alberto Brunete";
        this.clase = "B-12";
        this.asignatura = "Electrónica";
        this.grupo = "A-204";
        this.asiento_ocupado = "2";
        //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = this.userService.currentUser();
        this.page.actionBarHidden = true;
    }
    StudentComponent.prototype.ngOnInit = function () {
    };
    StudentComponent.prototype.identification = function () {
        this.routerExtensions.navigate(["/identification"]);
    };
    StudentComponent.prototype.logout = function () {
        var _this = this;
        this.currentUser = this.userService.currentUser();
        this.userService.logout(this.currentUser._id, this.currentUser).subscribe(function (data) {
            _this.routerExtensions.navigate(['/login']);
        }, function (error) {
            alert("Error al cerrar sesión");
        });
    };
    StudentComponent.ctorParameters = function () { return [
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"] },
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] }
    ]; };
    StudentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Student",
            template: __importDefault(__webpack_require__("./student/student.component.html")).default,
            styles: [__importDefault(__webpack_require__("./student/student.component.css")).default]
        }),
        __metadata("design:paramtypes", [_shared_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"], nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"], tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"]])
    ], StudentComponent);
    return StudentComponent;
}());



/***/ }),

/***/ "./teacher/teacher.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n\tvertical-align: center;\r\n\tfont-size: 20;\r\n\tmargin: 15;\r\n}"

/***/ }),

/***/ "./teacher/teacher.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Teacher\" class=\"action-bar\">\r\n    </ActionBar>\r\n    \r\n    <ScrollView class=\"page\">\r\n        <StackLayout class=\"home-panel\">\r\n            <Button text=\"Identificar clase\"  background-color:rgb(76, 91, 213)></Button>\r\n            <Button text=\"Crear test\" class=\"btn btn-primary m-t-20\"></Button>\r\n            <Button text=\"CrearPrueba\" class=\"btn btn-primary m-t-20\"></Button>\r\n            <Button text=\"Cerrar Sesión\" (tap)=\"logout()\"></Button>\r\n        </StackLayout>\r\n    </ScrollView>"

/***/ }),

/***/ "./teacher/teacher.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherComponent", function() { return TeacherComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);


var TeacherComponent = /** @class */ (function () {
    function TeacherComponent(routerExtensions) {
        this.routerExtensions = routerExtensions;
    }
    TeacherComponent.prototype.ngOnInit = function () {
    };
    TeacherComponent.prototype.logout = function () {
        this.routerExtensions.navigate(["/login"]);
    };
    TeacherComponent.ctorParameters = function () { return [
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"] }
    ]; };
    TeacherComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Teacher",
            template: __importDefault(__webpack_require__("./teacher/teacher.component.html")).default,
            styles: [__importDefault(__webpack_require__("./teacher/teacher.component.css")).default]
        }),
        __metadata("design:paramtypes", [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"]])
    ], TeacherComponent);
    return TeacherComponent;
}());



/***/ })

},[["./main.ts","runtime","vendor"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi4vJF9sYXp5X3JvdXRlX3Jlc291cmNlIGxhenkgbmFtZXNwYWNlIG9iamVjdCIsIndlYnBhY2s6Ly8vLi9hcHAuY29tcG9uZW50LnRzIiwid2VicGFjazovLy8uL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXBwLm1vZHVsZS50cyIsIndlYnBhY2s6Ly8vLi9hcHAucm91dGluZy50cyIsIndlYnBhY2s6Ly8vLi9oZWxwZXJzL2Vycm9yLmludGVyY2VwdG9yLnRzIiwid2VicGFjazovLy8uL2hlbHBlcnMvaW5kZXgudHMiLCJ3ZWJwYWNrOi8vLy4vaGVscGVycy9qd3QuaW50ZXJjZXB0b3IudHMiLCJ3ZWJwYWNrOi8vLy4vaWRlbnRpZmljYXRpb24vaWRlbnRpZmljYXRpb24uY29tcG9uZW50LmNzcyIsIndlYnBhY2s6Ly8vLi9pZGVudGlmaWNhdGlvbi9pZGVudGlmaWNhdGlvbi5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9pZGVudGlmaWNhdGlvbi9pZGVudGlmaWNhdGlvbi5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyIsIndlYnBhY2s6Ly8vLi9sb2dpbi9sb2dpbi5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9sb2dpbi9sb2dpbi5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vbWFpbi50cyIsIndlYnBhY2s6Ly8vLi9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuY3NzIiwid2VicGFjazovLy8uL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5odG1sIiwid2VicGFjazovLy8uL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9yZWdpc3RlcjIvcmVnaXN0ZXIuY29tcG9uZW50LmNzcyIsIndlYnBhY2s6Ly8vLi9yZWdpc3RlcjIvcmVnaXN0ZXIuY29tcG9uZW50Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vcmVnaXN0ZXIyL3JlZ2lzdGVyLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9zZXNzaW9uX2NvZGUvc2Vzc2lvbl9jb2RlLmNvbXBvbmVudC5jc3MiLCJ3ZWJwYWNrOi8vLy4vc2Vzc2lvbl9jb2RlL3Nlc3Npb25fY29kZS5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9zZXNzaW9uX2NvZGUvc2Vzc2lvbl9jb2RlLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9zaGFyZWQvY29uZmlnLnRzIiwid2VicGFjazovLy8uL3NoYXJlZC91c2VyL2F1dGhlbnRpY2F0ZS5zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3NoYXJlZC91c2VyL3VzZXIubW9kZWwudHMiLCJ3ZWJwYWNrOi8vLy4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3N0dWRlbnQvc3R1ZGVudC5jb21wb25lbnQuY3NzIiwid2VicGFjazovLy8uL3N0dWRlbnQvc3R1ZGVudC5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9zdHVkZW50L3N0dWRlbnQuY29tcG9uZW50LnRzIiwid2VicGFjazovLy8uL3RlYWNoZXIvdGVhY2hlci5jb21wb25lbnQuY3NzIiwid2VicGFjazovLy8uL3RlYWNoZXIvdGVhY2hlci5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi90ZWFjaGVyL3RlYWNoZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0EsNENBQTRDLFdBQVc7QUFDdkQ7QUFDQTtBQUNBLHlFOzs7Ozs7OztBQ1pBO0FBQUE7QUFBQTtBQUEwQztBQU0xQztJQUFBO0lBQTRCLENBQUM7SUFBaEIsWUFBWTtRQUp4QiwrREFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsUUFBUSxFQUFFLDJDQUEyQztTQUN0RCxDQUFDO09BQ1csWUFBWSxDQUFJO0lBQUQsbUJBQUM7Q0FBQTtBQUFKOzs7Ozs7OztBQ056Qix3SEFBMEUsbUJBQU8sQ0FBQyxzSUFBb0csR0FBRyxrQkFBa0Isa0NBQWtDLFVBQVUsMEVBQTBFLEVBQUUsNERBQTRELHNEQUFzRCxFQUFFLDhFQUE4RSxFQUFFLDREQUE0RCxFQUFFLHlEQUF5RCxFQUFFLDREQUE0RCxFQUFFLEVBQUUsMkRBQTJELHNEQUFzRCxFQUFFLCtFQUErRSxFQUFFLDZEQUE2RCxFQUFFLDREQUE0RCxFQUFFLHlEQUF5RCxFQUFFLDREQUE0RCxFQUFFLHNEQUFzRCxFQUFFLEVBQUUscUVBQXFFLHdEQUF3RCxFQUFFLEVBQUUsb0VBQW9FLHdEQUF3RCxFQUFFLEVBQUUscURBQXFELDZEQUE2RCxFQUFFLHNEQUFzRCxFQUFFLHVEQUF1RCxFQUFFLHFFQUFxRSxFQUFFLDBEQUEwRCxFQUFFLHVEQUF1RCxFQUFFO0FBQ3QxRCxRQUFRLElBQVU7QUFDbEI7QUFDQTtBQUNBLCtCQUErQixtQ0FBbUM7QUFDbEUsU0FBUztBQUNUOzs7Ozs7Ozs7O0FDTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5QztBQUM0QjtBQUNXO0FBQ0Y7QUFDUDtBQUV0QjtBQUNGO0FBQ1U7QUFDUztBQUNFO0FBQ0w7QUFDQTtBQUNlO0FBQ007QUFFM0I7QUFDZ0I7QUFFVTtBQWdDbkY7SUFBQTtJQUF5QixDQUFDO0lBQWIsU0FBUztRQTlCckIsOERBQVEsQ0FBQztZQUNSLE9BQU8sRUFBRTtnQkFDUCwyRkFBa0I7Z0JBQ2xCLGtGQUF1QjtnQkFDdkIsNkZBQTRCO2dCQUM1QixvRkFBd0I7Z0JBQ3hCLDJGQUFrQjtnQkFDbEIsa0ZBQXVCO2dCQUN2Qiw2REFBZ0I7Z0JBQ2hCLDZGQUE0QjthQUM3QjtZQUNELFlBQVksRUFBRTtnQkFDWiwyREFBWTtnQkFDWixxRUFBYztnQkFDZCw4RUFBaUI7Z0JBQ2pCLGdGQUFrQjtnQkFDbEIsNEVBQWdCO2dCQUNoQiw0RUFBZ0I7Z0JBQ2hCLDJGQUFxQjtnQkFDckIsaUdBQXVCO2FBRXhCO1lBQ0QsU0FBUyxFQUFFO2dCQUNULHNFQUFXO2dCQUNYLHNGQUFtQjtnQkFDbkIsc0VBQXNCO2dCQUN0QiwwQkFBMEI7YUFDN0I7WUFDQyxTQUFTLEVBQUUsQ0FBQywyREFBWSxDQUFDO1NBQzFCLENBQUM7T0FDVyxTQUFTLENBQUk7SUFBRCxnQkFBQztDQUFBO0FBQUo7Ozs7Ozs7OztBQ25EdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXlDO0FBRThCO0FBQ2Q7QUFFUztBQUNFO0FBQ0w7QUFDQTtBQUNlO0FBQ007QUFFcEYsSUFBTSxNQUFNLEdBQVc7SUFDbkIsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRTtJQUNyRCxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLHFFQUFjLEVBQUU7SUFDNUMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSw4RUFBaUIsRUFBRTtJQUNsRCxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLGdGQUFrQixFQUFFO0lBQ3BELEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsMkVBQWdCLEVBQUU7SUFDaEQsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSwyRUFBZ0IsRUFBRTtJQUNoRCxFQUFFLElBQUksRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLDBGQUFxQixFQUFFO0lBQzFELEVBQUUsSUFBSSxFQUFDLGdCQUFnQixFQUFFLFNBQVMsRUFBRSxnR0FBdUIsRUFBRTtDQUVoRSxDQUFDO0FBTUY7SUFBQTtJQUFnQyxDQUFDO0lBQXBCLGdCQUFnQjtRQUo1Qiw4REFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsb0ZBQXdCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25ELE9BQU8sRUFBRSxDQUFDLG9GQUF3QixDQUFDO1NBQ3RDLENBQUM7T0FDVyxnQkFBZ0IsQ0FBSTtJQUFELHVCQUFDO0NBQUE7QUFBSjs7Ozs7Ozs7O0FDNUI3QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEyQztBQUNvRTtBQUMxRTtBQUNIO0FBQ0Q7QUFHakM7SUFBQTtJQU9BLENBQUM7SUFORyxvQ0FBUyxHQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtRQUNsRCwwREFBMEQ7UUFDMUQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyx1QkFBYTtZQUMzQyxPQUFPLCtDQUFVLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBTlEsZ0JBQWdCO1FBRDVCLGdFQUFVLEVBQUU7T0FDQSxnQkFBZ0IsQ0FPNUI7SUFBRCx1QkFBQztDQUFBO0FBUDRCO0FBU3RCLElBQU0sd0JBQXdCLEdBQUc7SUFDcEMsT0FBTyxFQUFFLHNFQUFpQjtJQUMxQixRQUFRLEVBQUUsZ0JBQWdCO0lBQzFCLEtBQUssRUFBRSxJQUFJO0NBQ2QsQ0FBQzs7Ozs7Ozs7O0FDcEJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBb0M7QUFDRjs7Ozs7Ozs7O0FDRGxDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkM7QUFDb0U7QUFJL0c7SUFBQTtJQWNBLENBQUM7SUFiRyxrQ0FBUyxHQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtRQUNsRCx1REFBdUQ7UUFDdkQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7UUFDbEUsSUFBSSxXQUFXLElBQUksV0FBVyxDQUFDLEtBQUssRUFBRTtZQUNsQyxPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztnQkFDcEIsVUFBVSxFQUFFO29CQUNSLGFBQWEsRUFBRSxZQUFVLFdBQVcsQ0FBQyxLQUFPO2lCQUMvQzthQUNKLENBQUMsQ0FBQztTQUNOO1FBRUQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFiUSxjQUFjO1FBRDFCLGdFQUFVLEVBQUU7T0FDQSxjQUFjLENBYzFCO0lBQUQscUJBQUM7Q0FBQTtBQWQwQjtBQWdCcEIsSUFBTSxzQkFBc0IsR0FBRztJQUNsQyxPQUFPLEVBQUUsc0VBQWlCO0lBQzFCLFFBQVEsRUFBRSxjQUFjO0lBQ3hCLEtBQUssRUFBRSxJQUFJO0NBQ2QsQ0FBQzs7Ozs7Ozs7QUN6QkYsOEJBQThCLDZCQUE2QixvQkFBb0IsaUJBQWlCLEtBQUssaUJBQWlCLCtCQUErQixvQkFBb0IsdUJBQXVCLHdCQUF3Qix5QkFBeUIsNEJBQTRCLE9BQU8sQzs7Ozs7OztBQ0FwUiwyckM7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUU7QUFDRjtBQUVkO0FBQ1M7QUFFbUI7QUFFaEI7QUFDdEI7QUFVdkM7SUFPSSxpQ0FBcUIsSUFBVSxFQUFVLFdBQXdCLEVBQVMsZ0JBQWtDO1FBQXZGLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFTLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFKNUcsUUFBRyxHQUFHLElBQUksb0RBQUcsRUFBRSxDQUFDO1FBS1osSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLDREQUFJLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFFakMscUVBQXFFO1FBQ3JFLElBQUksQ0FBQyxXQUFXLEdBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7SUFDbkQsQ0FBQztJQUVKLDBDQUFRLEdBQVI7SUFDRyxDQUFDO0lBRUQsc0NBQUksR0FBSjtRQUVJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUU7SUFDaEMsQ0FBQztJQUVNLDZDQUFXLEdBQWxCO1FBQUEsaUJBb0NLO1FBbkNELElBQUksMEVBQWMsRUFBRSxDQUFDLElBQUksQ0FBQztZQUN0QixPQUFPLEVBQUUsaUJBQWlCO1lBQzFCLFdBQVcsRUFBRSxxQ0FBcUM7WUFDbEQsMEJBQTBCLEVBQUUsU0FBUztZQUNyQyxPQUFPLEVBQUUsd0NBQXdDO1lBQ2pELG9CQUFvQixFQUFFLElBQUk7WUFDMUIsaUJBQWlCLEVBQUUsS0FBSztZQUN4QixlQUFlLEVBQUUsSUFBSTtZQUNyQixVQUFVLEVBQUUsSUFBSTtZQUNoQixVQUFVLEVBQUUsSUFBSTtZQUNoQixPQUFPLEVBQUUsS0FBSztZQUNkLGFBQWEsRUFBRSxjQUFRLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsR0FBQztZQUNyRCxxQkFBcUIsRUFBRSxHQUFHO1lBRTFCLDJDQUEyQyxFQUFFLElBQUk7WUFDakQsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLDJOQUEyTjtTQUM5UCxDQUFDLENBQUMsSUFBSSxDQUNMLFVBQUMsTUFBTTtZQUNILEtBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUk7Z0JBQzFCLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFDLElBQUksQ0FBQztnQkFFbkQsS0FBSSxDQUFDLFNBQVMsRUFBRTtZQUVoQixTQUFTO1lBQ0wsdUJBQXVCO1lBQ3ZCLG1FQUFtRTtZQUNuRSxvQkFBb0I7WUFDdEIsS0FBSztRQUNWLENBQUMsRUFDRixVQUFDLFlBQVk7WUFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxZQUFZLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQ0YsQ0FBQztRQUNGLDBCQUEwQjtRQUN4QixLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUVFLDJDQUFTLEdBQWhCO1FBQ0ksSUFBRyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDOztZQUU3QixLQUFLLENBQUMsY0FBYyxDQUFDO0lBQzdCLENBQUM7SUFLTSwwQ0FBUSxHQUFmO1FBRUEsZ0hBQWdIO1FBQzVHLElBQUc7WUFDQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNwQyxJQUFHLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLE9BQU8sRUFDN0I7Z0JBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDckMsdUVBQXVFO2dCQUN2RSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDO2dCQUNoQyxPQUFPLElBQUksQ0FBQzthQUNmO2lCQUVEO2dCQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDOUIsT0FBTyxLQUFLLENBQUM7YUFDaEI7U0FDSjtRQUNELFdBQ0E7WUFDUSxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUN0QixLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzlCLE9BQU8sS0FBSyxDQUFDO1NBQ3BCO0lBQ0wsQ0FBQztJQUdNLGdEQUFjLEdBQXJCO1FBQ1EsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzlDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLDBIQUEwSDtRQUMxSCx3RkFBd0Y7SUFFNUYsQ0FBQztJQUdMLHdDQUFNLEdBQU4sVUFBTyxXQUFpQjtRQUF4QixpQkFhSztRQVpHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMzQyxrRUFBa0U7UUFDbEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUMzQyxjQUFJO1lBQ0EsS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7WUFDMUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDakQsQ0FBQyxFQUNELGVBQUs7WUFDRCxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQztJQUNmLENBQUM7SUFFRSxnREFBYyxHQUFyQjtRQUNJLEtBQUssQ0FBQywrQkFBK0IsQ0FBQztJQUMxQyxDQUFDO0lBQ00seUNBQU8sR0FBZDtRQUFBLGlCQXdCQztRQXRCQyxJQUFJLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLFVBQUMsSUFBaUI7WUFDckQsMkNBQTJDO1lBRTNDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDaEIsK0JBQStCO2dCQUM3QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFDLElBQUksQ0FBQztvQkFFbkQsS0FBSSxDQUFDLFFBQVEsRUFBRTtvQkFDZixLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDOUIsUUFBUTtnQkFDSiw4Q0FBOEM7Z0JBQ2xELFlBQVk7YUFDZjtRQUNILENBQUMsRUFBRTtZQUNELHVCQUF1QjtZQUN2QixrQkFBa0IsRUFBRSxJQUFJO1lBQ3hCLFFBQVEsRUFBRSxZQUFZO1NBQ3ZCLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7UUFDbkQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsd0NBQU0sR0FBTjtRQUFBLGlCQVNHO1FBUkssSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2xELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQ2pFLGNBQUk7WUFDQSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUMvQyxDQUFDLEVBQ0QsZUFBSztZQUNELEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxDQUFDO0lBQ2pCLENBQUM7O2dCQTdKd0IsNkRBQUk7Z0JBQXVCLHFFQUFXO2dCQUEyQiw0RUFBZ0I7O0lBUG5HLHVCQUF1QjtRQVBuQywrREFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLGdCQUFnQjtZQUV2QixTQUFTLEVBQUUsQ0FBQyxxRUFBVyxDQUFDO1lBQzNCLHdHQUE4Qzs7U0FFOUMsQ0FBQzt5Q0FRNkIsNkRBQUksRUFBdUIscUVBQVcsRUFBMkIsNEVBQWdCO09BUG5HLHVCQUF1QixDQXNLbkM7SUFBRCw4QkFBQztDQUFBO0FBdEttQzs7Ozs7Ozs7QUNuQnBDLHlCQUF5QiwwQkFBMEIsNkJBQTZCLEtBQUssV0FBVyxzQkFBc0IsdUJBQXVCLG1CQUFtQiw2QkFBNkIsS0FBSyxlQUFlLHdCQUF3Qix3QkFBd0IsS0FBSyxlQUFlLGlCQUFpQixpQkFBaUIseUNBQXlDLHdCQUF3Qix1QkFBdUIsb0JBQW9CLHVCQUF1QixLQUFLLFdBQVcsd0JBQXdCLGtCQUFrQiwrQkFBK0Isd0JBQXdCLEtBQUssYUFBYSwrQkFBK0Isb0JBQW9CLHVCQUF1Qix3QkFBd0IseUJBQXlCLDRCQUE0QixLQUFLLHNCQUFzQix3QkFBd0IsS0FBSyxZQUFZLG9CQUFvQixpQ0FBaUMsS0FBSyxxQkFBcUIsMkNBQTJDLG1CQUFtQixLQUFLLHNCQUFzQix3QkFBd0IsS0FBSyxxQkFBcUIsd0JBQXdCLEtBQUssc0JBQXNCLCtCQUErQixxQkFBcUIsb0JBQW9CLEtBQUssb0JBQW9CLHdCQUF3QixLQUFLLFdBQVcscUJBQXFCLE1BQU0sUzs7Ozs7OztBQ0F6c0MsK3hGOzs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5QztBQUVRO0FBQ1M7QUFDZ0I7QUFFZDtBQUNaO0FBQ2U7QUFXL0Q7SUFLSSx3QkFBb0IsSUFBVSxFQUFVLG1CQUF3QyxFQUFVLGdCQUFrQztRQUF4RyxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFINUgsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUdiLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSw0REFBSSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLDJDQUEyQztRQUMzQyxpQ0FBaUM7SUFDckMsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBRUQsZ0NBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFRCwrQkFBTSxHQUFOO1FBQ0ksSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNoQjthQUFNO1lBQ0gsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ2pCO0lBQ0wsQ0FBQztJQUVELDhCQUFLLEdBQUw7UUFBQSxpQkFrQkM7UUFqQkcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQ3BDLFNBQVMsQ0FDTixrQkFBUTtZQUNELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0MsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRXhCLENBQUMsRUFDTixVQUFDLFNBQVM7WUFDTixJQUFJLFNBQVMsQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUU7Z0JBQ2hELHlFQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUN0QztpQkFBTTtnQkFDSCx5RUFBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7YUFDekI7UUFDTCxDQUFDLENBQ0osQ0FBQztJQUNWLENBQUM7SUFDRCxrQ0FBUyxHQUFUO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVELHFDQUFZLEdBQVo7UUFFSSx5RUFBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCwrQkFBTSxHQUFOO1FBQUEsaUJBZUM7UUFkRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDdkMsU0FBUyxDQUNOO1lBQ0kseUVBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO1lBQ2hELEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN6QixDQUFDLEVBQ0QsVUFBQyxTQUFTO1lBQ04sSUFBSSxTQUFTLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO2dCQUNoRCx5RUFBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdEM7aUJBQU07Z0JBQ0gseUVBQUssQ0FBQyxTQUFTLENBQUM7YUFDbkI7UUFDTCxDQUFDLENBQ0osQ0FBQztJQUNWLENBQUM7SUFFRCx1Q0FBYyxHQUFkO1FBRUksSUFBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFDakIseUVBQUssQ0FBQyxrRUFBa0UsQ0FBQzs7WUFFekUseUVBQUssQ0FBQyw2REFBNkQsQ0FBQztJQUM1RSxDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3pDLENBQUM7O2dCQS9FeUIsNkRBQUk7Z0JBQStCLHFGQUFtQjtnQkFBNEIsNEVBQWdCOztJQUxuSCxjQUFjO1FBUjFCLCtEQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsVUFBVTtZQUNwQixTQUFTLEVBQUUsQ0FBQyxxRUFBVyxDQUFDO1lBR3hCLHNGQUFxQzs7U0FDeEMsQ0FBQzt5Q0FPNEIsNkRBQUksRUFBK0IscUZBQW1CLEVBQTRCLDRFQUFnQjtPQUxuSCxjQUFjLENBdUYxQjtJQUFELHFCQUFDO0NBQUE7QUF2RjBCOzs7Ozs7Ozs7Ozs7Ozs7QUNsQjNCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwyQkFBZSxDQUFDO0FBRS9DLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxjQUFjLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRXhCO0FBQ2pCLCtEQUF5RDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ056RCw2QkFBNkIsb0JBQW9CLHFCQUFxQixzQkFBc0IsdUJBQXVCLDBCQUEwQixHQUFHLGdCQUFnQiw2QkFBNkIscUJBQXFCLGlCQUFpQixHQUFHLFdBQVcsd0JBQXdCLHdCQUF3QixLQUFLLHlCQUF5Qix3QkFBd0IsR0FBRyxXQUFXLGlCQUFpQixHQUFHLFlBQVksc0JBQXNCLDZCQUE2QixHQUFHLFlBQVksd0JBQXdCLEdBQUcsRzs7Ozs7OztBQ0FsZSxzNkY7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBd0U7QUFDWjtBQUNaO0FBQ2U7QUFHZDtBQUNVO0FBQ0g7QUFFeEQsSUFBSSxFQUFFLEdBQUcsbUJBQU8sQ0FBQyw2REFBYSxDQUFDLENBQUM7QUFJaEMsK0RBQStEO0FBVS9EO0lBd0JDLDJCQUNTLElBQVUsRUFDVixXQUF3QixFQUN4QixnQkFBa0M7UUFGbEMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUF6QjNDLFVBQUssR0FBUSxFQUFFLENBQUM7UUFDYixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBRWhCLGFBQWE7UUFDYix1RUFBdUU7UUFFdkUsa0JBQWtCO1FBQ2xCLFNBQUksR0FBRyxJQUFJLENBQUM7UUFFZixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUdkLE1BQU07UUFDVCxpQkFBWSxHQUFVLElBQUksQ0FBQztRQWtCMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSw0REFBSSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUYsb0NBQVEsR0FBUjtJQUNBLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7T0FtQ0c7SUFFSSx3Q0FBWSxHQUFuQjtRQUFBLGlCQW1CQztRQWpCQSxJQUFJLE9BQU8sR0FBRywrREFBa0IsQ0FBQztZQUNoQyxJQUFJLEVBQUUsUUFBUSxDQUFDLHdDQUF3QztTQUN2RCxDQUFDLENBQUM7UUFFSCxPQUFPLENBQUMsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDO1lBQ3ZCLE9BQU8sT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFNBQVM7WUFFakIsS0FBSSxDQUFDLFFBQVEsR0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRTNDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3JDLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBRWhGLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUM7WUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCxxQ0FBUyxHQUFUO1FBRUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNoQywyRkFBMkY7UUFDM0YsbURBQW1EO1FBQ25ELG9EQUFvRDtRQUNwRCxvQkFBb0I7UUFDcEIsNENBQTRDO0lBQzdDLENBQUM7SUFFQyxvQ0FBUSxHQUFSO1FBQUEsaUJBYUY7UUFaQSxzQkFBc0I7UUFDdEIsMENBQTBDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7YUFDakMsU0FBUyxDQUNULGNBQUk7WUFDSCx5RUFBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDL0IsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDNUMsQ0FBQyxFQUNELGVBQUs7WUFDSix5RUFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2IsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7SUFDTixDQUFDO0lBQ0EsZ0NBQUksR0FBSjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFDRDs7Ozs7Ozs7Ozs7U0FXSztJQUNILDBDQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUNELG9DQUFRLEdBQVI7UUFBQSxpQkEyQkQ7UUExQkEsSUFBRztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEIsSUFBSSxJQUFJLEdBQUksNEVBQTRFLENBQUM7WUFDekYsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsSUFBTSxFQUFFLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUMxQixnRUFBZ0U7WUFDaEUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztZQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQztpQkFDN0IsU0FBUyxDQUNYLGNBQUk7Z0JBQ0gseUVBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO2dCQUNoQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUM1QyxDQUFDLEVBQ0QsZUFBSztnQkFDSixpQ0FBaUM7Z0JBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25CLHlFQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztZQUN2QyxDQUFDLENBQUMsQ0FBQztTQUVKO1FBQ0QsT0FBTSxDQUFDLEVBQUM7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2Y7SUFDRixDQUFDOztnQkFqSmMsNkRBQUk7Z0JBQ0cscUVBQVc7Z0JBQ04sNEVBQWdCOztJQTNCL0IsaUJBQWlCO1FBUDdCLCtEQUFTLENBQUM7WUFFVixRQUFRLEVBQUUsVUFBVTtZQUVwQiw0RkFBd0M7O1NBRXhDLENBQUM7eUNBMEJjLDZEQUFJO1lBQ0cscUVBQVc7WUFDTiw0RUFBZ0I7T0EzQi9CLGlCQUFpQixDQWtMN0I7SUFBRCx3QkFBQztDQUFBO0FBbEw2Qjs7Ozs7Ozs7QUN4QjlCLG1DQUFtQyxpQ0FBaUMsc0JBQXNCLHlCQUF5QiwwQkFBMEIsMkJBQTJCLDhCQUE4QixPQUFPLHdCQUF3QixpQ0FBaUMseUJBQXlCLHFCQUFxQixPQUFPLG1CQUFtQiw0QkFBNEIsNEJBQTRCLFNBQVMsaUNBQWlDLDRCQUE0QixPQUFPLG1CQUFtQixxQkFBcUIsT0FBTyxDOzs7Ozs7O0FDQWxnQixvSDs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXdFO0FBQ1o7QUFDWjtBQUNlO0FBQy9ELHFGQUFxRjtBQUVwQztBQUNVO0FBQzNELCtEQUErRDtBQVEvRDtJQXFCQyw0QkFDUyxJQUFVLEVBQ1YsV0FBd0IsRUFDeEIsZ0JBQWtDO1FBRmxDLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBdEIzQyxVQUFLLEdBQVEsRUFBRSxDQUFDO1FBQ2IsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUVoQixhQUFhO1FBQ2IsdUVBQXVFO1FBRXZFLGtCQUFrQjtRQUNsQixTQUFJLEdBQUcsSUFBSSxDQUFDO1FBRVosTUFBTTtRQUNULGlCQUFZLEdBQVUsSUFBSSxDQUFDO1FBa0IxQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLDREQUFJLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRixxQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUVELHNDQUFTLEdBQVQ7UUFFQyx5RUFBSyxDQUFDLG1DQUFtQyxDQUFDO0lBQzNDLENBQUM7O2dCQXBCYyw2REFBSTtnQkFDRyxxRUFBVztnQkFDTiw0RUFBZ0I7O0lBeEIvQixrQkFBa0I7UUFOOUIsK0RBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxXQUFXO1lBRXJCLDZGQUF3Qzs7U0FFeEMsQ0FBQzt5Q0F1QmMsNkRBQUk7WUFDRyxxRUFBVztZQUNOLDRFQUFnQjtPQXhCL0Isa0JBQWtCLENBNkM5QjtJQUFELHlCQUFDO0NBQUE7QUE3QzhCOzs7Ozs7OztBQ2hCL0IsOEJBQThCLCtCQUErQix1QkFBdUIsbUJBQW1CLEtBQUssMkJBQTJCLDBCQUEwQixLQUFLLGlCQUFpQixvQkFBb0IsdUJBQXVCLHdCQUF3Qix5QkFBeUIsa0JBQWtCLEtBQUssbUJBQW1CLG9CQUFvQix1QkFBdUIsd0JBQXdCLHlCQUF5QixLQUFLLEM7Ozs7Ozs7QUNBOVosd3pCOzs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBd0U7QUFDWjtBQUNaO0FBQ2U7QUFDL0QscUZBQXFGO0FBRXBDO0FBQ1U7QUFDM0QsK0RBQStEO0FBUS9EO0lBU0ksK0JBQXFCLElBQVUsRUFBVSxXQUF3QixFQUFTLGdCQUFrQztRQUF2RixTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBUyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBTjVHLFVBQUssR0FBUSxFQUFFLENBQUM7UUFDaEIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQU1aLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSw0REFBSSxFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLHFFQUFxRTtRQUNyRSxJQUFJLENBQUMsV0FBVyxHQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFO0lBQ25ELENBQUM7SUFHSix3Q0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUVELG1EQUFtQixHQUFuQjtRQUNPLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLElBQUUsRUFBRTtZQUMxQix5RUFBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7YUFFbkQ7WUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztZQUN4RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzNDLHlFQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUIseUVBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3hDO0lBQ0wsQ0FBQztJQUVELHNDQUFNLEdBQU4sVUFBTyxXQUFpQjtRQUF4QixpQkFZQztRQVhHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMzQyxrRUFBa0U7UUFDbEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUMzQyxjQUFJO1lBQ0EsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztRQUN4RCxDQUFDLEVBQ0QsZUFBSztZQUNELHlFQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNmLENBQUM7O2dCQXRDMEIsNkRBQUk7Z0JBQXVCLHFFQUFXO2dCQUEyQiw0RUFBZ0I7O0lBVG5HLHFCQUFxQjtRQU5qQywrREFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLGNBQWM7WUFFeEIsb0dBQTRDOztTQUU1QyxDQUFDO3lDQVU2Qiw2REFBSSxFQUF1QixxRUFBVyxFQUEyQiw0RUFBZ0I7T0FUbkcscUJBQXFCLENBZ0RqQztJQUFELDRCQUFDO0NBQUE7QUFoRGlDOzs7Ozs7Ozs7QUNoQmxDO0FBQUE7QUFBQTtJQUFBO0lBV0EsQ0FBQztJQVZDLHFEQUFxRDtJQUNyRCxxREFBcUQ7SUFDckQsOERBQThEO0lBQzlELHlEQUF5RDtJQUN6RCwyREFBMkQ7SUFDM0Qsd0RBQXdEO0lBQ2pELGFBQU0sR0FBRywwQkFBMEIsQ0FBQztJQUNwQyxhQUFNLEdBQUcsZUFBZSxDQUFDO0lBQ3pCLGlCQUFVLEdBQUcsc0VBQXNFLENBQUM7SUFDcEYsWUFBSyxHQUFHLEVBQUUsQ0FBQztJQUNwQixhQUFDO0NBQUE7QUFYa0I7Ozs7Ozs7OztBQ0FuQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkM7QUFDa0M7QUFDL0I7QUFDUTtBQUVJO0FBR3ZCO0FBR25DO0lBQ0ksNkJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7SUFBSSxDQUFDO0lBRXpDLHNDQUFRLEdBQVIsVUFBUyxJQUFVO1FBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2xDLE9BQU8sdURBQVUsQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO1NBQzNFO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDakIsOENBQU0sQ0FBQyxNQUFNLEdBQUcsT0FBTyxHQUFHLDhDQUFNLENBQUMsTUFBTSxFQUN2QyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ1gsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUTtZQUNwQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7U0FDMUIsQ0FBQyxFQUNGLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLENBQ3ZDLENBQUMsSUFBSSxDQUNGLGlFQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUNoQyxDQUFDO0lBQ04sQ0FBQztJQUVELG1DQUFLLEdBQUwsVUFBTSxJQUFVO1FBQ1osT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDakIsOENBQU0sQ0FBQyxNQUFNLEdBQUcscUJBQXFCLEVBQ3JDLENBQUM7WUFDRyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1NBQzFCLENBQUMsQ0FDTCxDQUFDLElBQUksQ0FDRiwwREFBRyxDQUFDLGtCQUFRLElBQUksZUFBUSxFQUFSLENBQVEsQ0FBQyxFQUN6QixpRUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FDaEMsQ0FBQztJQUVOLENBQUM7SUFFRCxvQ0FBTSxHQUFOLFVBQU8sSUFBVTtRQUNiLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcscUJBQXFCLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQzdHLFNBQVMsQ0FBQyxnQkFBTTtZQUNiLDBEQUEwRDtZQUMxRCxJQUFJLE1BQU0sRUFBRTtnQkFDUixrR0FBa0c7Z0JBQ2xHLGlFQUFvQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDN0Q7WUFFRCxPQUFPLE1BQU0sQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxvQ0FBTSxHQUFOLFVBQU8sSUFBVTtRQUNiLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUNELHlDQUFXLEdBQVgsVUFBWSxJQUFjO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDhDQUFNLENBQUMsTUFBTSxHQUFHLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBR0gsOENBQWdCLEdBQWhCO1FBQ0ksT0FBTztZQUNILGNBQWMsRUFBRSxrQkFBa0I7WUFDbEMsZUFBZSxFQUFFLDhDQUFNLENBQUMsVUFBVTtTQUNyQztJQUNMLENBQUM7SUFFRCxtQ0FBbUM7SUFDaEMsc0VBQXNFO0lBQ3pFLEdBQUc7SUFFSCwwQ0FBWSxHQUFaLFVBQWEsS0FBZTtRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUNuQyxPQUFPLHVEQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVELHdHQUF3RztJQUN4Ryw2RkFBNkY7SUFDN0Ysb0NBQU0sR0FBTixVQUFPLElBQVU7UUFDYixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLDhDQUFNLENBQUMsTUFBTSxHQUFHLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksRUFBQyxFQUFDLFlBQVksRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDO0lBQzVGLENBQUM7SUFFRCxvQ0FBTSxHQUFOLFVBQU8sR0FBVyxFQUFFLElBQVU7UUFDMUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsR0FBRyxHQUFHLEVBQUUsSUFBSSxFQUFDLEVBQUMsWUFBWSxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7SUFDOUYsQ0FBQztJQUVNLHFDQUFPLEdBQWQsVUFBZSxJQUFRO1FBQ25CLGlFQUFvQixDQUFDLGFBQWEsRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVNLHlDQUFXLEdBQWxCO1FBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLGlFQUFvQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVO0lBQ3RFLENBQUM7O2dCQXpGeUIsK0RBQVU7O0lBRDNCLG1CQUFtQjtRQUQvQixnRUFBVSxFQUFFO3lDQUVpQiwrREFBVTtPQUQzQixtQkFBbUIsQ0EyRi9CO0lBQUQsMEJBQUM7Q0FBQTtBQTNGK0I7Ozs7Ozs7OztBQ1hoQztBQUFBO0FBQUE7SUFBQTtJQWFFLENBQUM7SUFBRCxXQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7OztBQ2JIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEyQztBQUNrQztBQUMvQjtBQUNRO0FBRUk7QUFHdkI7QUFHbkM7SUFDSSxxQkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtJQUFJLENBQUM7SUFFekMsOEJBQVEsR0FBUixVQUFTLElBQVU7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbEMsT0FBTyx1REFBVSxDQUFDLG9EQUFvRCxDQUFDLENBQUM7U0FDM0U7UUFFRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNqQiw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxPQUFPLEdBQUcsOENBQU0sQ0FBQyxNQUFNLEVBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDWCxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3BCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtTQUMxQixDQUFDLEVBQ0YsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FDdkMsQ0FBQyxJQUFJLENBQ0YsaUVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQ2hDLENBQUM7SUFDTixDQUFDO0lBRUQsMkJBQUssR0FBTCxVQUFNLElBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNqQiw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxxQkFBcUIsRUFDckMsQ0FBQztZQUNHLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN2QixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7U0FDMUIsQ0FBQyxDQUNMLENBQUMsSUFBSSxDQUNGLDBEQUFHLENBQUMsa0JBQVEsSUFBSSxlQUFRLEVBQVIsQ0FBUSxDQUFDLEVBQ3pCLGlFQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUNoQyxDQUFDO0lBRU4sQ0FBQztJQUVELDRCQUFNLEdBQU4sVUFBTyxJQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxxQkFBcUIsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDN0csU0FBUyxDQUFDLGdCQUFNO1lBQ2IsMERBQTBEO1lBQzFELElBQUksTUFBTSxFQUFFO2dCQUNSLGtHQUFrRztnQkFDbEcsaUVBQW9CLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUM3RDtZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELDRCQUFNLEdBQU4sVUFBTyxJQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBQ0QsaUNBQVcsR0FBWCxVQUFZLElBQWM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFSCxrQ0FBWSxHQUFaO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM3QixvQkFBb0I7UUFDbEIseURBQXlEO1FBQ3pELDJGQUEyRjtRQUMzRixJQUFJLElBQUksR0FBSSw0RUFBNEUsQ0FBQztRQUV6RixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDcEQsSUFBSSxNQUFNLEdBQUcsbUJBQU8sQ0FBQyxpRUFBOEIsQ0FBQyxDQUFDO1FBQ3JELElBQUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDN0MsSUFBSSxPQUFPLEdBQUc7WUFDVixHQUFHLEVBQUUsOENBQU0sQ0FBQyxNQUFNLEdBQUcsVUFBVTtZQUMvQixNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRTtnQkFDTCxjQUFjLEVBQUUsMEJBQTBCO2dCQUMxQyxXQUFXLEVBQUUsSUFBSTthQUNwQjtZQUNELFdBQVcsRUFBRSxZQUFZLEdBQUcsSUFBSTtZQUNoQyw0Q0FBNEM7WUFDNUMsNEJBQTRCLEVBQUUsSUFBSTtTQUNyQyxDQUFDO1FBQ0YsSUFBSSxJQUFJLEdBQUUsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDNUMsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELHVDQUFpQixHQUFqQjtRQUNJLE9BQU87WUFDSCxjQUFjLEVBQUUsMEJBQTBCO1lBQzFDLFdBQVcsRUFBRSxJQUFJO1NBQ3BCO0lBQ0wsQ0FBQztJQUNELHNDQUFnQixHQUFoQjtRQUNJLE9BQU87WUFDSCxjQUFjLEVBQUUsa0JBQWtCO1lBQ2xDLGVBQWUsRUFBRSw4Q0FBTSxDQUFDLFVBQVU7U0FDckM7SUFDTCxDQUFDO0lBRUQsbUNBQW1DO0lBQ2hDLHNFQUFzRTtJQUN6RSxHQUFHO0lBRUgsa0NBQVksR0FBWixVQUFhLEtBQWU7UUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDbkMsT0FBTyx1REFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsNkZBQTZGO0lBQzdGLDRCQUFNLEdBQU4sVUFBTyxJQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUMsRUFBQyxZQUFZLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQztJQUM1RixDQUFDO0lBRUQsNEJBQU0sR0FBTixVQUFPLEdBQVcsRUFBRSxJQUFVO1FBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLEdBQUcsR0FBRyxFQUFFLElBQUksRUFBQyxFQUFDLFlBQVksRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFFTSw2QkFBTyxHQUFkLFVBQWUsSUFBUTtRQUNuQixpRUFBb0IsQ0FBQyxhQUFhLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFTSxpQ0FBVyxHQUFsQjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxpRUFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVTtJQUN0RSxDQUFDOztnQkF2SHlCLCtEQUFVOztJQUQzQixXQUFXO1FBRHZCLGdFQUFVLEVBQUU7eUNBRWlCLCtEQUFVO09BRDNCLFdBQVcsQ0F5SHZCO0lBQUQsa0JBQUM7Q0FBQTtBQXpIdUI7Ozs7Ozs7O0FDWHhCLDhCQUE4Qiw2QkFBNkIsb0JBQW9CLGlCQUFpQixLQUFLLGVBQWUsd0JBQXdCLGlCQUFpQixvQkFBb0IsMENBQTBDLHVCQUF1QixvQkFBb0IsS0FBSyxDOzs7Ozs7O0FDQTNRLGtqREFBa2pELFVBQVUsaU1BQWlNLE9BQU8seU1BQXlNLFlBQVksZ09BQWdPLE9BQU8sMk1BQTJNLGlCQUFpQixza0c7Ozs7Ozs7O0FDQTU1RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBeUU7QUFDVjtBQUVmO0FBRUM7QUFDUztBQU8xRDtJQVVJLDBCQUFxQixXQUF3QixFQUFTLGdCQUFrQyxFQUFTLElBQVU7UUFBdEYsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBUyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUN2RyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksNERBQUksRUFBRSxDQUFDO1FBRTlCLElBQUksQ0FBQyxRQUFRLEdBQUcsaUJBQWlCLENBQUM7UUFDbEMsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUM7UUFDcEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUM7UUFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUM7UUFDckIsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUM7UUFFM0IscUVBQXFFO1FBQ3JFLElBQUksQ0FBQyxXQUFXLEdBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7UUFDL0MsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ3JDLENBQUM7SUFJSixtQ0FBUSxHQUFSO0lBQ0csQ0FBQztJQUVELHlDQUFjLEdBQWQ7UUFFSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCxpQ0FBTSxHQUFOO1FBQUEsaUJBU0Q7UUFSSyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVMsQ0FDakUsY0FBSTtZQUNBLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQy9DLENBQUMsRUFDRCxlQUFLO1lBQ0QsS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDcEMsQ0FBQyxDQUFDLENBQUM7SUFDakIsQ0FBQzs7Z0JBakNtQyxxRUFBVztnQkFBMkIsNEVBQWdCO2dCQUFlLDZEQUFJOztJQVZsRyxnQkFBZ0I7UUFONUIsK0RBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxTQUFTO1lBRW5CLDBGQUF1Qzs7U0FFdkMsQ0FBQzt5Q0FXb0MscUVBQVcsRUFBMkIsNEVBQWdCLEVBQWUsNkRBQUk7T0FWbEcsZ0JBQWdCLENBNEM1QjtJQUFELHVCQUFDO0NBQUE7QUE1QzRCOzs7Ozs7OztBQ2I3Qiw4QkFBOEIsNkJBQTZCLG9CQUFvQixpQkFBaUIsS0FBSyxDOzs7Ozs7O0FDQXJHLHdqQjs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWtEO0FBQ2E7QUFRL0Q7SUFHSSwwQkFBb0IsZ0JBQWtDO1FBQWxDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFDdEQsQ0FBQztJQUVKLG1DQUFRLEdBQVI7SUFDRyxDQUFDO0lBRUQsaUNBQU0sR0FBTjtRQUVJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7O2dCQVRxQyw0RUFBZ0I7O0lBSDdDLGdCQUFnQjtRQU41QiwrREFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLFNBQVM7WUFFbkIsMEZBQXVDOztTQUV2QyxDQUFDO3lDQUl3Qyw0RUFBZ0I7T0FIN0MsZ0JBQWdCLENBYTVCO0lBQUQsdUJBQUM7Q0FBQTtBQWI0QiIsImZpbGUiOiJidW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiB3ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQocmVxKSB7XG5cdC8vIEhlcmUgUHJvbWlzZS5yZXNvbHZlKCkudGhlbigpIGlzIHVzZWQgaW5zdGVhZCBvZiBuZXcgUHJvbWlzZSgpIHRvIHByZXZlbnRcblx0Ly8gdW5jYXVnaHQgZXhjZXB0aW9uIHBvcHBpbmcgdXAgaW4gZGV2dG9vbHNcblx0cmV0dXJuIFByb21pc2UucmVzb2x2ZSgpLnRoZW4oZnVuY3Rpb24oKSB7XG5cdFx0dmFyIGUgPSBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInXCIpO1xuXHRcdGUuY29kZSA9ICdNT0RVTEVfTk9UX0ZPVU5EJztcblx0XHR0aHJvdyBlO1xuXHR9KTtcbn1cbndlYnBhY2tFbXB0eUFzeW5jQ29udGV4dC5rZXlzID0gZnVuY3Rpb24oKSB7IHJldHVybiBbXTsgfTtcbndlYnBhY2tFbXB0eUFzeW5jQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0VtcHR5QXN5bmNDb250ZXh0O1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQ7XG53ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQuaWQgPSBcIi4uLyQkX2xhenlfcm91dGVfcmVzb3VyY2UgbGF6eSByZWN1cnNpdmVcIjsiLCJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiZ3ItYXBwXCIsXG4gIHRlbXBsYXRlOiBcIjxwYWdlLXJvdXRlci1vdXRsZXQ+PC9wYWdlLXJvdXRlci1vdXRsZXQ+XCJcbn0pXG5leHBvcnQgY2xhc3MgQXBwQ29tcG9uZW50IHsgfSIsImdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIm5hdGl2ZXNjcmlwdC10aGVtZS1jb3JlL2Nzcy9jb3JlLmxpZ2h0LmNzc1wiLCAoKSA9PiByZXF1aXJlKFwiIW5hdGl2ZXNjcmlwdC1kZXYtd2VicGFjay9jc3MyanNvbi1sb2FkZXI/dXNlRm9ySW1wb3J0cyFuYXRpdmVzY3JpcHQtdGhlbWUtY29yZS9jc3MvY29yZS5saWdodC5jc3NcIikpO21vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W3tcInR5cGVcIjpcImltcG9ydFwiLFwiaW1wb3J0XCI6XCJcXFwibmF0aXZlc2NyaXB0LXRoZW1lLWNvcmUvY3NzL2NvcmUubGlnaHQuY3NzXFxcIlwifSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuYnRuLXByaW1hcnlcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJoZWlnaHRcIixcInZhbHVlXCI6XCI1MFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCJyZ2IoMjYsIDkxLCAyMTMpXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIyMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtd2VpZ2h0XCIsXCJ2YWx1ZVwiOlwiNjAwXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmJ0bi1zZWNvbmRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJoZWlnaHRcIixcInZhbHVlXCI6XCIzNVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCJyZ2IoMTMsIDEwMCwgMTcyKVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVzbW9rZVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCI1XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1zaXplXCIsXCJ2YWx1ZVwiOlwiMTJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXdlaWdodFwiLFwidmFsdWVcIjpcIjYwMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIndpZHRoXCIsXCJ2YWx1ZVwiOlwiMTA1XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmJ0bi1wcmltYXJ5OmRpc2FibGVkXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwib3BhY2l0eVwiLFwidmFsdWVcIjpcIjAuNVwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5idG4tc2Vjb25kOmRpc2FibGVkXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwib3BhY2l0eVwiLFwidmFsdWVcIjpcIjAuNVwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5jYXJkXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjIwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luXCIsXCJ2YWx1ZVwiOlwiMTJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJwYWRkaW5nXCIsXCJ2YWx1ZVwiOlwiMTBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiI0UzRTlGOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwiIzEzMTYzNlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImhlaWdodFwiLFwidmFsdWVcIjpcIjg2JVwifV19XSxcInBhcnNpbmdFcnJvcnNcIjpbXX19OztcbiAgICBpZiAobW9kdWxlLmhvdCkge1xuICAgICAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgICAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiAnc3R5bGUnLCBwYXRoOiAnLi9hcHAuY3NzJyB9KTtcbiAgICAgICAgfSlcbiAgICB9XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cENsaWVudE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwLWNsaWVudFwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL25hdGl2ZXNjcmlwdC5tb2R1bGVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuaW1wb3J0IHsgQXBwUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL2FwcC5yb3V0aW5nXCI7XG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tIFwiLi9hcHAuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBMb2dpbkNvbXBvbmVudCB9IGZyb20gXCIuL2xvZ2luL2xvZ2luLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUmVnaXN0ZXJDb21wb25lbnQgfSBmcm9tICcuL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBSZWdpc3RlcjJDb21wb25lbnQgfSBmcm9tICcuL3JlZ2lzdGVyMi9yZWdpc3Rlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgU3R1ZGVudENvbXBvbmVudCB9IGZyb20gJy4vc3R1ZGVudC9zdHVkZW50LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZWFjaGVyQ29tcG9uZW50IH0gZnJvbSAnLi90ZWFjaGVyL3RlYWNoZXIuY29tcG9uZW50JztcbmltcG9ydCB7IFNlc3Npb25fY29kZUNvbXBvbmVudCB9IGZyb20gJy4vc2Vzc2lvbl9jb2RlL3Nlc3Npb25fY29kZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgSWRlbnRpZmljYXRpb25Db21wb25lbnQgfSBmcm9tICcuL2lkZW50aWZpY2F0aW9uL2lkZW50aWZpY2F0aW9uLmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGVTZXJ2aWNlIH0gZnJvbSBcIi4vc2hhcmVkL3VzZXIvYXV0aGVudGljYXRlLnNlcnZpY2VcIjtcblxuaW1wb3J0IHsgSnd0SW50ZXJjZXB0b3JQcm92aWRlciwgRXJyb3JJbnRlcmNlcHRvclByb3ZpZGVyIH0gZnJvbSAnLi9oZWxwZXJzL2luZGV4JztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlLFxuICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXG4gICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXG4gICAgQXBwUm91dGluZ01vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEFwcENvbXBvbmVudCxcbiAgICBMb2dpbkNvbXBvbmVudCxcbiAgICBSZWdpc3RlckNvbXBvbmVudCxcbiAgICBSZWdpc3RlcjJDb21wb25lbnQsXG4gICAgU3R1ZGVudENvbXBvbmVudCxcbiAgICBUZWFjaGVyQ29tcG9uZW50LFxuICAgIFNlc3Npb25fY29kZUNvbXBvbmVudCxcbiAgICBJZGVudGlmaWNhdGlvbkNvbXBvbmVudFxuXG4gIF0sXG4gIHByb3ZpZGVyczogW1xuICAgIFVzZXJTZXJ2aWNlLFxuICAgIEF1dGhlbnRpY2F0ZVNlcnZpY2UsXG4gICAgSnd0SW50ZXJjZXB0b3JQcm92aWRlclxuICAgIC8vRXJyb3JJbnRlcmNlcHRvclByb3ZpZGVyXG5dLFxuICBib290c3RyYXA6IFtBcHBDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7IH1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVzIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi9sb2dpbi9sb2dpbi5jb21wb25lbnQnO1xyXG5cclxuaW1wb3J0IHsgUmVnaXN0ZXJDb21wb25lbnQgfSBmcm9tICcuL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFJlZ2lzdGVyMkNvbXBvbmVudCB9IGZyb20gJy4vcmVnaXN0ZXIyL3JlZ2lzdGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFN0dWRlbnRDb21wb25lbnQgfSBmcm9tICcuL3N0dWRlbnQvc3R1ZGVudC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUZWFjaGVyQ29tcG9uZW50IH0gZnJvbSAnLi90ZWFjaGVyL3RlYWNoZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2Vzc2lvbl9jb2RlQ29tcG9uZW50IH0gZnJvbSAnLi9zZXNzaW9uX2NvZGUvc2Vzc2lvbl9jb2RlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IElkZW50aWZpY2F0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9pZGVudGlmaWNhdGlvbi9pZGVudGlmaWNhdGlvbi5jb21wb25lbnQnO1xyXG5cclxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXHJcbiAgICB7IHBhdGg6IFwiXCIsIHJlZGlyZWN0VG86IFwiL2xvZ2luXCIsIHBhdGhNYXRjaDogXCJmdWxsXCIgfSxcclxuICAgIHsgcGF0aDogXCJsb2dpblwiLCBjb21wb25lbnQ6IExvZ2luQ29tcG9uZW50IH0sXHJcbiAgICB7IHBhdGg6IFwicmVnaXN0ZXJcIiwgY29tcG9uZW50OiBSZWdpc3RlckNvbXBvbmVudCB9LFxyXG4gICAgeyBwYXRoOiBcInJlZ2lzdGVyMlwiLCBjb21wb25lbnQ6IFJlZ2lzdGVyMkNvbXBvbmVudCB9LFxyXG4gICAgeyBwYXRoOiBcInN0dWRlbnRcIiwgY29tcG9uZW50OiBTdHVkZW50Q29tcG9uZW50IH0sXHJcbiAgICB7IHBhdGg6IFwidGVhY2hlclwiLCBjb21wb25lbnQ6IFRlYWNoZXJDb21wb25lbnQgfSxcclxuICAgIHsgcGF0aDogXCJzZXNzaW9uX2NvZGVcIiwgY29tcG9uZW50OiBTZXNzaW9uX2NvZGVDb21wb25lbnQgfSxcclxuICAgIHsgcGF0aDpcImlkZW50aWZpY2F0aW9uXCIsIGNvbXBvbmVudDogSWRlbnRpZmljYXRpb25Db21wb25lbnQgfVxyXG5cclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvclJvb3Qocm91dGVzKV0sXHJcbiAgICBleHBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwUm91dGluZ01vZHVsZSB7IH0iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIsIEh0dHBFdmVudCwgSHR0cEludGVyY2VwdG9yLCBIVFRQX0lOVEVSQ0VQVE9SUyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcydcbmltcG9ydCAncnhqcy9hZGQvb2JzZXJ2YWJsZS90aHJvdydcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvY2F0Y2gnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRXJyb3JJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG4gICAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgICAgICAvLyBleHRyYWN0IGVycm9yIG1lc3NhZ2UgZnJvbSBodHRwIGJvZHkgaWYgYW4gZXJyb3Igb2NjdXJzXG4gICAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KS5jYXRjaChlcnJvclJlc3BvbnNlID0+IHtcbiAgICAgICAgICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVycm9yUmVzcG9uc2UuZXJyb3IpXG4gICAgICAgIH0pO1xuICAgIH1cbn1cblxuZXhwb3J0IGNvbnN0IEVycm9ySW50ZXJjZXB0b3JQcm92aWRlciA9IHtcbiAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICB1c2VDbGFzczogRXJyb3JJbnRlcmNlcHRvcixcbiAgICBtdWx0aTogdHJ1ZSxcbn07IiwiZXhwb3J0ICogZnJvbSAnLi9lcnJvci5pbnRlcmNlcHRvcic7XG5leHBvcnQgKiBmcm9tICcuL2p3dC5pbnRlcmNlcHRvcic7IiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cFJlcXVlc3QsIEh0dHBIYW5kbGVyLCBIdHRwRXZlbnQsIEh0dHBJbnRlcmNlcHRvciwgSFRUUF9JTlRFUkNFUFRPUlMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBKd3RJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG4gICAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgICAgICAvLyBhZGQgYXV0aG9yaXphdGlvbiBoZWFkZXIgd2l0aCBqd3QgdG9rZW4gaWYgYXZhaWxhYmxlXG4gICAgICAgIGxldCBjdXJyZW50VXNlciA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRVc2VyJykpO1xuICAgICAgICBpZiAoY3VycmVudFVzZXIgJiYgY3VycmVudFVzZXIudG9rZW4pIHtcbiAgICAgICAgICAgIHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHtcbiAgICAgICAgICAgICAgICBzZXRIZWFkZXJzOiB7IFxuICAgICAgICAgICAgICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7Y3VycmVudFVzZXIudG9rZW59YFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNvbnN0IEp3dEludGVyY2VwdG9yUHJvdmlkZXIgPSB7XG4gICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgdXNlQ2xhc3M6IEp3dEludGVyY2VwdG9yLFxuICAgIG11bHRpOiB0cnVlLFxufTsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiLmhvbWUtcGFuZWx7XFxyXFxuXFx0dmVydGljYWwtYWxpZ246IGNlbnRlcjtcXHJcXG5cXHRmb250LXNpemU6IDIwO1xcclxcblxcdG1hcmdpbjogMTU7XFxyXFxufVxcclxcblxcclxcbi5oZWFkZXIge1xcclxcblxcdGhvcml6b250YWwtYWxpZ246IGNlbnRlcjtcXHJcXG5cXHRmb250LXNpemU6IDI1O1xcclxcblxcdGZvbnQtd2VpZ2h0OiA2MDA7XFxyXFxuXFx0bWFyZ2luLWJvdHRvbTogMjA7XFxyXFxuXFx0dGV4dC1hbGlnbjogY2VudGVyO1xcclxcblxcdGNvbG9yOiByZ2IoMzgsIDgsIDk5KTtcXHJcXG4gIH1cIiIsIm1vZHVsZS5leHBvcnRzID0gXCI8QWN0aW9uQmFyIHRpdGxlPVxcXCJTdHVkZW50IElkZW50aWZpY2F0aW9uXFxcIiBjbGFzcz1cXFwiYWN0aW9uLWJhclxcXCI+XFxyXFxuPC9BY3Rpb25CYXI+XFxyXFxuXFxyXFxuPFN0YWNrTGF5b3V0PlxcclxcbiAgICA8SW1hZ2UgY2xhc3M9XFxcImxvZ29cXFwiIHNyYz1cXFwifi9pbWFnZXMvTG9nb19wb2xpdGVjbmljYS5wbmdcXFwiPjwvSW1hZ2U+XFxyXFxuICAgIDxMYWJlbCAgY2xhc3M9XFxcImhlYWRlclxcXCIgdGV4dD1cXFwiUmVnaXN0cm8gZGUgYXNpc3RlbmNpYSBlbiBjbGFzZVxcXCJcXHJcXG4gICAgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBjbGFzcz1cXFwiaGVhZGVyXFxcIj48L0xhYmVsPlxcclxcblxcclxcbiAgICA8U2Nyb2xsVmlldyBjbGFzcz1cXFwicGFnZVxcXCI+XFxyXFxuICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImhvbWUtcGFuZWxcXFwiPlxcclxcbiAgICAgICAgICAgIDxHcmlkTGF5b3V0IHJvd3M9XFxcIjgwIDgwIDgwIDgwIDgwXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiAgcm93PVxcXCIwXFxcIiB0ZXh0PVxcXCJMZWN0b3IgUVJcXFwiICBiYWNrZ3JvdW5kLWNvbG9yOnJnYig3NiwgOTEsIDIxMykgKHRhcCk9XFxcInNjYW5CYXJjb2RlKClcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICA8QnV0dG9uICByb3c9XFxcIjFcXFwiIHRleHQ9XFxcIkxlY3RvciBORkNcXFwiIGJhY2tncm91bmQtY29sb3I6cmdiKDc2LCA5MSwgMjEzKSAodGFwKT1cXFwibWVuc2FqZVJlYWRORkMoKVxcXCIgKHRhcCk9XFxcIlJlYWRORkMoKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgIDxCdXR0b24gIHJvdz1cXFwiMlxcXCIgdGV4dD1cXFwiTWFudWFsXFxcIiBiYWNrZ3JvdW5kLWNvbG9yOnJnYig3NiwgOTEsIDIxMykgKHRhcCk9XFxcIm1vc3RyYXJtZW5zYWplKClcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICA8QnV0dG9uICByb3c9XFxcIjNcXFwidGV4dD1cXFwiUHJ1ZWJhIFVwZGF0ZVxcXCIgYmFja2dyb3VuZC1jb2xvcjpyZ2IoMjAwLCA5MSwgMjEzKSAodGFwKT1cXFwidXBkYXRlKClcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICA8QnV0dG9uICByb3c9XFxcIjRcXFwidGV4dD1cXFwiQ2VycmFyIFNlc2nDs25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgbS10LTIwXFxcIiAodGFwKT1cXFwibG9nb3V0KClcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgIDwvR3JpZExheW91dD5cXHJcXG4gICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuICAgIDwvU2Nyb2xsVmlldz5cXHJcXG48L1N0YWNrTGF5b3V0PlwiIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZhbHVlUHJvdmlkZXIgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IHsgVXNlciB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLm1vZGVsXCI7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3VzZXIuc2VydmljZVwiO1xyXG5cclxuaW1wb3J0IHsgUGFnZSwgaG9yaXpvbnRhbEFsaWdubWVudFByb3BlcnR5IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xyXG5cclxuaW1wb3J0IHsgQmFyY29kZVNjYW5uZXIgfSBmcm9tICduYXRpdmVzY3JpcHQtYmFyY29kZXNjYW5uZXInO1xyXG5pbXBvcnQgeyBOZmMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LW5mY1wiO1xyXG5pbXBvcnQgeyBOZmNOZGVmRGF0YSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtbmZjXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogXCJpZGVudGlmaWNhdGlvblwiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHByb3ZpZGVyczogW1VzZXJTZXJ2aWNlXSxcclxuXHR0ZW1wbGF0ZVVybDogXCIuL2lkZW50aWZpY2F0aW9uLmNvbXBvbmVudC5odG1sXCIsXHJcblx0c3R5bGVVcmxzOiBbJy4vaWRlbnRpZmljYXRpb24uY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBJZGVudGlmaWNhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBjdXJyZW50VXNlcjogVXNlcjtcclxuICAgIG1lbnNhamU6IGFueTtcclxuICAgIG5mYyA9IG5ldyBOZmMoKTtcclxuICAgIGJvbDogYm9vbGVhbjtcclxuICAgIHZhbGlkX1FSOiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXIgPSBuZXcgVXNlcigpO1xyXG4gICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xyXG5cclxuICAgICAgICAvL3RoaXMuY3VycmVudFVzZXIgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjdXJyZW50VXNlcicpKTtcclxuICAgICAgICB0aGlzLmN1cnJlbnRVc2VyPXRoaXMudXNlclNlcnZpY2UuY3VycmVudFVzZXIoKVxyXG4gICAgfVxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIH1cclxuICAgIFxyXG4gICAgYmFjaygpXHJcbiAgICB7XHJcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2soKVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzY2FuQmFyY29kZSgpIHtcclxuICAgICAgICBuZXcgQmFyY29kZVNjYW5uZXIoKS5zY2FuKHtcclxuICAgICAgICAgICAgZm9ybWF0czogXCJRUl9DT0RFLCBFQU5fMTNcIixcclxuICAgICAgICAgICAgY2FuY2VsTGFiZWw6IFwiRVhJVC4gQWxzbywgdHJ5IHRoZSB2b2x1bWUgYnV0dG9ucyFcIiwgLy8gaU9TIG9ubHksIGRlZmF1bHQgJ0Nsb3NlJ1xyXG4gICAgICAgICAgICBjYW5jZWxMYWJlbEJhY2tncm91bmRDb2xvcjogXCIjMzMzMzMzXCIsIC8vIGlPUyBvbmx5LCBkZWZhdWx0ICcjMDAwMDAwJyAoYmxhY2spXHJcbiAgICAgICAgICAgIG1lc3NhZ2U6IFwiVXNlIHRoZSB2b2x1bWUgYnV0dG9ucyBmb3IgZXh0cmEgbGlnaHRcIiwgLy8gQW5kcm9pZCBvbmx5LCBkZWZhdWx0IGlzICdQbGFjZSBhIGJhcmNvZGUgaW5zaWRlIHRoZSB2aWV3ZmluZGVyIHJlY3RhbmdsZSB0byBzY2FuIGl0LidcclxuICAgICAgICAgICAgc2hvd0ZsaXBDYW1lcmFCdXR0b246IHRydWUsICAgLy8gZGVmYXVsdCBmYWxzZVxyXG4gICAgICAgICAgICBwcmVmZXJGcm9udENhbWVyYTogZmFsc2UsICAgICAvLyBkZWZhdWx0IGZhbHNlXHJcbiAgICAgICAgICAgIHNob3dUb3JjaEJ1dHRvbjogdHJ1ZSwgICAgICAgIC8vIGRlZmF1bHQgZmFsc2VcclxuICAgICAgICAgICAgYmVlcE9uU2NhbjogdHJ1ZSwgICAgICAgICAgICAgLy8gUGxheSBvciBTdXBwcmVzcyBiZWVwIG9uIHNjYW4gKGRlZmF1bHQgdHJ1ZSlcclxuICAgICAgICAgICAgZnVsbFNjcmVlbjogdHJ1ZSwgICAgICAgICAgICAgLy8gQ3VycmVudGx5IG9ubHkgdXNlZCBvbiBpT1M7IHdpdGggaU9TIDEzIG1vZGFscyBhcmUgbm8gbG9uZ2VyIHNob3duIGZ1bGxTY3JlZW4gYnkgZGVmYXVsdCwgd2hpY2ggbWF5IGJlIGFjdHVhbGx5IHByZWZlcnJlZC4gQnV0IHRvIHVzZSB0aGUgb2xkIGZ1bGxTY3JlZW4gYXBwZWFyYW5jZSwgc2V0IHRoaXMgdG8gJ3RydWUnLiBEZWZhdWx0ICdmYWxzZScuXHJcbiAgICAgICAgICAgIHRvcmNoT246IGZhbHNlLCAgICAgICAgICAgICAgIC8vIGxhdW5jaCB3aXRoIHRoZSBmbGFzaGxpZ2h0IG9uIChkZWZhdWx0IGZhbHNlKVxyXG4gICAgICAgICAgICBjbG9zZUNhbGxiYWNrOiAoKSA9PiB7IGNvbnNvbGUubG9nKFwiU2Nhbm5lciBjbG9zZWRcIil9LCAvLyBpbnZva2VkIHdoZW4gdGhlIHNjYW5uZXIgd2FzIGNsb3NlZCAoc3VjY2VzcyBvciBhYm9ydClcclxuICAgICAgICAgICAgcmVzdWx0RGlzcGxheUR1cmF0aW9uOiA1MDAsICAgLy8gQW5kcm9pZCBvbmx5LCBkZWZhdWx0IDE1MDAgKG1zKSwgc2V0IHRvIDAgdG8gZGlzYWJsZSBlY2hvaW5nIHRoZSBzY2FubmVkIHRleHRcclxuXHJcbiAgICAgICAgICAgIG9wZW5TZXR0aW5nc0lmUGVybWlzc2lvbldhc1ByZXZpb3VzbHlEZW5pZWQ6IHRydWUsIC8vIE9uIGlPUyB5b3UgY2FuIHNlbmQgdGhlIHVzZXIgdG8gdGhlIHNldHRpbmdzIGFwcCBpZiBhY2Nlc3Mgd2FzIHByZXZpb3VzbHkgZGVuaWVkXHJcbiAgICAgICAgICAgIHByZXNlbnRJblJvb3RWaWV3Q29udHJvbGxlcjogdHJ1ZSAvLyBpT1Mtb25seTsgSWYgeW91J3JlIHN1cmUgeW91J3JlIG5vdCBwcmVzZW50aW5nIHRoZSAobm9uIGVtYmVkZGVkKSBzY2FubmVyIGluIGEgbW9kYWwsIG9yIGFyZSBleHBlcmllbmNpbmcgaXNzdWVzIHdpdGggZmkuIHRoZSBuYXZpZ2F0aW9uYmFyLCBzZXQgdGhpcyB0byAndHJ1ZScgYW5kIHNlZSBpZiBpdCB3b3JrcyBiZXR0ZXIgZm9yIHlvdXIgYXBwIChkZWZhdWx0IGZhbHNlKS5cclxuICAgICAgICAgIH0pLnRoZW4oXHJcbiAgICAgICAgICAgIChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubWVuc2FqZSA9IHJlc3VsdC50ZXh0LCBcclxuICAgICAgICAgICAgICAgIHRoaXMubWVuc2FqZSA9IHRoaXMubWVuc2FqZS5yZXBsYWNlKC8mIzM0Oy9naSxcIlxcXCJcIiksXHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5mdW5jdGlvbjEoKVxyXG5cclxuICAgICAgICAgICAgICAgIC8vYWxlcnQoe1xyXG4gICAgICAgICAgICAgICAgICAgIC8vdGl0bGU6IFwiU2NhbiByZXN1bHRcIixcclxuICAgICAgICAgICAgICAgICAgICAvL21lc3NhZ2U6IFwiRm9ybWF0OiBcIiArIHJlc3VsdC5mb3JtYXQgKyBcIixcXG5WYWx1ZTogXCIgKyByZXN1bHQudGV4dCxcclxuICAgICAgICAgICAgICAgICAgICAvL29rQnV0dG9uVGV4dDogXCJPS1wiXHJcbiAgICAgICAgICAgICAgICAgIC8vfSk7XHJcbiAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAoZXJyb3JNZXNzYWdlKSA9PiB7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJObyBzY2FuLiBcIiArIGVycm9yTWVzc2FnZSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgLy9pZih0aGlzLnZhbGlkX1FSPT1mYWxzZSlcclxuICAgICAgICAgICAgYWxlcnQoXCJRUiBubyB2w6FsaWRvXCIpO1xyXG4gICAgICAgIH0gICAgICAgIFxyXG5cclxuICAgIHB1YmxpYyBmdW5jdGlvbjEgKCkge1xyXG4gICAgICAgIGlmKHRoaXMuc2F2ZUpTT04oKSlcclxuICAgICAgICAgICAgdGhpcy51cGRhdGUodGhpcy5jdXJyZW50VXNlcilcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIGFsZXJ0KFwiUVIgbm8gdsOhbGlkb1wiKSAgICAgICAgXHJcbiAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgXHJcbiAgICBwdWJsaWMgc2F2ZUpTT04oKXtcclxuXHJcbiAgICAvLyAvbWVuc2FqZS9naSBzaXJ2ZSBwYXJhIHF1ZSByZWVtcGxhY2UgdG9kb3MgbG9zIHZhbG9yZXMgZW5jb250cmFkb3MsIHBvcnF1ZSBzaW4gZXN0byBzb2xvIHJlZW1wbGF6YSBlbCBwcmltZXJvXHJcbiAgICAgICAgdHJ5e1xyXG4gICAgICAgICAgICB2YXIganNvbiA9IEpTT04ucGFyc2UodGhpcy5tZW5zYWplKTtcclxuICAgICAgICAgICAgaWYoanNvbi5jbGFzZSAmJiBqc29uLmFzaWVudG8pXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXIuY2xhc3MgPSBqc29uLmNsYXNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlci5zZWF0ID0ganNvbi5hc2llbnRvO1xyXG4gICAgICAgICAgICAgICAgLy9sb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRVc2VyXCIsSlNPTi5zdHJpbmdpZnkodGhpcy5jdXJyZW50VXNlcikpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJNZW5zYWplIGRlIFBydWViYVwiKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnZhbGlkX1FSID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBhbGVydChcIlFSIG5vIHbDoWxpZG9cIik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlFSIG5vIHbDoWxpZG8gMVwiKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudmFsaWRfUVIgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGFsZXJ0KFwiUVIgbm8gdsOhbGlkb1wiKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUVIgbm8gdsOhbGlkbyAxXCIpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gICAgcHVibGljIG1vc3RyYXJWYWxvcmVzKCl7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXIpKTtcclxuICAgICAgICAgICAgYWxlcnQoSlNPTi5zdHJpbmdpZnkodGhpcy5jdXJyZW50VXNlcikpO1xyXG4gICAgICAgICAgICAvL2FsZXJ0KFwiTGEgY2xhc2UgbGXDrWRhIGRlbCBxciBlczogXCIgKyB0aGlzLmN1cnJlbnRVc2VyLmNsYXNzICsgXCJcXG4gRWwgYXNpZW50byBsZcOtZG8gZGVsIHFyIGVzOiBcIiArICB0aGlzLmN1cnJlbnRVc2VyLnNlYXRcclxuICAgICAgICAgICAgLy8rIFwiXFxuIEVsIGlkIGRlIGxhIHNlc2nDs24gIGVzOiBcIiArICB0aGlzLmN1cnJlbnRVc2VyLl9pZCArIHRoaXMuY3VycmVudFVzZXIudXNlcm5hbWUgKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICB1cGRhdGUoY3VycmVudFVzZXI6IFVzZXIpIHtcclxuICAgICAgICAgICAgdGhpcy51c2VyU2VydmljZS5zZXRVc2VyKHRoaXMuY3VycmVudFVzZXIpO1xyXG4gICAgICAgICAgICAvL2xvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudFVzZXJcIixKU09OLnN0cmluZ2lmeShjdXJyZW50VXNlcikpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmN1cnJlbnRVc2VyLnNlYXQpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLmN1cnJlbnRVc2VyKSk7XHJcbiAgICAgICAgICAgIHRoaXMudXNlclNlcnZpY2UudXBkYXRlKHRoaXMuY3VycmVudFVzZXIpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICBkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoXCJSZWdpc3RyYWRvIGVuIGxhIGNsYXNlIGNvbiDDqXhpdG9cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvc3R1ZGVudFwiXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgZGUgcmVnaXN0cm8gZW4gY2xhc2VcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIHB1YmxpYyBtZW5zYWplUmVhZE5GQygpe1xyXG4gICAgICAgIGFsZXJ0KFwiUG9yIGZhdm9yIGFjZXJxdWUgdW4gY2hpcCBORkNcIilcclxuICAgIH1cclxuICAgIHB1YmxpYyBSZWFkTkZDKClcclxuICAgIHtcclxuICAgICAgdGhpcy5uZmMuc2V0T25OZGVmRGlzY292ZXJlZExpc3RlbmVyKChkYXRhOiBOZmNOZGVmRGF0YSkgPT4ge1xyXG4gICAgICAgIC8vIGRhdGEubWVzc2FnZSBpcyBhbiBhcnJheSBvZiByZWNvcmRzLCBzbzpcclxuXHJcbiAgICAgICAgaWYgKGRhdGEubWVzc2FnZSkge1xyXG4gICAgICAgICAgLy9mb3IgKGxldCBtIGluIGRhdGEubWVzc2FnZSkge1xyXG4gICAgICAgICAgICBsZXQgcmVjb3JkID0gZGF0YS5tZXNzYWdlWzBdO1xyXG4gICAgICAgICAgICB0aGlzLm1lbnNhamUgPSByZWNvcmQucGF5bG9hZEFzU3RyaW5nOyBcclxuICAgICAgICAgICAgdGhpcy5tZW5zYWplID0gdGhpcy5tZW5zYWplLnJlcGxhY2UoLyYjMzQ7L2dpLFwiXFxcIlwiKSxcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZUpTT04oKSxcclxuICAgICAgICAgICAgdGhpcy51cGRhdGUodGhpcy5jdXJyZW50VXNlcik7XHJcbiAgICAgICAgICAgIC8vYWxlcnQoXHJcbiAgICAgICAgICAgICAgICAvL1wiXFxuIGRhdGEubWVzc2FnZTogXCIgKyByZWNvcmQucGF5bG9hZEFzU3RyaW5nXHJcbiAgICAgICAgICAgIC8vKSAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgfSwge1xyXG4gICAgICAgIC8vIGlPUy1zcGVjaWZpYyBvcHRpb25zXHJcbiAgICAgICAgc3RvcEFmdGVyRmlyc3RSZWFkOiB0cnVlLFxyXG4gICAgICAgIHNjYW5IaW50OiBcIlNjYW4gYSB0YWdcIlxyXG4gICAgICB9KS50aGVuKCgpID0+IHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiT25OZGVmRGlzY292ZXJlZCBsaXN0ZW5lciBhZGRlZFwiKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbG9nb3V0KCkge1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyID0gdGhpcy51c2VyU2VydmljZS5jdXJyZW50VXNlcigpO1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJTZXJ2aWNlLmxvZ291dCh0aGlzLmN1cnJlbnRVc2VyLl9pZCwgdGhpcy5jdXJyZW50VXNlcikuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgYWwgY2VycmFyIHNlc2nDs25cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuXHJcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IFwiLnBhZ2Uge1xcclxcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXHJcXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxyXFxufVxcclxcbi5mb3JtIHtcXHJcXG4gIG1hcmdpbi1sZWZ0OiAzMDtcXHJcXG4gIG1hcmdpbi1yaWdodDogMzA7XFxyXFxuICBmbGV4LWdyb3c6IDI7XFxyXFxuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xcclxcbn1cXHJcXG5cXHJcXG4ubG9nbyB7XFxyXFxuICBtYXJnaW4tYm90dG9tOiAzMjtcXHJcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcclxcbn1cXHJcXG5cXHJcXG4uYm90b257XFxyXFxuICB3aWR0aDogMjAwO1xcclxcbiAgaGVpZ2h0OiA1MDtcXHJcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNiwgOTEsIDIxMyk7XFxyXFxuICBjb2xvcjogd2hpdGVzbW9rZTtcXHJcXG4gIGJvcmRlci1yYWRpdXM6IDU7XFxyXFxuICBmb250LXNpemU6IDIwO1xcclxcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXHJcXG59XFxyXFxuLmxvZ28ye1xcclxcbiAgbWFyZ2luLWJvdHRvbTogMTI7XFxyXFxuICBoZWlnaHQ6IDEwMDtcXHJcXG4gIGhvcml6b250YWwtYWxpZ246IGNlbnRlcjtcXHJcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcclxcbn1cXHJcXG4uaGVhZGVyIHtcXHJcXG4gIGhvcml6b250YWwtYWxpZ246IGNlbnRlcjtcXHJcXG4gIGZvbnQtc2l6ZTogMjU7XFxyXFxuICBmb250LXdlaWdodDogNjAwO1xcclxcbiAgbWFyZ2luLWJvdHRvbTogMjA7XFxyXFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxyXFxuICBjb2xvcjogcmdiKDM4LCA4LCA5OSk7XFxyXFxufVxcclxcblxcclxcbi5pbnB1dC1maWVsZCB7XFxyXFxuICBtYXJnaW4tYm90dG9tOiAyNTtcXHJcXG59XFxyXFxuLmlucHV0IHtcXHJcXG4gIGZvbnQtc2l6ZTogMTg7XFxyXFxuICBwbGFjZWhvbGRlci1jb2xvcjogI0E4QThBODtcXHJcXG59XFxyXFxuLmlucHV0OmRpc2FibGVkIHtcXHJcXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcXHJcXG4gIG9wYWNpdHk6IDAuNTtcXHJcXG59XFxyXFxuXFxyXFxuLmJ0bi1wcmltYXJ5IHtcXHJcXG4gIG1hcmdpbjogMTUgNSAxNSA1O1xcclxcbn1cXHJcXG5cXHJcXG4uYnRuLXNlY29uZCB7XFxyXFxuICBtYXJnaW46IDE1IDUgMTUgNTtcXHJcXG59XFxyXFxuXFxyXFxuLmxvZ2luLWxhYmVsIHtcXHJcXG4gIGhvcml6b250YWwtYWxpZ246IGNlbnRlcjtcXHJcXG4gIGNvbG9yOiAjQThBOEE4O1xcclxcbiAgZm9udC1zaXplOiAxNjtcXHJcXG59XFxyXFxuLnNpZ24tdXAtbGFiZWwge1xcclxcbiAgbWFyZ2luLWJvdHRvbTogMjA7XFxyXFxufVxcclxcbi5ib2xkIHtcXHJcXG4gIGNvbG9yOiAjMDAwMDAwOyBcXHJcXG59XFxyXFxuXFxyXFxuXCIiLCJtb2R1bGUuZXhwb3J0cyA9IFwiPEFjdGlvbkJhciB0aXRsZT1cXFwiTG9naW4gUGFnZVxcXCI+PC9BY3Rpb25CYXI+XFxyXFxuXFxyXFxuPEZsZXhib3hMYXlvdXQgY2xhc3M9XFxcInBhZ2VcXFwiPlxcclxcbiAgICA8U3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgPEltYWdlIGNsYXNzPVxcXCJsb2dvXFxcIiBzcmM9XFxcIn4vaW1hZ2VzL0xvZ29fcG9saXRlY25pY2EucG5nXFxcIj48L0ltYWdlPlxcclxcbiAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwibG9nbzJcXFwiIHNyYz1cXFwifi9pbWFnZXMvZXJpc2lkMi5wbmdcXFwiPjwvSW1hZ2U+XFxyXFxuICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuXFxyXFxuICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwiZm9ybVxcXCI+XFxyXFxuICAgICAgICA8TGFiZWwgY2xhc3M9XFxcImhlYWRlclxcXCIgdGV4dD1cXFwiUFJPWUVDVE8gU0lESVNFQ1xcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgIDxHcmlkTGF5b3V0IGNvbHVtbnM9XFxcIiosKlxcXCIgcm93cz1cXFwiNjAsKiwqLCpcXFwiIHdpZHRoPVxcXCIyODBcXFwiIGhlaWdodD1cXFwiMzAwXFxcIlxcclxcbiAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcj1cXFwibGlnaHRncmF5XFxcIiBib3JkZXJDb2xvcj1cXFwicmdiKDE1LCA1LCA1OSlcXFwiIGJvcmRlcndpZHRoPVxcXCIzXFxcIj5cXHJcXG4gICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIkFsdW1ub3NzXFxcIiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMFxcXCIgY2xhc3M9XFxcImJ0biBidG4tc2Vjb25kXFxcIiBcXHJcXG4gICAgICAgICAgICAodGFwKT1cXFwic2VsZWN0KClcXFwiXFxyXFxuICAgICAgICAgID48L0J1dHRvbj5cXHJcXG5cXHJcXG4gICAgICAgICAgICA8TGFiZWwgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjBcXFwiICpuZ0lmPVxcXCIhc2VsZWN0aW9uXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIj5cXHJcXG4gICAgICAgICAgICA8L0xhYmVsPlxcclxcblxcclxcbiAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiUHJvZmVzb3Jlc1xcXCIgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjFcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXNlY29uZFxcXCIgKHRhcCk9XFxcInNlbGVjdDIoKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiAqbmdJZj1cXFwic2VsZWN0aW9uXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIj5cXHJcXG4gICAgICAgICAgICA8L0xhYmVsPlxcclxcblxcclxcbiAgICAgICAgICAgIDxUZXh0RmllbGQgcm93PVxcXCIgMVxcXCIgY29sPVxcXCIwXFxcIiBjb2xTcGFuPVxcXCIyXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIlxcclxcbiAgICAgICAgICAgICAgICB3aWR0aD1cXFwiMjYwXFxcIiBoZWlnaHQ9XFxcIjUwXFxcIiBjbGFzcz1cXFwiaW5wdXRcXFwiIGhpbnQ9XFxcIkVtYWlsXFxcIiBcXHJcXG4gICAgICAgICAgICAgICAgW2lzRW5hYmxlZF09XFxcIiFwcm9jZXNzaW5nXFxcIiBrZXlib2FyZFR5cGU9XFxcImVtYWlsXFxcIlxcclxcbiAgICAgICAgICAgICAgICBhdXRvY29ycmVjdD1cXFwiZmFsc2VcXFwiIGF1dG9jYXBpdGFsaXphdGlvblR5cGU9XFxcIm5vbmVcXFwiXFxyXFxuICAgICAgICAgICAgICAgIFsobmdNb2RlbCldPVxcXCJ1c2VyLnVzZXJuYW1lXFxcIj5cXHJcXG4gICAgICAgICAgICA8L1RleHRGaWVsZD5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgPFRleHRGaWVsZCAjcGFzc3dvcmQgY2xhc3M9XFxcImlucHV0XFxcIiByb3c9XFxcIjJcXFwiIGNvbD1cXFwiMFxcXCIgY29sU3Bhbj1cXFwiMlxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcj1cXFwid2hpdGVcXFwiIHdpZHRoPVxcXCIyNjBcXFwiIGhlaWdodD1cXFwiNTBcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBbaXNFbmFibGVkXT1cXFwiIXByb2Nlc3NpbmdcXFwiIGhpbnQ9XFxcIlBhc3N3b3JkXFxcIiBzZWN1cmU9XFxcInRydWVcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBbKG5nTW9kZWwpXT1cXFwidXNlci5wYXNzd29yZFxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIFtyZXR1cm5LZXlUeXBlXT1cXFwiaXNMb2dnaW5nSW4gPyAnZG9uZScgOiAnbmV4dCdcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAocmV0dXJuUHJlc3MpPVxcXCJmb2N1c0NvbmZpcm1QYXNzd29yZCgpXFxcIj48L1RleHRGaWVsZD5cXHJcXG4gICAgICAgICAgICBcXHJcXG4gICAgICAgICAgICAgICAgIDxCdXR0b24gW3RleHRdPVxcXCJpc0xvZ2dpbmdJbiA/ICdMb2dpbicgOiAnU2lnbiBVcCdcXFwiIHJvdz1cXFwiM1xcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIGNvbD1cXFwiMFxcXCIgKm5nSWY9XFxcInNlbGVjdGlvblxcXCIgW2lzRW5hYmxlZF09XFxcIiFwcm9jZXNzaW5nXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgKHRhcCk9XFxcInN1Ym1pdCgpXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IG0tdC0yMFxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgIDxCdXR0b24gW3RleHRdPVxcXCInTG9naW4gUHJvZmVzb3InXFxcIiByb3c9XFxcIjNcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBjb2w9XFxcIjBcXFwiIGNvbFNwYW49XFxcIjJcXFwiICpuZ0lmPVxcXCIhc2VsZWN0aW9uXFxcIiBbaXNFbmFibGVkXT1cXFwiIXByb2Nlc3NpbmdcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cXFwiYm90b25cXFwiICh0YXApPVxcXCJsb2dpbnRlYWNoZXIoKVxcXCI+PC9CdXR0b24+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgIDxCdXR0b24gW3RleHRdPVxcXCInUmVnaXN0cm8nXFxcIiByb3c9XFxcIjNcXFwiIGNvbD1cXFwiMVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnkgbS10LTIwXFxcIiAqbmdJZj1cXFwic2VsZWN0aW9uXFxcIiAodGFwKT1cXFwicmVnaXN0ZXIyKClcXFwiPjwvQnV0dG9uPlxcclxcblxcclxcblxcclxcbiAgICAgICAgPC9HcmlkTGF5b3V0PlxcclxcbiAgICAgICAgPExhYmVsICpuZ0lmPVxcXCJpc0xvZ2dpbmdJblxcXCIgdGV4dD1cXFwiwr9PbHZpZGFzdGUgbGEgY29udHJhc2XDsWE/XFxcIlxcclxcbiAgICAgICAgICAgIGNsYXNzPVxcXCJsb2dpbi1sYWJlbFxcXCIgKHRhcCk9XFxcImZvcmdvdFBhc3N3b3JkKClcXFwiPjwvTGFiZWw+XFxyXFxuICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuPC9GbGV4Ym94TGF5b3V0PlwiIiwiaW1wb3J0IHsgQ29tcG9uZW50fSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5tb2RlbFwiO1xyXG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQXV0aGVudGljYXRlU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci9hdXRoZW50aWNhdGUuc2VydmljZVwiO1xyXG5cclxuaW1wb3J0IHsgYWxlcnQsIHByb21wdCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2VcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgU2VsZWN0TXVsdGlwbGVDb250cm9sVmFsdWVBY2Nlc3NvciB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJnci1sb2dpblwiLFxyXG4gICAgcHJvdmlkZXJzOiBbVXNlclNlcnZpY2VdLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHN0eWxlVXJsczogW1wiLi9sb2dpbi5jb21wb25lbnQuY3NzXCJdLFxyXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9sb2dpbi5jb21wb25lbnQuaHRtbFwiXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnQge1xyXG4gICAgdXNlcjogVXNlcjtcclxuICAgIGlzTG9nZ2luZ0luID0gdHJ1ZTtcclxuICAgIHNlbGVjdGlvbiA9IHRydWU7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGF1dGhlbnRpY2F0ZVNlcnZpY2U6IEF1dGhlbnRpY2F0ZVNlcnZpY2UsIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xyXG4gICAgICAgIHRoaXMudXNlciA9IG5ldyBVc2VyKCk7XHJcbiAgICAgICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcbiAgICAgICAgLy90aGlzLnVzZXIudXNlcm5hbWU9XCJwcnVlYmE5MzBAZ21haWwuY29tXCI7XHJcbiAgICAgICAgLy90aGlzLnVzZXIucGFzc3dvcmQ9XCJwcnVlYmE5MzBcIjtcclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3QoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3Rpb24gPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdDIoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3Rpb24gPSBmYWxzZTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgc3VibWl0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzTG9nZ2luZ0luKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9naW4oKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNpZ25VcCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBsb2dpbigpIHtcclxuICAgICAgICB0aGlzLmF1dGhlbnRpY2F0ZVNlcnZpY2UubG9naW4odGhpcy51c2VyKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2UgPT57IFxyXG4gICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXV0aGVudGljYXRlU2VydmljZS5zZXRVc2VyKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3Nlc3Npb25fY29kZVwiXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5hdXRoZW50aWNhdGVTZXJ2aWNlLmN1cnJlbnRVc2VyKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXhjZXB0aW9uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGV4Y2VwdGlvbi5lcnJvciAmJiBleGNlcHRpb24uZXJyb3IuZGVzY3JpcHRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXhjZXB0aW9uLmVycm9yLmRlc2NyaXB0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGVydChleGNlcHRpb24uZXJyb3IpXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG4gICAgcmVnaXN0ZXIyKCkge1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcmVnaXN0ZXJcIl0pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ2ludGVhY2hlcigpXHJcbiAgICB7XHJcbiAgICAgICAgYWxlcnQoXCJCaWVudmVuaWRvXCIpO1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvdGVhY2hlclwiXSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2lnblVwKCkge1xyXG4gICAgICAgIHRoaXMuYXV0aGVudGljYXRlU2VydmljZS5yZWdpc3Rlcih0aGlzLnVzZXIpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnQoXCJZb3VyIGFjY291bnQgd2FzIHN1Y2Nlc3NmdWxseSBjcmVhdGVkLlwiKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZURpc3BsYXkoKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXhjZXB0aW9uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGV4Y2VwdGlvbi5lcnJvciAmJiBleGNlcHRpb24uZXJyb3IuZGVzY3JpcHRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXhjZXB0aW9uLmVycm9yLmRlc2NyaXB0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGVydChleGNlcHRpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcmdvdFBhc3N3b3JkKClcclxuICAgIHtcclxuICAgICAgICBpZih0aGlzLnVzZXIudXNlcm5hbWUpXHJcbiAgICAgICAgICAgIGFsZXJ0KFwiU2UgaGEgbWFuZGFkbyB1biBlbmxhY2UgYSBzdSBjb3JyZW8gcGFyYSByZXN0YXVyYXIgbGEgY29udHJhc2XDsWFcIilcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIGFsZXJ0KFwiUG9yIGZhdm9yIGludHJvZHV6Y2EgdW4gY29ycmVvIHBhcmEgcmVzdGF1cmFyIGxhIGNvbnRyYXNlw7FhXCIpXHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlRGlzcGxheSgpIHtcclxuICAgICAgICB0aGlzLmlzTG9nZ2luZ0luID0gIXRoaXMuaXNMb2dnaW5nSW47XHJcbiAgICB9XHJcblxyXG5cclxufVxyXG5cclxuIiwiaW1wb3J0IHsgcGxhdGZvcm1OYXRpdmVTY3JpcHREeW5hbWljIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3BsYXRmb3JtXCI7XG5pbXBvcnQgeyBlbmFibGVQcm9kTW9kZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbmltcG9ydCB7IEFwcE1vZHVsZSB9IGZyb20gXCIuL2FwcC5tb2R1bGVcIjtcblxuZW5hYmxlUHJvZE1vZGUoKTtcbnBsYXRmb3JtTmF0aXZlU2NyaXB0RHluYW1pYygpLmJvb3RzdHJhcE1vZHVsZShBcHBNb2R1bGUpO1xuIiwibW9kdWxlLmV4cG9ydHMgPSBcIlxcbi5oZWFkZXIge1xcblxcbiAgZm9udC1zaXplOiAyNTtcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxuICBtYXJnaW4tYm90dG9tOiAyMDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGNvbG9yOiByZ2IoMzgsIDgsIDk5KTtcXG59XFxuXFxuLmhvbWUtcGFuZWx7XFxuICAgIHZlcnRpY2FsLWFsaWduOiBjZW50ZXI7IFxcbiAgICBmb250LXNpemU6IDIwO1xcbiAgICBtYXJnaW46IDE1O1xcbn1cXG5cXG4ubG9nbyB7XFxuICAgIG1hcmdpbi1ib3R0b206IDMyO1xcbiAgICBmb250LXdlaWdodDogYm9sZDtcXG4gIH1cXG4gIFxcbi5kZXNjcmlwdGlvbi1sYWJlbHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMTU7XFxufVxcblxcbi5mb3JtIHtcXG4gICAgbWFyZ2luOiAyMDtcXG59XFxuXFxuLmxhYmVsIHtcXG4gICAgbWFyZ2luLXJpZ2h0OiA1O1xcbiAgICB2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xcbn1cXG5cXG4uaW5wdXQge1xcbiAgICBtYXJnaW4tYm90dG9tOiAxMDtcXG59XFxuXCIiLCJtb2R1bGUuZXhwb3J0cyA9IFwiPEZsZXhib3hMYXlvdXQ+XFxuICAgIDxTY3JvbGxWaWV3PlxcbiAgICAgICAgPFN0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICA8SW1hZ2UgY2xhc3M9XFxcImxvZ29cXFwiIHNyYz1cXFwifi9pbWFnZXMvTG9nb19wb2xpdGVjbmljYS5wbmdcXFwiPjwvSW1hZ2U+XFxuICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJGb3JtdWxhcmlvIGRlIFJlZ2lzdHJvXFxcIlxcbiAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiIGNsYXNzPVxcXCJoZWFkZXJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93cz1cXFwiKiwgKiwgKiwgKiwgKlxcXCIgY29sdW1ucz1cXFwiODMsICpcXFwiIGNsYXNzPVxcXCJmb3JtXFxcIj5cXG5cXG4gICAgICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIiB0ZXh0PVxcXCJOb21icmU6XFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwicmlnaHRcXFwiIGNsYXNzPVxcXCJsYWJlbFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgPFRleHRGaWVsZCByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMVxcXCIgWyhuZ01vZGVsKV09XFxcIm1vZGVsLmZpcnN0TmFtZVxcXCJcXG4gICAgICAgICAgICAgICAgICAgIGhpbnQ9XFxcIkVzY3JpYmEgc3Ugbm9tYnJlXFxcIiBjbGFzcz1cXFwiaW5wdXQgaW5wdXQtYm9yZGVyXFxcIiBuYW1lPVxcXCJsYXN0TmFtZVxcXCIgcmVxdWlyZWQ+XFxuICAgICAgICAgICAgICAgIDwvVGV4dEZpZWxkPlxcblxcbiAgICAgICAgICAgICAgICA8TGFiZWwgcm93PVxcXCIxXFxcIiBjb2w9XFxcIjBcXFwiIHRleHQ9XFxcIkFwZWxsaWRvc1xcXCJcXG4gICAgICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcInJpZ2h0XFxcIiBjbGFzcz1cXFwibGFiZWxcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgIDxUZXh0RmllbGQgcm93PVxcXCIxXFxcIiBjb2w9XFxcIjFcXFwiIFsobmdNb2RlbCldPVxcXCJtb2RlbC5sYXN0TmFtZVxcXCJcXG4gICAgICAgICAgICAgICAgICAgIGhpbnQ9XFxcIkVzY3JpYmEgc3VzIGFwZWxsaWRvc1xcXCIgY2xhc3M9XFxcImlucHV0IGlucHV0LWJvcmRlclxcXCJcXG4gICAgICAgICAgICAgICAgICAgIGtleWJvYXJkVHlwZT1cXFwiZW1haWxcXFwiPjwvVGV4dEZpZWxkPlxcblxcbiAgICAgICAgICAgICAgICA8TGFiZWwgcm93PVxcXCIyXFxcIiBjb2w9XFxcIjBcXFwiIHRleHQ9XFxcIkVtYWlsOlxcXCJcXG4gICAgICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcInJpZ2h0XFxcIiBjbGFzcz1cXFwibGFiZWxcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgIDxUZXh0RmllbGQgcm93PVxcXCIyXFxcIiBjb2w9XFxcIjFcXFwiIFsobmdNb2RlbCldPVxcXCJtb2RlbC51c2VybmFtZVxcXCJcXG4gICAgICAgICAgICAgICAgICAgIGhpbnQ9XFxcIkVzY3JpYmEgc3UgZW1haWxcXFwiIGNsYXNzPVxcXCJpbnB1dCBpbnB1dC1ib3JkZXJcXFwiXFxuICAgICAgICAgICAgICAgICAgICBrZXlib2FyZFR5cGU9XFxcImVtYWlsXFxcIiBuYW1lPVxcXCJ1c2VybmFtZVxcXCIgcmVxdWlyZWQ+PC9UZXh0RmllbGQ+XFxuXFxuICAgICAgICAgICAgICAgIDxMYWJlbCByb3c9XFxcIjNcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwiTWF0csOtY3VsYTpcXFwiXFxuICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJyaWdodFxcXCIgY2xhc3M9XFxcImxhYmVsXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICA8VGV4dEZpZWxkIHJvdz1cXFwiM1xcXCIgY29sPVxcXCIxXFxcIiBbKG5nTW9kZWwpXT1cXFwibW9kZWwubnVtYXRcXFwiXFxuICAgICAgICAgICAgICAgICAgICBoaW50PVxcXCJFc2NyaWJhIHN1IG7Dum1lcm8gZGUgbWF0csOtY3VsYVxcXCJcXG4gICAgICAgICAgICAgICAgICAgIG1heExlbmd0aD1cXFwiNVxcXCJcXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJpbnB1dCBpbnB1dC1ib3JkZXJcXFwiIG5hbWU9XFxcIm51bWF0XFxcIiByZXF1aXJlZD5cXG4gICAgICAgICAgICAgICAgPC9UZXh0RmllbGQ+XFxuICAgICAgICAgICAgICAgIDxMYWJlbCByb3c9XFxcIjRcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwiQ29udHJhc2XDsWE6XFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwicmlnaHRcXFwiIGNsYXNzPVxcXCJsYWJlbFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgPFRleHRGaWVsZCByb3c9XFxcIjRcXFwiIGNvbD1cXFwiMVxcXCIgW3R5cGVdPVxcXCJoaWRlID8gJ3Bhc3N3b3JkJyA6ICd0ZXh0J1xcXCIgWyhuZ01vZGVsKV09XFxcIm1vZGVsLnBhc3N3b3JkXFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaGludD1cXFwiRXNjcmliYSBzdSBjb250cmFzZcOxYVxcXCIgY2xhc3M9XFxcImlucHV0IGlucHV0LWJvcmRlclxcXCIgbmFtZT1cXFwicGFzc3dvcmRcXFwiIHJlcXVpcmVkIHNlY3VyZT1cXFwidHJ1ZVxcXCI+XFxuICAgICAgICAgICAgICAgIDwvVGV4dEZpZWxkPlxcbiAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG5cXG4gICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIlNlbGVjY2lvbmFyIEltYWdlblxcXCIgIGJhY2tncm91bmRjb2xvcj1cXFwiZ3JlZW5cXFwiIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5XFxcIlxcbiAgICAgICAgICAgICh0YXApPVxcXCJpbWFnZV9waWNrZXIoKVxcXCI+PC9CdXR0b24+XFxuXFxuICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93cz1cXFwiKlxcXCIgY29sdW1ucz1cXFwiKiwqXFxcIiB3aWR0aD1cXFwiMzEwXFxcIiBoZWlnaHQ9XFxcIjgwXFxcIj5cXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJSZWdpc3Ryb1xcXCIgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjBcXFwiXFxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5XFxcIiAodGFwKT1cXFwib25VcGxvYWQoKVxcXCI+XFxuICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcblxcbiAgICAgICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIlZvbHZlclxcXCIgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjFcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXByaW1hcnlcXFwiXFxuICAgICAgICAgICAgICAgICAgICAodGFwKT1cXFwiYmFjaygpXFxcIj5cXG4gICAgICAgICAgICAgICAgPC9CdXR0b24+XFxuICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcblxcbiAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgPC9TY3JvbGxWaWV3PlxcbjwvRmxleGJveExheW91dD5cIiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBFbGVtZW50UmVmLCBWaWV3Q2hpbGR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBhbGVydCwgcHJvbXB0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2VcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBNYXRTbmFja0JhciwgTWF0Qm90dG9tU2hlZXQsIE1hdEJvdHRvbVNoZWV0UmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5pbXBvcnQgeyBVc2VyIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3VzZXIubW9kZWxcIjtcbmltcG9ydCB7ICBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLnNlcnZpY2VcIjtcbmltcG9ydCAqIGFzIGltYWdlcGlja2VyIGZyb20gXCJuYXRpdmVzY3JpcHQtaW1hZ2VwaWNrZXJcIjtcbmltcG9ydCAqIGFzIEltYWdlU291cmNlTW9kdWxlIGZyb20gXCJpbWFnZS1zb3VyY2VcIjtcbnZhciBmcyA9IHJlcXVpcmUoXCJmaWxlLXN5c3RlbVwiKTtcbmltcG9ydCB7IEltYWdlIH0gZnJvbSBcInVpL2ltYWdlXCI7XG5pbXBvcnQgKiBhcyBpbWFnZVNjb3VyZSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9pbWFnZS1zb3VyY2VcIjtcbmltcG9ydCB7IEltYWdlQXNzZXQgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9pbWFnZS1hc3NldC9pbWFnZS1hc3NldFwiO1xuLy9pbXBvcnQgeyBBbGVydFNlcnZpY2UsIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL2FsZXJ0LnNlcnZpY2VcIjtcblxuXG5AQ29tcG9uZW50KHtcblxuXHRzZWxlY3RvcjogXCJSZWdpc3RlclwiLFxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxuXHR0ZW1wbGF0ZVVybDogXCIuL3JlZ2lzdGVyLmNvbXBvbmVudC5odG1sXCIsXG5cdHN0eWxlVXJsczogWycuL3JlZ2lzdGVyLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBSZWdpc3RlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cblx0bW9kZWw6IGFueSA9IHt9O1xuICAgIGxvYWRpbmcgPSBmYWxzZTtcblxuICAgIC8vQ2FtcG8gZW1haWxcbiAgICAvL2VtYWlsID0gbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5lbWFpbF0pO1xuXG4gICAgLy9DYW1wbyBDb250cmFzZcOxYVxuICAgIGhpZGUgPSB0cnVlO1xuXG5cdGltYWdlQXNzZXRzID0gW107XG5cdGltYWdlU3JjOiBhbnk7XG5cdFxuICAgIC8vRk9UT1xuXHRzZWxlY3RlZEZpbGUgOiBGaWxlID0gbnVsbDtcblx0XG5cdHVzZXI6IFVzZXI7XG5cdHRvRW1haWw7XG5cdGNjRW1haWw7XG5cdGJjY0VtYWlsO1xuXHRzdWJqZWN0O1xuXHRtZXNzYWdlO1xuXG5cdGNvbnN0cnVjdG9yKFx0XG5cdFx0cHJpdmF0ZSBwYWdlOiBQYWdlLCBcblx0XHRwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgXG5cdFx0cHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBcblx0XHQvL3ByaXZhdGUgYWxlcnRTZXJ2aWNlOiBBbGVydFNlcnZpY2UsICAgICAgICBcblx0XHQvL3B1YmxpYyBzbmFja0JhcjogTWF0U25hY2tCYXJcblx0XHQpIFxuXHRcdHtcblx0XHRcblx0XHR0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcblx0XHR0aGlzLnVzZXIgPSBuZXcgVXNlcigpO1xuXHRcdHRoaXMudXNlci51c2VybmFtZSA9IFwiXCI7XG5cdFx0dGhpcy51c2VyLnBhc3N3b3JkID0gXCJcIjtcblx0XHR9XG5cblx0bmdPbkluaXQoKTogdm9pZCB7XG5cdH1cblxuXHQvKnB1YmxpYyBpbWFnZV9waWNrZXIzKCl7XG5cblx0XHRcdGxldCB0aGF0ID0gdGhpc1xuXHRcdFx0bGV0IGNvbnRleHQgPSBpbWFnZXBpY2tlci5jcmVhdGUoeyBtb2RlOiBcInNpbmdsZVwiIH0pO1xuXHRcdFx0Y29udGV4dFxuXHRcdFx0XHQuYXV0aG9yaXplKClcblx0XHRcdFx0LnRoZW4oKCkgPT4ge1xuXHRcdFx0XHRcdHJldHVybiBjb250ZXh0LnByZXNlbnQoKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LnRoZW4oc2VsZWN0aW9uID0+IHtcblx0XHRcdFx0XHRjb25zdCBpbWFnZUFzc2V0ID0gc2VsZWN0aW9uLmxlbmd0aCA+IDAgPyBzZWxlY3Rpb25bMF0gOiBudWxsO1xuXHRcdFx0XHRcdEltYWdlU291cmNlTW9kdWxlLmZyb21Bc3NldChpbWFnZUFzc2V0KS50aGVuKFxuXHRcdFx0XHRcdFx0c2F2ZWRJbWFnZSA9PiB7XG5cdFx0XHRcdFx0XHRcdGxldCBmaWxlbmFtZSA9IFwiaW1hZ2VcIiArIFwiLVwiICsgbmV3IERhdGUoKS5nZXRUaW1lKCkgKyBcIi5wbmdcIjtcblx0XHRcdFx0XHRcdFx0bGV0IGZvbGRlciA9IGZzLmtub3duRm9sZGVycy5kb2N1bWVudHMoKTtcblx0XHRcdFx0XHRcdFx0bGV0IHBhdGggPSBmcy5wYXRoLmpvaW4oZm9sZGVyLnBhdGgsIGZpbGVuYW1lKTtcblx0XHRcdFx0XHRcdFx0c2F2ZWRJbWFnZS5zYXZlVG9GaWxlKHBhdGgsIFwicG5nXCIpO1xuXHRcdFx0XHRcdFx0XHR2YXIgbG9hZGVkSW1hZ2UgPSBJbWFnZVNvdXJjZU1vZHVsZS5mcm9tRmlsZShwYXRoKTtcblx0XHRcdFx0XHRcdFx0Ly9sb2FkZWRJbWFnZS5maWxlbmFtZSA9IGZpbGVuYW1lO1xuXHRcdFx0XHRcdFx0XHQvL2xvYWRlZEltYWdlLm5vdGUgPSBcIlwiO1xuXHRcdFx0XHRcdFx0XHQvL3RoYXQuYXJyYXlQaWN0dXJlcy51bnNoaWZ0KGxvYWRlZEltYWdlKTtcblx0XHRcdFx0XHRcdFx0Ly90aGF0LnN0b3JlRGF0YSgpO1xuXHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhsb2FkZWRJbWFnZSk7XG5cblx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRlcnIgPT4ge1xuXHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhcIkZhaWxlZCB0byBsb2FkIGZyb20gYXNzZXRcIik7XG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKGVycilcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHQpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQuY2F0Y2goZXJyID0+IHtcblx0XHRcdFx0XHRjb25zb2xlLmxvZyhlcnIpO1xuXHRcdFx0XHR9KTtcblx0XG5cdH0qL1xuXG5cdHB1YmxpYyBpbWFnZV9waWNrZXIoKXtcblxuXHRcdGxldCBjb250ZXh0ID0gaW1hZ2VwaWNrZXIuY3JlYXRlKHtcblx0XHRcdG1vZGU6IFwic2luZ2xlXCIgLy8gdXNlIFwibXVsdGlwbGVcIiBmb3IgbXVsdGlwbGUgc2VsZWN0aW9uXG5cdFx0fSk7XG5cblx0XHRjb250ZXh0LmF1dGhvcml6ZSgpLnRoZW4oKCkgPT4ge1xuXHRcdFx0XHRyZXR1cm4gY29udGV4dC5wcmVzZW50KCk7XG5cdFx0XHR9KS50aGVuKChzZWxlY3Rpb24pID0+IHtcblxuXHRcdFx0XHR0aGlzLmltYWdlU3JjPUpTT04uc3RyaW5naWZ5KHNlbGVjdGlvblswXSk7XG5cblx0XHRcdFx0dmFyIGpzb24gPSBKU09OLnBhcnNlKHRoaXMuaW1hZ2VTcmMpO1xuXHRcdFx0XHR0aGlzLmltYWdlU3JjID0gSlNPTi5zdHJpbmdpZnkoanNvbi5fYW5kcm9pZCk7XG5cdFx0XHRcdGNvbnNvbGUubG9nKFwiUnV0YSBkZWwgYXJjaGl2byBzZWxlY2Npb25hZG86IFwiICsgSlNPTi5zdHJpbmdpZnkoanNvbi5fYW5kcm9pZCkpO1xuXG5cdFx0XHR9KS5jYXRjaChmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhlKTtcblx0XHR9KTtcblx0fVxuXG5cdHJlZ2lzdGVyMigpXG5cdHtcblx0XHR0aGlzLnVzZXJTZXJ2aWNlLmNyZWF0ZXBob3RvMigpO1xuXHRcdC8vdmFyIGZpbGUgPSAgXCIvc3RvcmFnZS9lbXVsYXRlZC8wL1doYXRzQXBwL01lZGlhL1doYXRzQXBwIEltYWdlcy9JTUctMjAyMDAyMjUtV0EwMDAyLmpwZ1wiO1xuXHRcdC8vdmFyIHVybCA9IFwiaHR0cHM6Ly9zb21lLnJlbW90ZS5zZXJ2aWNlLmNvbS9wYXRoXCI7XG5cdFx0Ly92YXIgbmFtZSA9IGZpbGUuc3Vic3RyKGZpbGUubGFzdEluZGV4T2YoXCIvXCIpICsgMSk7XG5cdFx0Ly9jb25zb2xlLmxvZyhuYW1lKTtcblx0XHQvL2FsZXJ0KFwiRnVuY2nDs24gbm8gZGlzcG9uaWJsZSBhY3R1YWxtZW50ZVwiKVxuXHR9XG5cbiAgIHJlZ2lzdGVyKCkge1xuXHQvL3RoaXMubG9hZGluZyA9IHRydWU7XG5cdC8vdGhpcy5tb2RlbC5zcmMgPSB0aGlzLnNlbGVjdGVkRmlsZS5uYW1lO1xuXHR0aGlzLnVzZXJTZXJ2aWNlLmNyZWF0ZSh0aGlzLm1vZGVsKVxuXHRcdC5zdWJzY3JpYmUoXG5cdFx0XHRkYXRhID0+IHtcblx0XHRcdFx0YWxlcnQoXCJSZWdpc3RyYWRvIGNvbiDDqXhpdG8gXCIpO1xuXHRcdFx0XHR0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XG5cdFx0XHR9LFxuXHRcdFx0ZXJyb3IgPT4ge1xuXHRcdFx0XHRhbGVydChlcnJvcik7XG5cdFx0XHRcdHRoaXMubG9hZGluZyA9IGZhbHNlO1xuXHRcdFx0fSk7XG59XG5cdGJhY2soKSB7XG5cdFx0XHR0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2xvZ2luXCJdKTtcblx0fVxuXHQvKnVwbG9hZChhdHRhY2htZW50KSB7XG5cdFx0Y29uc29sZS5sb2coYXR0YWNobWVudCk7XG5cdFx0ICB0aGlzLnVzZXJTZXJ2aWNlLmNyZWF0ZXBob3RvKGF0dGFjaG1lbnQpXG5cdFx0XHQgIC5zdWJzY3JpYmUoXG5cdFx0XHRcdCAgZGF0YSA9PiB7XG5cdFx0XHRcdFx0ICBhbGVydChcIlN1YmlkbyBjb24gw6l4aXRvIFwiKTtcblx0XHRcdFx0XHQgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcblx0XHRcdFx0ICB9LFxuXHRcdFx0XHQgIGVycm9yID0+IHtcblx0XHRcdFx0XHQgIGFsZXJ0KGVycm9yKTtcblx0XHRcdFx0ICB9KTtcblx0ICB9Ki9cblx0ICBvbkZpbGVTZWxlY3RlZChldmVudCl7XG5cdFx0dGhpcy5zZWxlY3RlZEZpbGUgPSBldmVudC50YXJnZXQuZmlsZXNbMF07XG5cdFx0Y29uc29sZS5sb2codGhpcy5zZWxlY3RlZEZpbGUpO1xuXHQgIH1cblx0ICBvblVwbG9hZCgpe1xuXHRcdHRyeXtcblx0XHRcdGNvbnNvbGUubG9nKFwiUHJ1ZWJhXCIpO1xuXHRcdFx0dmFyIGZpbGUgPSAgXCIvc3RvcmFnZS9lbXVsYXRlZC8wL1doYXRzQXBwL01lZGlhL1doYXRzQXBwIEltYWdlcy9JTUctMjAyMDAzMDEtV0EwMDAxLmpwZ1wiO1xuXHRcdFx0dmFyIG5hbWUgPSBmaWxlLnN1YnN0cihmaWxlLmxhc3RJbmRleE9mKFwiL1wiKSArIDEpO1xuXHRcdFx0Y29uc29sZS5sb2cobmFtZSk7XG5cdFx0XHRjb25zdCBmZCA9IG5ldyBGb3JtRGF0YSgpO1xuXHRcdFx0Ly9mZC5hcHBlbmQoJ2ltYWdlJywgdGhpcy5zZWxlY3RlZEZpbGUsIHRoaXMuc2VsZWN0ZWRGaWxlLm5hbWUpO1xuXHRcdFx0ZmQuYXBwZW5kKCdpbWFnZW4nLCBmaWxlLCBuYW1lKTtcblx0XHRcdGNvbnNvbGUubG9nKCcqKioqKioqKioqKioqKioqKioqKioqKioqKioqKionKTtcblx0XHRcdGNvbnNvbGUubG9nKGZkKTtcblx0XHRcdHRoaXMudXNlclNlcnZpY2UuY3JlYXRlcGhvdG8oZmQpXG5cdFx0XHQgIC5zdWJzY3JpYmUoXG5cdFx0XHRcdGRhdGEgPT4ge1xuXHRcdFx0XHRcdGFsZXJ0KFwiU3ViaWRhIGZvdG8gY29uIMOpeGl0byBcIik7XG5cdFx0XHRcdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRlcnJvciA9PiB7XG5cdFx0XHRcdFx0Ly90aGlzLmFsZXJ0U2VydmljZS5lcnJvcihlcnJvcik7XG5cdFx0XHRcdFx0Y29uc29sZS5sb2coZXJyb3IpO1xuXHRcdFx0XHRcdGFsZXJ0KFwiRXJyb3IgZGUgc3ViaWRhIGVuIGxhIGltYWdlblwiKTtcblx0XHRcdFx0fSk7XG5cblx0XHR9XG5cdFx0Y2F0Y2goZSl7XG5cdFx0XHRjb25zb2xlLmxvZyhlKTtcblx0XHR9XG5cdH1cblxuXHQgIFxuXHQvL29wZW5TbmFja0JhcigpIHtcblx0XHQvL3RoaXMuc25hY2tCYXIub3BlbihcIlJlZ2lzdHJhZG8gY29uIGV4aXRvXCIsIFwiQ2VycmFyXCIsIHtcblx0XHQgIC8vZHVyYXRpb246IDIwMDBcblx0XHQvL30pO1xuXHQgLy8gfVxufSIsIm1vZHVsZS5leHBvcnRzID0gXCJcXHJcXG5cXHJcXG4uaGVhZGVyIHtcXHJcXG4gICAgaG9yaXpvbnRhbC1hbGlnbjogY2VudGVyO1xcclxcbiAgICBmb250LXNpemU6IDI1O1xcclxcbiAgICBmb250LXdlaWdodDogNjAwO1xcclxcbiAgICBtYXJnaW4tYm90dG9tOiAyMDtcXHJcXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbiAgICBjb2xvcjogcmdiKDM4LCA4LCA5OSk7XFxyXFxuICB9XFxyXFxuICBcXHJcXG4gIC5ob21lLXBhbmVse1xcclxcbiAgICAgIHZlcnRpY2FsLWFsaWduOiBjZW50ZXI7IFxcclxcbiAgICAgIGZvbnQtc2l6ZTogMjA7XFxyXFxuICAgICAgbWFyZ2luOiAxNTtcXHJcXG4gIH1cXHJcXG4gIFxcclxcbiAgLmxvZ28ge1xcclxcbiAgICAgIG1hcmdpbi1ib3R0b206IDMyO1xcclxcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcclxcbiAgICB9XFxyXFxuICAgIFxcclxcbiAgLmRlc2NyaXB0aW9uLWxhYmVse1xcclxcbiAgICAgIG1hcmdpbi1ib3R0b206IDE1O1xcclxcbiAgfVxcclxcbiAgXFxyXFxuICAuZm9ybSB7XFxyXFxuICAgICAgbWFyZ2luOiAyMDtcXHJcXG4gIH1cIiIsIm1vZHVsZS5leHBvcnRzID0gXCI8TGFiZWwgdGV4dD1cXFwiSGVsbG9cXFwiXFxyXFxuICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgY2xhc3M9XFxcImhlYWRlclxcXCI+PC9MYWJlbD5cIiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBFbGVtZW50UmVmLCBWaWV3Q2hpbGR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IGFsZXJ0LCBwcm9tcHQgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbi8vaW1wb3J0IHsgTWF0U25hY2tCYXIsIE1hdEJvdHRvbVNoZWV0LCBNYXRCb3R0b21TaGVldFJlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5tb2RlbFwiO1xyXG5pbXBvcnQgeyAgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlXCI7XHJcbi8vaW1wb3J0IHsgQWxlcnRTZXJ2aWNlLCB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci9hbGVydC5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogXCJSZWdpc3RlcjJcIixcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG5cdHRlbXBsYXRlVXJsOiBcIi4vcmVnaXN0ZXIuY29tcG9uZW50Lmh0bWxcIixcclxuXHRzdHlsZVVybHM6IFsnLi9yZWdpc3Rlci5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFJlZ2lzdGVyMkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cdG1vZGVsOiBhbnkgPSB7fTtcclxuICAgIGxvYWRpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAvL0NhbXBvIGVtYWlsXHJcbiAgICAvL2VtYWlsID0gbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5lbWFpbF0pO1xyXG5cclxuICAgIC8vQ2FtcG8gQ29udHJhc2XDsWFcclxuICAgIGhpZGUgPSB0cnVlO1xyXG5cclxuICAgIC8vRk9UT1xyXG5cdHNlbGVjdGVkRmlsZSA6IEZpbGUgPSBudWxsO1xyXG5cdFxyXG5cdHVzZXI6IFVzZXI7XHJcblx0dG9FbWFpbDtcclxuXHRjY0VtYWlsO1xyXG5cdGJjY0VtYWlsO1xyXG5cdHN1YmplY3Q7XHJcblx0bWVzc2FnZTtcclxuXHJcblx0Y29uc3RydWN0b3IoXHRcclxuXHRcdHByaXZhdGUgcGFnZTogUGFnZSwgXHJcblx0XHRwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgXHJcblx0XHRwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsIFxyXG5cdFx0Ly9wcml2YXRlIGFsZXJ0U2VydmljZTogQWxlcnRTZXJ2aWNlLCAgICAgICAgXHJcbiAgICAgICAgLy9wdWJsaWMgc25hY2tCYXI6IE1hdFNuYWNrQmFyXHJcbiAgICAgICAgKSBcclxuXHRcdHtcclxuXHJcblx0XHR0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcclxuXHRcdHRoaXMudXNlciA9IG5ldyBVc2VyKCk7XHJcblx0XHR0aGlzLnVzZXIudXNlcm5hbWUgPSBcIlwiO1xyXG5cdFx0dGhpcy51c2VyLnBhc3N3b3JkID0gXCJcIjtcclxuXHRcdH1cclxuXHJcblx0bmdPbkluaXQoKTogdm9pZCB7XHJcblx0fVxyXG5cclxuXHRyZWdpc3RlcjIoKVxyXG5cdHtcclxuXHRcdGFsZXJ0KFwiRnVuY2nDs24gbm8gZGlzcG9uaWJsZSBhY3R1YWxtZW50ZVwiKVxyXG5cdH1cclxuXHJcblxyXG59IiwibW9kdWxlLmV4cG9ydHMgPSBcIi5ob21lLXBhbmVse1xcclxcbiAgICB2ZXJ0aWNhbC1hbGlnbjogY2VudGVyOyBcXHJcXG4gICAgZm9udC1zaXplOiAyMDtcXHJcXG4gICAgbWFyZ2luOiAxNTtcXHJcXG59XFxyXFxuXFxyXFxuLmRlc2NyaXB0aW9uLWxhYmVse1xcclxcbiAgICBtYXJnaW4tYm90dG9tOiAxNTtcXHJcXG59XFxyXFxuXFxyXFxuLnBydWViYSB7XFxyXFxuICBmb250LXNpemU6IDI1O1xcclxcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXHJcXG4gIG1hcmdpbi1ib3R0b206IDIwO1xcclxcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbiAgY29sb3I6d2hpdGU7XFxyXFxufVxcclxcblxcclxcbi5yZWN1YWRybyB7XFxyXFxuICBmb250LXNpemU6IDM1O1xcclxcbiAgZm9udC13ZWlnaHQ6IDgwMDtcXHJcXG4gIG1hcmdpbi1ib3R0b206IDIwO1xcclxcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbn1cIiIsIm1vZHVsZS5leHBvcnRzID0gXCI8QWN0aW9uQmFyIHRpdGxlPVxcXCJIb21lXFxcIj5cXHJcXG48L0FjdGlvbkJhcj5cXHJcXG5cXHJcXG48R3JpZExheW91dCBiYWNrZ3JvdW5kQ29sb3I9XFxcIiMwMzc0ZGRcXFwiPlxcclxcbiAgICA8U2Nyb2xsVmlldz5cXHJcXG4gICAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwiaG9tZS1wYW5lbFxcXCI+XFxyXFxuICAgICAgICAgICAgPCEtLUFkZCB5b3VyIHBhZ2UgY29udGVudCBoZXJlLS0+XFxyXFxuICAgICAgICAgICAgPExhYmVsIHRleHRXcmFwPVxcXCJ0cnVlXFxcIiB0ZXh0PVxcXCJFc2NyaWJhIHN1IGPDs2RpZ28gZGUgc2VzacOzblxcXCJcXHJcXG4gICAgICAgICAgICAgICAgdGV4dEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBjbGFzcz1cXFwicHJ1ZWJhXFxcIlxcclxcbiAgICAgICAgICAgICAgICB0ZXh0RmllbGRCYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgIDxUZXh0RmllbGQgaGVpZ2h0PVxcXCIzMFxcXCIgbWF4TGVuZ3RoPVxcXCI0XFxcIiByb3c9XFxcIjNcXFwiIGNvbD1cXFwiMVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgWyhuZ01vZGVsKV09XFxcIm1vZGVsLmNvZGVfc2Vzc2lvblxcXCIgY2xhc3M9XFxcInJlY3VhZHJvXFxcIlxcclxcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIiBoZWlnaHQ9XFxcIjgwXFxcIiB3aWR0aD1cXFwiMjAwXFxcIj5cXHJcXG4gICAgICAgICAgICA8L1RleHRGaWVsZD5cXHJcXG4gICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIkNvbnRpbnVhclxcXCIgKHRhcCk9XFxcImluc2VydF9jb2RlX3Nlc3Npb24oKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICA8L1N0YWNrTGF5b3V0PlxcclxcbiAgICA8L1Njcm9sbFZpZXc+XFxyXFxuPC9HcmlkTGF5b3V0PlwiIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEVsZW1lbnRSZWYsIFZpZXdDaGlsZH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgYWxlcnQsIHByb21wdCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2VcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuLy9pbXBvcnQgeyBNYXRTbmFja0JhciwgTWF0Qm90dG9tU2hlZXQsIE1hdEJvdHRvbVNoZWV0UmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuaW1wb3J0IHsgVXNlciB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLm1vZGVsXCI7XHJcbmltcG9ydCB7ICBVc2VyU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLnNlcnZpY2VcIjtcclxuLy9pbXBvcnQgeyBBbGVydFNlcnZpY2UsIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL2FsZXJ0LnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiBcIlNlc3Npb25fY29kZVwiLFxyXG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcblx0dGVtcGxhdGVVcmw6IFwiLi9zZXNzaW9uX2NvZGUuY29tcG9uZW50Lmh0bWxcIixcclxuXHRzdHlsZVVybHM6IFsnLi9zZXNzaW9uX2NvZGUuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZXNzaW9uX2NvZGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIFxyXG4gICAgY3VycmVudFVzZXI6IFVzZXI7XHJcbiAgICBtb2RlbDogYW55ID0ge307XHJcbiAgICBsb2FkaW5nID0gZmFsc2U7XHJcbiAgICBtZW5zYWplOiBhbnk7XHJcblxyXG4gICAgYm9sOiBib29sZWFuXHJcblxyXG4gICAgY29uc3RydWN0b3IoIHByaXZhdGUgcGFnZTogUGFnZSwgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UscHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VXNlciA9IG5ldyBVc2VyKCk7XHJcbiAgICAgICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcbiAgICAgICAgLy90aGlzLmN1cnJlbnRVc2VyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSk7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VXNlcj10aGlzLnVzZXJTZXJ2aWNlLmN1cnJlbnRVc2VyKClcclxuICAgIH1cclxuXHJcblxyXG5cdG5nT25Jbml0KCk6IHZvaWQge1xyXG5cdH1cclxuXHJcblx0aW5zZXJ0X2NvZGVfc2Vzc2lvbigpXHJcbiAgICB7ICAgaWYodGhpcy5tb2RlbC5jb2RlX3Nlc3Npb249PScnKVxyXG4gICAgICAgICAgICBhbGVydChcIkludHJvZHV6Y2EgdW4gY8OzZGlnbyBkZSBzZXNpw7NuIHbDoWxpZG9cIik7XHJcbiAgICAgICAgZWxzZVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlci5zZXNzaW9uX2NvZGUgPSB0aGlzLm1vZGVsLmNvZGVfc2Vzc2lvbjtcclxuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5tb2RlbC5jb2RlX3Nlc3Npb24pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmN1cnJlbnRVc2VyLnNlc3Npb25fY29kZSk7XHJcbiAgICAgICAgICAgIGFsZXJ0KHRoaXMuY3VycmVudFVzZXIuc2Vzc2lvbl9jb2RlKTtcclxuICAgICAgICAgICAgdGhpcy51c2VyU2VydmljZS5zZXRVc2VyKHRoaXMuY3VycmVudFVzZXIpO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZSh0aGlzLmN1cnJlbnRVc2VyKTtcclxuICAgICAgICAgICAgYWxlcnQodGhpcy5jdXJyZW50VXNlci5zZXNzaW9uX2NvZGUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgdXBkYXRlKGN1cnJlbnRVc2VyOiBVc2VyKSB7XHJcbiAgICAgICAgdGhpcy51c2VyU2VydmljZS5zZXRVc2VyKHRoaXMuY3VycmVudFVzZXIpO1xyXG4gICAgICAgIC8vbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50VXNlclwiLEpTT04uc3RyaW5naWZ5KGN1cnJlbnRVc2VyKSk7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5jdXJyZW50VXNlci5zZWF0KTtcclxuICAgICAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLmN1cnJlbnRVc2VyKSk7XHJcbiAgICAgICAgdGhpcy51c2VyU2VydmljZS51cGRhdGUodGhpcy5jdXJyZW50VXNlcikuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9pZGVudGlmaWNhdGlvblwiXSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgYWwgYWN0dWFsaXphciBlbCBjw7NkaWdvIGRlIHNlc3Npw7NuXCIpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iLCJleHBvcnQgY2xhc3MgQ29uZmlnIHtcbiAgLy8gY2FzYSAvL3N0YXRpYyBhcGlVcmwgPSBcImh0dHA6Ly8xOTIuMTY4LjEuMzY6NDAwMFwiO1xuICAvLyBlbXVsYWRvciAvL3N0YXRpYyBhcGlVcmwgPSBcImh0dHA6Ly8xMC4wLjMuMjo0MDAwXCI7XG4gIC8vIGFlIGF2YW5hIHdpZmkgLy9zdGF0aWMgYXBpVXJsID0gXCJodHRwOi8vMTkyLjE2OC4zNS4yOjQwMDBcIjtcbiAgLy8gbW92aWwgLy8gc3RhdGljIGFwaVVybCA9IFwiaHR0cDovLzE5Mi4xNjguNDMuMjI2OjQwMDBcIjtcbiAgLy8gZWR1cm9hbSAvLyBzdGF0aWMgYXBpVXJsID0gXCJodHRwOi8vMTAuMTUwLjIzMi4yMzE6NDAwMFwiO1xuICAvLyBhdmFuYSAvLyBzdGF0aWMgYXBpVXJsID0gXCJodHRwOi8vMTAuOTkuMTUzLjEyODo0MDAwXCI7XG4gIHN0YXRpYyBhcGlVcmwgPSBcImh0dHA6Ly8xOTIuMTY4LjM1LjI6NDAwMFwiO1xuICBzdGF0aWMgYXBwS2V5ID0gXCJraWRfSHlIb1RfUkVmXCI7XG4gIHN0YXRpYyBhdXRoSGVhZGVyID0gXCJCYXNpYyBhMmxrWDBoNVNHOVVYMUpGWmpvMU1Ua3hNREpsWldGaE16UTBNek15T0RGak4yTXlPRE0zTUdRNU9USXpNUVwiO1xuICBzdGF0aWMgdG9rZW4gPSBcIlwiO1xufSIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cFJlc3BvbnNlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIHRocm93RXJyb3IgfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAsIHRhcCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5cclxuaW1wb3J0ICogYXMgbG9jYWxTdG9yYWdlIGZyb20gXCJuYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlXCI7XHJcblxyXG5pbXBvcnQgeyBVc2VyIH0gZnJvbSBcIi4vdXNlci5tb2RlbFwiO1xyXG5pbXBvcnQgeyBDb25maWcgfSBmcm9tIFwiLi4vY29uZmlnXCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBdXRoZW50aWNhdGVTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XHJcblxyXG4gICAgcmVnaXN0ZXIodXNlcjogVXNlcikge1xyXG4gICAgICAgIGlmICghdXNlci51c2VybmFtZSB8fCAhdXNlci5wYXNzd29yZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihcIlBsZWFzZSBwcm92aWRlIGJvdGggYW4gZW1haWwgYWRkcmVzcyBhbmQgcGFzc3dvcmQuXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KFxyXG4gICAgICAgICAgICBDb25maWcuYXBpVXJsICsgXCJ1c2VyL1wiICsgQ29uZmlnLmFwcEtleSxcclxuICAgICAgICAgICAgSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXIudXNlcm5hbWUsXHJcbiAgICAgICAgICAgICAgICBlbWFpbDogdXNlci51c2VybmFtZSxcclxuICAgICAgICAgICAgICAgIHBhc3N3b3JkOiB1c2VyLnBhc3N3b3JkXHJcbiAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICB7IGhlYWRlcnM6IHRoaXMuZ2V0Q29tbW9uSGVhZGVycygpIH1cclxuICAgICAgICApLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IodGhpcy5oYW5kbGVFcnJvcnMpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBsb2dpbih1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KFxyXG4gICAgICAgICAgICBDb25maWcuYXBpVXJsICsgJy91c2Vycy9hdXRoZW50aWNhdGUnLFxyXG4gICAgICAgICAgICAoe1xyXG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXIudXNlcm5hbWUsXHJcbiAgICAgICAgICAgICAgICBwYXNzd29yZDogdXNlci5wYXNzd29yZFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICkucGlwZShcclxuICAgICAgICAgICAgbWFwKHJlc3BvbnNlID0+IHJlc3BvbnNlKSxcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcih0aGlzLmhhbmRsZUVycm9ycylcclxuICAgICAgICApO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBsb2dpbjUodXNlcjogVXNlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChDb25maWcuYXBpVXJsICsgJy91c2Vycy9hdXRoZW50aWNhdGUnLCB7IHVzZXJuYW1lOiB1c2VyLnVzZXJuYW1lLCBwYXNzd29yZDogdXNlci5wYXNzd29yZCB9KVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHVzZXIzMiA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBsb2dpbiBzdWNjZXNzZnVsIGlmIHRoZXJlJ3MgYSBqd3QgdG9rZW4gaW4gdGhlIHJlc3BvbnNlXHJcbiAgICAgICAgICAgICAgICBpZiAodXNlcjMyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc3RvcmUgdXNlciBkZXRhaWxzIGFuZCBqd3QgdG9rZW4gaW4gbG9jYWwgc3RvcmFnZSB0byBrZWVwIHVzZXIgbG9nZ2VkIGluIGJldHdlZW4gcGFnZSByZWZyZXNoZXNcclxuICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY3VycmVudFVzZXInLCBKU09OLnN0cmluZ2lmeSh1c2VyKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHVzZXIzMjtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlKHVzZXI6IFVzZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvcmVnaXN0ZXInLCB1c2VyKTtcclxuICAgIH1cclxuICAgIGNyZWF0ZXBob3RvKGZpbGU6IEZvcm1EYXRhKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCIqKiEhRklMRSEhKipcIik7XHJcbiAgICAgICAgY29uc29sZS5sb2coZmlsZSk7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoQ29uZmlnLmFwaVVybCArICcvdXBsb2FkJywgZmlsZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICBcclxuICAgIGdldENvbW1vbkhlYWRlcnMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgICAgIFwiQXV0aG9yaXphdGlvblwiOiBDb25maWcuYXV0aEhlYWRlclxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvL2xvZ291dChfaWQ6IHN0cmluZywgdXNlcjogVXNlcikge1xyXG4gICAgICAgLy8gcmV0dXJuIHRoaXMuaHR0cC5wdXQoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvbG9nb3V0LycgKyBfaWQsIHVzZXIpO1xyXG4gICAgLy99XHJcblxyXG4gICAgaGFuZGxlRXJyb3JzKGVycm9yOiBSZXNwb25zZSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KGVycm9yKSk7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vSGVtb3MgYcOxYWRpZG8gZWwge3Jlc3BvbnNlVHlwZTogJ3RleHQnfSBwb3JxdWUgZGFiYSB1biBlcnJvciBkZSBhcHJzaW5nIGN1YW5kbyBsZcOtYSBlbCB2YWxvciByZXRvcm5hZG9cclxuICAgIC8vIEVsIG3DqXRvZG8gZW4gcmVhbGlkYWQgZXMgIHJldHVybiB0aGlzLmh0dHAucHV0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzLycgKyB1c2VyLl9pZCwgdXNlcilcclxuICAgIHVwZGF0ZSh1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wdXQoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvJyArIHVzZXIuX2lkLCB1c2VyLHtyZXNwb25zZVR5cGU6ICd0ZXh0J30pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dChfaWQ6IHN0cmluZywgdXNlcjogVXNlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucHV0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2xvZ291dC8nICsgX2lkLCB1c2VyLHtyZXNwb25zZVR5cGU6ICd0ZXh0J30pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRVc2VyKGRhdGE6YW55KXtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRVc2VyXCIsSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjdXJyZW50VXNlcigpe1xyXG4gICAgICAgIHJldHVybiBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudFVzZXJcIikpOyAvL3x8IG51bGw7XHJcbiAgICB9XHJcbn0iLCJleHBvcnQgY2xhc3MgVXNlciB7XHJcbiAgICBfaWQ6IHN0cmluZztcclxuICAgIHVzZXJuYW1lOiBzdHJpbmc7XHJcbiAgICBwYXNzd29yZDogc3RyaW5nO1xyXG4gICAgZmlyc3ROYW1lOiBzdHJpbmc7XHJcbiAgICBsYXN0TmFtZTogc3RyaW5nO1xyXG4gICAgY2xhc3M6IHN0cmluZztcclxuICAgIHNlYXQ6IHN0cmluZztcclxuICAgIG51bWF0OiBzdHJpbmc7XHJcbiAgICBxcnN0cmluZzogc3RyaW5nO1xyXG4gICAgYW5zd2VyOiBzdHJpbmc7XHJcbiAgICBzZXNzaW9uX2NvZGU6IHN0cmluZztcclxuICAgIHNyYzogc3RyaW5nO1xyXG4gIH0iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBSZXNwb25zZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCB0aHJvd0Vycm9yIH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCB0YXAgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuXHJcbmltcG9ydCAqIGFzIGxvY2FsU3RvcmFnZSBmcm9tIFwibmF0aXZlc2NyaXB0LWxvY2Fsc3RvcmFnZVwiO1xyXG5cclxuaW1wb3J0IHsgVXNlciB9IGZyb20gXCIuL3VzZXIubW9kZWxcIjtcclxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSBcIi4uL2NvbmZpZ1wiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7IH1cclxuXHJcbiAgICByZWdpc3Rlcih1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgaWYgKCF1c2VyLnVzZXJuYW1lIHx8ICF1c2VyLnBhc3N3b3JkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKFwiUGxlYXNlIHByb3ZpZGUgYm90aCBhbiBlbWFpbCBhZGRyZXNzIGFuZCBwYXNzd29yZC5cIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoXHJcbiAgICAgICAgICAgIENvbmZpZy5hcGlVcmwgKyBcInVzZXIvXCIgKyBDb25maWcuYXBwS2V5LFxyXG4gICAgICAgICAgICBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgICAgICAgICB1c2VybmFtZTogdXNlci51c2VybmFtZSxcclxuICAgICAgICAgICAgICAgIGVtYWlsOiB1c2VyLnVzZXJuYW1lLFxyXG4gICAgICAgICAgICAgICAgcGFzc3dvcmQ6IHVzZXIucGFzc3dvcmRcclxuICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgIHsgaGVhZGVyczogdGhpcy5nZXRDb21tb25IZWFkZXJzKCkgfVxyXG4gICAgICAgICkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcih0aGlzLmhhbmRsZUVycm9ycylcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ2luKHVzZXI6IFVzZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoXHJcbiAgICAgICAgICAgIENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2F1dGhlbnRpY2F0ZScsXHJcbiAgICAgICAgICAgICh7XHJcbiAgICAgICAgICAgICAgICB1c2VybmFtZTogdXNlci51c2VybmFtZSxcclxuICAgICAgICAgICAgICAgIHBhc3N3b3JkOiB1c2VyLnBhc3N3b3JkXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKS5waXBlKFxyXG4gICAgICAgICAgICBtYXAocmVzcG9uc2UgPT4gcmVzcG9uc2UpLFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKHRoaXMuaGFuZGxlRXJyb3JzKVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGxvZ2luNSh1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2F1dGhlbnRpY2F0ZScsIHsgdXNlcm5hbWU6IHVzZXIudXNlcm5hbWUsIHBhc3N3b3JkOiB1c2VyLnBhc3N3b3JkIH0pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUodXNlcjMyID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIGxvZ2luIHN1Y2Nlc3NmdWwgaWYgdGhlcmUncyBhIGp3dCB0b2tlbiBpbiB0aGUgcmVzcG9uc2VcclxuICAgICAgICAgICAgICAgIGlmICh1c2VyMzIpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBzdG9yZSB1c2VyIGRldGFpbHMgYW5kIGp3dCB0b2tlbiBpbiBsb2NhbCBzdG9yYWdlIHRvIGtlZXAgdXNlciBsb2dnZWQgaW4gYmV0d2VlbiBwYWdlIHJlZnJlc2hlc1xyXG4gICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50VXNlcicsIEpTT04uc3RyaW5naWZ5KHVzZXIpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdXNlcjMyO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGUodXNlcjogVXNlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChDb25maWcuYXBpVXJsICsgJy91c2Vycy9yZWdpc3RlcicsIHVzZXIpO1xyXG4gICAgfVxyXG4gICAgY3JlYXRlcGhvdG8oZmlsZTogRm9ybURhdGEpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIioqISFGSUxFISEqKlwiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhmaWxlKTtcclxuICAgICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChDb25maWcuYXBpVXJsICsgJy91cGxvYWQnLCBmaWxlKTtcclxuICAgICAgfVxyXG5cclxuICAgIGNyZWF0ZXBob3RvMigpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIioqISFGSUxFZSEhKipcIik7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhmaWxlKTtcclxuICAgICAgICAgIC8vcmV0dXJuIHRoaXMuaHR0cC5wb3N0KENvbmZpZy5hcGlVcmwgKyAnL3VwbG9hZCcsIGZpbGUpO1xyXG4gICAgICAgICAgLy92YXIgZmlsZSA9ICBcIi9zdG9yYWdlL2VtdWxhdGVkLzAvV2hhdHNBcHAvTWVkaWEvV2hhdHNBcHAgSW1hZ2VzL0lNRy0yMDIwMDIyNS1XQTAwMDEuanBnXCI7XHJcbiAgICAgICAgICB2YXIgZmlsZSA9ICBcIi9zdG9yYWdlL2VtdWxhdGVkLzAvV2hhdHNBcHAvTWVkaWEvV2hhdHNBcHAgSW1hZ2VzL0lNRy0yMDIwMDMwMS1XQTAwMDEuanBnXCI7XHJcblxyXG4gICAgICAgICAgdmFyIG5hbWUgPSBmaWxlLnN1YnN0cihmaWxlLmxhc3RJbmRleE9mKFwiL1wiKSArIDEpO1xyXG4gICAgICAgIHZhciBiZ2h0dHAgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWJhY2tncm91bmQtaHR0cFwiKTtcclxuICAgICAgICB2YXIgc2Vzc2lvbiA9IGJnaHR0cC5zZXNzaW9uKFwiaW1hZ2UtdXBsb2FkXCIpO1xyXG4gICAgICAgIHZhciByZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICB1cmw6IENvbmZpZy5hcGlVcmwgKyAnL3VwbG9hZDInLFxyXG4gICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL29jdGV0LXN0cmVhbVwiLFxyXG4gICAgICAgICAgICAgICAgXCJGaWxlLU5hbWVcIjogbmFtZVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJVcGxvYWRpbmcgXCIgKyBuYW1lLFxyXG4gICAgICAgICAgICAvL2FuZHJvaWREaXNwbGF5Tm90aWZpY2F0aW9uUHJvZ3Jlc3M6IGZhbHNlLFxyXG4gICAgICAgICAgICBhbmRyb2lkQXV0b0NsZWFyTm90aWZpY2F0aW9uOiB0cnVlLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdmFyIHRhc2sgPXNlc3Npb24udXBsb2FkRmlsZShmaWxlLCByZXF1ZXN0KTtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb21tb25IZWFkZXJzMigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL29jdGV0LXN0cmVhbVwiLFxyXG4gICAgICAgICAgICBcIkZpbGUtTmFtZVwiOiBuYW1lXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgZ2V0Q29tbW9uSGVhZGVycygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6IENvbmZpZy5hdXRoSGVhZGVyXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vbG9nb3V0KF9pZDogc3RyaW5nLCB1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAvLyByZXR1cm4gdGhpcy5odHRwLnB1dChDb25maWcuYXBpVXJsICsgJy91c2Vycy9sb2dvdXQvJyArIF9pZCwgdXNlcik7XHJcbiAgICAvL31cclxuXHJcbiAgICBoYW5kbGVFcnJvcnMoZXJyb3I6IFJlc3BvbnNlKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkoZXJyb3IpKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvcik7XHJcbiAgICB9XHJcblxyXG4gICAgLy9IZW1vcyBhw7FhZGlkbyBlbCB7cmVzcG9uc2VUeXBlOiAndGV4dCd9IHBvcnF1ZSBkYWJhIHVuIGVycm9yIGRlIGFwcnNpbmcgY3VhbmRvIGxlw61hIGVsIHZhbG9yIHJldG9ybmFkb1xyXG4gICAgLy8gRWwgbcOpdG9kbyBlbiByZWFsaWRhZCBlcyAgcmV0dXJuIHRoaXMuaHR0cC5wdXQoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvJyArIHVzZXIuX2lkLCB1c2VyKVxyXG4gICAgdXBkYXRlKHVzZXI6IFVzZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnB1dChDb25maWcuYXBpVXJsICsgJy91c2Vycy8nICsgdXNlci5faWQsIHVzZXIse3Jlc3BvbnNlVHlwZTogJ3RleHQnfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbG9nb3V0KF9pZDogc3RyaW5nLCB1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wdXQoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvbG9nb3V0LycgKyBfaWQsIHVzZXIse3Jlc3BvbnNlVHlwZTogJ3RleHQnfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldFVzZXIoZGF0YTphbnkpe1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudFVzZXJcIixKU09OLnN0cmluZ2lmeShkYXRhKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGN1cnJlbnRVc2VyKCl7XHJcbiAgICAgICAgcmV0dXJuIEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50VXNlclwiKSk7IC8vfHwgbnVsbDtcclxuICAgIH1cclxufSIsIm1vZHVsZS5leHBvcnRzID0gXCIuaG9tZS1wYW5lbHtcXHJcXG5cXHR2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xcclxcblxcdGZvbnQtc2l6ZTogMjA7XFxyXFxuXFx0bWFyZ2luOiAxNTtcXHJcXG59XFxyXFxuXFxyXFxuLmNhcmQge1xcclxcblxcdGJvcmRlci1yYWRpdXM6IDIwO1xcclxcblxcdG1hcmdpbjogMTI7XFxyXFxuICAgIHBhZGRpbmc6IDEwO1xcclxcblxcdGJhY2tncm91bmQtY29sb3I6IHJnYigxNzksIDEyNywgMjQpO1xcclxcbiAgICBjb2xvcjogIzM2MjcxMztcXHJcXG4gICAgaGVpZ2h0OiA1NiU7XFxyXFxufVwiIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxBY3Rpb25CYXIgdGl0bGU9XFxcIlN0dWRlbnQgTWFpbiBQYWdlXFxcIiBjbGFzcz1cXFwiYWN0aW9uLWJhclxcXCI+XFxyXFxuPC9BY3Rpb25CYXI+XFxyXFxuXFxyXFxuPFN0YWNrTGF5b3V0PlxcclxcbiAgICA8SW1hZ2UgY2xhc3M9XFxcImxvZ29cXFwiIHNyYz1cXFwifi9pbWFnZXMvTG9nb19wb2xpdGVjbmljYS5wbmdcXFwiPjwvSW1hZ2U+XFxyXFxuICAgIDxMYWJlbCB0ZXh0PVxcXCJCaWVudmVuaWRvIGEgbGEgQVBQIFNJRElTRUNcXFwiPjwvTGFiZWw+XFxyXFxuICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiPjwvTGFiZWw+XFxyXFxuICAgIDxTdGFja0xheW91dD5cXHJcXG4gICAgICAgIDxUYWJzIGhlaWdodD1cXFwiMjAwMHB4XFxcIj5cXHJcXG4gICAgICAgICAgICA8VGFiU3RyaXA+XFxyXFxuICAgICAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiRGF0b3MgZGUgbGEgY2xhc2VcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJyZXM6Ly9ob21lXFxcIj48L0ltYWdlPlxcclxcbiAgICAgICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXHJcXG4gICAgICAgICAgICAgICAgPFRhYlN0cmlwSXRlbT5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJUZXN0cyBkaXNwb25pYmxlc1xcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcInJlczovL3NldHRpbmdzXFxcIj48L0ltYWdlPlxcclxcbiAgICAgICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXHJcXG4gICAgICAgICAgICAgICAgPFRhYlN0cmlwSXRlbT5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJLYWhvb3QgR2FtZVxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcInJlczovL3NlYXJjaFxcXCI+PC9JbWFnZT5cXHJcXG4gICAgICAgICAgICAgICAgPC9UYWJTdHJpcEl0ZW0+XFxyXFxuICAgICAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiQWp1c3Rlc1xcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcInJlczovL3NldHRpbmdzXFxcIj48L0ltYWdlPlxcclxcbiAgICAgICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXHJcXG4gICAgICAgICAgICA8L1RhYlN0cmlwPlxcclxcbiAgICAgICAgICAgIDxUYWJDb250ZW50SXRlbT5cXHJcXG4gICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJIb21lIFBhZ2VcXFwiIGNsYXNzPVxcXCJoMiB0ZXh0LWNlbnRlclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlByb2Zlc29yOiB7e3Byb2Zlc29yfX1cXFwiIGNsYXNzPVxcXCJoMiB0ZXh0LWNlbnRlclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIkNsYXNlOiB7e2NsYXNlfX1cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJoMiB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIkFzaWduYXR1cmE6IHt7YXNpZ25hdHVyYX19XFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJHcnVwbzoge3tncnVwb319XFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJBc2llbnRvIG9jdXBhZG86IHt7YXNpZW50b19vY3VwYWRvfX1cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJoMiB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXHJcXG4gICAgICAgICAgICA8L1RhYkNvbnRlbnRJdGVtPlxcclxcbiAgICAgICAgICAgIDxUYWJDb250ZW50SXRlbT5cXHJcXG4gICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJUZXN0cyBQYWdlXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiVGVzdCAxXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiMzNUE2RThcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwidGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJUZXN0IDJcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiIzM1QTZFOFxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I9XFxcIndoaXRlc21va2VcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIlRlc3QgM1xcXCIgYmFja2dyb3VuZENvbG9yPVxcXCIjMzVBNkU4XFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcj1cXFwid2hpdGVzbW9rZVxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiVGVzdCA0XFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiMzNUE2RThcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwidGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJUZXN0IDVcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiIzM1QTZFOFxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I9XFxcIndoaXRlc21va2VcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXHJcXG4gICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcclxcbiAgICAgICAgICAgIDwvVGFiQ29udGVudEl0ZW0+XFxyXFxuICAgICAgICAgICAgPFRhYkNvbnRlbnRJdGVtPlxcclxcbiAgICAgICAgICAgICAgICA8R3JpZExheW91dD5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJTZWFyY2ggUGFnZVxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXHJcXG4gICAgICAgICAgICA8L1RhYkNvbnRlbnRJdGVtPlxcclxcbiAgICAgICAgICAgIDxUYWJDb250ZW50SXRlbT5cXHJcXG4gICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJBanVzdGVzXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiU2FsaXIgZGUgbGEgY2xhc2VcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcj1cXFwiIzM1QTZFOFxcXCIgY29sb3I9XFxcIndoaXRlc21va2VcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIkNlcnJhciBzZXNpw7NuXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiMzNUE2RThcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXHJcXG4gICAgICAgICAgICA8L1RhYkNvbnRlbnRJdGVtPlxcclxcbiAgICAgICAgPC9UYWJzPlxcclxcbiAgICA8L1N0YWNrTGF5b3V0PlxcclxcbiAgICA8QnV0dG9uIHRleHQ9XFxcIkNlcnJhciBTZXNpw7NuXFxcIiAodGFwKT1cXFwibG9nb3V0KClcXFwiPjwvQnV0dG9uPlxcclxcbjwvU3RhY2tMYXlvdXQ+XCIiLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlXCI7XHJcblxyXG5pbXBvcnQgeyBVc2VyIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3VzZXIubW9kZWxcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlXCI7XHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiBcIlN0dWRlbnRcIixcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG5cdHRlbXBsYXRlVXJsOiBcIi4vc3R1ZGVudC5jb21wb25lbnQuaHRtbFwiLFxyXG5cdHN0eWxlVXJsczogWycuL3N0dWRlbnQuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTdHVkZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBjdXJyZW50VXNlcjogVXNlcjtcclxuXHJcbiAgICBwcm9mZXNvcjogc3RyaW5nO1xyXG4gICAgY2xhc2U6IHN0cmluZztcclxuICAgIGFzaWduYXR1cmE6IHN0cmluZztcclxuICAgIGdydXBvOiBzdHJpbmc7XHJcbiAgICBhc2llbnRvX29jdXBhZG86IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UscHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLHByaXZhdGUgcGFnZTogUGFnZSkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXIgPSBuZXcgVXNlcigpO1xyXG5cclxuICAgICAgICB0aGlzLnByb2Zlc29yID0gXCJBbGJlcnRvIEJydW5ldGVcIjtcclxuICAgICAgICB0aGlzLmNsYXNlID0gXCJCLTEyXCI7XHJcbiAgICAgICAgdGhpcy5hc2lnbmF0dXJhID0gXCJFbGVjdHLDs25pY2FcIjtcclxuICAgICAgICB0aGlzLmdydXBvID0gXCJBLTIwNFwiO1xyXG4gICAgICAgIHRoaXMuYXNpZW50b19vY3VwYWRvID0gXCIyXCI7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy90aGlzLmN1cnJlbnRVc2VyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSk7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VXNlcj10aGlzLnVzZXJTZXJ2aWNlLmN1cnJlbnRVc2VyKClcclxuICAgICAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcblxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIH1cclxuICAgIFxyXG4gICAgaWRlbnRpZmljYXRpb24oKVxyXG4gICAge1xyXG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvaWRlbnRpZmljYXRpb25cIl0pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dCgpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRVc2VyID0gdGhpcy51c2VyU2VydmljZS5jdXJyZW50VXNlcigpO1xyXG4gICAgICAgIHRoaXMudXNlclNlcnZpY2UubG9nb3V0KHRoaXMuY3VycmVudFVzZXIuX2lkLCB0aGlzLmN1cnJlbnRVc2VyKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICBkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgYWwgY2VycmFyIHNlc2nDs25cIik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICB9XHJcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IFwiLmhvbWUtcGFuZWx7XFxyXFxuXFx0dmVydGljYWwtYWxpZ246IGNlbnRlcjtcXHJcXG5cXHRmb250LXNpemU6IDIwO1xcclxcblxcdG1hcmdpbjogMTU7XFxyXFxufVwiIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxBY3Rpb25CYXIgdGl0bGU9XFxcIlRlYWNoZXJcXFwiIGNsYXNzPVxcXCJhY3Rpb24tYmFyXFxcIj5cXHJcXG4gICAgPC9BY3Rpb25CYXI+XFxyXFxuICAgIFxcclxcbiAgICA8U2Nyb2xsVmlldyBjbGFzcz1cXFwicGFnZVxcXCI+XFxyXFxuICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImhvbWUtcGFuZWxcXFwiPlxcclxcbiAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiSWRlbnRpZmljYXIgY2xhc2VcXFwiICBiYWNrZ3JvdW5kLWNvbG9yOnJnYig3NiwgOTEsIDIxMyk+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJDcmVhciB0ZXN0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IG0tdC0yMFxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJDcmVhclBydWViYVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBtLXQtMjBcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiQ2VycmFyIFNlc2nDs25cXFwiICh0YXApPVxcXCJsb2dvdXQoKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICA8L1N0YWNrTGF5b3V0PlxcclxcbiAgICA8L1Njcm9sbFZpZXc+XCIiLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogXCJUZWFjaGVyXCIsXHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHR0ZW1wbGF0ZVVybDogXCIuL3RlYWNoZXIuY29tcG9uZW50Lmh0bWxcIixcclxuXHRzdHlsZVVybHM6IFsnLi90ZWFjaGVyLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGVhY2hlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xyXG4gICAgfVxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIH1cclxuICAgIFxyXG4gICAgbG9nb3V0KClcclxuICAgIHtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2xvZ2luXCJdKTtcclxuICAgIH1cclxufSJdLCJzb3VyY2VSb290IjoiIn0=