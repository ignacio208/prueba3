require("./runtime.js");require("./vendor.js");module.exports =
(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["bundle"],{

/***/ "../$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "gr-app",
            template: "<page-router-outlet></page-router-outlet>"
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("nativescript-theme-core/css/core.light.css", () => __webpack_require__("../node_modules/nativescript-dev-webpack/css2json-loader.js?useForImports!../node_modules/nativescript-theme-core/css/core.light.css"));module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"import","import":"\"nativescript-theme-core/css/core.light.css\""},{"type":"rule","selectors":[".btn-primary"],"declarations":[{"type":"declaration","property":"height","value":"50"},{"type":"declaration","property":"background-color","value":"rgb(26, 91, 213)"},{"type":"declaration","property":"border-radius","value":"5"},{"type":"declaration","property":"font-size","value":"20"},{"type":"declaration","property":"font-weight","value":"600"}]},{"type":"rule","selectors":[".btn-second"],"declarations":[{"type":"declaration","property":"height","value":"35"},{"type":"declaration","property":"background-color","value":"rgb(13, 100, 172)"},{"type":"declaration","property":"color","value":"whitesmoke"},{"type":"declaration","property":"border-radius","value":"5"},{"type":"declaration","property":"font-size","value":"12"},{"type":"declaration","property":"font-weight","value":"600"},{"type":"declaration","property":"width","value":"105"}]},{"type":"rule","selectors":[".btn-primary:disabled"],"declarations":[{"type":"declaration","property":"opacity","value":"0.5"}]},{"type":"rule","selectors":[".btn-second:disabled"],"declarations":[{"type":"declaration","property":"opacity","value":"0.5"}]},{"type":"rule","selectors":[".card"],"declarations":[{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"margin","value":"12"},{"type":"declaration","property":"padding","value":"10"},{"type":"declaration","property":"background-color","value":"#E3E9F8"},{"type":"declaration","property":"color","value":"#131636"},{"type":"declaration","property":"height","value":"86%"}]}],"parsingErrors":[]}};;
    if (true) {
        module.hot.accept();
        module.hot.dispose(() => {
            global.hmrRefresh({ type: 'style', path: './app.css' });
        })
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/forms/index.js");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/nativescript-angular/http-client/index.js");
/* harmony import */ var nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/nativescript.module.js");
/* harmony import */ var nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./app.routing.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./app.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("./login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("./register/register.component.ts");
/* harmony import */ var _register2_register_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("./register2/register.component.ts");
/* harmony import */ var _student_student_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("./student/student.component.ts");
/* harmony import */ var _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__("./teacher/teacher.component.ts");
/* harmony import */ var _session_code_session_code_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__("./session_code/session_code.component.ts");
/* harmony import */ var _identification_identification_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__("./identification/identification.component.ts");
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__("./reset_password/reset_password.component.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__("./shared/user/authenticate.service.ts");
/* harmony import */ var _helpers_index__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__("./helpers/index.ts");


















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3__["NativeScriptModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1__["NativeScriptFormsModule"],
                nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2__["NativeScriptHttpClientModule"],
                nativescript_angular_router__WEBPACK_IMPORTED_MODULE_4__["NativeScriptRouterModule"],
                nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_3__["NativeScriptModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_1__["NativeScriptFormsModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                nativescript_angular_http_client__WEBPACK_IMPORTED_MODULE_2__["NativeScriptHttpClientModule"]
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"],
                _register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"],
                _register2_register_component__WEBPACK_IMPORTED_MODULE_9__["Register2Component"],
                _student_student_component__WEBPACK_IMPORTED_MODULE_10__["StudentComponent"],
                _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_11__["TeacherComponent"],
                _session_code_session_code_component__WEBPACK_IMPORTED_MODULE_12__["Session_codeComponent"],
                _identification_identification_component__WEBPACK_IMPORTED_MODULE_13__["IdentificationComponent"],
                _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_14__["Reset_PasswordComponent"]
            ],
            providers: [
                _shared_user_user_service__WEBPACK_IMPORTED_MODULE_15__["UserService"],
                _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_16__["AuthenticateService"],
                _helpers_index__WEBPACK_IMPORTED_MODULE_17__["JwtInterceptorProvider"]
                //ErrorInterceptorProvider
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./register/register.component.ts");
/* harmony import */ var _register2_register_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./register2/register.component.ts");
/* harmony import */ var _student_student_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./student/student.component.ts");
/* harmony import */ var _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./teacher/teacher.component.ts");
/* harmony import */ var _session_code_session_code_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("./session_code/session_code.component.ts");
/* harmony import */ var _identification_identification_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("./identification/identification.component.ts");
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("./reset_password/reset_password.component.ts");










var routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "login", component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: "register", component: _register_register_component__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"] },
    { path: "register2", component: _register2_register_component__WEBPACK_IMPORTED_MODULE_4__["Register2Component"] },
    { path: "student", component: _student_student_component__WEBPACK_IMPORTED_MODULE_5__["StudentComponent"] },
    { path: "teacher", component: _teacher_teacher_component__WEBPACK_IMPORTED_MODULE_6__["TeacherComponent"] },
    { path: "session_code", component: _session_code_session_code_component__WEBPACK_IMPORTED_MODULE_7__["Session_codeComponent"] },
    { path: "identification", component: _identification_identification_component__WEBPACK_IMPORTED_MODULE_8__["IdentificationComponent"] },
    { path: "reset_password", component: _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_9__["Reset_PasswordComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forRoot(routes)],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./helpers/error.interceptor.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return ErrorInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptorProvider", function() { return ErrorInterceptorProvider; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/rxjs/add/observable/throw.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/rxjs/add/operator/catch.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4__);





var ErrorInterceptor = /** @class */ (function () {
    function ErrorInterceptor() {
    }
    ErrorInterceptor.prototype.intercept = function (request, next) {
        // extract error message from http body if an error occurs
        return next.handle(request).catch(function (errorResponse) {
            return rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(errorResponse.error);
        });
    };
    ErrorInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], ErrorInterceptor);
    return ErrorInterceptor;
}());

var ErrorInterceptorProvider = {
    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HTTP_INTERCEPTORS"],
    useClass: ErrorInterceptor,
    multi: true,
};


/***/ }),

/***/ "./helpers/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _error_interceptor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./helpers/error.interceptor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return _error_interceptor__WEBPACK_IMPORTED_MODULE_0__["ErrorInterceptor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptorProvider", function() { return _error_interceptor__WEBPACK_IMPORTED_MODULE_0__["ErrorInterceptorProvider"]; });

/* harmony import */ var _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./helpers/jwt.interceptor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__["JwtInterceptor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptorProvider", function() { return _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__["JwtInterceptorProvider"]; });





/***/ }),

/***/ "./helpers/jwt.interceptor.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptorProvider", function() { return JwtInterceptorProvider; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/common/fesm5/http.js");


var JwtInterceptor = /** @class */ (function () {
    function JwtInterceptor() {
    }
    JwtInterceptor.prototype.intercept = function (request, next) {
        // add authorization header with jwt token if available
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + currentUser.token
                }
            });
        }
        return next.handle(request);
    };
    JwtInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], JwtInterceptor);
    return JwtInterceptor;
}());

var JwtInterceptorProvider = {
    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HTTP_INTERCEPTORS"],
    useClass: JwtInterceptor,
    multi: true,
};


/***/ }),

/***/ "./identification/identification.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n\tvertical-align: center;\r\n\tfont-size: 20;\r\n\tmargin: 15;\r\n}\r\n\r\n.header {\r\n\thorizontal-align: center;\r\n\tfont-size: 25;\r\n\tfont-weight: 600;\r\n\tmargin-bottom: 20;\r\n\ttext-align: center;\r\n\tcolor: rgb(38, 8, 99);\r\n  }\r\n\r\n  .btn-img{\r\n    border-radius: 5;\r\n    border-width: 1;\r\n    color: black;\r\n\tmargin: 25;\r\n\tmargin-top: 15;\r\n    font-size: 35;\r\n    border-color: #2b3c6a;\r\n\tbackground-color: rgb(226, 227, 236);\r\n}\r\n\r\n.bt2{\r\n\tborder-radius: 5;\r\n\tborder-width: 1;\r\n\tborder-color: #2b3c6a;\r\n\tbackground-color:rgb(250, 227, 227)\r\n}"

/***/ }),

/***/ "./identification/identification.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Student Identification\" class=\"action-bar\">\r\n</ActionBar>\r\n\r\n<StackLayout>\r\n    <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\r\n    <Label text=\"\" class=\"h3 text-center\"></Label>\r\n    <Label  class=\"header\" text=\"Registro de asistencia en clase\"\r\n    horizontalAlignment=\"center\" class=\"header\"></Label>\r\n\r\n    <StackLayout class=\"btn-img\"  orientation=\"horizontal\" padding=\"5\" (tap)=\"scanBarcode()\">\r\n        <Image src=\"~/images/qr-code.png\"  width=\"40%\" height=\"22%\"  marginRight=\"10\"></Image>\r\n        <Label text=\"Lector QR\" verticalAlignment=\"center\"></Label>\r\n     </StackLayout>\r\n     <StackLayout class=\"btn-img\"  orientation=\"horizontal\" padding=\"5\" (tap)=\"mensajeReadNFC()\" (tap)=\"ReadNFC()\" >\r\n        <Image src=\"~/images/nfc-round-tag.png\"  width=\"40%\" height=\"31%\"  marginRight=\"10\"></Image>\r\n        <Label text=\"Lector NFC\" verticalAlignment=\"center\"></Label>\r\n     </StackLayout>\r\n     <!-- \r\n        <StackLayout class=\"home-panel\" width=\"80%\">\r\n                <Button   text=\"Lector QR\" class=\"h1 text-center\" background-color:rgb(76, 91, 213) (tap)=\"scanBarcode()\"></Button>\r\n                <Label text=\"\" class=\"h3 text-center\"></Label>\r\n                <Button  class=\"h1 text-center\" background-color:rgb(76, 91, 213) (tap)=\"mensajeReadNFC()\" (tap)=\"ReadNFC()\">\r\n                    <FormattedString>\r\n                        <Span text=\"Lector NFC\"  fontWeight=\"bold\"></Span>\r\n                    </FormattedString>\r\n                </Button>\r\n                <Label text=\"\" class=\"h3 text-center\"></Label>\r\n                <Button   text=\"Manual\" background-color:rgb(76, 91, 213) (tap)=\"mostrarmensaje()\"></Button>\r\n                <Label text=\"\" class=\"h3 text-center\"></Label>\r\n               \r\n                <Button   text=\"Elegir Clase\" background-color:rgb(76, 91, 213) (tap)=\"onItemTapClasses($event)\"></Button>\r\n                <ListView [items]=\"classes\" (itemTap)=\"onItemTapClasses($event)\"\r\n                *ngIf=\"enableAulas\" style=\"height:550px\">\r\n                <ng-template let-classroom=\"item\">\r\n                    <FlexboxLayout flexDirection=\"row\">\r\n                        <Label [text]=\"classroom.aula\" class=\"t-12\"\r\n                            verticalAlignment=\"center\" style=\"width: 90%\" style=\"height: 30%\">\r\n                        </Label>\r\n                    </FlexboxLayout>\r\n                </ng-template>\r\n                </ListView>\r\n                <Button  text=\"Elegir Asiento\" background-color:rgb(76, 91, 213) (tap)=\"onItemTapSeats($event)\"></Button>\r\n                <ListView [items]=\"seats\" (itemTap)=\"onItemTapSeats($event)\"\r\n                *ngIf=\"enableSeats\" style=\"height:550px\">\r\n                <ng-template let-classroom=\"seat\">\r\n                    <FlexboxLayout flexDirection=\"row\">\r\n                        <Label [text]=\"seat.value\" class=\"t-12\"\r\n                            verticalAlignment=\"center\" style=\"width: 60%\">\r\n                        </Label>\r\n                    </FlexboxLayout>\r\n                </ng-template>\r\n                </ListView>\r\n            -->\r\n\r\n            <StackLayout class=\"home-panel\" width=\"70%\" >\r\n                <Button  text=\"Directo 'A-10 Asiento:4' \"  class=\"bt2\" (tap)=\"update2()\"></Button>\r\n                <Label text=\"\" class=\"h3 text-center\"></Label>\r\n                <Button width=\"60%\" text=\"Volver\"  (tap)=\"back()\" backgroundcolor=\"rgb(79, 152, 235)\" color=\"white\"></Button>\r\n                <Button  width=\"70%\" text=\"Cerrar Sesión\" (tap)=\"logout()\" backgroundcolor=\"rgb(26, 91, 213)\" color=\"white\"></Button>\r\n            </StackLayout>\r\n\r\n</StackLayout>"

/***/ }),

/***/ "./identification/identification.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdentificationComponent", function() { return IdentificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("../node_modules/nativescript-barcodescanner/barcodescanner.js");
/* harmony import */ var nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var nativescript_nfc__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-nfc/nfc.js");
/* harmony import */ var nativescript_nfc__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_nfc__WEBPACK_IMPORTED_MODULE_6__);







var IdentificationComponent = /** @class */ (function () {
    function IdentificationComponent(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.nfc = new nativescript_nfc__WEBPACK_IMPORTED_MODULE_6__["Nfc"]();
        this.classes = [];
        this.enableAulas = false;
        this.enableSeats = false;
        this.seats = [
            { value: '1' }, { value: '2' }, { value: '3' }, { value: '4' }, { value: '5' }, { value: '6' }, { value: '7' }, { value: '8' }, { value: '9' }, { value: '10' }, { value: '11' }, { value: '12' }, { value: '13' }, { value: '14' }, { value: '15' }, { value: '16' }, { value: '17' },
            { value: '18' }, { value: '19' }, { value: '20' }, { value: '21' }, { value: '22' }, { value: '23' }, { value: '24' }, { value: '25' }, { value: '26' }, { value: '27' }, { value: '28' }, { value: '29' }, { value: '30' }, { value: '30' }, { value: '31' }, { value: '32' }, { value: '33' }
        ];
        this.currentUser = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_2__["User"]();
        this.page.actionBarHidden = true;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //console.log(JSON.stringify(this.currentUser));
        //this.currentUser=this.userService.currentUser()
    }
    IdentificationComponent.prototype.ngOnInit = function () {
        this.loadAllClassrooms();
    };
    IdentificationComponent.prototype.loadAllClassrooms = function () {
        var _this = this;
        this.userService.getAllClassrooms().subscribe(function (classes) { _this.classes = classes; });
    };
    IdentificationComponent.prototype.back = function () {
        var _this = this;
        this.currentUser.code_session = null;
        //this.userService.setUser(this.currentUser);
        localStorage.setItem("currentUser", JSON.stringify(this.currentUser));
        //console.log(this.currentUser.seat);
        //console.log(JSON.stringify(this.currentUser));
        this.userService.update(this.currentUser).subscribe(function (data) {
            //alert("Registrado en la clase con éxito");
            _this.routerExtensions.back();
        }, function (error) {
            alert("Error al volver a seleccionar código de sessión");
        });
    };
    IdentificationComponent.prototype.onItemTapClasses = function () {
        this.enableAulas = true;
    };
    IdentificationComponent.prototype.onItemTapSeats = function () {
        this.enableSeats = true;
    };
    IdentificationComponent.prototype.scanBarcode = function () {
        var _this = this;
        new nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_5__["BarcodeScanner"]().scan({
            formats: "QR_CODE, EAN_13",
            cancelLabel: "EXIT. Also, try the volume buttons!",
            cancelLabelBackgroundColor: "#333333",
            message: "Use the volume buttons for extra light",
            showFlipCameraButton: true,
            preferFrontCamera: false,
            showTorchButton: true,
            beepOnScan: true,
            fullScreen: true,
            torchOn: false,
            closeCallback: function () { console.log("Scanner closed"); },
            resultDisplayDuration: 500,
            openSettingsIfPermissionWasPreviouslyDenied: true,
            presentInRootViewController: true // iOS-only; If you're sure you're not presenting the (non embedded) scanner in a modal, or are experiencing issues with fi. the navigationbar, set this to 'true' and see if it works better for your app (default false).
        }).then(function (result) {
            _this.mensaje = result.text,
                _this.mensaje = _this.mensaje.replace(/&#34;/gi, "\""),
                _this.function1();
            //alert({
            //title: "Scan result",
            //message: "Format: " + result.format + ",\nValue: " + result.text,
            //okButtonText: "OK"
            //});
        }, function (errorMessage) {
            console.log("No scan. " + errorMessage);
        });
        //if(this.valid_QR==false)
        alert("QR no válido");
    };
    IdentificationComponent.prototype.function1 = function () {
        if (this.saveJSON())
            this.update(this.currentUser);
        else
            alert("QR no válido");
    };
    IdentificationComponent.prototype.mostrarmensaje = function () {
        alert("Función no habilitada actualmente");
    };
    IdentificationComponent.prototype.saveJSON = function () {
        // /mensaje/gi sirve para que reemplace todos los valores encontrados, porque sin esto solo reemplaza el primero
        try {
            var json = JSON.parse(this.mensaje);
            if (json.clase && json.asiento) {
                this.currentUser.class = json.clase;
                this.currentUser.seat = json.asiento;
                //localStorage.setItem("currentUser",JSON.stringify(this.currentUser));
                console.log("Mensaje de Prueba");
                return true;
            }
            else {
                this.valid_QR = false;
                alert("QR no válido");
                console.log("QR no válido 1");
                return false;
            }
        }
        catch (_a) {
            this.valid_QR = false;
            alert("QR no válido");
            console.log("QR no válido 1");
            return false;
        }
    };
    IdentificationComponent.prototype.mostrarValores = function () {
        console.log(JSON.stringify(this.currentUser));
        alert(JSON.stringify(this.currentUser));
        //alert("La clase leída del qr es: " + this.currentUser.class + "\n El asiento leído del qr es: " +  this.currentUser.seat
        //+ "\n El id de la sesión  es: " +  this.currentUser._id + this.currentUser.username );
    };
    IdentificationComponent.prototype.update = function (currentUser) {
        var _this = this;
        if (this.currentUser.seat != "" && this.currentUser.class != "") {
            this.userService.setUser(this.currentUser);
            //localStorage.setItem("currentUser",JSON.stringify(currentUser));
            console.log(this.currentUser.seat);
            console.log(JSON.stringify(this.currentUser));
            //this.userService.update(this.currentUser).subscribe(
            this.userService.connectStudentToSession(this.currentUser).subscribe(function (data) {
                alert("Registrado en la clase con éxito");
                _this.routerExtensions.navigate(["/student"]);
            }, function (error) {
                alert("Error de registro en clase");
            });
        }
        else {
            alert("Asiento y clase vacíos");
        }
    };
    IdentificationComponent.prototype.update2 = function () {
        var _this = this;
        this.currentUser.seat = "4";
        this.currentUser.class = "A-10";
        if (this.currentUser.seat != "" && this.currentUser.class != "") {
            //console.log(JSON.stringify(this.currentUser));
            localStorage.setItem("currentUser", JSON.stringify(this.currentUser));
            this.userService.connectStudentToSession(this.currentUser).subscribe(function (session) {
                console.log(JSON.stringify(session));
                localStorage.setItem('currentSession', JSON.stringify(session));
                //alert("Registrado en la clase con éxito");
                _this.routerExtensions.navigate(["/student"]);
            }, function (error) {
                alert("Error de registro en clase");
            });
        }
        else {
            alert("Por favor rellene los campos de clase y asiento");
        }
    };
    IdentificationComponent.prototype.mensajeReadNFC = function () {
        alert("Por favor acerque un chip NFC");
    };
    IdentificationComponent.prototype.ReadNFC = function () {
        var _this = this;
        this.nfc.setOnNdefDiscoveredListener(function (data) {
            // data.message is an array of records, so:
            if (data.message) {
                //for (let m in data.message) {
                var record = data.message[0];
                _this.mensaje = record.payloadAsString;
                _this.mensaje = _this.mensaje.replace(/&#34;/gi, "\""),
                    _this.saveJSON(),
                    _this.update(_this.currentUser);
                //alert(
                //"\n data.message: " + record.payloadAsString
                //)         
            }
        }, {
            // iOS-specific options
            stopAfterFirstRead: true,
            scanHint: "Scan a tag"
        }).then(function () {
            console.log("OnNdefDiscovered listener added");
        });
    };
    IdentificationComponent.prototype.logout = function () {
        var _this = this;
        this.currentUser = this.userService.currentUser();
        this.userService.logout(this.currentUser._id, this.currentUser).subscribe(function (data) {
            _this.routerExtensions.navigate(['/login'], { clearHistory: true });
        }, function (error) {
            alert("Error al cerrar sesión");
        });
    };
    IdentificationComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"] }
    ]; };
    IdentificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "identification",
            providers: [_shared_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]],
            template: __importDefault(__webpack_require__("./identification/identification.component.html")).default,
            styles: [__importDefault(__webpack_require__("./identification/identification.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_4__["Page"], _shared_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"]])
    ], IdentificationComponent);
    return IdentificationComponent;
}());



/***/ }),

/***/ "./login/login.component.css":
/***/ (function(module, exports) {

module.exports = ".page {\r\n  align-items: center;\r\n  flex-direction: column;\r\n}\r\n.form {\r\n  margin-left: 30;\r\n  margin-right: 30;\r\n  flex-grow: 2;\r\n  vertical-align: middle;\r\n}\r\n\r\n.logo {\r\n  margin-bottom: 32;\r\n  font-weight: bold;\r\n}\r\n\r\n.boton{\r\n  width: 200;\r\n  height: 50;\r\n  background-color: rgb(26, 91, 213);\r\n  color: whitesmoke;\r\n  border-radius: 5;\r\n  font-size: 20;\r\n  font-weight: 600;\r\n}\r\n.logo2{\r\n  margin-bottom: 12;\r\n  height: 100;\r\n  horizontal-align: center;\r\n  font-weight: bold;\r\n}\r\n.header {\r\n  horizontal-align: center;\r\n  font-size: 25;\r\n  font-weight: 600;\r\n  margin-bottom: 20;\r\n  text-align: center;\r\n  color: rgb(38, 8, 99);\r\n}\r\n\r\n.input-field {\r\n  margin-bottom: 25;\r\n}\r\n.input {\r\n  font-size: 18;\r\n  placeholder-color: #A8A8A8;\r\n}\r\n.input:disabled {\r\n  background-color: rgb(255, 255, 255);\r\n  opacity: 0.5;\r\n}\r\n\r\n.btn-primary {\r\n  margin: 15 5 15 5;\r\n}\r\n\r\n.btn-second {\r\n  margin: 15 5 15 5;\r\n}\r\n\r\n.login-label {\r\n  horizontal-align: center;\r\n  color: #A8A8A8;\r\n  font-size: 16;\r\n}\r\n.sign-up-label {\r\n  margin-bottom: 20;\r\n}\r\n.bold {\r\n  color: #000000; \r\n}\r\n\r\n"

/***/ }),

/***/ "./login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Login Page\"></ActionBar>\r\n\r\n<FlexboxLayout class=\"page\">\r\n    <StackLayout>\r\n            <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\r\n            <Image class=\"logo2\" src=\"~/images/erisid2.png\"></Image>\r\n\r\n    </StackLayout>\r\n\r\n    <StackLayout class=\"form\">\r\n        <Label class=\"header\" text=\"PROYECTO SIDISEC\"></Label>\r\n        <GridLayout columns=\"*,*\" rows=\"60,*,*,*\" width=\"280\" height=\"300\"\r\n            backgroundColor=\"lightgray\" borderColor=\"rgb(15, 5, 59)\" borderwidth=\"3\">\r\n            <Button text=\"Alumnos\" row=\"0\" col=\"0\" class=\"btn btn-second\" \r\n            (tap)=\"select()\"\r\n          ></Button>\r\n\r\n            <Label row=\"0\" col=\"0\" *ngIf=\"!selection\" backgroundColor=\"white\">\r\n            </Label>\r\n\r\n            <Button text=\"Profesores\" row=\"0\" col=\"1\" class=\"btn btn-second\" (tap)=\"select2()\"></Button>\r\n            <Label row=\"0\" col=\"1\" *ngIf=\"selection\" backgroundColor=\"white\">\r\n            </Label>\r\n\r\n            <TextField row=\" 1\" col=\"0\" colSpan=\"2\"  backgroundColor=\"white\"\r\n                width=\"260\" height=\"50\" class=\"input\" hint=\"Email\" \r\n                [isEnabled]=\"!processing\" keyboardType=\"email\"\r\n                autocorrect=\"false\" autocapitalizationType=\"none\"\r\n                [(ngModel)]=\"user.username\">\r\n            </TextField>\r\n\r\n                <TextField #password class=\"input\" row=\"2\" col=\"0\" colSpan=\"2\"\r\n                    backgroundColor=\"white\" width=\"260\" height=\"50\"\r\n                    [isEnabled]=\"!processing\" hint=\"Password\" secure=\"true\"\r\n                    [(ngModel)]=\"user.password\"\r\n                    [returnKeyType]=\"isLoggingIn ? 'done' : 'next'\"\r\n                    (returnPress)=\"focusConfirmPassword()\"></TextField>\r\n            \r\n                 <Button [text]=\"isLoggingIn ? 'Login' : 'Sign Up'\" row=\"3\"\r\n                    col=\"0\" *ngIf=\"selection\" [isEnabled]=\"!processing\"\r\n                    (tap)=\"submit()\" class=\"btn btn-primary m-t-20\"></Button>\r\n                <Button [text]=\"'Login Profesorr'\" row=\"3\"\r\n                    col=\"0\" colSpan=\"2\" *ngIf=\"!selection\" [isEnabled]=\"!processing\"\r\n                    class=\"boton\" (tap)=\"loginteacher()\"></Button>\r\n\r\n                <Button [text]=\"'Registro'\" row=\"3\" col=\"1\"\r\n                    class=\"btn btn-primary m-t-20\" *ngIf=\"selection\" (tap)=\"register2()\"></Button>\r\n\r\n\r\n        </GridLayout>\r\n        <Label *ngIf=\"isLoggingIn\" text=\"¿Olvidaste la contraseña?\"\r\n            class=\"login-label\" (tap)=\"forgotPassword()\"></Label>\r\n    </StackLayout>\r\n</FlexboxLayout>"

/***/ }),

/***/ "./login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./shared/user/authenticate.service.ts");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6__);







var LoginComponent = /** @class */ (function () {
    function LoginComponent(page, authenticateService, routerExtensions, userService) {
        this.page = page;
        this.authenticateService = authenticateService;
        this.routerExtensions = routerExtensions;
        this.userService = userService;
        this.isLoggingIn = true;
        this.selection = true;
        this.model = {};
        this.user = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_1__["User"]();
        this.page.actionBarHidden = true;
        this.currentUser = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_1__["User"]();
        //localStorage.setItem('currentUser', "a")
        //this.user.username="prueba21@gmail.com";
        //this.user.password="prueba21";
    }
    LoginComponent.prototype.select = function () {
        this.selection = true;
    };
    LoginComponent.prototype.select2 = function () {
        this.selection = false;
    };
    LoginComponent.prototype.submit = function () {
        if (this.isLoggingIn) {
            this.login();
        }
        else {
            this.signUp();
        }
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        //console.log(JSON.stringify(this.user));
        this.authenticateService.login(this.user)
            .subscribe(function (response) {
            //console.log("111");
            //this.authenticateService.setUser(response);
            //this.authenticateService.setUser(response);
            console.log("1" + response);
            console.log("1" + JSON.stringify(response));
            localStorage.setItem('currentUser', JSON.stringify(response));
            _this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
            console.log("Class: " + _this.currentUser.class);
            console.log("Code session: " + _this.currentUser.code_session);
            if (_this.currentUser.class != null && _this.currentUser.code_session != null) {
                _this.routerExtensions.navigate(["/student"]);
            }
            else {
                _this.routerExtensions.navigate(["/session_code"]);
                //console.log("222");
                //console.log(this.authenticateService.currentUser());
                console.log(response);
            }
        }, function (exception) {
            //console.log("Error de logueo");
            if (exception.error && exception.error.description) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception.error.description);
            }
            else {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception.error);
            }
        });
    };
    LoginComponent.prototype.register2 = function () {
        this.routerExtensions.navigate(["/register"]);
    };
    LoginComponent.prototype.loginteacher = function () {
        Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])("Bienvenidoo");
        this.routerExtensions.navigate(["/teacher"]);
    };
    LoginComponent.prototype.signUp = function () {
        var _this = this;
        this.authenticateService.register(this.user)
            .subscribe(function () {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])("Your account was successfully created.");
            _this.toggleDisplay();
        }, function (exception) {
            if (exception.error && exception.error.description) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception.error.description);
            }
            else {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception);
            }
        });
    };
    LoginComponent.prototype.forgotPassword = function () {
        var _this = this;
        if (this.user.username) {
            //alert("Se ha mandado un enlace a su correo para restaurar la contraseña");         
            {
                this.model.username = this.user.username;
                //this.model.password ="hola";
                //this.model.token ="hola";
                console.log(JSON.stringify(this.model));
                this.userService.reset_password_email(this.model)
                    .subscribe(function (data) {
                    Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])("Se ha mandado un enlace a su correo para restaurar la contraseña");
                    _this.routerExtensions.navigate(["/reset_password"]);
                    //console.log(data);
                }, function (exception) {
                    if (exception.error && exception.error.description) {
                        Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception.error.description);
                    }
                    else {
                        Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])(exception.error);
                    }
                });
            }
        }
        else {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_4__["alert"])("Por favor introduzca un correo para restaurar la contraseña");
        }
    };
    LoginComponent.prototype.toggleDisplay = function () {
        this.isLoggingIn = !this.isLoggingIn;
    };
    LoginComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5__["Page"] },
        { type: _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticateService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterExtensions"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"] }
    ]; };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "gr-login",
            providers: [_shared_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]],
            template: __importDefault(__webpack_require__("./login/login.component.html")).default,
            styles: [__importDefault(__webpack_require__("./login/login.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_5__["Page"], _shared_user_authenticate_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticateService"], nativescript_angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterExtensions"], _shared_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-angular/platform.js");
/* harmony import */ var nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app.module.ts");

        let applicationCheckPlatform = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
        if (applicationCheckPlatform.android && !global["__snapshot"]) {
            __webpack_require__("../node_modules/tns-core-modules/ui/frame/frame.js");
__webpack_require__("../node_modules/tns-core-modules/ui/frame/activity.js");
        }

        
            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-angular.js")();
            
            
        if (true) {
            const hmrUpdate = __webpack_require__("../node_modules/nativescript-dev-webpack/hmr/index.js").hmrUpdate;
            global.__initialHmrUpdate = true;
            global.__hmrSyncBackup = global.__onLiveSync;

            global.__onLiveSync = function () {
                hmrUpdate();
            };

            global.hmrRefresh = function({ type, path } = {}) {
                if (global.__initialHmrUpdate) {
                    return;
                }

                setTimeout(() => {
                    global.__hmrSyncBackup({ type, path });
                });
            };

            hmrUpdate().then(() => {
                global.__initialHmrUpdate = false;
            })
        }
        
            
        __webpack_require__("../node_modules/tns-core-modules/bundle-entry-points.js");
        


var options_Generated = {};

if (true) {
    options_Generated = {
        hmrOptions: {
            moduleTypeFactory: function () { return __webpack_require__("./app.module.ts").AppModule; },
            livesyncCallback: function (platformReboot) { setTimeout(platformReboot, 0); }
        }
    };
}

if (true) {
    module["hot"].accept(["./app.module.ts"], function(__WEBPACK_OUTDATED_DEPENDENCIES__) { /* harmony import */ _app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app.module.ts");
(function () {
        global["hmrRefresh"]({});
    })(__WEBPACK_OUTDATED_DEPENDENCIES__); });
}
Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__["platformNativeScriptDynamic"](Object.assign({}, options_Generated)).bootstrapModule(_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]);

    
        
        
    
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"android":{"v8Flags":"--expose_gc","forceLog":true,"markingMode":"none"},"name":"nativescript-template-ng-groceries","version":"3.0.0","main":"main.js"};

/***/ }),

/***/ "./register/register.component.css":
/***/ (function(module, exports) {

module.exports = "\n.header {\n\n  font-size: 25;\n  font-weight: 600;\n  margin-bottom: 20;\n  text-align: center;\n  color: rgb(38, 8, 99);\n}\n\n.home-panel{\n    vertical-align: center; \n    font-size: 20;\n    margin: 15;\n}\n\n.logo {\n    margin-bottom: 32;\n    font-weight: bold;\n  }\n  \n.description-label{\n    margin-bottom: 15;\n}\n\n.form {\n    margin: 20;\n}\n\n.label {\n    margin-right: 5;\n    vertical-align: center;\n}\n\n.input {\n    margin-bottom: 10;\n}\n"

/***/ }),

/***/ "./register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<FlexboxLayout>\n    <ScrollView>\n        <StackLayout>\n                <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\n                <Label text=\"Formulario de Registro\"\n                horizontalAlignment=\"center\" class=\"header\"></Label>\n            <GridLayout rows=\"*, *, *, *, *\" columns=\"83, *\" class=\"form\">\n\n                <Label row=\"0\" col=\"0\" text=\"Nombre:\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"0\" col=\"1\" [(ngModel)]=\"model.firstName\"\n                    hint=\"Escriba su nombre\" class=\"input input-border\" name=\"lastName\" required>\n                </TextField>\n\n                <Label row=\"1\" col=\"0\" text=\"Apellidos\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"1\" col=\"1\" [(ngModel)]=\"model.lastName\"\n                    hint=\"Escriba sus apellidos\" class=\"input input-border\"\n                    keyboardType=\"email\"></TextField>\n\n                <Label row=\"2\" col=\"0\" text=\"Email:\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"2\" col=\"1\" [(ngModel)]=\"model.username\"\n                    hint=\"Escriba su email\" class=\"input input-border\" \n                    keyboardType=\"email\" name=\"username\" required *ngIf=\"email.invalid\"></TextField>\n\n                <Label row=\"3\" col=\"0\" text=\"Matrícula:\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"3\" col=\"1\" [(ngModel)]=\"model.numat\"\n                    hint=\"Escriba su número de matrícula\"\n                    maxLength=\"5\"\n                    class=\"input input-border\" name=\"numat\" required>\n                </TextField>\n                <Label row=\"4\" col=\"0\" text=\"Contraseña:\"\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\n                <TextField row=\"4\" col=\"1\" [type]=\"hide ? 'password' : 'text'\" [(ngModel)]=\"model.password\"\n                    hint=\"Escriba su contraseña\" class=\"input input-border\" name=\"password\" required secure=\"true\">\n                </TextField>\n            </GridLayout>\n\n            <Button text=\"Seleccionar Imagen\"  backgroundcolor=\"green\" row=\"0\" col=\"1\" class=\"btn btn-primary\"\n            (tap)=\"image_picker()\"></Button>\n\n            <GridLayout rows=\"*\" columns=\"*,*\" width=\"310\" height=\"80\">\n                <Button text=\"Registro\" row=\"0\" col=\"0\"\n                    class=\"btn btn-primary\" (tap)=\"register2()\">\n                </Button>\n\n                <Button text=\"Volver\" row=\"0\" col=\"1\" class=\"btn btn-primary\"\n                    (tap)=\"back()\">\n                </Button>\n            </GridLayout>\n\n        </StackLayout>\n    </ScrollView>\n</FlexboxLayout>"

/***/ }),

/***/ "./register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("../node_modules/nativescript-imagepicker/imagepicker.js");
/* harmony import */ var nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_7__);








var fs = __webpack_require__("../node_modules/tns-core-modules/file-system/file-system.js");
//import { AlertService, } from "../shared/user/alert.service";
var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.model = {};
        this.loading = false;
        //Campo email
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]);
        //Campo Contraseña
        this.hide = true;
        this.imageAssets = [];
        //FOTO
        this.selectedFile = null;
        this.page.actionBarHidden = true;
        this.user = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_5__["User"]();
        this.user.username = "";
        this.user.password = "";
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    /*public image_picker3(){

            let that = this
            let context = imagepicker.create({ mode: "single" });
            context
                .authorize()
                .then(() => {
                    return context.present();
                })
                .then(selection => {
                    const imageAsset = selection.length > 0 ? selection[0] : null;
                    ImageSourceModule.fromAsset(imageAsset).then(
                        savedImage => {
                            let filename = "image" + "-" + new Date().getTime() + ".png";
                            let folder = fs.knownFolders.documents();
                            let path = fs.path.join(folder.path, filename);
                            savedImage.saveToFile(path, "png");
                            var loadedImage = ImageSourceModule.fromFile(path);
                            //loadedImage.filename = filename;
                            //loadedImage.note = "";
                            //that.arrayPictures.unshift(loadedImage);
                            //that.storeData();
                            console.log(loadedImage);

                        },
                        err => {
                            console.log("Failed to load from asset");
                            console.log(err)
                        }
                    );
                })
                .catch(err => {
                    console.log(err);
                });
    
    }*/
    RegisterComponent.prototype.image_picker = function () {
        var _this = this;
        var context = nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_7__["create"]({
            mode: "single" // use "multiple" for multiple selection
        });
        context.authorize().then(function () {
            return context.present();
        }).then(function (selection) {
            _this.imageSrc = JSON.stringify(selection[0]);
            var json = JSON.parse(_this.imageSrc);
            _this.imageSrc = JSON.stringify(json._android);
            _this.imageSrc = _this.imageSrc.replace(/"/gi, "");
            _this.model.src = _this.imageSrc.substr(_this.imageSrc.lastIndexOf("/") + 1);
            console.log("Ruta del archivo seleccionado: " + JSON.stringify(json._android));
        }).catch(function (e) {
            console.log(e);
        });
    };
    RegisterComponent.prototype.getErrorMessage = function () {
        return this.email.hasError('required') ? 'Debes rellenar el campo' :
            this.email.hasError('email') ? 'Email invalido' :
                '';
    };
    RegisterComponent.prototype.register2 = function () {
        var _this = this;
        if (this.model.firstName && this.model.lastName && this.model.username && this.model.numat && this.model.password && this.imageSrc) {
            //console.log("hola" + (JSON.stringify(this.model)));
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("hola");
            if (this.userService.createphoto2(this.imageSrc)) {
                console.log((JSON.stringify(this.model)));
                this.userService.create(this.model)
                    .subscribe(function (data) {
                    Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Registrado con éxito");
                    _this.routerExtensions.navigate(['/login']);
                }, function (error) {
                    Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(error);
                    _this.loading = false;
                });
            }
            else {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("No se ha podido subir su foto");
            }
        }
        else {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Complete todos los campos requeridos");
        }
        //var file =  "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200225-WA0002.jpg";
        //var url = "https://some.remote.service.com/path";
        //var name = file.substr(file.lastIndexOf("/") + 1);
        //console.log(name);
        //alert("Función no disponible actualmente")
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        //this.loading = true;
        //this.model.src = this.selectedFile.name;
        this.userService.create(this.model)
            .subscribe(function (data) {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Registrado con éxito ");
            _this.routerExtensions.navigate(['/login']);
            console.log(data);
        }, function (exception) {
            if (exception.error && exception.error.description) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(exception.error.description);
            }
            else {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(exception.error);
            }
        });
    };
    RegisterComponent.prototype.back = function () {
        this.routerExtensions.navigate(["/login"]);
    };
    /*upload(attachment) {
        console.log(attachment);
          this.userService.createphoto(attachment)
              .subscribe(
                  data => {
                      alert("Subido con éxito ");
                      this.routerExtensions.navigate(['/login']);
                  },
                  error => {
                      alert(error);
                  });
      }*/
    RegisterComponent.prototype.onFileSelected = function (event) {
        this.selectedFile = event.target.files[0];
        console.log(this.selectedFile);
    };
    RegisterComponent.prototype.onUpload = function () {
        var _this = this;
        try {
            console.log("Prueba");
            var file = "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200301-WA0001.jpg";
            var name = file.substr(file.lastIndexOf("/") + 1);
            console.log(name);
            var fd = new FormData();
            //fd.append('image', this.selectedFile, this.selectedFile.name);
            fd.append('imagen', file, name);
            console.log('******************************');
            console.log(fd);
            this.userService.createphoto(fd)
                .subscribe(function (data) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Subida foto con éxito ");
                _this.routerExtensions.navigate(['/login']);
            }, function (error) {
                //this.alertService.error(error);
                console.log(error);
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Error de subida en la imagen");
            });
        }
        catch (e) {
            console.log(e);
        }
    };
    RegisterComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"] }
    ]; };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Register",
            template: __importDefault(__webpack_require__("./register/register.component.html")).default,
            styles: [__importDefault(__webpack_require__("./register/register.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"],
            _shared_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"],
            nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./register2/register.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n\r\n.header {\r\n    horizontal-align: center;\r\n    font-size: 25;\r\n    font-weight: 600;\r\n    margin-bottom: 20;\r\n    text-align: center;\r\n    color: rgb(38, 8, 99);\r\n  }\r\n  \r\n  .home-panel{\r\n      vertical-align: center; \r\n      font-size: 20;\r\n      margin: 15;\r\n  }\r\n  \r\n  .logo {\r\n      margin-bottom: 32;\r\n      font-weight: bold;\r\n    }\r\n    \r\n  .description-label{\r\n      margin-bottom: 15;\r\n  }\r\n  \r\n  .form {\r\n      margin: 20;\r\n  }"

/***/ }),

/***/ "./register2/register.component.html":
/***/ (function(module, exports) {

module.exports = "<Label text=\"Hello\"\r\n                horizontalAlignment=\"center\" class=\"header\"></Label>"

/***/ }),

/***/ "./register2/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Register2Component", function() { return Register2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/user/user.service.ts");




//import { MatSnackBar, MatBottomSheet, MatBottomSheetRef } from '@angular/material';


//import { AlertService, } from "../shared/user/alert.service";
var Register2Component = /** @class */ (function () {
    function Register2Component(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.model = {};
        this.loading = false;
        //Campo email
        //email = new FormControl('', [Validators.required, Validators.email]);
        //Campo Contraseña
        this.hide = true;
        //FOTO
        this.selectedFile = null;
        this.page.actionBarHidden = true;
        this.user = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__["User"]();
        this.user.username = "";
        this.user.password = "";
    }
    Register2Component.prototype.ngOnInit = function () {
    };
    Register2Component.prototype.register2 = function () {
        Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Función no disponible actualmente");
    };
    Register2Component.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"] }
    ]; };
    Register2Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Register2",
            template: __importDefault(__webpack_require__("./register2/register.component.html")).default,
            styles: [__importDefault(__webpack_require__("./register2/register.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"],
            _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"]])
    ], Register2Component);
    return Register2Component;
}());



/***/ }),

/***/ "./reset_password/reset_password.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n.header {\r\n\r\n    font-size: 25;\r\n    font-weight: 600;\r\n    margin-bottom: 20;\r\n    text-align: center;\r\n    color: rgb(38, 8, 99);\r\n  }\r\n  \r\n  .home-panel{\r\n      vertical-align: center; \r\n      font-size: 20;\r\n      margin: 15;\r\n  }\r\n  \r\n  .logo {\r\n      margin-bottom: 32;\r\n      font-weight: bold;\r\n    }\r\n    \r\n  .description-label{\r\n      margin-bottom: 15;\r\n  }\r\n  \r\n  .form {\r\n      margin: 20;\r\n  }\r\n  \r\n  .label {\r\n      margin-right: 5;\r\n      vertical-align: center;\r\n  }\r\n  \r\n  .input {\r\n      margin-bottom: 10;\r\n  }\r\n  "

/***/ }),

/***/ "./reset_password/reset_password.component.html":
/***/ (function(module, exports) {

module.exports = "<FlexboxLayout>\r\n    <ScrollView>\r\n        <StackLayout>\r\n                <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\r\n                <Label text=\"Reseteo de contraseña\"\r\n                horizontalAlignment=\"center\" class=\"header\"></Label>\r\n            <GridLayout rows=\"*, *, *, * \" columns=\"90, *\" class=\"form\">\r\n\r\n                <Label row=\"0\" col=\"0\" text=\"Email:\"\r\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\r\n                <TextField row=\"0\" col=\"1\" [(ngModel)]=\"model.username\"\r\n                    hint=\"Escriba su nombre\" class=\"input input-border\" name=\"lastName\" required>\r\n                </TextField>\r\n\r\n                <Label row=\"1\" col=\"0\" text=\"Contraseña:\"\r\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\r\n                <TextField row=\"1\" col=\"1\" [type]=\"hide ? 'password' : 'text'\" [(ngModel)]=\"model2.password\"\r\n                    hint=\"Escriba su contraseña\" class=\"input input-border\" name=\"password\" required secure=\"true\">\r\n                </TextField>\r\n\r\n                <Label row=\"2\" col=\"0\" text=\"Contraseña:\"\r\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\r\n                <TextField row=\"2\" col=\"1\" [type]=\"hide ? 'password' : 'text'\" [(ngModel)]=\"model2.password1\"\r\n                    hint=\"Reescriba su contraseña\" class=\"input input-border\" name=\"password\" required secure=\"true\">\r\n                </TextField>\r\n\r\n                <Label row=\"3\" col=\"0\" text=\"Código:\"\r\n                    horizontalAlignment=\"right\" class=\"label\"></Label>\r\n                <TextField row=\"3\" col=\"1\" [(ngModel)]=\"model.token\"\r\n                    hint=\"Escriba su código de reseteo\"\r\n                    class=\"input input-border\" name=\"numat\" required>\r\n                </TextField>\r\n\r\n            </GridLayout>\r\n\r\n\r\n            <GridLayout rows=\"*\" columns=\"*,*\" width=\"310\" height=\"80\">\r\n                <Button text=\"Resetear\" row=\"0\" col=\"0\"\r\n                    class=\"btn btn-primary\" (tap)=\"resetear()\">\r\n                </Button>\r\n\r\n                <Button text=\"Volver\" row=\"0\" col=\"1\" class=\"btn btn-primary\"\r\n                    (tap)=\"back()\">\r\n                </Button>\r\n            </GridLayout>\r\n\r\n        </StackLayout>\r\n    </ScrollView>\r\n</FlexboxLayout>"

/***/ }),

/***/ "./reset_password/reset_password.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Reset_PasswordComponent", function() { return Reset_PasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/user/user.service.ts");
/* harmony import */ var nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-imagepicker/imagepicker.js");
/* harmony import */ var nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_6__);





//import { User2 } from "./shared/user/user.model copy";


var fs = __webpack_require__("../node_modules/tns-core-modules/file-system/file-system.js");
//import { AlertService, } from "../shared/user/alert.service";
var Reset_PasswordComponent = /** @class */ (function () {
    function Reset_PasswordComponent(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.model = {};
        this.model2 = {};
        this.loading = false;
        //Campo email
        //email = new FormControl('', [Validators.required, Validators.email]);
        //Campo Contraseña
        this.hide = true;
        this.imageAssets = [];
        //FOTO
        this.selectedFile = null;
        this.page.actionBarHidden = true;
        this.user = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__["User"]();
        this.user.username = "";
        this.user.password = "";
    }
    Reset_PasswordComponent.prototype.ngOnInit = function () {
    };
    /*public image_picker3(){

            let that = this
            let context = imagepicker.create({ mode: "single" });
            context
                .authorize()
                .then(() => {
                    return context.present();
                })
                .then(selection => {
                    const imageAsset = selection.length > 0 ? selection[0] : null;
                    ImageSourceModule.fromAsset(imageAsset).then(
                        savedImage => {
                            let filename = "image" + "-" + new Date().getTime() + ".png";
                            let folder = fs.knownFolders.documents();
                            let path = fs.path.join(folder.path, filename);
                            savedImage.saveToFile(path, "png");
                            var loadedImage = ImageSourceModule.fromFile(path);
                            //loadedImage.filename = filename;
                            //loadedImage.note = "";
                            //that.arrayPictures.unshift(loadedImage);
                            //that.storeData();
                            console.log(loadedImage);

                        },
                        err => {
                            console.log("Failed to load from asset");
                            console.log(err)
                        }
                    );
                })
                .catch(err => {
                    console.log(err);
                });
    
    }*/
    Reset_PasswordComponent.prototype.image_picker = function () {
        var _this = this;
        var context = nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_6__["create"]({
            mode: "single" // use "multiple" for multiple selection
        });
        context.authorize().then(function () {
            return context.present();
        }).then(function (selection) {
            _this.imageSrc = JSON.stringify(selection[0]);
            var json = JSON.parse(_this.imageSrc);
            _this.imageSrc = JSON.stringify(json._android);
            _this.imageSrc = _this.imageSrc.replace(/"/gi, "");
            _this.model.src = _this.imageSrc.substr(_this.imageSrc.lastIndexOf("/") + 1);
            console.log("Ruta del archivo seleccionado: " + JSON.stringify(json._android));
        }).catch(function (e) {
            console.log(e);
        });
    };
    Reset_PasswordComponent.prototype.register2 = function () {
        var _this = this;
        console.log("hola55");
        if (this.model.firstName && this.model.lastName && this.model.username && this.model.numat && this.model.password && this.imageSrc) {
            console.log("hola234");
            console.log("hola" + (JSON.stringify(this.model)));
            if (this.userService.createphoto2(this.imageSrc)) {
                console.log((JSON.stringify(this.model)));
                this.userService.create(this.model)
                    .subscribe(function (data) {
                    Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Registrado con éxito");
                    _this.routerExtensions.navigate(['/login']);
                }, function (error) {
                    Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(error);
                    _this.loading = false;
                });
            }
            else {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("No se ha podido subir su foto");
            }
        }
        else {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Complete todos los campos requeridos");
        }
        //var file =  "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200225-WA0002.jpg";
        //var url = "https://some.remote.service.com/path";
        //var name = file.substr(file.lastIndexOf("/") + 1);
        //console.log(name);
        //alert("Función no disponible actualmente")
    };
    Reset_PasswordComponent.prototype.register = function () {
        var _this = this;
        //this.loading = true;
        //this.model.src = this.selectedFile.name;
        this.userService.create(this.model)
            .subscribe(function (data) {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Registrado con éxito");
            _this.routerExtensions.navigate(['/login']);
            console.log(data);
        }, function (exception) {
            if (exception.error && exception.error.description) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(exception.error.description);
            }
            else {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(exception.error);
            }
        });
    };
    Reset_PasswordComponent.prototype.comprobar_contraseña = function () {
        if (this.model2.password && this.model2.password1 && this.password == this.password1)
            return true;
        else
            return false;
    };
    Reset_PasswordComponent.prototype.resetear = function () {
        var _this = this;
        if (this.model2.password && this.model2.password1 && this.model.token && this.model.username) {
            this.model.password = this.model2.password;
            console.log(JSON.stringify(this.model));
            this.userService.resetPassword(this.model)
                .subscribe(function (data) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Su contraseña se ha reseteado");
                _this.routerExtensions.navigate(['/login']);
                //console.log(data);
            }, function (exception) {
                if (exception.error && exception.error.description) {
                    Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(exception.error.description);
                }
                else {
                    Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])(exception.error);
                }
            });
        }
        else {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Rellene todos los campos");
        }
    };
    Reset_PasswordComponent.prototype.back = function () {
        this.routerExtensions.navigate(["/login"]);
    };
    /*upload(attachment) {
        console.log(attachment);
          this.userService.createphoto(attachment)
              .subscribe(
                  data => {
                      alert("Subido con éxito ");
                      this.routerExtensions.navigate(['/login']);
                  },
                  error => {
                      alert(error);
                  });
      }*/
    Reset_PasswordComponent.prototype.onFileSelected = function (event) {
        this.selectedFile = event.target.files[0];
        console.log(this.selectedFile);
    };
    Reset_PasswordComponent.prototype.onUpload = function () {
        var _this = this;
        try {
            console.log("Prueba");
            var file = "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200301-WA0001.jpg";
            var name = file.substr(file.lastIndexOf("/") + 1);
            console.log(name);
            var fd = new FormData();
            //fd.append('image', this.selectedFile, this.selectedFile.name);
            fd.append('imagen', file, name);
            console.log('******************************');
            console.log(fd);
            this.userService.createphoto(fd)
                .subscribe(function (data) {
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Subida foto con éxito ");
                _this.routerExtensions.navigate(['/login']);
            }, function (error) {
                //this.alertService.error(error);
                console.log(error);
                Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Error de subida en la imagen");
            });
        }
        catch (e) {
            console.log(e);
        }
    };
    Reset_PasswordComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"] }
    ]; };
    Reset_PasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "reset_password",
            template: __importDefault(__webpack_require__("./reset_password/reset_password.component.html")).default,
            styles: [__importDefault(__webpack_require__("./reset_password/reset_password.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"],
            _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"],
            nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"]])
    ], Reset_PasswordComponent);
    return Reset_PasswordComponent;
}());



/***/ }),

/***/ "./session_code/session_code.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n    vertical-align: center; \r\n    font-size: 20;\r\n    margin: 15;\r\n}\r\n\r\n.description-label{\r\n    margin-bottom: 15;\r\n}\r\n\r\n.prueba {\r\n  font-size: 25;\r\n  font-weight: 600;\r\n  margin-bottom: 20;\r\n  text-align: center;\r\n  color:white;\r\n}\r\n\r\n.recuadro {\r\n  font-size: 35;\r\n  font-weight: 800;\r\n  margin-bottom: 20;\r\n  text-align: center;\r\n}"

/***/ }),

/***/ "./session_code/session_code.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Home\">\r\n</ActionBar>\r\n\r\n<GridLayout backgroundColor=\"#0374dd\">\r\n    <ScrollView>\r\n        <StackLayout class=\"home-panel\">\r\n            <!--Add your page content here-->\r\n            <Label textWrap=\"true\" text=\"Escriba su código de sesión\"\r\n                textAlignment=\"center\" class=\"prueba\"\r\n                textFieldBackgroundColor=\"white\"></Label>\r\n            <TextField height=\"30\" maxLength=\"4\" row=\"3\" col=\"1\"\r\n                [(ngModel)]=\"model.code_session\" class=\"recuadro\"\r\n                backgroundColor=\"white\" height=\"80\" width=\"200\">\r\n            </TextField>\r\n            <Button width=\"90%\" text=\"Continuar\" (tap)=\"insert_code_session()\"></Button>\r\n        </StackLayout>\r\n    </ScrollView>\r\n</GridLayout>"

/***/ }),

/***/ "./session_code/session_code.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Session_codeComponent", function() { return Session_codeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/user/user.service.ts");




//import { MatSnackBar, MatBottomSheet, MatBottomSheetRef } from '@angular/material';


//import { AlertService, } from "../shared/user/alert.service";
var Session_codeComponent = /** @class */ (function () {
    function Session_codeComponent(page, userService, routerExtensions) {
        this.page = page;
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.model = {};
        this.loading = false;
        this.currentUser = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_4__["User"]();
        this.page.actionBarHidden = true;
        //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = this.userService.currentUser();
    }
    Session_codeComponent.prototype.ngOnInit = function () {
    };
    Session_codeComponent.prototype.insert_code_session = function () {
        if (this.model.code_session == "")
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Introduzca un código de sesión válido");
        else {
            this.currentUser.code_session = parseInt(this.model.code_session);
            localStorage.setItem("currentUser", JSON.stringify(this.currentUser));
            //console.log(this.model.code_session);
            //console.log(this.currentUser.session_code);
            //alert(this.currentUser.session_code);
            //this.userService.setUser(this.currentUser);
            this.update(this.currentUser);
            //alert(this.currentUser.session_code);
        }
    };
    Session_codeComponent.prototype.update = function (currentUser) {
        var _this = this;
        //this.userService.setUser(this.currentUser);
        localStorage.setItem("currentUser", JSON.stringify(currentUser));
        //console.log(this.currentUser.seat);
        //console.log(JSON.stringify(this.currentUser));
        this.userService.update(this.currentUser).subscribe(function (data) {
            _this.routerExtensions.navigate(["/identification"]);
        }, function (error) {
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_1__["alert"])("Error al actualizar el código de sessión");
        });
    };
    Session_codeComponent.ctorParameters = function () { return [
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] },
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"] }
    ]; };
    Session_codeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Session_code",
            template: __importDefault(__webpack_require__("./session_code/session_code.component.html")).default,
            styles: [__importDefault(__webpack_require__("./session_code/session_code.component.css")).default]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"], _shared_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"], nativescript_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterExtensions"]])
    ], Session_codeComponent);
    return Session_codeComponent;
}());



/***/ }),

/***/ "./shared/config.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Config", function() { return Config; });
var Config = /** @class */ (function () {
    function Config() {
    }
    // casa //static apiUrl = "http://192.168.1.36:4000";
    // emulador //static apiUrl = "http://10.0.3.2:4000";
    // ae avana wifi //static apiUrl = "http://192.168.35.2:4000";
    // movil // static apiUrl = "http://192.168.43.226:4000";
    // eduroam // static apiUrl = "http://10.150.232.231:4000";
    // avana // static apiUrl = "http://10.99.153.128:4000";
    Config.apiUrl = "http://192.168.1.36:4000";
    Config.appKey = "kid_HyHoT_REf";
    Config.authHeader = "Basic a2lkX0h5SG9UX1JFZjo1MTkxMDJlZWFhMzQ0MzMyODFjN2MyODM3MGQ5OTIzMQ";
    Config.token = "";
    return Config;
}());



/***/ }),

/***/ "./shared/user/authenticate.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticateService", function() { return AuthenticateService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/nativescript-localstorage/localstorage.js");
/* harmony import */ var nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/config.ts");






var AuthenticateService = /** @class */ (function () {
    function AuthenticateService(http) {
        this.http = http;
    }
    AuthenticateService.prototype.register = function (user) {
        if (!user.username || !user.password) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])("Please provide both an email address and password.");
        }
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + "user/" + _config__WEBPACK_IMPORTED_MODULE_5__["Config"].appKey, JSON.stringify({
            username: user.username,
            email: user.username,
            password: user.password
        }), { headers: this.getCommonHeaders() }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    AuthenticateService.prototype.login = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/authenticate', ({
            username: user.username,
            password: user.password
        })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    AuthenticateService.prototype.login5 = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/authenticate', { username: user.username, password: user.password })
            .subscribe(function (user32) {
            // login successful if there's a jwt token in the response
            if (user32) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["setItem"]('currentUser', JSON.stringify(user));
            }
            return user32;
        });
    };
    AuthenticateService.prototype.create = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/register', user);
    };
    AuthenticateService.prototype.createphoto = function (file) {
        console.log("**!!FILE!!**");
        console.log(file);
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/upload', file);
    };
    AuthenticateService.prototype.getCommonHeaders = function () {
        return {
            "Content-Type": "application/json",
            "Authorization": _config__WEBPACK_IMPORTED_MODULE_5__["Config"].authHeader
        };
    };
    //logout(_id: string, user: User) {
    // return this.http.put(Config.apiUrl + '/users/logout/' + _id, user);
    //}
    AuthenticateService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error));
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
    };
    //Hemos añadido el {responseType: 'text'} porque daba un error de aprsing cuando leía el valor retornado
    // El método en realidad es  return this.http.put(Config.apiUrl + '/users/' + user._id, user)
    AuthenticateService.prototype.update = function (user) {
        return this.http.put(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/' + user._id, user, { responseType: 'text' });
    };
    AuthenticateService.prototype.logout = function (_id, user) {
        return this.http.put(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/logout/' + _id, user, { responseType: 'text' });
    };
    AuthenticateService.prototype.setUser = function (data) {
        nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["setItem"]("currentUser", JSON.stringify(data));
    };
    AuthenticateService.prototype.currentUser = function () {
        return JSON.parse(nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["getItem"]("currentUser")); //|| null;
    };
    AuthenticateService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    AuthenticateService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthenticateService);
    return AuthenticateService;
}());



/***/ }),

/***/ "./shared/user/user.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./shared/user/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/nativescript-localstorage/localstorage.js");
/* harmony import */ var nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./shared/config.ts");






var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.register = function (user) {
        if (!user.username || !user.password) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])("Please provide both an email address and password.");
        }
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + "user/" + _config__WEBPACK_IMPORTED_MODULE_5__["Config"].appKey, JSON.stringify({
            username: user.username,
            email: user.username,
            password: user.password
        }), { headers: this.getCommonHeaders() }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    UserService.prototype.login = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/authenticate', ({
            username: user.username,
            password: user.password
        })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    UserService.prototype.login5 = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/authenticate', { username: user.username, password: user.password })
            .subscribe(function (user32) {
            // login successful if there's a jwt token in the response
            if (user32) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["setItem"]('currentUser', JSON.stringify(user));
            }
            return user32;
        });
    };
    UserService.prototype.resetPassword = function (user) {
        console.log(JSON.stringify(user));
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/resetpassword', user
        /*({
            username: user.username,
            password: user.password,
            token: user.token
        })*/ , { responseType: 'text' }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    UserService.prototype.create = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/register', user, { responseType: 'text' }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
        ;
    };
    UserService.prototype.reset_password_email = function (user) {
        if (user === void 0) { user = {}; }
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/mailresetpassword2', user, { responseType: 'text' }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
        ;
    };
    UserService.prototype.createphoto = function (file) {
        console.log("**!!FILE!!**");
        console.log(file);
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/upload', file);
    };
    UserService.prototype.createphoto2 = function (file) {
        var name = file.substr(file.lastIndexOf("/") + 1);
        var bghttp = __webpack_require__("../node_modules/nativescript-background-http/background-http.js");
        var session = bghttp.session("image-upload");
        var request = {
            url: _config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/upload2',
            method: "POST",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": name
            },
            description: "Uploading " + name,
            //androidDisplayNotificationProgress: false,
            androidAutoClearNotification: true,
        };
        console.log("4");
        var task = session.uploadFile(file, request);
        console.log("Imagen subida correctamente");
        return true;
    };
    UserService.prototype.getCommonHeaders2 = function () {
        return {
            "Content-Type": "application/octet-stream",
            "File-Name": name
        };
    };
    UserService.prototype.getCommonHeaders = function () {
        return {
            "Content-Type": "application/json",
            "Authorization": _config__WEBPACK_IMPORTED_MODULE_5__["Config"].authHeader
        };
    };
    //logout(_id: string, user: User) {
    // return this.http.put(Config.apiUrl + '/users/logout/' + _id, user);
    //}
    UserService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error));
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
    };
    //Hemos añadido el {responseType: 'text'} porque daba un error de aprsing cuando leía el valor retornado
    // El método en realidad es  return this.http.put(Config.apiUrl + '/users/' + user._id, user)
    UserService.prototype.update = function (user) {
        return this.http.put(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/' + user._id, user, { responseType: 'text' });
    };
    /*updateID(user: User) {
        return this.http.put(Config.apiUrl + '/users/' + user._id, user,{responseType: 'text'});
    }*/
    UserService.prototype.getTestByClass = function (_idsession) {
        return this.http.get(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/getsessionteacher/' + _idsession);
        //return this.http.get(appConfig.apiUrl + '/users/getsession/' + _id);
    };
    /*connectUsertoSession(user: User) {
        return this.http.put(Config.apiUrl + '/users/idsession/' + user._id, user,{responseType: 'text'}).pipe(
            map(response => response),
            catchError(this.handleErrors)
        );;
    }*/
    UserService.prototype.getAllClassrooms = function () {
        return this.http.get(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/getaulas');
    };
    UserService.prototype.connectStudentToSession = function (user) {
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/connectstudenttosession', user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleErrors));
    };
    //Teacher
    UserService.prototype.getTeacher = function (_teacher) {
        return this.http.get(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/getteacher/' + _teacher);
    };
    //Cursos
    UserService.prototype.getCurso = function (_curso) {
        return this.http.get(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/getcurso/' + _curso);
    };
    UserService.prototype.logout = function (_id, user) {
        return this.http.put(_config__WEBPACK_IMPORTED_MODULE_5__["Config"].apiUrl + '/users/logout/' + _id, user, { responseType: 'text' });
    };
    UserService.prototype.setUser = function (data) {
        nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["setItem"]("currentUser", JSON.stringify(data));
    };
    UserService.prototype.currentUser = function () {
        return JSON.parse(nativescript_localstorage__WEBPACK_IMPORTED_MODULE_4__["getItem"]("currentUser")); //|| null;
    };
    UserService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./student/student.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n\tvertical-align: center;\r\n\tfont-size: 20;\r\n\tmargin: 15;\r\n}\r\n\r\n.card {\r\n\tborder-radius: 20;\r\n\tmargin: 12;\r\n    padding: 10;\r\n\tbackground-color: rgb(179, 127, 24);\r\n    color: #362713;\r\n    height: 56%;\r\n}\r\n\r\n.button-no-clicked\r\n{\r\n\tbackground-color: rgb(168, 194, 206);\r\n}\r\n\r\n.button-clicked\r\n{\r\n\tbackground-color: rgb(81, 156, 190);\r\n}\r\n"

/***/ }),

/***/ "./student/student.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Student Main Page\" class=\"action-bar\">\r\n</ActionBar>\r\n\r\n<StackLayout>\r\n    <Image class=\"logo\" src=\"~/images/Logo_politecnica.png\"></Image>\r\n\r\n    <StackLayout>\r\n        <Tabs >\r\n            <TabStrip>\r\n                <TabStripItem>\r\n                    <Label text=\"Clase\"></Label>\r\n                    <Image src=\"res://home\"></Image>\r\n                </TabStripItem>\r\n                <TabStripItem>\r\n                    <Label text=\"Tests\"></Label>\r\n                    <Image class=\"logo\" src=\"~/images/test.png\"></Image>\r\n                </TabStripItem>\r\n                <TabStripItem>\r\n                    <Label text=\"Ajustes\"></Label>\r\n                    <Image src=\"res://settings\"></Image>\r\n                </TabStripItem>\r\n            </TabStrip>\r\n            <TabContentItem>\r\n                <GridLayout>\r\n                    <StackLayout width=\"80%\">\r\n                        <StackLayout class=\"h2\">\r\n                            <Label text=\"\" class=\"h3 text-center\"></Label>\r\n                            <Label text=\"Datos de la sesión\"  fontWeight=\"bold\" class=\"h1 text-center\"></Label>\r\n\r\n                            <Label text=\"\" class=\"h3 text-center\"></Label>\r\n\r\n                            <Label>\r\n                                <FormattedString>\r\n                                    <Span text=\"Alumno: \"  fontWeight=\"bold\"></Span>\r\n                                    <Span text=\"{{currentUser.firstName}} {{currentUser.lastName}}\"></Span>\r\n                                </FormattedString>\r\n                            </Label>\r\n\r\n                            <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                            <Label>\r\n                                <FormattedString>\r\n                                    <Span text=\"Profesor: \"  fontWeight=\"bold\"></Span>\r\n                                    <Span text=\"{{currentTeacher.firstName}} {{currentTeacher.lastName}}\"></Span>\r\n                                </FormattedString>\r\n                            </Label>\r\n                            <Label>\r\n                                <FormattedString>\r\n                                    <Span text=\"Email: \"  fontWeight=\"bold\" class=\"h3\"></Span>\r\n                                    <Span text=\"{{currentTeacher.username}}\" class=\"h3\"></Span>\r\n                                </FormattedString>\r\n                            </Label>\r\n                            <!--\r\n                            <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                            <Label text=\"Email profesor: {{currentTeacher.username}}\"></Label>\r\n                            -->\r\n                            <Label text=\"\" class=\"h3 text-center\"></Label>\r\n                            <Label>\r\n                                <FormattedString>\r\n                                        <Span text=\"Aula: \"  fontWeight=\"bold\"></Span>\r\n                                        <Span text=\"{{currentUser.class}}\"></Span>\r\n                                </FormattedString>\r\n                            </Label>\r\n                                \r\n\r\n                            <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                            \r\n                            <Label>\r\n                                <FormattedString>\r\n                                    <Span text=\"Asiento ocupado: \"  fontWeight=\"bold\"></Span>\r\n                                    <Span text=\"{{currentUser.seat}}\"></Span>\r\n                                </FormattedString>\r\n                            </Label>\r\n\r\n                            <Label text=\"\" class=\"h2 text-center\"></Label>\r\n\r\n                            <Label>\r\n                                <FormattedString>\r\n                                    <Span text=\"Asignatura: \"  fontWeight=\"bold\"></Span>\r\n                                    <Span text=\"{{asignatura}}\"></Span>\r\n                                </FormattedString>\r\n                            </Label>\r\n\r\n                            <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                            <Label>\r\n                                <FormattedString>\r\n                                    <Span text=\"Grupo: \"  fontWeight=\"bold\"></Span>\r\n                                    <Span text=\"{{grupo}}\"></Span>\r\n                                </FormattedString>\r\n                            </Label>\r\n                        </StackLayout>\r\n                    </StackLayout>\r\n                </GridLayout>\r\n            </TabContentItem>\r\n            <TabContentItem>\r\n                <GridLayout>\r\n                    <StackLayout>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Tests Disponibles\"  fontWeight=\"bold\" class=\"h1 text-center\"></Label>\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n\r\n                    <!--\r\n                        <StackLayout>\r\n                            <GridLayout *ngFor=\"let item of questions; let i = index\"\r\n                            rows=\"*,*,*,*\" columns=\"*, auto\" height=\"140\" (tap)=\"navigateToQuiz(i)\" >\r\n                            <Button row=\"0\" col=\"0\" text=\"Test---Pregunta: {{item.question}}\"\r\n                            backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\"></Button>\r\n                            <Button row=\"1\" col=\"0\" text=\"A: {{item.answer1}}\"\r\n                            backgroundColor=\"red\"\r\n                            color=\"whitesmoke\" (tap)=\"navigateToQuiz(i)\"></Button>\r\n                            <Button row=\"2\" col=\"0\" text=\"B: {{item.answer2}}\"\r\n                            backgroundColor=\"red\"\r\n                            color=\"whitesmoke\" (tap)=\"navigateToQuiz(i)\"></Button>\r\n                            <Button row=\"3\" col=\"0\" text=\"C: {{item.answer3}}\"\r\n                            backgroundColor=\"red\"\r\n                            color=\"whitesmoke\" (tap)=\"navigateToQuiz(i)\"></Button>\r\n                        </GridLayout>\r\n                        </StackLayout>\r\n                    -->\r\n                    <StackLayout *ngFor=\"let item of questions; let i = index\">\r\n\r\n                        <Button row=\"0\" col=\"0\" text=\"{{item.question}}\"\r\n                        backgroundColor=\"#35A6E8\"\r\n                        color=\"whitesmoke\" (tap)=\"toogleSelection2(i)\" width= \"80%\"></Button>\r\n\r\n                    \r\n                        <StackLayout *ngIf=\"item.open\" width=\"60%\">\r\n                            <Button  text=\"a) {{item.answer1}}\" [ngClass]=\"{'button-clicked': item.openA, 'button-no-clicked': !item.openA}\"\r\n                            color=\"whitesmoke\" (tap)=\"switchColor3(i,0)\"></Button>\r\n                            <Button  text=\"b) {{item.answer2}}\" [ngClass]=\"{'button-clicked': item.openB, 'button-no-clicked': !item.openB}\"\r\n                            color=\"whitesmoke\" (tap)=\"switchColor3(i,1)\"></Button>\r\n                            <Button  text=\"C) {{item.answer3}}\" [ngClass]=\"{'button-clicked': item.openC, 'button-no-clicked': !item.openC}\"\r\n                            color=\"whitesmoke\" (tap)=\"switchColor3(i,2)\"></Button>\r\n                            <Button  text=\"Enviar\" backgroundColor=\"green\" (tap)=\"sendResponse2(i)\"\r\n                            color=\"whitesmoke\"></Button>\r\n                        </StackLayout> \r\n\r\n                        <Label text=\"\" class=\"h3 text-center\"></Label>\r\n\r\n\r\n                    </StackLayout>\r\n                        <StackLayout *ngFor=\"let item of information; let i = index\">\r\n\r\n                            <Button row=\"0\" col=\"0\" text=\"{{item.question}}\"\r\n                            backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\" (tap)=\"toogleSelection(i)\" width= \"80%\"></Button>\r\n\r\n                        \r\n                            <StackLayout *ngIf=\"item.open\" width=\"60%\">\r\n                                <Button  text=\"a) {{item.answers.a}}\" [ngClass]=\"{'button-clicked': item.answers.openA, 'button-no-clicked': !item.answers.openA}\"\r\n                                color=\"whitesmoke\" (tap)=\"switchColor(i,0)\"></Button>\r\n                                <Button  text=\"b) {{item.answers.b}}\" [ngClass]=\"{'button-clicked': item.answers.openB, 'button-no-clicked': !item.answers.openB}\"\r\n                                color=\"whitesmoke\" (tap)=\"switchColor(i,1)\"></Button>\r\n                                <Button  text=\"C) {{item.answers.c}}\" [ngClass]=\"{'button-clicked': item.answers.openC, 'button-no-clicked': !item.answers.openC}\"\r\n                                color=\"whitesmoke\" (tap)=\"switchColor(i,2)\"></Button>\r\n                                <Button  text=\"Enviar\" backgroundColor=\"green\" (tap)=\"sendResponse(i)\"\r\n                                color=\"whitesmoke\"></Button>\r\n                            </StackLayout> \r\n\r\n                            <Label text=\"\" class=\"h3 text-center\"></Label>\r\n\r\n\r\n                        </StackLayout>\r\n                       \r\n\r\n                        <Label text=\"\" class=\"text-center\">\r\n                        </Label>\r\n                        <Button text=\"Ver tests disponibles/Actualizar\" backgroundColor=\"blue\" width=\"75%\"\r\n                            color=\"whitesmoke\" (tap)=\"getTest()\">\r\n                        </Button>\r\n\r\n                    </StackLayout>\r\n                </GridLayout>\r\n            </TabContentItem>\r\n            <TabContentItem>\r\n                <GridLayout>\r\n                    <StackLayout width=\"80%\">\r\n                        <Label text=\"\" class=\"h2 text-center\"></Label>\r\n                        <Label text=\"Ajustes\"  fontWeight=\"bold\" class=\"h1 text-center\"></Label>\r\n                        <Label text=\"\" class=\"text-center\">\r\n                        </Label>\r\n                        <Button text=\"Modificar datos\"\r\n                            backgroundColor=\"#35A6E8\" color=\"whitesmoke\" >\r\n                        </Button>\r\n                        <Label text=\"\" class=\"text-center\">\r\n                        </Label>\r\n                        <Button text=\"Salir de la clase\"\r\n                            backgroundColor=\"#35A6E8\" color=\"whitesmoke\" (tap)=\"salirclase()\">\r\n                        </Button>\r\n                        <Label text=\"\" class=\"text-center\">\r\n                        </Label>\r\n                        <Button text=\"Cerrar sesión\" backgroundColor=\"#35A6E8\"\r\n                            color=\"whitesmoke\" (tap)=\"logout()\">\r\n                        </Button>\r\n                    </StackLayout>\r\n                </GridLayout>\r\n            </TabContentItem>\r\n        </Tabs>\r\n    </StackLayout>\r\n    <Button text=\"Cerrar Sesión\" (tap)=\"logout()\"></Button>\r\n</StackLayout>"

/***/ }),

/***/ "./student/student.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentComponent", function() { return StudentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _shared_user_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./shared/user/user.model.ts");
/* harmony import */ var _shared_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./shared/user/user.service.ts");





var StudentComponent = /** @class */ (function () {
    function StudentComponent(userService, routerExtensions, page) {
        this.userService = userService;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.questions = [];
        this.information = [
            {
                "question": "¿Cuál es el símbolo químico del Potasio?",
                "answers": {
                    "a": "Ca",
                    "b": "K",
                    "c": "Pt"
                }
            },
            {
                "question": "¿Cuál es el símbolo químico del Carbono?",
                "answers": {
                    "a": "Ca",
                    "b": "K",
                    "c": "C"
                }
            }
        ];
        this.currentUser = new _shared_user_user_model__WEBPACK_IMPORTED_MODULE_3__["User"]();
        this.profesor = "Alberto Brunete";
        this.clase = "B-12";
        this.asignatura = "Electrónica";
        this.grupo = "A-204";
        this.asiento_ocupado = "2";
        //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = this.userService.currentUser();
        this.currentSession = JSON.parse(localStorage.getItem('currentSession'));
        this.page.actionBarHidden = true;
        this.information[0].open = false;
        this.information[0].answers.openA = false;
        this.information[0].answers.openB = false;
        this.information[0].answers.openC = false;
    }
    StudentComponent.prototype.ngOnInit = function () {
        this.getCurso();
    };
    StudentComponent.prototype.getCurso = function () {
        var _this = this;
        this.userService.getCurso(this.currentSession.curso).subscribe(function (curso) {
            _this.currentCurso = curso[0];
            _this.userService.getTeacher(_this.currentCurso.teacher).subscribe(function (teacher) {
                _this.currentTeacher = teacher[0];
            }, function (error) {
                alert("No se ha podido obtener la información del profesor al que pertenece la sesión, Error: " + error);
            });
        }, function (error) {
            alert("No se ha podido obtener la información del curso al que pertenece la sesión, Error: " + error);
        });
    };
    StudentComponent.prototype.onItemTap = function (args) {
        console.log('Item with index: ' + args.index + ' tapped');
    };
    StudentComponent.prototype.navigateToQuiz = function (index) {
        console.log("Has pulsado ell index" + index);
    };
    StudentComponent.prototype.toogleSelection = function (index) {
        this.information[index].open = !this.information[index].open;
    };
    StudentComponent.prototype.toogleSelection2 = function (index) {
        this.questions[index].open = !this.questions[index].open;
    };
    StudentComponent.prototype.identification = function () {
        this.routerExtensions.navigate(["/identification"]);
    };
    StudentComponent.prototype.salirclase = function () {
        alert("Función no disponible actualmente");
    };
    /*
    switchColor2(index, indexAnswer){

        for (let i = 0; i < 3; i++)
        {
            if (i == indexAnswer) {
                this.information[index].answers[indexAnswer].open = true;
            }
            else
            {
                this.information[index].answers[indexAnswer].open = false;
            }
        }
    }*/
    StudentComponent.prototype.switchColor = function (index, indexAnswer) {
        if (indexAnswer == "0") {
            this.information[index].answers.openA = !this.information[index].answers.openA;
            this.information[index].answers.openB = false;
            this.information[index].answers.openC = false;
        }
        else if (indexAnswer == "1") {
            this.information[index].answers.openA = false;
            this.information[index].answers.openB = !this.information[index].answers.openB;
            this.information[index].answers.openC = false;
        }
        else if (indexAnswer == "2") {
            this.information[index].answers.openA = false;
            this.information[index].answers.openB = false;
            this.information[index].answers.openC = !this.information[index].answers.openC;
        }
    };
    StudentComponent.prototype.sendResponse = function (index) {
        if (this.information[index].answers.openA) {
            alert("La respuesta elegida es la a) ");
        }
        else if (this.information[index].answers.openB) {
            alert("La respuesta elegida es la b) ");
        }
        else if (this.information[index].answers.openC) {
            alert("La respuesta elegida es la c) ");
        }
        else {
            alert("Por favor seleccione una respuesta");
        }
    };
    StudentComponent.prototype.sendResponse2 = function (index) {
        if (this.questions[index].openA) {
            alert("La respuesta elegida es la a) ");
        }
        else if (this.questions[index].openB) {
            alert("La respuesta elegida es la b) ");
        }
        else if (this.questions[index].openC) {
            alert("La respuesta elegida es la c) ");
        }
        else {
            alert("Por favor seleccione una respuesta");
        }
    };
    StudentComponent.prototype.switchColor3 = function (index, indexAnswer) {
        if (indexAnswer == "0") {
            this.questions[index].openA = !this.questions[index].openA;
            this.questions[index].openB = false;
            this.questions[index].openC = false;
        }
        else if (indexAnswer == "1") {
            this.questions[index].openA = false;
            this.questions[index].openB = !this.questions[index].openB;
            this.questions[index].openC = false;
        }
        else if (indexAnswer == "2") {
            this.questions[index].openA = false;
            this.questions[index].openB = false;
            this.questions[index].openC = !this.questions[index].openC;
        }
    };
    StudentComponent.prototype.getTest = function () {
        var _this = this;
        this.userService.getTestByClass(this.currentSession._id).subscribe(function (tests) {
            _this.questions = tests[0].questions;
            _this.questions[0].open = false;
            _this.questions[0].openA = false;
            _this.questions[0].openB = false;
            _this.questions[0].openC = false;
        }, function (err) {
            console.log('Error al obtener los tests: ' + JSON.stringify(err));
        });
    };
    StudentComponent.prototype.logout = function () {
        var _this = this;
        this.currentUser = this.userService.currentUser();
        this.userService.logout(this.currentUser._id, this.currentUser).subscribe(function (data) {
            _this.routerExtensions.navigate(['/login'], { clearHistory: true });
        }, function (error) {
            alert("Error al cerrar sesión");
        });
    };
    StudentComponent.ctorParameters = function () { return [
        { type: _shared_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"] },
        { type: tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"] }
    ]; };
    StudentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Student",
            template: __importDefault(__webpack_require__("./student/student.component.html")).default,
            styles: [__importDefault(__webpack_require__("./student/student.component.css")).default]
        }),
        __metadata("design:paramtypes", [_shared_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"], nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"], tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_2__["Page"]])
    ], StudentComponent);
    return StudentComponent;
}());



/***/ }),

/***/ "./teacher/teacher.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\r\n\tvertical-align: center;\r\n\tfont-size: 20;\r\n\tmargin: 15;\r\n}"

/***/ }),

/***/ "./teacher/teacher.component.html":
/***/ (function(module, exports) {

module.exports = "<ActionBar title=\"Teacher\" class=\"action-bar\">\r\n    </ActionBar>\r\n    \r\n    <ScrollView class=\"page\">\r\n        <StackLayout class=\"home-panel\">\r\n            <Button text=\"Identificar clase\"  background-color:rgb(76, 91, 213)></Button>\r\n            <Button text=\"Crear test\" class=\"btn btn-primary m-t-20\"></Button>\r\n            <Button text=\"CrearPrueba\" class=\"btn btn-primary m-t-20\"></Button>\r\n            <Button text=\"Cerrar Sesión\" (tap)=\"logout()\"></Button>\r\n        </StackLayout>\r\n    </ScrollView>"

/***/ }),

/***/ "./teacher/teacher.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherComponent", function() { return TeacherComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);


var TeacherComponent = /** @class */ (function () {
    function TeacherComponent(routerExtensions) {
        this.routerExtensions = routerExtensions;
    }
    TeacherComponent.prototype.ngOnInit = function () {
    };
    TeacherComponent.prototype.logout = function () {
        this.routerExtensions.navigate(["/login"]);
    };
    TeacherComponent.ctorParameters = function () { return [
        { type: nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"] }
    ]; };
    TeacherComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Teacher",
            template: __importDefault(__webpack_require__("./teacher/teacher.component.html")).default,
            styles: [__importDefault(__webpack_require__("./teacher/teacher.component.css")).default]
        }),
        __metadata("design:paramtypes", [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterExtensions"]])
    ], TeacherComponent);
    return TeacherComponent;
}());



/***/ })

},[["./main.ts","runtime","vendor"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi4vJF9sYXp5X3JvdXRlX3Jlc291cmNlIGxhenkgbmFtZXNwYWNlIG9iamVjdCIsIndlYnBhY2s6Ly8vLi9hcHAuY29tcG9uZW50LnRzIiwid2VicGFjazovLy8uL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXBwLm1vZHVsZS50cyIsIndlYnBhY2s6Ly8vLi9hcHAucm91dGluZy50cyIsIndlYnBhY2s6Ly8vLi9oZWxwZXJzL2Vycm9yLmludGVyY2VwdG9yLnRzIiwid2VicGFjazovLy8uL2hlbHBlcnMvaW5kZXgudHMiLCJ3ZWJwYWNrOi8vLy4vaGVscGVycy9qd3QuaW50ZXJjZXB0b3IudHMiLCJ3ZWJwYWNrOi8vLy4vaWRlbnRpZmljYXRpb24vaWRlbnRpZmljYXRpb24uY29tcG9uZW50LmNzcyIsIndlYnBhY2s6Ly8vLi9pZGVudGlmaWNhdGlvbi9pZGVudGlmaWNhdGlvbi5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9pZGVudGlmaWNhdGlvbi9pZGVudGlmaWNhdGlvbi5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyIsIndlYnBhY2s6Ly8vLi9sb2dpbi9sb2dpbi5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9sb2dpbi9sb2dpbi5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vbWFpbi50cyIsIndlYnBhY2s6Ly8vLi9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuY3NzIiwid2VicGFjazovLy8uL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5odG1sIiwid2VicGFjazovLy8uL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9yZWdpc3RlcjIvcmVnaXN0ZXIuY29tcG9uZW50LmNzcyIsIndlYnBhY2s6Ly8vLi9yZWdpc3RlcjIvcmVnaXN0ZXIuY29tcG9uZW50Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vcmVnaXN0ZXIyL3JlZ2lzdGVyLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9yZXNldF9wYXNzd29yZC9yZXNldF9wYXNzd29yZC5jb21wb25lbnQuY3NzIiwid2VicGFjazovLy8uL3Jlc2V0X3Bhc3N3b3JkL3Jlc2V0X3Bhc3N3b3JkLmNvbXBvbmVudC5odG1sIiwid2VicGFjazovLy8uL3Jlc2V0X3Bhc3N3b3JkL3Jlc2V0X3Bhc3N3b3JkLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9zZXNzaW9uX2NvZGUvc2Vzc2lvbl9jb2RlLmNvbXBvbmVudC5jc3MiLCJ3ZWJwYWNrOi8vLy4vc2Vzc2lvbl9jb2RlL3Nlc3Npb25fY29kZS5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9zZXNzaW9uX2NvZGUvc2Vzc2lvbl9jb2RlLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9zaGFyZWQvY29uZmlnLnRzIiwid2VicGFjazovLy8uL3NoYXJlZC91c2VyL2F1dGhlbnRpY2F0ZS5zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3NoYXJlZC91c2VyL3VzZXIubW9kZWwudHMiLCJ3ZWJwYWNrOi8vLy4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3N0dWRlbnQvc3R1ZGVudC5jb21wb25lbnQuY3NzIiwid2VicGFjazovLy8uL3N0dWRlbnQvc3R1ZGVudC5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9zdHVkZW50L3N0dWRlbnQuY29tcG9uZW50LnRzIiwid2VicGFjazovLy8uL3RlYWNoZXIvdGVhY2hlci5jb21wb25lbnQuY3NzIiwid2VicGFjazovLy8uL3RlYWNoZXIvdGVhY2hlci5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi90ZWFjaGVyL3RlYWNoZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0EsNENBQTRDLFdBQVc7QUFDdkQ7QUFDQTtBQUNBLHlFOzs7Ozs7OztBQ1pBO0FBQUE7QUFBQTtBQUEwQztBQU0xQztJQUFBO0lBQTRCLENBQUM7SUFBaEIsWUFBWTtRQUp4QiwrREFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsUUFBUSxFQUFFLDJDQUEyQztTQUN0RCxDQUFDO09BQ1csWUFBWSxDQUFJO0lBQUQsbUJBQUM7Q0FBQTtBQUFKOzs7Ozs7OztBQ056Qix3SEFBMEUsbUJBQU8sQ0FBQyxzSUFBb0csR0FBRyxrQkFBa0Isa0NBQWtDLFVBQVUsMEVBQTBFLEVBQUUsNERBQTRELHNEQUFzRCxFQUFFLDhFQUE4RSxFQUFFLDREQUE0RCxFQUFFLHlEQUF5RCxFQUFFLDREQUE0RCxFQUFFLEVBQUUsMkRBQTJELHNEQUFzRCxFQUFFLCtFQUErRSxFQUFFLDZEQUE2RCxFQUFFLDREQUE0RCxFQUFFLHlEQUF5RCxFQUFFLDREQUE0RCxFQUFFLHNEQUFzRCxFQUFFLEVBQUUscUVBQXFFLHdEQUF3RCxFQUFFLEVBQUUsb0VBQW9FLHdEQUF3RCxFQUFFLEVBQUUscURBQXFELDZEQUE2RCxFQUFFLHNEQUFzRCxFQUFFLHVEQUF1RCxFQUFFLHFFQUFxRSxFQUFFLDBEQUEwRCxFQUFFLHVEQUF1RCxFQUFFO0FBQ3QxRCxRQUFRLElBQVU7QUFDbEI7QUFDQTtBQUNBLCtCQUErQixtQ0FBbUM7QUFDbEUsU0FBUztBQUNUOzs7Ozs7Ozs7O0FDTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXlDO0FBQzRCO0FBQ1c7QUFDRjtBQUNQO0FBRXRCO0FBQ0Y7QUFDVTtBQUNTO0FBQ0U7QUFDTDtBQUNBO0FBQ2U7QUFDTTtBQUNBO0FBRzNCO0FBQ2dCO0FBRVU7QUFnQ25GO0lBQUE7SUFBeUIsQ0FBQztJQUFiLFNBQVM7UUE5QnJCLDhEQUFRLENBQUM7WUFDUixPQUFPLEVBQUU7Z0JBQ1AsMkZBQWtCO2dCQUNsQixrRkFBdUI7Z0JBQ3ZCLDZGQUE0QjtnQkFDNUIsb0ZBQXdCO2dCQUN4QiwyRkFBa0I7Z0JBQ2xCLGtGQUF1QjtnQkFDdkIsNkRBQWdCO2dCQUNoQiw2RkFBNEI7YUFDN0I7WUFDRCxZQUFZLEVBQUU7Z0JBQ1osMkRBQVk7Z0JBQ1oscUVBQWM7Z0JBQ2QsOEVBQWlCO2dCQUNqQixnRkFBa0I7Z0JBQ2xCLDRFQUFnQjtnQkFDaEIsNEVBQWdCO2dCQUNoQiwyRkFBcUI7Z0JBQ3JCLGlHQUF1QjtnQkFDdkIsaUdBQXVCO2FBQ3hCO1lBQ0QsU0FBUyxFQUFFO2dCQUNULHNFQUFXO2dCQUNYLHNGQUFtQjtnQkFDbkIsc0VBQXNCO2dCQUN0QiwwQkFBMEI7YUFDN0I7WUFDQyxTQUFTLEVBQUUsQ0FBQywyREFBWSxDQUFDO1NBQzFCLENBQUM7T0FDVyxTQUFTLENBQUk7SUFBRCxnQkFBQztDQUFBO0FBQUo7Ozs7Ozs7OztBQ3JEdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBeUM7QUFFOEI7QUFDZDtBQUVTO0FBQ0U7QUFDTDtBQUNBO0FBQ2U7QUFDTTtBQUNBO0FBRXBGLElBQU0sTUFBTSxHQUFXO0lBQ25CLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUU7SUFDckQsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxxRUFBYyxFQUFFO0lBQzVDLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsOEVBQWlCLEVBQUU7SUFDbEQsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxnRkFBa0IsRUFBRTtJQUNwRCxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLDJFQUFnQixFQUFFO0lBQ2hELEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsMkVBQWdCLEVBQUU7SUFDaEQsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLFNBQVMsRUFBRSwwRkFBcUIsRUFBRTtJQUMxRCxFQUFFLElBQUksRUFBQyxnQkFBZ0IsRUFBRSxTQUFTLEVBQUUsZ0dBQXVCLEVBQUU7SUFDN0QsRUFBRSxJQUFJLEVBQUMsZ0JBQWdCLEVBQUUsU0FBUyxFQUFFLGdHQUF1QixFQUFFO0NBRWhFLENBQUM7QUFNRjtJQUFBO0lBQWdDLENBQUM7SUFBcEIsZ0JBQWdCO1FBSjVCLDhEQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxvRkFBd0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkQsT0FBTyxFQUFFLENBQUMsb0ZBQXdCLENBQUM7U0FDdEMsQ0FBQztPQUNXLGdCQUFnQixDQUFJO0lBQUQsdUJBQUM7Q0FBQTtBQUFKOzs7Ozs7Ozs7QUM5QjdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTJDO0FBQ29FO0FBQzFFO0FBQ0g7QUFDRDtBQUdqQztJQUFBO0lBT0EsQ0FBQztJQU5HLG9DQUFTLEdBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO1FBQ2xELDBEQUEwRDtRQUMxRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLHVCQUFhO1lBQzNDLE9BQU8sK0NBQVUsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFOUSxnQkFBZ0I7UUFENUIsZ0VBQVUsRUFBRTtPQUNBLGdCQUFnQixDQU81QjtJQUFELHVCQUFDO0NBQUE7QUFQNEI7QUFTdEIsSUFBTSx3QkFBd0IsR0FBRztJQUNwQyxPQUFPLEVBQUUsc0VBQWlCO0lBQzFCLFFBQVEsRUFBRSxnQkFBZ0I7SUFDMUIsS0FBSyxFQUFFLElBQUk7Q0FDZCxDQUFDOzs7Ozs7Ozs7QUNwQkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFvQztBQUNGOzs7Ozs7Ozs7QUNEbEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEyQztBQUNvRTtBQUkvRztJQUFBO0lBY0EsQ0FBQztJQWJHLGtDQUFTLEdBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO1FBQ2xELHVEQUF1RDtRQUN2RCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztRQUNsRSxJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFO1lBQ2xDLE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUNwQixVQUFVLEVBQUU7b0JBQ1IsYUFBYSxFQUFFLFlBQVUsV0FBVyxDQUFDLEtBQU87aUJBQy9DO2FBQ0osQ0FBQyxDQUFDO1NBQ047UUFFRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQWJRLGNBQWM7UUFEMUIsZ0VBQVUsRUFBRTtPQUNBLGNBQWMsQ0FjMUI7SUFBRCxxQkFBQztDQUFBO0FBZDBCO0FBZ0JwQixJQUFNLHNCQUFzQixHQUFHO0lBQ2xDLE9BQU8sRUFBRSxzRUFBaUI7SUFDMUIsUUFBUSxFQUFFLGNBQWM7SUFDeEIsS0FBSyxFQUFFLElBQUk7Q0FDZCxDQUFDOzs7Ozs7OztBQ3pCRiw4QkFBOEIsNkJBQTZCLG9CQUFvQixpQkFBaUIsS0FBSyxpQkFBaUIsK0JBQStCLG9CQUFvQix1QkFBdUIsd0JBQXdCLHlCQUF5Qiw0QkFBNEIsT0FBTyxtQkFBbUIseUJBQXlCLHdCQUF3QixxQkFBcUIsaUJBQWlCLHFCQUFxQixzQkFBc0IsOEJBQThCLDJDQUEyQyxLQUFLLGFBQWEsdUJBQXVCLHNCQUFzQiw0QkFBNEIsOENBQThDLEM7Ozs7Ozs7QUNBM25CLDQySDs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpRTtBQUNGO0FBRWQ7QUFDUztBQUVtQjtBQUVoQjtBQUN0QjtBQVV2QztJQWlCSSxpQ0FBcUIsSUFBVSxFQUFVLFdBQXdCLEVBQVMsZ0JBQWtDO1FBQXZGLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFTLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFkNUcsUUFBRyxHQUFHLElBQUksb0RBQUcsRUFBRSxDQUFDO1FBSWhCLFlBQU8sR0FBVSxFQUFFLENBQUM7UUFFcEIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFFcEIsVUFBSyxHQUFHO1lBQ0osRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLEVBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDO1lBQ3BPLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxFQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQztTQUM5TyxDQUFDO1FBR0EsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLDREQUFJLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFFakMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztRQUNuRSxnREFBZ0Q7UUFDaEQsaURBQWlEO0lBQ3JELENBQUM7SUFFSiwwQ0FBUSxHQUFSO1FBQ08sSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVPLG1EQUFpQixHQUF6QjtRQUFBLGlCQUdEO1FBRkssSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsQ0FDM0MsaUJBQU8sSUFBTSxLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFDQyxzQ0FBSSxHQUFKO1FBQUEsaUJBZUM7UUFiRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksR0FBQyxJQUFJLENBQUM7UUFDbkMsNkNBQTZDO1FBQzdDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDakUscUNBQXFDO1FBQ3JDLGdEQUFnRDtRQUNwRCxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUMvQyxjQUFJO1lBQ0ksNENBQTRDO1lBQzVDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNyQyxDQUFDLEVBQ0QsZUFBSztZQUNELEtBQUssQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO1FBQzdELENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUdNLGtEQUFnQixHQUF2QjtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFTSxnREFBYyxHQUFyQjtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFTSw2Q0FBVyxHQUFsQjtRQUFBLGlCQW9DSztRQW5DRCxJQUFJLDBFQUFjLEVBQUUsQ0FBQyxJQUFJLENBQUM7WUFDdEIsT0FBTyxFQUFFLGlCQUFpQjtZQUMxQixXQUFXLEVBQUUscUNBQXFDO1lBQ2xELDBCQUEwQixFQUFFLFNBQVM7WUFDckMsT0FBTyxFQUFFLHdDQUF3QztZQUNqRCxvQkFBb0IsRUFBRSxJQUFJO1lBQzFCLGlCQUFpQixFQUFFLEtBQUs7WUFDeEIsZUFBZSxFQUFFLElBQUk7WUFDckIsVUFBVSxFQUFFLElBQUk7WUFDaEIsVUFBVSxFQUFFLElBQUk7WUFDaEIsT0FBTyxFQUFFLEtBQUs7WUFDZCxhQUFhLEVBQUUsY0FBUSxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLEdBQUM7WUFDckQscUJBQXFCLEVBQUUsR0FBRztZQUUxQiwyQ0FBMkMsRUFBRSxJQUFJO1lBQ2pELDJCQUEyQixFQUFFLElBQUksQ0FBQywyTkFBMk47U0FDOVAsQ0FBQyxDQUFDLElBQUksQ0FDTCxVQUFDLE1BQU07WUFDSCxLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJO2dCQUMxQixLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBQyxJQUFJLENBQUM7Z0JBRW5ELEtBQUksQ0FBQyxTQUFTLEVBQUU7WUFFaEIsU0FBUztZQUNMLHVCQUF1QjtZQUN2QixtRUFBbUU7WUFDbkUsb0JBQW9CO1lBQ3RCLEtBQUs7UUFDVixDQUFDLEVBQ0YsVUFBQyxZQUFZO1lBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsWUFBWSxDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUNGLENBQUM7UUFDRiwwQkFBMEI7UUFDeEIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRSwyQ0FBUyxHQUFoQjtRQUNJLElBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNkLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQzs7WUFFN0IsS0FBSyxDQUFDLGNBQWMsQ0FBQztJQUM3QixDQUFDO0lBR00sZ0RBQWMsR0FBckI7UUFDSSxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRU0sMENBQVEsR0FBZjtRQUVBLGdIQUFnSDtRQUM1RyxJQUFHO1lBQ0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDcEMsSUFBRyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQzdCO2dCQUNJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ3JDLHVFQUF1RTtnQkFDdkUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQztnQkFDaEMsT0FBTyxJQUFJLENBQUM7YUFDZjtpQkFFRDtnQkFDSSxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztnQkFDdEIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzlCLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxXQUNBO1lBQ1EsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDdEIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5QixPQUFPLEtBQUssQ0FBQztTQUNwQjtJQUNMLENBQUM7SUFHTSxnREFBYyxHQUFyQjtRQUNRLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUM5QyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUN4QywwSEFBMEg7UUFDMUgsd0ZBQXdGO0lBRTVGLENBQUM7SUFJTCx3Q0FBTSxHQUFOLFVBQU8sV0FBaUI7UUFBeEIsaUJBb0JDO1FBbkJHLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUUsRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxJQUFFLEVBQUUsRUFDMUQ7WUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDM0Msa0VBQWtFO1lBQ2xFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDOUMsc0RBQXNEO1lBQ3RELElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVMsQ0FDNUQsY0FBSTtnQkFDQSxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQztnQkFDMUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDakQsQ0FBQyxFQUNELGVBQUs7Z0JBQ0QsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUM7WUFDeEMsQ0FBQyxDQUFDLENBQUM7U0FDZDthQUNHO1lBQ0EsS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7U0FDbkM7SUFDTCxDQUFDO0lBR0QseUNBQU8sR0FBUDtRQUFBLGlCQXVCQztRQXRCRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBQyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUMsTUFBTSxDQUFDO1FBQzlCLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUUsRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxJQUFFLEVBQUUsRUFDMUQ7WUFFSSxnREFBZ0Q7WUFDaEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUVyRSxJQUFJLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQzVELGlCQUFPO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDaEUsNENBQTRDO2dCQUM1QyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNqRCxDQUFDLEVBQ0QsZUFBSztnQkFDRCxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztTQUNkO2FBQ0c7WUFDQSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztTQUM1RDtJQUNMLENBQUM7SUFJTSxnREFBYyxHQUFyQjtRQUNJLEtBQUssQ0FBQywrQkFBK0IsQ0FBQztJQUMxQyxDQUFDO0lBQ00seUNBQU8sR0FBZDtRQUFBLGlCQXdCQztRQXRCQyxJQUFJLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLFVBQUMsSUFBaUI7WUFDckQsMkNBQTJDO1lBRTNDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDaEIsK0JBQStCO2dCQUM3QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFDLElBQUksQ0FBQztvQkFFbkQsS0FBSSxDQUFDLFFBQVEsRUFBRTtvQkFDZixLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDOUIsUUFBUTtnQkFDSiw4Q0FBOEM7Z0JBQ2xELFlBQVk7YUFDZjtRQUNILENBQUMsRUFBRTtZQUNELHVCQUF1QjtZQUN2QixrQkFBa0IsRUFBRSxJQUFJO1lBQ3hCLFFBQVEsRUFBRSxZQUFZO1NBQ3ZCLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7UUFDbkQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsd0NBQU0sR0FBTjtRQUFBLGlCQVNHO1FBUkssSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2xELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQ2pFLGNBQUk7WUFDQSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUMsRUFBQyxZQUFZLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztRQUNwRSxDQUFDLEVBQ0QsZUFBSztZQUNELEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxDQUFDO0lBQ2pCLENBQUM7O2dCQTlOd0IsNkRBQUk7Z0JBQXVCLHFFQUFXO2dCQUEyQiw0RUFBZ0I7O0lBakJuRyx1QkFBdUI7UUFQbkMsK0RBQVMsQ0FBQztZQUNWLFFBQVEsRUFBRSxnQkFBZ0I7WUFFdkIsU0FBUyxFQUFFLENBQUMscUVBQVcsQ0FBQztZQUMzQix3R0FBOEM7O1NBRTlDLENBQUM7eUNBa0I2Qiw2REFBSSxFQUF1QixxRUFBVyxFQUEyQiw0RUFBZ0I7T0FqQm5HLHVCQUF1QixDQWlQbkM7SUFBRCw4QkFBQztDQUFBO0FBalBtQzs7Ozs7Ozs7QUNuQnBDLHlCQUF5QiwwQkFBMEIsNkJBQTZCLEtBQUssV0FBVyxzQkFBc0IsdUJBQXVCLG1CQUFtQiw2QkFBNkIsS0FBSyxlQUFlLHdCQUF3Qix3QkFBd0IsS0FBSyxlQUFlLGlCQUFpQixpQkFBaUIseUNBQXlDLHdCQUF3Qix1QkFBdUIsb0JBQW9CLHVCQUF1QixLQUFLLFdBQVcsd0JBQXdCLGtCQUFrQiwrQkFBK0Isd0JBQXdCLEtBQUssYUFBYSwrQkFBK0Isb0JBQW9CLHVCQUF1Qix3QkFBd0IseUJBQXlCLDRCQUE0QixLQUFLLHNCQUFzQix3QkFBd0IsS0FBSyxZQUFZLG9CQUFvQixpQ0FBaUMsS0FBSyxxQkFBcUIsMkNBQTJDLG1CQUFtQixLQUFLLHNCQUFzQix3QkFBd0IsS0FBSyxxQkFBcUIsd0JBQXdCLEtBQUssc0JBQXNCLCtCQUErQixxQkFBcUIsb0JBQW9CLEtBQUssb0JBQW9CLHdCQUF3QixLQUFLLFdBQVcscUJBQXFCLE1BQU0sUzs7Ozs7OztBQ0F6c0Msb3lGOzs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5QztBQUVRO0FBQ1M7QUFDZ0I7QUFFZDtBQUNaO0FBQ2U7QUFXL0Q7SUFXSSx3QkFBb0IsSUFBVSxFQUFVLG1CQUF3QyxFQUFVLGdCQUFrQyxFQUFTLFdBQXdCO1FBQXpJLFNBQUksR0FBSixJQUFJLENBQU07UUFBVSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFTLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBUjdKLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ25CLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFHakIsVUFBSyxHQUFRLEVBQUUsQ0FBQztRQUtaLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSw0REFBSSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBR2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSw0REFBSSxFQUFFLENBQUM7UUFFOUIsMENBQTBDO1FBQzFDLDBDQUEwQztRQUMxQyxnQ0FBZ0M7SUFDcEMsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBRUQsZ0NBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFRCwrQkFBTSxHQUFOO1FBQ0ksSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNoQjthQUFNO1lBQ0gsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ2pCO0lBQ0wsQ0FBQztJQUVELDhCQUFLLEdBQUw7UUFBQSxpQkFtQ0M7UUFsQ0cseUNBQXlDO1FBQ3pDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUNwQyxTQUFTLENBQ04sa0JBQVE7WUFDQSxxQkFBcUI7WUFDckIsNkNBQTZDO1lBQzlDLDZDQUE2QztZQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUMsQ0FBQztZQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFFNUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzlELEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDbkUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFFLENBQUM7WUFDL0QsSUFBRyxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBRSxJQUFJLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLElBQUUsSUFBSSxFQUN0RTtnQkFDSyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzthQUNqRDtpQkFDRztnQkFDSCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDbEQscUJBQXFCO2dCQUNyQixzREFBc0Q7Z0JBQ3RELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDdEI7UUFDSCxDQUFDLEVBQ04sVUFBQyxTQUFTO1lBQ04saUNBQWlDO1lBQ2pDLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTtnQkFDaEQseUVBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3RDO2lCQUFNO2dCQUNILHlFQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQzthQUN6QjtRQUNMLENBQUMsQ0FDSixDQUFDO0lBQ1YsQ0FBQztJQUNELGtDQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQscUNBQVksR0FBWjtRQUVJLHlFQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELCtCQUFNLEdBQU47UUFBQSxpQkFlQztRQWRHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUN2QyxTQUFTLENBQ047WUFDSSx5RUFBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7WUFDaEQsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pCLENBQUMsRUFDRCxVQUFDLFNBQVM7WUFDTixJQUFJLFNBQVMsQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUU7Z0JBQ2hELHlFQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUN0QztpQkFBTTtnQkFDSCx5RUFBSyxDQUFDLFNBQVMsQ0FBQzthQUNuQjtRQUNMLENBQUMsQ0FDSixDQUFDO0lBQ1YsQ0FBQztJQUVELHVDQUFjLEdBQWQ7UUFBQSxpQkE4QkM7UUE1QkcsSUFBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFDckI7WUFDSSxxRkFBcUY7WUFDckY7Z0JBQ0ksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3pDLDhCQUE4QjtnQkFDOUIsMkJBQTJCO2dCQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBRXhDLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztxQkFDaEQsU0FBUyxDQUNOLGNBQUk7b0JBQ0EseUVBQUssQ0FBQyxrRUFBa0UsQ0FBQyxDQUFDO29CQUMxRSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUNwRCxvQkFBb0I7Z0JBQ3hCLENBQUMsRUFDRCxVQUFDLFNBQVM7b0JBQ04sSUFBSSxTQUFTLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO3dCQUNoRCx5RUFBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7cUJBQ3RDO3lCQUFNO3dCQUNILHlFQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMxQjtnQkFDTCxDQUFDLENBQUM7YUFDTDtTQUNSO2FBQ0c7WUFDQSx5RUFBSyxDQUFDLDZEQUE2RCxDQUFDO1NBQ3ZFO0lBQ0wsQ0FBQztJQUVELHNDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUN6QyxDQUFDOztnQkE3SHlCLDZEQUFJO2dCQUErQixxRkFBbUI7Z0JBQTRCLDRFQUFnQjtnQkFBc0IscUVBQVc7O0lBWHBKLGNBQWM7UUFSMUIsK0RBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFNBQVMsRUFBRSxDQUFDLHFFQUFXLENBQUM7WUFHeEIsc0ZBQXFDOztTQUN4QyxDQUFDO3lDQWE0Qiw2REFBSSxFQUErQixxRkFBbUIsRUFBNEIsNEVBQWdCLEVBQXNCLHFFQUFXO09BWHBKLGNBQWMsQ0EySTFCO0lBQUQscUJBQUM7Q0FBQTtBQTNJMEI7Ozs7Ozs7Ozs7Ozs7OztBQ2xCM0IsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDJCQUFlLENBQUM7QUFFL0MsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGNBQWMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFeEI7QUFDakIsK0RBQXlEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTnpELDZCQUE2QixvQkFBb0IscUJBQXFCLHNCQUFzQix1QkFBdUIsMEJBQTBCLEdBQUcsZ0JBQWdCLDZCQUE2QixxQkFBcUIsaUJBQWlCLEdBQUcsV0FBVyx3QkFBd0Isd0JBQXdCLEtBQUsseUJBQXlCLHdCQUF3QixHQUFHLFdBQVcsaUJBQWlCLEdBQUcsWUFBWSxzQkFBc0IsNkJBQTZCLEdBQUcsWUFBWSx3QkFBd0IsR0FBRyxHOzs7Ozs7O0FDQWxlLGc4Rjs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXdFO0FBQ1o7QUFDWjtBQUNlO0FBRWtCO0FBRWhDO0FBQ1U7QUFDSDtBQUV4RCxJQUFJLEVBQUUsR0FBRyxtQkFBTyxDQUFDLDZEQUFhLENBQUMsQ0FBQztBQUloQywrREFBK0Q7QUFVL0Q7SUF3QkMsMkJBQ1MsSUFBVSxFQUNWLFdBQXdCLEVBQ3hCLGdCQUFrQztRQUZsQyxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQ1YsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQXpCM0MsVUFBSyxHQUFRLEVBQUUsQ0FBQztRQUNiLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFFaEIsYUFBYTtRQUNiLFVBQUssR0FBRyxJQUFJLDBEQUFXLENBQUMsRUFBRSxFQUFFLENBQUMseURBQVUsQ0FBQyxRQUFRLEVBQUUseURBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBRXJFLGtCQUFrQjtRQUNsQixTQUFJLEdBQUcsSUFBSSxDQUFDO1FBRWYsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFHZCxNQUFNO1FBQ1QsaUJBQVksR0FBVSxJQUFJLENBQUM7UUFrQjFCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNqQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksNERBQUksRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVGLG9DQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O09BbUNHO0lBRUksd0NBQVksR0FBbkI7UUFBQSxpQkFxQkM7UUFuQkEsSUFBSSxPQUFPLEdBQUcsK0RBQWtCLENBQUM7WUFDaEMsSUFBSSxFQUFFLFFBQVEsQ0FBQyx3Q0FBd0M7U0FDdkQsQ0FBQyxDQUFDO1FBRUgsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQztZQUN2QixPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFTO1lBRWpCLEtBQUksQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUUzQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNyQyxLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlDLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2hELEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUVoRixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQsMkNBQWUsR0FBZjtRQUNDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDbkUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ2hELEVBQUUsQ0FBQztJQUNKLENBQUM7SUFFSCxxQ0FBUyxHQUFUO1FBQUEsaUJBbUNDO1FBaENBLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsRUFDakk7WUFDQyxxREFBcUQ7WUFDckQseUVBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNkLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUMvQztnQkFDQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO3FCQUNsQyxTQUFTLENBQ1QsY0FBSTtvQkFDSCx5RUFBSyxDQUFDLHNCQUFzQixDQUFDLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUM1QyxDQUFDLEVBQ0QsZUFBSztvQkFDSix5RUFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNiLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixDQUFDLENBQUMsQ0FBQzthQUVKO2lCQUNHO2dCQUNILHlFQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQzthQUN2QztTQUNEO2FBQ0c7WUFDSCx5RUFBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7U0FDOUM7UUFFRCwyRkFBMkY7UUFDM0YsbURBQW1EO1FBQ25ELG9EQUFvRDtRQUNwRCxvQkFBb0I7UUFDcEIsNENBQTRDO0lBQzdDLENBQUM7SUFFQyxvQ0FBUSxHQUFSO1FBQUEsaUJBa0JEO1FBakJELHNCQUFzQjtRQUN0QiwwQ0FBMEM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzthQUNqQyxTQUFTLENBQ1QsY0FBSTtZQUNILHlFQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQztZQUMvQixLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25CLENBQUMsRUFDRCxVQUFDLFNBQVM7WUFDVCxJQUFJLFNBQVMsQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUU7Z0JBQ25ELHlFQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNuQztpQkFBTTtnQkFDTix5RUFBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN2QjtRQUNGLENBQUMsQ0FBQztJQUVKLENBQUM7SUFJRCxnQ0FBSSxHQUFKO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUNEOzs7Ozs7Ozs7OztTQVdLO0lBQ0gsMENBQWMsR0FBZCxVQUFlLEtBQUs7UUFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBQ0Qsb0NBQVEsR0FBUjtRQUFBLGlCQTJCRDtRQTFCQSxJQUFHO1lBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN0QixJQUFJLElBQUksR0FBSSw0RUFBNEUsQ0FBQztZQUN6RixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQixJQUFNLEVBQUUsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1lBQzFCLGdFQUFnRTtZQUNoRSxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO1lBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDO2lCQUM3QixTQUFTLENBQ1gsY0FBSTtnQkFDSCx5RUFBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7Z0JBQ2hDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzVDLENBQUMsRUFDRCxlQUFLO2dCQUNKLGlDQUFpQztnQkFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbkIseUVBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyxDQUFDO1NBRUo7UUFDRCxPQUFNLENBQUMsRUFBQztZQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDZjtJQUNGLENBQUM7O2dCQTVMYyw2REFBSTtnQkFDRyxxRUFBVztnQkFDTiw0RUFBZ0I7O0lBM0IvQixpQkFBaUI7UUFQN0IsK0RBQVMsQ0FBQztZQUVWLFFBQVEsRUFBRSxVQUFVO1lBRXBCLDRGQUF3Qzs7U0FFeEMsQ0FBQzt5Q0EwQmMsNkRBQUk7WUFDRyxxRUFBVztZQUNOLDRFQUFnQjtPQTNCL0IsaUJBQWlCLENBNk43QjtJQUFELHdCQUFDO0NBQUE7QUE3TjZCOzs7Ozs7OztBQ3pCOUIsbUNBQW1DLGlDQUFpQyxzQkFBc0IseUJBQXlCLDBCQUEwQiwyQkFBMkIsOEJBQThCLE9BQU8sd0JBQXdCLGlDQUFpQyx5QkFBeUIscUJBQXFCLE9BQU8sbUJBQW1CLDRCQUE0Qiw0QkFBNEIsU0FBUyxpQ0FBaUMsNEJBQTRCLE9BQU8sbUJBQW1CLHFCQUFxQixPQUFPLEM7Ozs7Ozs7QUNBbGdCLG9IOzs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBd0U7QUFDWjtBQUNaO0FBQ2U7QUFDL0QscUZBQXFGO0FBRXBDO0FBQ1U7QUFDM0QsK0RBQStEO0FBUS9EO0lBcUJDLDRCQUNTLElBQVUsRUFDVixXQUF3QixFQUN4QixnQkFBa0M7UUFGbEMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUF0QjNDLFVBQUssR0FBUSxFQUFFLENBQUM7UUFDYixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBRWhCLGFBQWE7UUFDYix1RUFBdUU7UUFFdkUsa0JBQWtCO1FBQ2xCLFNBQUksR0FBRyxJQUFJLENBQUM7UUFFWixNQUFNO1FBQ1QsaUJBQVksR0FBVSxJQUFJLENBQUM7UUFrQjFCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNqQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksNERBQUksRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVGLHFDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsc0NBQVMsR0FBVDtRQUVDLHlFQUFLLENBQUMsbUNBQW1DLENBQUM7SUFDM0MsQ0FBQzs7Z0JBcEJjLDZEQUFJO2dCQUNHLHFFQUFXO2dCQUNOLDRFQUFnQjs7SUF4Qi9CLGtCQUFrQjtRQU45QiwrREFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLFdBQVc7WUFFckIsNkZBQXdDOztTQUV4QyxDQUFDO3lDQXVCYyw2REFBSTtZQUNHLHFFQUFXO1lBQ04sNEVBQWdCO09BeEIvQixrQkFBa0IsQ0E2QzlCO0lBQUQseUJBQUM7Q0FBQTtBQTdDOEI7Ozs7Ozs7O0FDaEIvQiwrQkFBK0IsMEJBQTBCLHlCQUF5QiwwQkFBMEIsMkJBQTJCLDhCQUE4QixPQUFPLHdCQUF3QixpQ0FBaUMseUJBQXlCLHFCQUFxQixPQUFPLG1CQUFtQiw0QkFBNEIsNEJBQTRCLFNBQVMsaUNBQWlDLDRCQUE0QixPQUFPLG1CQUFtQixxQkFBcUIsT0FBTyxvQkFBb0IsMEJBQTBCLGlDQUFpQyxPQUFPLG9CQUFvQiw0QkFBNEIsT0FBTyxPOzs7Ozs7O0FDQTltQixzL0U7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBd0U7QUFDWjtBQUNaO0FBQ2U7QUFHZDtBQUNqRCx3REFBd0Q7QUFDRztBQUNIO0FBRXhELElBQUksRUFBRSxHQUFHLG1CQUFPLENBQUMsNkRBQWEsQ0FBQyxDQUFDO0FBSWhDLCtEQUErRDtBQVUvRDtJQTJCQyxpQ0FDUyxJQUFVLEVBQ1YsV0FBd0IsRUFDeEIsZ0JBQWtDO1FBRmxDLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBNUJ4QyxVQUFLLEdBQVEsRUFBRSxDQUFDO1FBQ2hCLFdBQU0sR0FBUSxFQUFFLENBQUM7UUFDakIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUVoQixhQUFhO1FBQ2IsdUVBQXVFO1FBRXZFLGtCQUFrQjtRQUNsQixTQUFJLEdBQUcsSUFBSSxDQUFDO1FBSWYsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFHZCxNQUFNO1FBQ1QsaUJBQVksR0FBVSxJQUFJLENBQUM7UUFrQjFCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNqQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksNERBQUksRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVGLDBDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O09BbUNHO0lBRUksOENBQVksR0FBbkI7UUFBQSxpQkFxQkM7UUFuQkEsSUFBSSxPQUFPLEdBQUcsK0RBQWtCLENBQUM7WUFDaEMsSUFBSSxFQUFFLFFBQVEsQ0FBQyx3Q0FBd0M7U0FDdkQsQ0FBQyxDQUFDO1FBRUgsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQztZQUN2QixPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFTO1lBRWpCLEtBQUksQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUUzQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNyQyxLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlDLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2hELEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUVoRixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQsMkNBQVMsR0FBVDtRQUFBLGlCQW1DQztRQWpDQSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RCLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsRUFDakk7WUFDQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25ELElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUMvQztnQkFDQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO3FCQUNsQyxTQUFTLENBQ1QsY0FBSTtvQkFDSCx5RUFBSyxDQUFDLHNCQUFzQixDQUFDLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUM1QyxDQUFDLEVBQ0QsZUFBSztvQkFDSix5RUFBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNiLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixDQUFDLENBQUMsQ0FBQzthQUVKO2lCQUNHO2dCQUNILHlFQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQzthQUN2QztTQUNEO2FBQ0c7WUFDSCx5RUFBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7U0FDOUM7UUFFRCwyRkFBMkY7UUFDM0YsbURBQW1EO1FBQ25ELG9EQUFvRDtRQUNwRCxvQkFBb0I7UUFDcEIsNENBQTRDO0lBQzdDLENBQUM7SUFFQywwQ0FBUSxHQUFSO1FBQUEsaUJBa0JEO1FBakJELHNCQUFzQjtRQUN0QiwwQ0FBMEM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzthQUNqQyxTQUFTLENBQ1QsY0FBSTtZQUNILHlFQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUM5QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25CLENBQUMsRUFDRCxVQUFDLFNBQVM7WUFDVCxJQUFJLFNBQVMsQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUU7Z0JBQ25ELHlFQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNuQztpQkFBTTtnQkFDTix5RUFBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN2QjtRQUNGLENBQUMsQ0FBQztJQUVKLENBQUM7SUFFRSxzREFBb0IsR0FBcEI7UUFDSSxJQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUUsSUFBSSxDQUFDLFNBQVM7WUFDN0UsT0FBTyxJQUFJLENBQUM7O1lBRVosT0FBTyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQUNKLDBDQUFRLEdBQVI7UUFBQSxpQkF3Qkk7UUF2QkcsSUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFDM0Y7WUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztpQkFDekMsU0FBUyxDQUNOLGNBQUk7Z0JBQ0EseUVBQUssQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO2dCQUN2QyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDM0Msb0JBQW9CO1lBQ3hCLENBQUMsRUFDRCxVQUFDLFNBQVM7Z0JBQ04sSUFBSSxTQUFTLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO29CQUNoRCx5RUFBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7aUJBQ3RDO3FCQUFNO29CQUNILHlFQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUMxQjtZQUNMLENBQUMsQ0FBQztTQUVUO2FBQ0c7WUFDQSx5RUFBSyxDQUFDLDBCQUEwQixDQUFDLENBQUM7U0FDckM7SUFDTCxDQUFDO0lBQ0osc0NBQUksR0FBSjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFDRDs7Ozs7Ozs7Ozs7U0FXSztJQUNILGdEQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUNELDBDQUFRLEdBQVI7UUFBQSxpQkEyQkQ7UUExQkEsSUFBRztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEIsSUFBSSxJQUFJLEdBQUksNEVBQTRFLENBQUM7WUFDekYsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsSUFBTSxFQUFFLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUMxQixnRUFBZ0U7WUFDaEUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztZQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQztpQkFDN0IsU0FBUyxDQUNYLGNBQUk7Z0JBQ0gseUVBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO2dCQUNoQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUM1QyxDQUFDLEVBQ0QsZUFBSztnQkFDSixpQ0FBaUM7Z0JBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25CLHlFQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztZQUN2QyxDQUFDLENBQUMsQ0FBQztTQUVKO1FBQ0QsT0FBTSxDQUFDLEVBQUM7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2Y7SUFDRixDQUFDOztnQkFuTmMsNkRBQUk7Z0JBQ0cscUVBQVc7Z0JBQ04sNEVBQWdCOztJQTlCL0IsdUJBQXVCO1FBUG5DLCtEQUFTLENBQUM7WUFFVixRQUFRLEVBQUUsZ0JBQWdCO1lBRTFCLHdHQUE4Qzs7U0FFOUMsQ0FBQzt5Q0E2QmMsNkRBQUk7WUFDRyxxRUFBVztZQUNOLDRFQUFnQjtPQTlCL0IsdUJBQXVCLENBdVBuQztJQUFELDhCQUFDO0NBQUE7QUF2UG1DOzs7Ozs7OztBQ3pCcEMsOEJBQThCLCtCQUErQix1QkFBdUIsbUJBQW1CLEtBQUssMkJBQTJCLDBCQUEwQixLQUFLLGlCQUFpQixvQkFBb0IsdUJBQXVCLHdCQUF3Qix5QkFBeUIsa0JBQWtCLEtBQUssbUJBQW1CLG9CQUFvQix1QkFBdUIsd0JBQXdCLHlCQUF5QixLQUFLLEM7Ozs7Ozs7QUNBOVosczBCOzs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBd0U7QUFDWjtBQUNaO0FBQ2U7QUFDL0QscUZBQXFGO0FBRXBDO0FBQ1U7QUFDM0QsK0RBQStEO0FBUS9EO0lBU0ksK0JBQXFCLElBQVUsRUFBVSxXQUF3QixFQUFTLGdCQUFrQztRQUF2RixTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBUyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBTjVHLFVBQUssR0FBUSxFQUFFLENBQUM7UUFDaEIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQU1aLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSw0REFBSSxFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLHFFQUFxRTtRQUNyRSxJQUFJLENBQUMsV0FBVyxHQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFO0lBQ25ELENBQUM7SUFHSix3Q0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUVELG1EQUFtQixHQUFuQjtRQUNPLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLElBQUUsRUFBRTtZQUMxQix5RUFBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7YUFFbkQ7WUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNsRSxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLHVDQUF1QztZQUN2Qyw2Q0FBNkM7WUFDN0MsdUNBQXVDO1lBQ3ZDLDZDQUE2QztZQUM3QyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM5Qix1Q0FBdUM7U0FDMUM7SUFDTCxDQUFDO0lBRUQsc0NBQU0sR0FBTixVQUFPLFdBQWlCO1FBQXhCLGlCQVlDO1FBWEcsNkNBQTZDO1FBQzdDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUNoRSxxQ0FBcUM7UUFDckMsZ0RBQWdEO1FBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQzNDLGNBQUk7WUFDQSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1FBQ3hELENBQUMsRUFDRCxlQUFLO1lBQ0QseUVBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFDO0lBQ2YsQ0FBQzs7Z0JBdkMwQiw2REFBSTtnQkFBdUIscUVBQVc7Z0JBQTJCLDRFQUFnQjs7SUFUbkcscUJBQXFCO1FBTmpDLCtEQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsY0FBYztZQUV4QixvR0FBNEM7O1NBRTVDLENBQUM7eUNBVTZCLDZEQUFJLEVBQXVCLHFFQUFXLEVBQTJCLDRFQUFnQjtPQVRuRyxxQkFBcUIsQ0FpRGpDO0lBQUQsNEJBQUM7Q0FBQTtBQWpEaUM7Ozs7Ozs7OztBQ2hCbEM7QUFBQTtBQUFBO0lBQUE7SUFXQSxDQUFDO0lBVkMscURBQXFEO0lBQ3JELHFEQUFxRDtJQUNyRCw4REFBOEQ7SUFDOUQseURBQXlEO0lBQ3pELDJEQUEyRDtJQUMzRCx3REFBd0Q7SUFDakQsYUFBTSxHQUFHLDBCQUEwQixDQUFDO0lBQ3BDLGFBQU0sR0FBRyxlQUFlLENBQUM7SUFDekIsaUJBQVUsR0FBRyxzRUFBc0UsQ0FBQztJQUNwRixZQUFLLEdBQUcsRUFBRSxDQUFDO0lBQ3BCLGFBQUM7Q0FBQTtBQVhrQjs7Ozs7Ozs7O0FDQW5CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEyQztBQUNrQztBQUMvQjtBQUNRO0FBRUk7QUFHdkI7QUFHbkM7SUFDSSw2QkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtJQUFJLENBQUM7SUFFekMsc0NBQVEsR0FBUixVQUFTLElBQVU7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbEMsT0FBTyx1REFBVSxDQUFDLG9EQUFvRCxDQUFDLENBQUM7U0FDM0U7UUFFRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNqQiw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxPQUFPLEdBQUcsOENBQU0sQ0FBQyxNQUFNLEVBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDWCxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3BCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtTQUMxQixDQUFDLEVBQ0YsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FDdkMsQ0FBQyxJQUFJLENBQ0YsaUVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQ2hDLENBQUM7SUFDTixDQUFDO0lBRUQsbUNBQUssR0FBTCxVQUFNLElBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUNqQiw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxxQkFBcUIsRUFDckMsQ0FBQztZQUNHLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN2QixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7U0FDMUIsQ0FBQyxDQUNMLENBQUMsSUFBSSxDQUNGLDBEQUFHLENBQUMsa0JBQVEsSUFBSSxlQUFRLEVBQVIsQ0FBUSxDQUFDLEVBQ3pCLGlFQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUNoQyxDQUFDO0lBRU4sQ0FBQztJQUVELG9DQUFNLEdBQU4sVUFBTyxJQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxxQkFBcUIsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDN0csU0FBUyxDQUFDLGdCQUFNO1lBQ2IsMERBQTBEO1lBQzFELElBQUksTUFBTSxFQUFFO2dCQUNSLGtHQUFrRztnQkFDbEcsaUVBQW9CLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUM3RDtZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELG9DQUFNLEdBQU4sVUFBTyxJQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBQ0QseUNBQVcsR0FBWCxVQUFZLElBQWM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFHSCw4Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPO1lBQ0gsY0FBYyxFQUFFLGtCQUFrQjtZQUNsQyxlQUFlLEVBQUUsOENBQU0sQ0FBQyxVQUFVO1NBQ3JDO0lBQ0wsQ0FBQztJQUVELG1DQUFtQztJQUNoQyxzRUFBc0U7SUFDekUsR0FBRztJQUVILDBDQUFZLEdBQVosVUFBYSxLQUFlO1FBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ25DLE9BQU8sdURBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLDZGQUE2RjtJQUM3RixvQ0FBTSxHQUFOLFVBQU8sSUFBVTtRQUNiLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcsU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFDLEVBQUMsWUFBWSxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQUVELG9DQUFNLEdBQU4sVUFBTyxHQUFXLEVBQUUsSUFBVTtRQUMxQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLDhDQUFNLENBQUMsTUFBTSxHQUFHLGdCQUFnQixHQUFHLEdBQUcsRUFBRSxJQUFJLEVBQUMsRUFBQyxZQUFZLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRU0scUNBQU8sR0FBZCxVQUFlLElBQVE7UUFDbkIsaUVBQW9CLENBQUMsYUFBYSxFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRU0seUNBQVcsR0FBbEI7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsaUVBQW9CLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVU7SUFDdEUsQ0FBQzs7Z0JBekZ5QiwrREFBVTs7SUFEM0IsbUJBQW1CO1FBRC9CLGdFQUFVLEVBQUU7eUNBRWlCLCtEQUFVO09BRDNCLG1CQUFtQixDQTJGL0I7SUFBRCwwQkFBQztDQUFBO0FBM0YrQjs7Ozs7Ozs7O0FDWGhDO0FBQUE7QUFBQTtJQUFBO0lBY0UsQ0FBQztJQUFELFdBQUM7QUFBRCxDQUFDOzs7Ozs7Ozs7O0FDZEg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTJDO0FBQ2tDO0FBQy9CO0FBQ1E7QUFFSTtBQUl2QjtBQUduQztJQUNJLHFCQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO0lBQUksQ0FBQztJQUV6Qyw4QkFBUSxHQUFSLFVBQVMsSUFBVTtRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNsQyxPQUFPLHVEQUFVLENBQUMsb0RBQW9ELENBQUMsQ0FBQztTQUMzRTtRQUVELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ2pCLDhDQUFNLENBQUMsTUFBTSxHQUFHLE9BQU8sR0FBRyw4Q0FBTSxDQUFDLE1BQU0sRUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNYLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN2QixLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDcEIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1NBQzFCLENBQUMsRUFDRixFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxDQUN2QyxDQUFDLElBQUksQ0FDRixpRUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FDaEMsQ0FBQztJQUNOLENBQUM7SUFFRCwyQkFBSyxHQUFMLFVBQU0sSUFBVTtRQUNaLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQ2pCLDhDQUFNLENBQUMsTUFBTSxHQUFHLHFCQUFxQixFQUNyQyxDQUFDO1lBQ0csUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtTQUMxQixDQUFDLENBQ0wsQ0FBQyxJQUFJLENBQ0YsMERBQUcsQ0FBQyxrQkFBUSxJQUFJLGVBQVEsRUFBUixDQUFRLENBQUMsRUFDekIsaUVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQ2hDLENBQUM7SUFFTixDQUFDO0lBRUQsNEJBQU0sR0FBTixVQUFPLElBQVU7UUFDYixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDhDQUFNLENBQUMsTUFBTSxHQUFHLHFCQUFxQixFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUM3RyxTQUFTLENBQUMsZ0JBQU07WUFDYiwwREFBMEQ7WUFDMUQsSUFBSSxNQUFNLEVBQUU7Z0JBQ1Isa0dBQWtHO2dCQUNsRyxpRUFBb0IsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQzdEO1lBRUQsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsbUNBQWEsR0FBYixVQUFjLElBQVc7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDbEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDakIsOENBQU0sQ0FBQyxNQUFNLEdBQUcsc0JBQXNCLEVBQUUsSUFBSTtRQUM1Qzs7OztZQUlJLEdBQUMsRUFBQyxZQUFZLEVBQUUsTUFBTSxFQUFDLENBQzlCLENBQUMsSUFBSSxDQUNGLDBEQUFHLENBQUMsa0JBQVEsSUFBSSxlQUFRLEVBQVIsQ0FBUSxDQUFDLEVBQ3pCLGlFQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUNoQyxDQUFDO0lBRU4sQ0FBQztJQUVELDRCQUFNLEdBQU4sVUFBTyxJQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxpQkFBaUIsRUFBRSxJQUFJLEVBQUMsRUFBQyxZQUFZLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQ3RGLDBEQUFHLENBQUMsa0JBQVEsSUFBSSxlQUFRLEVBQVIsQ0FBUSxDQUFDLEVBQ3pCLGlFQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUNoQyxDQUFDO1FBQUEsQ0FBQztJQUNQLENBQUM7SUFFRCwwQ0FBb0IsR0FBcEIsVUFBcUIsSUFBYztRQUFkLGdDQUFjO1FBQy9CLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcscUJBQXFCLEVBQUUsSUFBSSxFQUFDLEVBQUMsWUFBWSxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUMsSUFBSSxDQUMxRiwwREFBRyxDQUFDLGtCQUFRLElBQUksZUFBUSxFQUFSLENBQVEsQ0FBQyxFQUN6QixpRUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FDaEMsQ0FBQztRQUFBLENBQUM7SUFDUCxDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLElBQWM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFSCxrQ0FBWSxHQUFaLFVBQWEsSUFBVztRQUVwQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDbEQsSUFBSSxNQUFNLEdBQUcsbUJBQU8sQ0FBQyxpRUFBOEIsQ0FBQyxDQUFDO1FBQ3JELElBQUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFFN0MsSUFBSSxPQUFPLEdBQUc7WUFDVixHQUFHLEVBQUUsOENBQU0sQ0FBQyxNQUFNLEdBQUcsVUFBVTtZQUMvQixNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRTtnQkFDTCxjQUFjLEVBQUUsMEJBQTBCO2dCQUMxQyxXQUFXLEVBQUUsSUFBSTthQUNwQjtZQUNELFdBQVcsRUFBRSxZQUFZLEdBQUcsSUFBSTtZQUNoQyw0Q0FBNEM7WUFDNUMsNEJBQTRCLEVBQUUsSUFBSTtTQUNyQyxDQUFDO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVqQixJQUFJLElBQUksR0FBRSxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixDQUFDLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELHVDQUFpQixHQUFqQjtRQUNJLE9BQU87WUFDSCxjQUFjLEVBQUUsMEJBQTBCO1lBQzFDLFdBQVcsRUFBRSxJQUFJO1NBQ3BCO0lBQ0wsQ0FBQztJQUNELHNDQUFnQixHQUFoQjtRQUNJLE9BQU87WUFDSCxjQUFjLEVBQUUsa0JBQWtCO1lBQ2xDLGVBQWUsRUFBRSw4Q0FBTSxDQUFDLFVBQVU7U0FDckM7SUFDTCxDQUFDO0lBRUQsbUNBQW1DO0lBQ2hDLHNFQUFzRTtJQUN6RSxHQUFHO0lBRUgsa0NBQVksR0FBWixVQUFhLEtBQWU7UUFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDbkMsT0FBTyx1REFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsNkZBQTZGO0lBQzdGLDRCQUFNLEdBQU4sVUFBTyxJQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUMsRUFBQyxZQUFZLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQztJQUM1RixDQUFDO0lBRUQ7O09BRUc7SUFFSCxvQ0FBYyxHQUFkLFVBQWUsVUFBa0I7UUFDN0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBTSw4Q0FBTSxDQUFDLE1BQU0sR0FBRywyQkFBMkIsR0FBRyxVQUFVLENBQUMsQ0FBQztRQUNwRixzRUFBc0U7SUFDMUUsQ0FBQztJQUVEOzs7OztPQUtHO0lBRUgsc0NBQWdCLEdBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUSw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ25FLENBQUM7SUFFRCw2Q0FBdUIsR0FBdkIsVUFBd0IsSUFBVTtRQUM5QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDhDQUFNLENBQUMsTUFBTSxHQUFHLGdDQUFnQyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDOUUsMERBQUcsQ0FBQyxrQkFBUSxJQUFJLGVBQVEsRUFBUixDQUFRLENBQUMsRUFDekIsaUVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQ2hDLENBQUM7SUFDTixDQUFDO0lBRUQsU0FBUztJQUVULGdDQUFVLEdBQVYsVUFBVyxRQUFnQjtRQUN2QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFNLDhDQUFNLENBQUMsTUFBTSxHQUFHLG9CQUFvQixHQUFHLFFBQVEsQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFRCxRQUFRO0lBRVIsOEJBQVEsR0FBUixVQUFTLE1BQWM7UUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBTSw4Q0FBTSxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsR0FBRyxNQUFNLENBQUMsQ0FBQztJQUMzRSxDQUFDO0lBRUQsNEJBQU0sR0FBTixVQUFPLEdBQVcsRUFBRSxJQUFVO1FBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsOENBQU0sQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLEdBQUcsR0FBRyxFQUFFLElBQUksRUFBQyxFQUFDLFlBQVksRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFFTSw2QkFBTyxHQUFkLFVBQWUsSUFBUTtRQUNuQixpRUFBb0IsQ0FBQyxhQUFhLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFTSxpQ0FBVyxHQUFsQjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxpRUFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVTtJQUN0RSxDQUFDOztnQkF4THlCLCtEQUFVOztJQUQzQixXQUFXO1FBRHZCLGdFQUFVLEVBQUU7eUNBRWlCLCtEQUFVO09BRDNCLFdBQVcsQ0EyTHZCO0lBQUQsa0JBQUM7Q0FBQTtBQTNMdUI7Ozs7Ozs7O0FDWnhCLDhCQUE4Qiw2QkFBNkIsb0JBQW9CLGlCQUFpQixLQUFLLGVBQWUsd0JBQXdCLGlCQUFpQixvQkFBb0IsMENBQTBDLHVCQUF1QixvQkFBb0IsS0FBSywrQkFBK0IsMkNBQTJDLEtBQUssNEJBQTRCLDBDQUEwQyxLQUFLLEs7Ozs7Ozs7QUNBcmEsd2hEQUF3aEQsdUJBQXVCLEdBQUcsc0JBQXNCLCthQUErYSwwQkFBMEIsR0FBRyx5QkFBeUIsbVdBQW1XLHlCQUF5QiwyU0FBMlMseUJBQXlCLHFYQUFxWCxtQkFBbUIsMGZBQTBmLGtCQUFrQixxYkFBcWIsWUFBWSw0YUFBNGEsT0FBTyxtd0JBQW13Qix1TkFBdU4sZUFBZSxtTUFBbU0sY0FBYywyTkFBMk4sY0FBYywyTkFBMk4sY0FBYyxxVUFBcVUsc0ZBQXNGLGVBQWUsNFRBQTRULGNBQWMsZ0JBQWdCLCtEQUErRCxpSkFBaUosY0FBYyxnQkFBZ0IsK0RBQStELGlKQUFpSixjQUFjLGdCQUFnQiwrREFBK0QsNGZBQTRmLDBGQUEwRixlQUFlLCtVQUErVSxnQkFBZ0IsZ0JBQWdCLCtFQUErRSx3SkFBd0osZ0JBQWdCLGdCQUFnQiwrRUFBK0Usd0pBQXdKLGdCQUFnQixnQkFBZ0IsK0VBQStFLG95RTs7Ozs7Ozs7QUNBajlSO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5RTtBQUNWO0FBSWY7QUFFQztBQUdTO0FBTzFEO0lBcUNJLDBCQUFxQixXQUF3QixFQUFTLGdCQUFrQyxFQUFTLElBQVU7UUFBdEYsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBUyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVMsU0FBSSxHQUFKLElBQUksQ0FBTTtRQXhCM0csY0FBUyxHQUFVLEVBQUUsQ0FBQztRQUV0QixnQkFBVyxHQUFTO1lBQ2hCO2dCQUNFLFVBQVUsRUFBRSwwQ0FBMEM7Z0JBQ3RELFNBQVMsRUFDVDtvQkFDRSxHQUFHLEVBQUUsSUFBSTtvQkFDVCxHQUFHLEVBQUUsR0FBRztvQkFDUixHQUFHLEVBQUUsSUFBSTtpQkFDVjthQUNGO1lBQ0Q7Z0JBQ0EsVUFBVSxFQUFFLDBDQUEwQztnQkFDdEQsU0FBUyxFQUNMO29CQUNBLEdBQUcsRUFBRSxJQUFJO29CQUNULEdBQUcsRUFBRSxHQUFHO29CQUNSLEdBQUcsRUFBRSxHQUFHO2lCQUNQO2FBQ0w7U0FDSCxDQUFDO1FBSUUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLDREQUFJLEVBQUUsQ0FBQztRQUU5QixJQUFJLENBQUMsUUFBUSxHQUFHLGlCQUFpQixDQUFDO1FBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxVQUFVLEdBQUcsYUFBYSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxlQUFlLEdBQUcsR0FBRyxDQUFDO1FBRTNCLHFFQUFxRTtRQUNyRSxJQUFJLENBQUMsV0FBVyxHQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFO1FBQy9DLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUMxQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQzlDLENBQUM7SUFJSixtQ0FBUSxHQUFSO1FBQ08sSUFBSSxDQUFDLFFBQVEsRUFBRTtJQUNuQixDQUFDO0lBR08sbUNBQVEsR0FBaEI7UUFBQSxpQkFjQztRQWJHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLGVBQUs7WUFDbEUsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsS0FBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsaUJBQU87Z0JBQ3RFLEtBQUksQ0FBQyxjQUFjLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25DLENBQUMsRUFDRCxlQUFLO2dCQUNILEtBQUssQ0FBQyx5RkFBeUYsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUMzRyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsRUFDRCxlQUFLO1lBQ0gsS0FBSyxDQUFDLHNGQUFzRixHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQ3hHLENBQUMsQ0FDRixDQUFDO0lBQ0osQ0FBQztJQUVELG9DQUFTLEdBQVQsVUFBVSxJQUFtQjtRQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELHlDQUFjLEdBQWQsVUFBZSxLQUFhO1FBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEdBQUcsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDBDQUFlLEdBQWYsVUFBZ0IsS0FBSztRQUNqQixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ2pFLENBQUM7SUFFRCwyQ0FBZ0IsR0FBaEIsVUFBaUIsS0FBSztRQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQzdELENBQUM7SUFFRCx5Q0FBYyxHQUFkO1FBRUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBQ0QscUNBQVUsR0FBVjtRQUNJLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7OztPQWFHO0lBRUssc0NBQVcsR0FBbkIsVUFBb0IsS0FBSyxFQUFFLFdBQVc7UUFFbEMsSUFBSSxXQUFXLElBQUksR0FBRyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUNoRixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQzlDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7U0FDakQ7YUFBTSxJQUFJLFdBQVcsSUFBSSxHQUFHLEVBQUU7WUFDM0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDL0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztTQUNqRDthQUFNLElBQUksV0FBVyxJQUFJLEdBQUcsRUFBRTtZQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQzlDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDOUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO1NBQ2xGO0lBQ0wsQ0FBQztJQUVELHVDQUFZLEdBQVosVUFBYSxLQUFLO1FBQ2QsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDdkMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDO1NBQzFDO2FBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDOUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDO1NBQzFDO2FBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDOUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDO1NBQzFDO2FBQ0c7WUFDQSxLQUFLLENBQUMsb0NBQW9DLENBQUM7U0FDOUM7SUFDTCxDQUFDO0lBRUQsd0NBQWEsR0FBYixVQUFjLEtBQUs7UUFDZixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxFQUFFO1lBQzdCLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQztTQUMxQzthQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUU7WUFDcEMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDO1NBQzFDO2FBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssRUFBRTtZQUNwQyxLQUFLLENBQUMsZ0NBQWdDLENBQUM7U0FDMUM7YUFDRztZQUNBLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQztTQUM5QztJQUNMLENBQUM7SUFFTyx1Q0FBWSxHQUFwQixVQUFxQixLQUFLLEVBQUUsV0FBVztRQUVuQyxJQUFJLFdBQVcsSUFBSSxHQUFHLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUM1RCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1NBQ3ZDO2FBQU0sSUFBSSxXQUFXLElBQUksR0FBRyxFQUFFO1lBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQzNELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztTQUN2QzthQUFNLElBQUksV0FBVyxJQUFJLEdBQUcsRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUM7U0FDOUQ7SUFDTCxDQUFDO0lBR0Qsa0NBQU8sR0FBUDtRQUFBLGlCQVlDO1FBWEcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQzlELGVBQUs7WUFDRCxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDcEMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1lBQy9CLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNoQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDaEMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3BDLENBQUMsRUFDRCxhQUFHO1lBQ0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDdEUsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBQ0QsaUNBQU0sR0FBTjtRQUFBLGlCQVNEO1FBUkssSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2xELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQ2pFLGNBQUk7WUFDQSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUMsRUFBQyxZQUFZLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztRQUNwRSxDQUFDLEVBQ0QsZUFBSztZQUNELEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxDQUFDO0lBQ2pCLENBQUM7O2dCQXBLbUMscUVBQVc7Z0JBQTJCLDRFQUFnQjtnQkFBZSw2REFBSTs7SUFyQ2xHLGdCQUFnQjtRQU41QiwrREFBUyxDQUFDO1lBQ1YsUUFBUSxFQUFFLFNBQVM7WUFFbkIsMEZBQXVDOztTQUV2QyxDQUFDO3lDQXNDb0MscUVBQVcsRUFBMkIsNEVBQWdCLEVBQWUsNkRBQUk7T0FyQ2xHLGdCQUFnQixDQTRNNUI7SUFBRCx1QkFBQztDQUFBO0FBNU00Qjs7Ozs7Ozs7QUNqQjdCLDhCQUE4Qiw2QkFBNkIsb0JBQW9CLGlCQUFpQixLQUFLLEM7Ozs7Ozs7QUNBckcsd2pCOzs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBa0Q7QUFDYTtBQVEvRDtJQUdJLDBCQUFvQixnQkFBa0M7UUFBbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtJQUN0RCxDQUFDO0lBRUosbUNBQVEsR0FBUjtJQUNHLENBQUM7SUFFRCxpQ0FBTSxHQUFOO1FBRUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Z0JBVHFDLDRFQUFnQjs7SUFIN0MsZ0JBQWdCO1FBTjVCLCtEQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsU0FBUztZQUVuQiwwRkFBdUM7O1NBRXZDLENBQUM7eUNBSXdDLDRFQUFnQjtPQUg3QyxnQkFBZ0IsQ0FhNUI7SUFBRCx1QkFBQztDQUFBO0FBYjRCIiwiZmlsZSI6ImJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIHdlYnBhY2tFbXB0eUFzeW5jQ29udGV4dChyZXEpIHtcblx0Ly8gSGVyZSBQcm9taXNlLnJlc29sdmUoKS50aGVuKCkgaXMgdXNlZCBpbnN0ZWFkIG9mIG5ldyBQcm9taXNlKCkgdG8gcHJldmVudFxuXHQvLyB1bmNhdWdodCBleGNlcHRpb24gcG9wcGluZyB1cCBpbiBkZXZ0b29sc1xuXHRyZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCkudGhlbihmdW5jdGlvbigpIHtcblx0XHR2YXIgZSA9IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIgKyByZXEgKyBcIidcIik7XG5cdFx0ZS5jb2RlID0gJ01PRFVMRV9OT1RfRk9VTkQnO1xuXHRcdHRocm93IGU7XG5cdH0pO1xufVxud2VicGFja0VtcHR5QXN5bmNDb250ZXh0LmtleXMgPSBmdW5jdGlvbigpIHsgcmV0dXJuIFtdOyB9O1xud2VicGFja0VtcHR5QXN5bmNDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQ7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tFbXB0eUFzeW5jQ29udGV4dDtcbndlYnBhY2tFbXB0eUFzeW5jQ29udGV4dC5pZCA9IFwiLi4vJCRfbGF6eV9yb3V0ZV9yZXNvdXJjZSBsYXp5IHJlY3Vyc2l2ZVwiOyIsImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJnci1hcHBcIixcbiAgdGVtcGxhdGU6IFwiPHBhZ2Utcm91dGVyLW91dGxldD48L3BhZ2Utcm91dGVyLW91dGxldD5cIlxufSlcbmV4cG9ydCBjbGFzcyBBcHBDb21wb25lbnQgeyB9IiwiZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwibmF0aXZlc2NyaXB0LXRoZW1lLWNvcmUvY3NzL2NvcmUubGlnaHQuY3NzXCIsICgpID0+IHJlcXVpcmUoXCIhbmF0aXZlc2NyaXB0LWRldi13ZWJwYWNrL2NzczJqc29uLWxvYWRlcj91c2VGb3JJbXBvcnRzIW5hdGl2ZXNjcmlwdC10aGVtZS1jb3JlL2Nzcy9jb3JlLmxpZ2h0LmNzc1wiKSk7bW9kdWxlLmV4cG9ydHMgPSB7XCJ0eXBlXCI6XCJzdHlsZXNoZWV0XCIsXCJzdHlsZXNoZWV0XCI6e1wicnVsZXNcIjpbe1widHlwZVwiOlwiaW1wb3J0XCIsXCJpbXBvcnRcIjpcIlxcXCJuYXRpdmVzY3JpcHQtdGhlbWUtY29yZS9jc3MvY29yZS5saWdodC5jc3NcXFwiXCJ9LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5idG4tcHJpbWFyeVwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImhlaWdodFwiLFwidmFsdWVcIjpcIjUwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcInJnYigyNiwgOTEsIDIxMylcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiNVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjIwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC13ZWlnaHRcIixcInZhbHVlXCI6XCI2MDBcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuYnRuLXNlY29uZFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImhlaWdodFwiLFwidmFsdWVcIjpcIjM1XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcInJnYigxMywgMTAwLCAxNzIpXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZXNtb2tlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxMlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtd2VpZ2h0XCIsXCJ2YWx1ZVwiOlwiNjAwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwid2lkdGhcIixcInZhbHVlXCI6XCIxMDVcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuYnRuLXByaW1hcnk6ZGlzYWJsZWRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJvcGFjaXR5XCIsXCJ2YWx1ZVwiOlwiMC41XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmJ0bi1zZWNvbmQ6ZGlzYWJsZWRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJvcGFjaXR5XCIsXCJ2YWx1ZVwiOlwiMC41XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmNhcmRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMjBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW5cIixcInZhbHVlXCI6XCIxMlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInBhZGRpbmdcIixcInZhbHVlXCI6XCIxMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCIjRTNFOUY4XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCIjMTMxNjM2XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiaGVpZ2h0XCIsXCJ2YWx1ZVwiOlwiODYlXCJ9XX1dLFwicGFyc2luZ0Vycm9yc1wiOltdfX07O1xuICAgIGlmIChtb2R1bGUuaG90KSB7XG4gICAgICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6ICdzdHlsZScsIHBhdGg6ICcuL2FwcC5jc3MnIH0pO1xuICAgICAgICB9KVxuICAgIH1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2Zvcm1zXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2h0dHAtY2xpZW50XCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbmF0aXZlc2NyaXB0Lm1vZHVsZVwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuXG5pbXBvcnQgeyBBcHBSb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vYXBwLnJvdXRpbmdcIjtcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gXCIuL2FwcC5jb21wb25lbnRcIjtcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSBcIi4vbG9naW4vbG9naW4uY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSZWdpc3RlckNvbXBvbmVudCB9IGZyb20gJy4vcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50JztcbmltcG9ydCB7IFJlZ2lzdGVyMkNvbXBvbmVudCB9IGZyb20gJy4vcmVnaXN0ZXIyL3JlZ2lzdGVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTdHVkZW50Q29tcG9uZW50IH0gZnJvbSAnLi9zdHVkZW50L3N0dWRlbnQuY29tcG9uZW50JztcbmltcG9ydCB7IFRlYWNoZXJDb21wb25lbnQgfSBmcm9tICcuL3RlYWNoZXIvdGVhY2hlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgU2Vzc2lvbl9jb2RlQ29tcG9uZW50IH0gZnJvbSAnLi9zZXNzaW9uX2NvZGUvc2Vzc2lvbl9jb2RlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBJZGVudGlmaWNhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vaWRlbnRpZmljYXRpb24vaWRlbnRpZmljYXRpb24uY29tcG9uZW50JztcbmltcG9ydCB7IFJlc2V0X1Bhc3N3b3JkQ29tcG9uZW50IH0gZnJvbSAnLi9yZXNldF9wYXNzd29yZC9yZXNldF9wYXNzd29yZC5jb21wb25lbnQnO1xuXG5cbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGVTZXJ2aWNlIH0gZnJvbSBcIi4vc2hhcmVkL3VzZXIvYXV0aGVudGljYXRlLnNlcnZpY2VcIjtcblxuaW1wb3J0IHsgSnd0SW50ZXJjZXB0b3JQcm92aWRlciwgRXJyb3JJbnRlcmNlcHRvclByb3ZpZGVyIH0gZnJvbSAnLi9oZWxwZXJzL2luZGV4JztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlLFxuICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXG4gICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXG4gICAgQXBwUm91dGluZ01vZHVsZSxcbiAgICBOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEFwcENvbXBvbmVudCxcbiAgICBMb2dpbkNvbXBvbmVudCxcbiAgICBSZWdpc3RlckNvbXBvbmVudCxcbiAgICBSZWdpc3RlcjJDb21wb25lbnQsXG4gICAgU3R1ZGVudENvbXBvbmVudCxcbiAgICBUZWFjaGVyQ29tcG9uZW50LFxuICAgIFNlc3Npb25fY29kZUNvbXBvbmVudCxcbiAgICBJZGVudGlmaWNhdGlvbkNvbXBvbmVudCxcbiAgICBSZXNldF9QYXNzd29yZENvbXBvbmVudFxuICBdLFxuICBwcm92aWRlcnM6IFtcbiAgICBVc2VyU2VydmljZSxcbiAgICBBdXRoZW50aWNhdGVTZXJ2aWNlLFxuICAgIEp3dEludGVyY2VwdG9yUHJvdmlkZXJcbiAgICAvL0Vycm9ySW50ZXJjZXB0b3JQcm92aWRlclxuXSxcbiAgYm9vdHN0cmFwOiBbQXBwQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBBcHBNb2R1bGUgeyB9XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlcyB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBMb2dpbkNvbXBvbmVudCB9IGZyb20gJy4vbG9naW4vbG9naW4uY29tcG9uZW50JztcclxuXHJcbmltcG9ydCB7IFJlZ2lzdGVyQ29tcG9uZW50IH0gZnJvbSAnLi9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBSZWdpc3RlcjJDb21wb25lbnQgfSBmcm9tICcuL3JlZ2lzdGVyMi9yZWdpc3Rlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTdHVkZW50Q29tcG9uZW50IH0gZnJvbSAnLi9zdHVkZW50L3N0dWRlbnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGVhY2hlckNvbXBvbmVudCB9IGZyb20gJy4vdGVhY2hlci90ZWFjaGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlc3Npb25fY29kZUNvbXBvbmVudCB9IGZyb20gJy4vc2Vzc2lvbl9jb2RlL3Nlc3Npb25fY29kZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBJZGVudGlmaWNhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vaWRlbnRpZmljYXRpb24vaWRlbnRpZmljYXRpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgUmVzZXRfUGFzc3dvcmRDb21wb25lbnQgfSBmcm9tICcuL3Jlc2V0X3Bhc3N3b3JkL3Jlc2V0X3Bhc3N3b3JkLmNvbXBvbmVudCc7XHJcblxyXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcclxuICAgIHsgcGF0aDogXCJcIiwgcmVkaXJlY3RUbzogXCIvbG9naW5cIiwgcGF0aE1hdGNoOiBcImZ1bGxcIiB9LFxyXG4gICAgeyBwYXRoOiBcImxvZ2luXCIsIGNvbXBvbmVudDogTG9naW5Db21wb25lbnQgfSxcclxuICAgIHsgcGF0aDogXCJyZWdpc3RlclwiLCBjb21wb25lbnQ6IFJlZ2lzdGVyQ29tcG9uZW50IH0sXHJcbiAgICB7IHBhdGg6IFwicmVnaXN0ZXIyXCIsIGNvbXBvbmVudDogUmVnaXN0ZXIyQ29tcG9uZW50IH0sXHJcbiAgICB7IHBhdGg6IFwic3R1ZGVudFwiLCBjb21wb25lbnQ6IFN0dWRlbnRDb21wb25lbnQgfSxcclxuICAgIHsgcGF0aDogXCJ0ZWFjaGVyXCIsIGNvbXBvbmVudDogVGVhY2hlckNvbXBvbmVudCB9LFxyXG4gICAgeyBwYXRoOiBcInNlc3Npb25fY29kZVwiLCBjb21wb25lbnQ6IFNlc3Npb25fY29kZUNvbXBvbmVudCB9LFxyXG4gICAgeyBwYXRoOlwiaWRlbnRpZmljYXRpb25cIiwgY29tcG9uZW50OiBJZGVudGlmaWNhdGlvbkNvbXBvbmVudCB9LFxyXG4gICAgeyBwYXRoOlwicmVzZXRfcGFzc3dvcmRcIiwgY29tcG9uZW50OiBSZXNldF9QYXNzd29yZENvbXBvbmVudCB9XHJcblxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yUm9vdChyb3V0ZXMpXSxcclxuICAgIGV4cG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBSb3V0aW5nTW9kdWxlIHsgfSIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBSZXF1ZXN0LCBIdHRwSGFuZGxlciwgSHR0cEV2ZW50LCBIdHRwSW50ZXJjZXB0b3IsIEhUVFBfSU5URVJDRVBUT1JTIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJ1xuaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL3Rocm93J1xuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9jYXRjaCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBFcnJvckludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcbiAgICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgICAgIC8vIGV4dHJhY3QgZXJyb3IgbWVzc2FnZSBmcm9tIGh0dHAgYm9keSBpZiBhbiBlcnJvciBvY2N1cnNcbiAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpLmNhdGNoKGVycm9yUmVzcG9uc2UgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyb3JSZXNwb25zZS5lcnJvcilcbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgY29uc3QgRXJyb3JJbnRlcmNlcHRvclByb3ZpZGVyID0ge1xuICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgIHVzZUNsYXNzOiBFcnJvckludGVyY2VwdG9yLFxuICAgIG11bHRpOiB0cnVlLFxufTsiLCJleHBvcnQgKiBmcm9tICcuL2Vycm9yLmludGVyY2VwdG9yJztcbmV4cG9ydCAqIGZyb20gJy4vand0LmludGVyY2VwdG9yJzsiLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIsIEh0dHBFdmVudCwgSHR0cEludGVyY2VwdG9yLCBIVFRQX0lOVEVSQ0VQVE9SUyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcydcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEp3dEludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcbiAgICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgICAgIC8vIGFkZCBhdXRob3JpemF0aW9uIGhlYWRlciB3aXRoIGp3dCB0b2tlbiBpZiBhdmFpbGFibGVcbiAgICAgICAgbGV0IGN1cnJlbnRVc2VyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSk7XG4gICAgICAgIGlmIChjdXJyZW50VXNlciAmJiBjdXJyZW50VXNlci50b2tlbikge1xuICAgICAgICAgICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoe1xuICAgICAgICAgICAgICAgIHNldEhlYWRlcnM6IHsgXG4gICAgICAgICAgICAgICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtjdXJyZW50VXNlci50b2tlbn1gXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxufVxuXG5leHBvcnQgY29uc3QgSnd0SW50ZXJjZXB0b3JQcm92aWRlciA9IHtcbiAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICB1c2VDbGFzczogSnd0SW50ZXJjZXB0b3IsXG4gICAgbXVsdGk6IHRydWUsXG59OyIsIm1vZHVsZS5leHBvcnRzID0gXCIuaG9tZS1wYW5lbHtcXHJcXG5cXHR2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xcclxcblxcdGZvbnQtc2l6ZTogMjA7XFxyXFxuXFx0bWFyZ2luOiAxNTtcXHJcXG59XFxyXFxuXFxyXFxuLmhlYWRlciB7XFxyXFxuXFx0aG9yaXpvbnRhbC1hbGlnbjogY2VudGVyO1xcclxcblxcdGZvbnQtc2l6ZTogMjU7XFxyXFxuXFx0Zm9udC13ZWlnaHQ6IDYwMDtcXHJcXG5cXHRtYXJnaW4tYm90dG9tOiAyMDtcXHJcXG5cXHR0ZXh0LWFsaWduOiBjZW50ZXI7XFxyXFxuXFx0Y29sb3I6IHJnYigzOCwgOCwgOTkpO1xcclxcbiAgfVxcclxcblxcclxcbiAgLmJ0bi1pbWd7XFxyXFxuICAgIGJvcmRlci1yYWRpdXM6IDU7XFxyXFxuICAgIGJvcmRlci13aWR0aDogMTtcXHJcXG4gICAgY29sb3I6IGJsYWNrO1xcclxcblxcdG1hcmdpbjogMjU7XFxyXFxuXFx0bWFyZ2luLXRvcDogMTU7XFxyXFxuICAgIGZvbnQtc2l6ZTogMzU7XFxyXFxuICAgIGJvcmRlci1jb2xvcjogIzJiM2M2YTtcXHJcXG5cXHRiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjI2LCAyMjcsIDIzNik7XFxyXFxufVxcclxcblxcclxcbi5idDJ7XFxyXFxuXFx0Ym9yZGVyLXJhZGl1czogNTtcXHJcXG5cXHRib3JkZXItd2lkdGg6IDE7XFxyXFxuXFx0Ym9yZGVyLWNvbG9yOiAjMmIzYzZhO1xcclxcblxcdGJhY2tncm91bmQtY29sb3I6cmdiKDI1MCwgMjI3LCAyMjcpXFxyXFxufVwiIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxBY3Rpb25CYXIgdGl0bGU9XFxcIlN0dWRlbnQgSWRlbnRpZmljYXRpb25cXFwiIGNsYXNzPVxcXCJhY3Rpb24tYmFyXFxcIj5cXHJcXG48L0FjdGlvbkJhcj5cXHJcXG5cXHJcXG48U3RhY2tMYXlvdXQ+XFxyXFxuICAgIDxJbWFnZSBjbGFzcz1cXFwibG9nb1xcXCIgc3JjPVxcXCJ+L2ltYWdlcy9Mb2dvX3BvbGl0ZWNuaWNhLnBuZ1xcXCI+PC9JbWFnZT5cXHJcXG4gICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgzIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICA8TGFiZWwgIGNsYXNzPVxcXCJoZWFkZXJcXFwiIHRleHQ9XFxcIlJlZ2lzdHJvIGRlIGFzaXN0ZW5jaWEgZW4gY2xhc2VcXFwiXFxyXFxuICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgY2xhc3M9XFxcImhlYWRlclxcXCI+PC9MYWJlbD5cXHJcXG5cXHJcXG4gICAgPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJidG4taW1nXFxcIiAgb3JpZW50YXRpb249XFxcImhvcml6b250YWxcXFwiIHBhZGRpbmc9XFxcIjVcXFwiICh0YXApPVxcXCJzY2FuQmFyY29kZSgpXFxcIj5cXHJcXG4gICAgICAgIDxJbWFnZSBzcmM9XFxcIn4vaW1hZ2VzL3FyLWNvZGUucG5nXFxcIiAgd2lkdGg9XFxcIjQwJVxcXCIgaGVpZ2h0PVxcXCIyMiVcXFwiICBtYXJnaW5SaWdodD1cXFwiMTBcXFwiPjwvSW1hZ2U+XFxyXFxuICAgICAgICA8TGFiZWwgdGV4dD1cXFwiTGVjdG9yIFFSXFxcIiB2ZXJ0aWNhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgPC9TdGFja0xheW91dD5cXHJcXG4gICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwiYnRuLWltZ1xcXCIgIG9yaWVudGF0aW9uPVxcXCJob3Jpem9udGFsXFxcIiBwYWRkaW5nPVxcXCI1XFxcIiAodGFwKT1cXFwibWVuc2FqZVJlYWRORkMoKVxcXCIgKHRhcCk9XFxcIlJlYWRORkMoKVxcXCIgPlxcclxcbiAgICAgICAgPEltYWdlIHNyYz1cXFwifi9pbWFnZXMvbmZjLXJvdW5kLXRhZy5wbmdcXFwiICB3aWR0aD1cXFwiNDAlXFxcIiBoZWlnaHQ9XFxcIjMxJVxcXCIgIG1hcmdpblJpZ2h0PVxcXCIxMFxcXCI+PC9JbWFnZT5cXHJcXG4gICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJMZWN0b3IgTkZDXFxcIiB2ZXJ0aWNhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgPC9TdGFja0xheW91dD5cXHJcXG4gICAgIDwhLS0gXFxyXFxuICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImhvbWUtcGFuZWxcXFwiIHdpZHRoPVxcXCI4MCVcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8QnV0dG9uICAgdGV4dD1cXFwiTGVjdG9yIFFSXFxcIiBjbGFzcz1cXFwiaDEgdGV4dC1jZW50ZXJcXFwiIGJhY2tncm91bmQtY29sb3I6cmdiKDc2LCA5MSwgMjEzKSAodGFwKT1cXFwic2NhbkJhcmNvZGUoKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJoMyB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiAgY2xhc3M9XFxcImgxIHRleHQtY2VudGVyXFxcIiBiYWNrZ3JvdW5kLWNvbG9yOnJnYig3NiwgOTEsIDIxMykgKHRhcCk9XFxcIm1lbnNhamVSZWFkTkZDKClcXFwiICh0YXApPVxcXCJSZWFkTkZDKClcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPEZvcm1hdHRlZFN0cmluZz5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8U3BhbiB0ZXh0PVxcXCJMZWN0b3IgTkZDXFxcIiAgZm9udFdlaWdodD1cXFwiYm9sZFxcXCI+PC9TcGFuPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtYXR0ZWRTdHJpbmc+XFxyXFxuICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDMgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgIDxCdXR0b24gICB0ZXh0PVxcXCJNYW51YWxcXFwiIGJhY2tncm91bmQtY29sb3I6cmdiKDc2LCA5MSwgMjEzKSAodGFwKT1cXFwibW9zdHJhcm1lbnNhamUoKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJoMyB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICBcXHJcXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiAgIHRleHQ9XFxcIkVsZWdpciBDbGFzZVxcXCIgYmFja2dyb3VuZC1jb2xvcjpyZ2IoNzYsIDkxLCAyMTMpICh0YXApPVxcXCJvbkl0ZW1UYXBDbGFzc2VzKCRldmVudClcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICA8TGlzdFZpZXcgW2l0ZW1zXT1cXFwiY2xhc3Nlc1xcXCIgKGl0ZW1UYXApPVxcXCJvbkl0ZW1UYXBDbGFzc2VzKCRldmVudClcXFwiXFxyXFxuICAgICAgICAgICAgICAgICpuZ0lmPVxcXCJlbmFibGVBdWxhc1xcXCIgc3R5bGU9XFxcImhlaWdodDo1NTBweFxcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDxuZy10ZW1wbGF0ZSBsZXQtY2xhc3Nyb29tPVxcXCJpdGVtXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxGbGV4Ym94TGF5b3V0IGZsZXhEaXJlY3Rpb249XFxcInJvd1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIFt0ZXh0XT1cXFwiY2xhc3Nyb29tLmF1bGFcXFwiIGNsYXNzPVxcXCJ0LTEyXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2ZXJ0aWNhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBzdHlsZT1cXFwid2lkdGg6IDkwJVxcXCIgc3R5bGU9XFxcImhlaWdodDogMzAlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9GbGV4Ym94TGF5b3V0PlxcclxcbiAgICAgICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxcclxcbiAgICAgICAgICAgICAgICA8L0xpc3RWaWV3PlxcclxcbiAgICAgICAgICAgICAgICA8QnV0dG9uICB0ZXh0PVxcXCJFbGVnaXIgQXNpZW50b1xcXCIgYmFja2dyb3VuZC1jb2xvcjpyZ2IoNzYsIDkxLCAyMTMpICh0YXApPVxcXCJvbkl0ZW1UYXBTZWF0cygkZXZlbnQpXFxcIj48L0J1dHRvbj5cXHJcXG4gICAgICAgICAgICAgICAgPExpc3RWaWV3IFtpdGVtc109XFxcInNlYXRzXFxcIiAoaXRlbVRhcCk9XFxcIm9uSXRlbVRhcFNlYXRzKCRldmVudClcXFwiXFxyXFxuICAgICAgICAgICAgICAgICpuZ0lmPVxcXCJlbmFibGVTZWF0c1xcXCIgc3R5bGU9XFxcImhlaWdodDo1NTBweFxcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDxuZy10ZW1wbGF0ZSBsZXQtY2xhc3Nyb29tPVxcXCJzZWF0XFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxGbGV4Ym94TGF5b3V0IGZsZXhEaXJlY3Rpb249XFxcInJvd1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIFt0ZXh0XT1cXFwic2VhdC52YWx1ZVxcXCIgY2xhc3M9XFxcInQtMTJcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiIHN0eWxlPVxcXCJ3aWR0aDogNjAlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9GbGV4Ym94TGF5b3V0PlxcclxcbiAgICAgICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxcclxcbiAgICAgICAgICAgICAgICA8L0xpc3RWaWV3PlxcclxcbiAgICAgICAgICAgIC0tPlxcclxcblxcclxcbiAgICAgICAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwiaG9tZS1wYW5lbFxcXCIgd2lkdGg9XFxcIjcwJVxcXCIgPlxcclxcbiAgICAgICAgICAgICAgICA8QnV0dG9uICB0ZXh0PVxcXCJEaXJlY3RvICdBLTEwIEFzaWVudG86NCcgXFxcIiAgY2xhc3M9XFxcImJ0MlxcXCIgKHRhcCk9XFxcInVwZGF0ZTIoKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJoMyB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiB3aWR0aD1cXFwiNjAlXFxcIiB0ZXh0PVxcXCJWb2x2ZXJcXFwiICAodGFwKT1cXFwiYmFjaygpXFxcIiBiYWNrZ3JvdW5kY29sb3I9XFxcInJnYig3OSwgMTUyLCAyMzUpXFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICA8QnV0dG9uICB3aWR0aD1cXFwiNzAlXFxcIiB0ZXh0PVxcXCJDZXJyYXIgU2VzacOzblxcXCIgKHRhcCk9XFxcImxvZ291dCgpXFxcIiBiYWNrZ3JvdW5kY29sb3I9XFxcInJnYigyNiwgOTEsIDIxMylcXFwiIGNvbG9yPVxcXCJ3aGl0ZVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXHJcXG5cXHJcXG48L1N0YWNrTGF5b3V0PlwiIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZhbHVlUHJvdmlkZXIgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IHsgVXNlciB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLm1vZGVsXCI7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3VzZXIuc2VydmljZVwiO1xyXG5cclxuaW1wb3J0IHsgUGFnZSwgaG9yaXpvbnRhbEFsaWdubWVudFByb3BlcnR5IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xyXG5cclxuaW1wb3J0IHsgQmFyY29kZVNjYW5uZXIgfSBmcm9tICduYXRpdmVzY3JpcHQtYmFyY29kZXNjYW5uZXInO1xyXG5pbXBvcnQgeyBOZmMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LW5mY1wiO1xyXG5pbXBvcnQgeyBOZmNOZGVmRGF0YSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtbmZjXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogXCJpZGVudGlmaWNhdGlvblwiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHByb3ZpZGVyczogW1VzZXJTZXJ2aWNlXSxcclxuXHR0ZW1wbGF0ZVVybDogXCIuL2lkZW50aWZpY2F0aW9uLmNvbXBvbmVudC5odG1sXCIsXHJcblx0c3R5bGVVcmxzOiBbJy4vaWRlbnRpZmljYXRpb24uY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBJZGVudGlmaWNhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBjdXJyZW50VXNlcjogVXNlcjtcclxuICAgIG1lbnNhamU6IGFueTtcclxuICAgIG5mYyA9IG5ldyBOZmMoKTtcclxuICAgIGJvbDogYm9vbGVhbjtcclxuICAgIHZhbGlkX1FSOiBib29sZWFuO1xyXG5cclxuICAgIGNsYXNzZXMgOiBhbnlbXSA9W107XHJcblxyXG4gICAgZW5hYmxlQXVsYXMgPSBmYWxzZTtcclxuICAgIGVuYWJsZVNlYXRzID0gZmFsc2U7XHJcblxyXG4gICAgc2VhdHMgPSBbXHJcbiAgICAgICAge3ZhbHVlOiAnMSd9LHt2YWx1ZTogJzInfSx7dmFsdWU6ICczJ30se3ZhbHVlOiAnNCd9LHt2YWx1ZTogJzUnfSx7dmFsdWU6ICc2J30se3ZhbHVlOiAnNyd9LHt2YWx1ZTogJzgnfSx7dmFsdWU6ICc5J30se3ZhbHVlOiAnMTAnfSx7dmFsdWU6ICcxMSd9LHt2YWx1ZTogJzEyJ30se3ZhbHVlOiAnMTMnfSx7dmFsdWU6ICcxNCd9LHt2YWx1ZTogJzE1J30se3ZhbHVlOiAnMTYnfSx7dmFsdWU6ICcxNyd9LFxyXG4gICAgICAgIHt2YWx1ZTogJzE4J30se3ZhbHVlOiAnMTknfSx7dmFsdWU6ICcyMCd9LHt2YWx1ZTogJzIxJ30se3ZhbHVlOiAnMjInfSx7dmFsdWU6ICcyMyd9LHt2YWx1ZTogJzI0J30se3ZhbHVlOiAnMjUnfSx7dmFsdWU6ICcyNid9LHt2YWx1ZTogJzI3J30se3ZhbHVlOiAnMjgnfSx7dmFsdWU6ICcyOSd9LHt2YWx1ZTogJzMwJ30se3ZhbHVlOiAnMzAnfSx7dmFsdWU6ICczMSd9LHt2YWx1ZTogJzMyJ30se3ZhbHVlOiAnMzMnfVxyXG4gICAgICBdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXIgPSBuZXcgVXNlcigpO1xyXG4gICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xyXG5cclxuICAgICAgICB0aGlzLmN1cnJlbnRVc2VyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSk7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLmN1cnJlbnRVc2VyKSk7XHJcbiAgICAgICAgLy90aGlzLmN1cnJlbnRVc2VyPXRoaXMudXNlclNlcnZpY2UuY3VycmVudFVzZXIoKVxyXG4gICAgfVxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmxvYWRBbGxDbGFzc3Jvb21zKCk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIHByaXZhdGUgbG9hZEFsbENsYXNzcm9vbXMoKSB7XHJcbiAgICAgICAgdGhpcy51c2VyU2VydmljZS5nZXRBbGxDbGFzc3Jvb21zKCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgY2xhc3NlcyA9PiB7IHRoaXMuY2xhc3NlcyA9IGNsYXNzZXM7IH0pO1xyXG4gIH1cclxuICAgIGJhY2soKVxyXG4gICAge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXIuY29kZV9zZXNzaW9uPW51bGw7XHJcbiAgICAgICAgLy90aGlzLnVzZXJTZXJ2aWNlLnNldFVzZXIodGhpcy5jdXJyZW50VXNlcik7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50VXNlclwiLEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXIpKTtcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyh0aGlzLmN1cnJlbnRVc2VyLnNlYXQpO1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXIpKTtcclxuICAgICAgICB0aGlzLnVzZXJTZXJ2aWNlLnVwZGF0ZSh0aGlzLmN1cnJlbnRVc2VyKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vYWxlcnQoXCJSZWdpc3RyYWRvIGVuIGxhIGNsYXNlIGNvbiDDqXhpdG9cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2soKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgYWxlcnQoXCJFcnJvciBhbCB2b2x2ZXIgYSBzZWxlY2Npb25hciBjw7NkaWdvIGRlIHNlc3Npw7NuXCIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIFxyXG5cclxuICAgIHB1YmxpYyBvbkl0ZW1UYXBDbGFzc2VzKCkge1xyXG4gICAgICAgIHRoaXMuZW5hYmxlQXVsYXMgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbkl0ZW1UYXBTZWF0cygpIHtcclxuICAgICAgICB0aGlzLmVuYWJsZVNlYXRzID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2NhbkJhcmNvZGUoKSB7XHJcbiAgICAgICAgbmV3IEJhcmNvZGVTY2FubmVyKCkuc2Nhbih7XHJcbiAgICAgICAgICAgIGZvcm1hdHM6IFwiUVJfQ09ERSwgRUFOXzEzXCIsXHJcbiAgICAgICAgICAgIGNhbmNlbExhYmVsOiBcIkVYSVQuIEFsc28sIHRyeSB0aGUgdm9sdW1lIGJ1dHRvbnMhXCIsIC8vIGlPUyBvbmx5LCBkZWZhdWx0ICdDbG9zZSdcclxuICAgICAgICAgICAgY2FuY2VsTGFiZWxCYWNrZ3JvdW5kQ29sb3I6IFwiIzMzMzMzM1wiLCAvLyBpT1Mgb25seSwgZGVmYXVsdCAnIzAwMDAwMCcgKGJsYWNrKVxyXG4gICAgICAgICAgICBtZXNzYWdlOiBcIlVzZSB0aGUgdm9sdW1lIGJ1dHRvbnMgZm9yIGV4dHJhIGxpZ2h0XCIsIC8vIEFuZHJvaWQgb25seSwgZGVmYXVsdCBpcyAnUGxhY2UgYSBiYXJjb2RlIGluc2lkZSB0aGUgdmlld2ZpbmRlciByZWN0YW5nbGUgdG8gc2NhbiBpdC4nXHJcbiAgICAgICAgICAgIHNob3dGbGlwQ2FtZXJhQnV0dG9uOiB0cnVlLCAgIC8vIGRlZmF1bHQgZmFsc2VcclxuICAgICAgICAgICAgcHJlZmVyRnJvbnRDYW1lcmE6IGZhbHNlLCAgICAgLy8gZGVmYXVsdCBmYWxzZVxyXG4gICAgICAgICAgICBzaG93VG9yY2hCdXR0b246IHRydWUsICAgICAgICAvLyBkZWZhdWx0IGZhbHNlXHJcbiAgICAgICAgICAgIGJlZXBPblNjYW46IHRydWUsICAgICAgICAgICAgIC8vIFBsYXkgb3IgU3VwcHJlc3MgYmVlcCBvbiBzY2FuIChkZWZhdWx0IHRydWUpXHJcbiAgICAgICAgICAgIGZ1bGxTY3JlZW46IHRydWUsICAgICAgICAgICAgIC8vIEN1cnJlbnRseSBvbmx5IHVzZWQgb24gaU9TOyB3aXRoIGlPUyAxMyBtb2RhbHMgYXJlIG5vIGxvbmdlciBzaG93biBmdWxsU2NyZWVuIGJ5IGRlZmF1bHQsIHdoaWNoIG1heSBiZSBhY3R1YWxseSBwcmVmZXJyZWQuIEJ1dCB0byB1c2UgdGhlIG9sZCBmdWxsU2NyZWVuIGFwcGVhcmFuY2UsIHNldCB0aGlzIHRvICd0cnVlJy4gRGVmYXVsdCAnZmFsc2UnLlxyXG4gICAgICAgICAgICB0b3JjaE9uOiBmYWxzZSwgICAgICAgICAgICAgICAvLyBsYXVuY2ggd2l0aCB0aGUgZmxhc2hsaWdodCBvbiAoZGVmYXVsdCBmYWxzZSlcclxuICAgICAgICAgICAgY2xvc2VDYWxsYmFjazogKCkgPT4geyBjb25zb2xlLmxvZyhcIlNjYW5uZXIgY2xvc2VkXCIpfSwgLy8gaW52b2tlZCB3aGVuIHRoZSBzY2FubmVyIHdhcyBjbG9zZWQgKHN1Y2Nlc3Mgb3IgYWJvcnQpXHJcbiAgICAgICAgICAgIHJlc3VsdERpc3BsYXlEdXJhdGlvbjogNTAwLCAgIC8vIEFuZHJvaWQgb25seSwgZGVmYXVsdCAxNTAwIChtcyksIHNldCB0byAwIHRvIGRpc2FibGUgZWNob2luZyB0aGUgc2Nhbm5lZCB0ZXh0XHJcblxyXG4gICAgICAgICAgICBvcGVuU2V0dGluZ3NJZlBlcm1pc3Npb25XYXNQcmV2aW91c2x5RGVuaWVkOiB0cnVlLCAvLyBPbiBpT1MgeW91IGNhbiBzZW5kIHRoZSB1c2VyIHRvIHRoZSBzZXR0aW5ncyBhcHAgaWYgYWNjZXNzIHdhcyBwcmV2aW91c2x5IGRlbmllZFxyXG4gICAgICAgICAgICBwcmVzZW50SW5Sb290Vmlld0NvbnRyb2xsZXI6IHRydWUgLy8gaU9TLW9ubHk7IElmIHlvdSdyZSBzdXJlIHlvdSdyZSBub3QgcHJlc2VudGluZyB0aGUgKG5vbiBlbWJlZGRlZCkgc2Nhbm5lciBpbiBhIG1vZGFsLCBvciBhcmUgZXhwZXJpZW5jaW5nIGlzc3VlcyB3aXRoIGZpLiB0aGUgbmF2aWdhdGlvbmJhciwgc2V0IHRoaXMgdG8gJ3RydWUnIGFuZCBzZWUgaWYgaXQgd29ya3MgYmV0dGVyIGZvciB5b3VyIGFwcCAoZGVmYXVsdCBmYWxzZSkuXHJcbiAgICAgICAgICB9KS50aGVuKFxyXG4gICAgICAgICAgICAocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1lbnNhamUgPSByZXN1bHQudGV4dCwgXHJcbiAgICAgICAgICAgICAgICB0aGlzLm1lbnNhamUgPSB0aGlzLm1lbnNhamUucmVwbGFjZSgvJiMzNDsvZ2ksXCJcXFwiXCIpLFxyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuZnVuY3Rpb24xKClcclxuXHJcbiAgICAgICAgICAgICAgICAvL2FsZXJ0KHtcclxuICAgICAgICAgICAgICAgICAgICAvL3RpdGxlOiBcIlNjYW4gcmVzdWx0XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgLy9tZXNzYWdlOiBcIkZvcm1hdDogXCIgKyByZXN1bHQuZm9ybWF0ICsgXCIsXFxuVmFsdWU6IFwiICsgcmVzdWx0LnRleHQsXHJcbiAgICAgICAgICAgICAgICAgICAgLy9va0J1dHRvblRleHQ6IFwiT0tcIlxyXG4gICAgICAgICAgICAgICAgICAvL30pO1xyXG4gICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKGVycm9yTWVzc2FnZSkgPT4ge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTm8gc2Nhbi4gXCIgKyBlcnJvck1lc3NhZ2UpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIC8vaWYodGhpcy52YWxpZF9RUj09ZmFsc2UpXHJcbiAgICAgICAgICAgIGFsZXJ0KFwiUVIgbm8gdsOhbGlkb1wiKTtcclxuICAgICAgICB9ICAgICAgICBcclxuXHJcbiAgICBwdWJsaWMgZnVuY3Rpb24xICgpIHtcclxuICAgICAgICBpZih0aGlzLnNhdmVKU09OKCkpXHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlKHRoaXMuY3VycmVudFVzZXIpXHJcbiAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICBhbGVydChcIlFSIG5vIHbDoWxpZG9cIikgICAgICAgIFxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBwdWJsaWMgbW9zdHJhcm1lbnNhamUoKXtcclxuICAgICAgICBhbGVydChcIkZ1bmNpw7NuIG5vIGhhYmlsaXRhZGEgYWN0dWFsbWVudGVcIik7XHJcbiAgICB9ICAgXHJcbiAgICAgICAgXHJcbiAgICBwdWJsaWMgc2F2ZUpTT04oKXtcclxuXHJcbiAgICAvLyAvbWVuc2FqZS9naSBzaXJ2ZSBwYXJhIHF1ZSByZWVtcGxhY2UgdG9kb3MgbG9zIHZhbG9yZXMgZW5jb250cmFkb3MsIHBvcnF1ZSBzaW4gZXN0byBzb2xvIHJlZW1wbGF6YSBlbCBwcmltZXJvXHJcbiAgICAgICAgdHJ5e1xyXG4gICAgICAgICAgICB2YXIganNvbiA9IEpTT04ucGFyc2UodGhpcy5tZW5zYWplKTtcclxuICAgICAgICAgICAgaWYoanNvbi5jbGFzZSAmJiBqc29uLmFzaWVudG8pXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXIuY2xhc3MgPSBqc29uLmNsYXNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlci5zZWF0ID0ganNvbi5hc2llbnRvO1xyXG4gICAgICAgICAgICAgICAgLy9sb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRVc2VyXCIsSlNPTi5zdHJpbmdpZnkodGhpcy5jdXJyZW50VXNlcikpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJNZW5zYWplIGRlIFBydWViYVwiKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnZhbGlkX1FSID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBhbGVydChcIlFSIG5vIHbDoWxpZG9cIik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlFSIG5vIHbDoWxpZG8gMVwiKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBjYXRjaFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudmFsaWRfUVIgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGFsZXJ0KFwiUVIgbm8gdsOhbGlkb1wiKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUVIgbm8gdsOhbGlkbyAxXCIpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gICAgcHVibGljIG1vc3RyYXJWYWxvcmVzKCl7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXIpKTtcclxuICAgICAgICAgICAgYWxlcnQoSlNPTi5zdHJpbmdpZnkodGhpcy5jdXJyZW50VXNlcikpO1xyXG4gICAgICAgICAgICAvL2FsZXJ0KFwiTGEgY2xhc2UgbGXDrWRhIGRlbCBxciBlczogXCIgKyB0aGlzLmN1cnJlbnRVc2VyLmNsYXNzICsgXCJcXG4gRWwgYXNpZW50byBsZcOtZG8gZGVsIHFyIGVzOiBcIiArICB0aGlzLmN1cnJlbnRVc2VyLnNlYXRcclxuICAgICAgICAgICAgLy8rIFwiXFxuIEVsIGlkIGRlIGxhIHNlc2nDs24gIGVzOiBcIiArICB0aGlzLmN1cnJlbnRVc2VyLl9pZCArIHRoaXMuY3VycmVudFVzZXIudXNlcm5hbWUgKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBcclxuXHJcbiAgICB1cGRhdGUoY3VycmVudFVzZXI6IFVzZXIpIHtcclxuICAgICAgICBpZih0aGlzLmN1cnJlbnRVc2VyLnNlYXQhPVwiXCIgJiYgdGhpcy5jdXJyZW50VXNlci5jbGFzcyE9XCJcIilcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMudXNlclNlcnZpY2Uuc2V0VXNlcih0aGlzLmN1cnJlbnRVc2VyKTtcclxuICAgICAgICAgICAgLy9sb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRVc2VyXCIsSlNPTi5zdHJpbmdpZnkoY3VycmVudFVzZXIpKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5jdXJyZW50VXNlci5zZWF0KTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkodGhpcy5jdXJyZW50VXNlcikpO1xyXG4gICAgICAgICAgICAvL3RoaXMudXNlclNlcnZpY2UudXBkYXRlKHRoaXMuY3VycmVudFVzZXIpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgdGhpcy51c2VyU2VydmljZS5jb25uZWN0U3R1ZGVudFRvU2Vzc2lvbih0aGlzLmN1cnJlbnRVc2VyKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiUmVnaXN0cmFkbyBlbiBsYSBjbGFzZSBjb24gw6l4aXRvXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3N0dWRlbnRcIl0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGVydChcIkVycm9yIGRlIHJlZ2lzdHJvIGVuIGNsYXNlXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICBhbGVydChcIkFzaWVudG8geSBjbGFzZSB2YWPDrW9zXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gICAgdXBkYXRlMigpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRVc2VyLnNlYXQ9XCI0XCI7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VXNlci5jbGFzcz1cIkEtMTBcIjtcclxuICAgICAgICBpZih0aGlzLmN1cnJlbnRVc2VyLnNlYXQhPVwiXCIgJiYgdGhpcy5jdXJyZW50VXNlci5jbGFzcyE9XCJcIilcclxuICAgICAgICB7XHJcblxyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXIpKTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50VXNlclwiLEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXIpKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudXNlclNlcnZpY2UuY29ubmVjdFN0dWRlbnRUb1Nlc3Npb24odGhpcy5jdXJyZW50VXNlcikuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeShzZXNzaW9uKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50U2Vzc2lvbicsIEpTT04uc3RyaW5naWZ5KHNlc3Npb24pKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy9hbGVydChcIlJlZ2lzdHJhZG8gZW4gbGEgY2xhc2UgY29uIMOpeGl0b1wiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9zdHVkZW50XCJdKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoXCJFcnJvciBkZSByZWdpc3RybyBlbiBjbGFzZVwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgYWxlcnQoXCJQb3IgZmF2b3IgcmVsbGVuZSBsb3MgY2FtcG9zIGRlIGNsYXNlIHkgYXNpZW50b1wiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuXHJcbiAgICBwdWJsaWMgbWVuc2FqZVJlYWRORkMoKXtcclxuICAgICAgICBhbGVydChcIlBvciBmYXZvciBhY2VycXVlIHVuIGNoaXAgTkZDXCIpXHJcbiAgICB9XHJcbiAgICBwdWJsaWMgUmVhZE5GQygpXHJcbiAgICB7XHJcbiAgICAgIHRoaXMubmZjLnNldE9uTmRlZkRpc2NvdmVyZWRMaXN0ZW5lcigoZGF0YTogTmZjTmRlZkRhdGEpID0+IHtcclxuICAgICAgICAvLyBkYXRhLm1lc3NhZ2UgaXMgYW4gYXJyYXkgb2YgcmVjb3Jkcywgc286XHJcblxyXG4gICAgICAgIGlmIChkYXRhLm1lc3NhZ2UpIHtcclxuICAgICAgICAgIC8vZm9yIChsZXQgbSBpbiBkYXRhLm1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgbGV0IHJlY29yZCA9IGRhdGEubWVzc2FnZVswXTtcclxuICAgICAgICAgICAgdGhpcy5tZW5zYWplID0gcmVjb3JkLnBheWxvYWRBc1N0cmluZzsgXHJcbiAgICAgICAgICAgIHRoaXMubWVuc2FqZSA9IHRoaXMubWVuc2FqZS5yZXBsYWNlKC8mIzM0Oy9naSxcIlxcXCJcIiksXHJcblxyXG4gICAgICAgICAgICB0aGlzLnNhdmVKU09OKCksXHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlKHRoaXMuY3VycmVudFVzZXIpO1xyXG4gICAgICAgICAgICAvL2FsZXJ0KFxyXG4gICAgICAgICAgICAgICAgLy9cIlxcbiBkYXRhLm1lc3NhZ2U6IFwiICsgcmVjb3JkLnBheWxvYWRBc1N0cmluZ1xyXG4gICAgICAgICAgICAvLykgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgIH0sIHtcclxuICAgICAgICAvLyBpT1Mtc3BlY2lmaWMgb3B0aW9uc1xyXG4gICAgICAgIHN0b3BBZnRlckZpcnN0UmVhZDogdHJ1ZSxcclxuICAgICAgICBzY2FuSGludDogXCJTY2FuIGEgdGFnXCJcclxuICAgICAgfSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIk9uTmRlZkRpc2NvdmVyZWQgbGlzdGVuZXIgYWRkZWRcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dCgpIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlciA9IHRoaXMudXNlclNlcnZpY2UuY3VycmVudFVzZXIoKTtcclxuICAgICAgICAgICAgdGhpcy51c2VyU2VydmljZS5sb2dvdXQodGhpcy5jdXJyZW50VXNlci5faWQsIHRoaXMuY3VycmVudFVzZXIpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICBkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFsnL2xvZ2luJ10se2NsZWFySGlzdG9yeTogdHJ1ZX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGVydChcIkVycm9yIGFsIGNlcnJhciBzZXNpw7NuXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG59IiwibW9kdWxlLmV4cG9ydHMgPSBcIi5wYWdlIHtcXHJcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxyXFxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcclxcbn1cXHJcXG4uZm9ybSB7XFxyXFxuICBtYXJnaW4tbGVmdDogMzA7XFxyXFxuICBtYXJnaW4tcmlnaHQ6IDMwO1xcclxcbiAgZmxleC1ncm93OiAyO1xcclxcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcXHJcXG59XFxyXFxuXFxyXFxuLmxvZ28ge1xcclxcbiAgbWFyZ2luLWJvdHRvbTogMzI7XFxyXFxuICBmb250LXdlaWdodDogYm9sZDtcXHJcXG59XFxyXFxuXFxyXFxuLmJvdG9ue1xcclxcbiAgd2lkdGg6IDIwMDtcXHJcXG4gIGhlaWdodDogNTA7XFxyXFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjYsIDkxLCAyMTMpO1xcclxcbiAgY29sb3I6IHdoaXRlc21va2U7XFxyXFxuICBib3JkZXItcmFkaXVzOiA1O1xcclxcbiAgZm9udC1zaXplOiAyMDtcXHJcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxyXFxufVxcclxcbi5sb2dvMntcXHJcXG4gIG1hcmdpbi1ib3R0b206IDEyO1xcclxcbiAgaGVpZ2h0OiAxMDA7XFxyXFxuICBob3Jpem9udGFsLWFsaWduOiBjZW50ZXI7XFxyXFxuICBmb250LXdlaWdodDogYm9sZDtcXHJcXG59XFxyXFxuLmhlYWRlciB7XFxyXFxuICBob3Jpem9udGFsLWFsaWduOiBjZW50ZXI7XFxyXFxuICBmb250LXNpemU6IDI1O1xcclxcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXHJcXG4gIG1hcmdpbi1ib3R0b206IDIwO1xcclxcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbiAgY29sb3I6IHJnYigzOCwgOCwgOTkpO1xcclxcbn1cXHJcXG5cXHJcXG4uaW5wdXQtZmllbGQge1xcclxcbiAgbWFyZ2luLWJvdHRvbTogMjU7XFxyXFxufVxcclxcbi5pbnB1dCB7XFxyXFxuICBmb250LXNpemU6IDE4O1xcclxcbiAgcGxhY2Vob2xkZXItY29sb3I6ICNBOEE4QTg7XFxyXFxufVxcclxcbi5pbnB1dDpkaXNhYmxlZCB7XFxyXFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XFxyXFxuICBvcGFjaXR5OiAwLjU7XFxyXFxufVxcclxcblxcclxcbi5idG4tcHJpbWFyeSB7XFxyXFxuICBtYXJnaW46IDE1IDUgMTUgNTtcXHJcXG59XFxyXFxuXFxyXFxuLmJ0bi1zZWNvbmQge1xcclxcbiAgbWFyZ2luOiAxNSA1IDE1IDU7XFxyXFxufVxcclxcblxcclxcbi5sb2dpbi1sYWJlbCB7XFxyXFxuICBob3Jpem9udGFsLWFsaWduOiBjZW50ZXI7XFxyXFxuICBjb2xvcjogI0E4QThBODtcXHJcXG4gIGZvbnQtc2l6ZTogMTY7XFxyXFxufVxcclxcbi5zaWduLXVwLWxhYmVsIHtcXHJcXG4gIG1hcmdpbi1ib3R0b206IDIwO1xcclxcbn1cXHJcXG4uYm9sZCB7XFxyXFxuICBjb2xvcjogIzAwMDAwMDsgXFxyXFxufVxcclxcblxcclxcblwiIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxBY3Rpb25CYXIgdGl0bGU9XFxcIkxvZ2luIFBhZ2VcXFwiPjwvQWN0aW9uQmFyPlxcclxcblxcclxcbjxGbGV4Ym94TGF5b3V0IGNsYXNzPVxcXCJwYWdlXFxcIj5cXHJcXG4gICAgPFN0YWNrTGF5b3V0PlxcclxcbiAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwibG9nb1xcXCIgc3JjPVxcXCJ+L2ltYWdlcy9Mb2dvX3BvbGl0ZWNuaWNhLnBuZ1xcXCI+PC9JbWFnZT5cXHJcXG4gICAgICAgICAgICA8SW1hZ2UgY2xhc3M9XFxcImxvZ28yXFxcIiBzcmM9XFxcIn4vaW1hZ2VzL2VyaXNpZDIucG5nXFxcIj48L0ltYWdlPlxcclxcblxcclxcbiAgICA8L1N0YWNrTGF5b3V0PlxcclxcblxcclxcbiAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImZvcm1cXFwiPlxcclxcbiAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJoZWFkZXJcXFwiIHRleHQ9XFxcIlBST1lFQ1RPIFNJRElTRUNcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCIqLCpcXFwiIHJvd3M9XFxcIjYwLCosKiwqXFxcIiB3aWR0aD1cXFwiMjgwXFxcIiBoZWlnaHQ9XFxcIjMwMFxcXCJcXHJcXG4gICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I9XFxcImxpZ2h0Z3JheVxcXCIgYm9yZGVyQ29sb3I9XFxcInJnYigxNSwgNSwgNTkpXFxcIiBib3JkZXJ3aWR0aD1cXFwiM1xcXCI+XFxyXFxuICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJBbHVtbm9zXFxcIiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMFxcXCIgY2xhc3M9XFxcImJ0biBidG4tc2Vjb25kXFxcIiBcXHJcXG4gICAgICAgICAgICAodGFwKT1cXFwic2VsZWN0KClcXFwiXFxyXFxuICAgICAgICAgID48L0J1dHRvbj5cXHJcXG5cXHJcXG4gICAgICAgICAgICA8TGFiZWwgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjBcXFwiICpuZ0lmPVxcXCIhc2VsZWN0aW9uXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIj5cXHJcXG4gICAgICAgICAgICA8L0xhYmVsPlxcclxcblxcclxcbiAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiUHJvZmVzb3Jlc1xcXCIgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjFcXFwiIGNsYXNzPVxcXCJidG4gYnRuLXNlY29uZFxcXCIgKHRhcCk9XFxcInNlbGVjdDIoKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiAqbmdJZj1cXFwic2VsZWN0aW9uXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIj5cXHJcXG4gICAgICAgICAgICA8L0xhYmVsPlxcclxcblxcclxcbiAgICAgICAgICAgIDxUZXh0RmllbGQgcm93PVxcXCIgMVxcXCIgY29sPVxcXCIwXFxcIiBjb2xTcGFuPVxcXCIyXFxcIiAgYmFja2dyb3VuZENvbG9yPVxcXCJ3aGl0ZVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgd2lkdGg9XFxcIjI2MFxcXCIgaGVpZ2h0PVxcXCI1MFxcXCIgY2xhc3M9XFxcImlucHV0XFxcIiBoaW50PVxcXCJFbWFpbFxcXCIgXFxyXFxuICAgICAgICAgICAgICAgIFtpc0VuYWJsZWRdPVxcXCIhcHJvY2Vzc2luZ1xcXCIga2V5Ym9hcmRUeXBlPVxcXCJlbWFpbFxcXCJcXHJcXG4gICAgICAgICAgICAgICAgYXV0b2NvcnJlY3Q9XFxcImZhbHNlXFxcIiBhdXRvY2FwaXRhbGl6YXRpb25UeXBlPVxcXCJub25lXFxcIlxcclxcbiAgICAgICAgICAgICAgICBbKG5nTW9kZWwpXT1cXFwidXNlci51c2VybmFtZVxcXCI+XFxyXFxuICAgICAgICAgICAgPC9UZXh0RmllbGQ+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgIDxUZXh0RmllbGQgI3Bhc3N3b3JkIGNsYXNzPVxcXCJpbnB1dFxcXCIgcm93PVxcXCIyXFxcIiBjb2w9XFxcIjBcXFwiIGNvbFNwYW49XFxcIjJcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIiB3aWR0aD1cXFwiMjYwXFxcIiBoZWlnaHQ9XFxcIjUwXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgW2lzRW5hYmxlZF09XFxcIiFwcm9jZXNzaW5nXFxcIiBoaW50PVxcXCJQYXNzd29yZFxcXCIgc2VjdXJlPVxcXCJ0cnVlXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgWyhuZ01vZGVsKV09XFxcInVzZXIucGFzc3dvcmRcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBbcmV0dXJuS2V5VHlwZV09XFxcImlzTG9nZ2luZ0luID8gJ2RvbmUnIDogJ25leHQnXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgKHJldHVyblByZXNzKT1cXFwiZm9jdXNDb25maXJtUGFzc3dvcmQoKVxcXCI+PC9UZXh0RmllbGQ+XFxyXFxuICAgICAgICAgICAgXFxyXFxuICAgICAgICAgICAgICAgICA8QnV0dG9uIFt0ZXh0XT1cXFwiaXNMb2dnaW5nSW4gPyAnTG9naW4nIDogJ1NpZ24gVXAnXFxcIiByb3c9XFxcIjNcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBjb2w9XFxcIjBcXFwiICpuZ0lmPVxcXCJzZWxlY3Rpb25cXFwiIFtpc0VuYWJsZWRdPVxcXCIhcHJvY2Vzc2luZ1xcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICh0YXApPVxcXCJzdWJtaXQoKVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBtLXQtMjBcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICA8QnV0dG9uIFt0ZXh0XT1cXFwiJ0xvZ2luIFByb2Zlc29ycidcXFwiIHJvdz1cXFwiM1xcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIGNvbD1cXFwiMFxcXCIgY29sU3Bhbj1cXFwiMlxcXCIgKm5nSWY9XFxcIiFzZWxlY3Rpb25cXFwiIFtpc0VuYWJsZWRdPVxcXCIhcHJvY2Vzc2luZ1xcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJib3RvblxcXCIgKHRhcCk9XFxcImxvZ2ludGVhY2hlcigpXFxcIj48L0J1dHRvbj5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiBbdGV4dF09XFxcIidSZWdpc3RybydcXFwiIHJvdz1cXFwiM1xcXCIgY29sPVxcXCIxXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBtLXQtMjBcXFwiICpuZ0lmPVxcXCJzZWxlY3Rpb25cXFwiICh0YXApPVxcXCJyZWdpc3RlcjIoKVxcXCI+PC9CdXR0b24+XFxyXFxuXFxyXFxuXFxyXFxuICAgICAgICA8L0dyaWRMYXlvdXQ+XFxyXFxuICAgICAgICA8TGFiZWwgKm5nSWY9XFxcImlzTG9nZ2luZ0luXFxcIiB0ZXh0PVxcXCLCv09sdmlkYXN0ZSBsYSBjb250cmFzZcOxYT9cXFwiXFxyXFxuICAgICAgICAgICAgY2xhc3M9XFxcImxvZ2luLWxhYmVsXFxcIiAodGFwKT1cXFwiZm9yZ290UGFzc3dvcmQoKVxcXCI+PC9MYWJlbD5cXHJcXG4gICAgPC9TdGFja0xheW91dD5cXHJcXG48L0ZsZXhib3hMYXlvdXQ+XCIiLCJpbXBvcnQgeyBDb21wb25lbnR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgVXNlciB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLm1vZGVsXCI7XHJcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3VzZXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBBdXRoZW50aWNhdGVTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL2F1dGhlbnRpY2F0ZS5zZXJ2aWNlXCI7XHJcblxyXG5pbXBvcnQgeyBhbGVydCwgcHJvbXB0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBTZWxlY3RNdWx0aXBsZUNvbnRyb2xWYWx1ZUFjY2Vzc29yIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiBcImdyLWxvZ2luXCIsXHJcbiAgICBwcm92aWRlcnM6IFtVc2VyU2VydmljZV0sXHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc3R5bGVVcmxzOiBbXCIuL2xvZ2luLmNvbXBvbmVudC5jc3NcIl0sXHJcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2xvZ2luLmNvbXBvbmVudC5odG1sXCJcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCB7XHJcbiAgICB1c2VyOiBVc2VyO1xyXG5cclxuICAgIGlzTG9nZ2luZ0luID0gdHJ1ZTtcclxuICAgIHNlbGVjdGlvbiA9IHRydWU7XHJcblxyXG4gICAgY3VycmVudFVzZXI6IFVzZXI7XHJcbiAgICBtb2RlbDogYW55ID0ge307XHJcblxyXG4gICAgXHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIGF1dGhlbnRpY2F0ZVNlcnZpY2U6IEF1dGhlbnRpY2F0ZVNlcnZpY2UsIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMudXNlciA9IG5ldyBVc2VyKCk7XHJcbiAgICAgICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcblxyXG5cclxuICAgICAgICB0aGlzLmN1cnJlbnRVc2VyID0gbmV3IFVzZXIoKTtcclxuXHJcbiAgICAgICAgLy9sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY3VycmVudFVzZXInLCBcImFcIilcclxuICAgICAgICAvL3RoaXMudXNlci51c2VybmFtZT1cInBydWViYTIxQGdtYWlsLmNvbVwiO1xyXG4gICAgICAgIC8vdGhpcy51c2VyLnBhc3N3b3JkPVwicHJ1ZWJhMjFcIjtcclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3QoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3Rpb24gPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdDIoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3Rpb24gPSBmYWxzZTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgc3VibWl0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzTG9nZ2luZ0luKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9naW4oKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNpZ25VcCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBsb2dpbigpIHtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KHRoaXMudXNlcikpO1xyXG4gICAgICAgIHRoaXMuYXV0aGVudGljYXRlU2VydmljZS5sb2dpbih0aGlzLnVzZXIpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZSA9PnsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCIxMTFcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vdGhpcy5hdXRoZW50aWNhdGVTZXJ2aWNlLnNldFVzZXIocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIC8vdGhpcy5hdXRoZW50aWNhdGVTZXJ2aWNlLnNldFVzZXIocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiMVwiICsgcmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiMVwiICsgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2N1cnJlbnRVc2VyJywgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTsgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJDbGFzczogXCIgKyB0aGlzLmN1cnJlbnRVc2VyLmNsYXNzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkNvZGUgc2Vzc2lvbjogXCIgKyB0aGlzLmN1cnJlbnRVc2VyLmNvZGVfc2Vzc2lvbiApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuY3VycmVudFVzZXIuY2xhc3MhPW51bGwgJiYgdGhpcy5jdXJyZW50VXNlci5jb2RlX3Nlc3Npb24hPW51bGwpXHJcbiAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9zdHVkZW50XCJdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9zZXNzaW9uX2NvZGVcIl0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiMjIyXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKHRoaXMuYXV0aGVudGljYXRlU2VydmljZS5jdXJyZW50VXNlcigpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChleGNlcHRpb24pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiRXJyb3IgZGUgbG9ndWVvXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChleGNlcHRpb24uZXJyb3IgJiYgZXhjZXB0aW9uLmVycm9yLmRlc2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KGV4Y2VwdGlvbi5lcnJvci5kZXNjcmlwdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXhjZXB0aW9uLmVycm9yKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuICAgIHJlZ2lzdGVyMigpIHtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL3JlZ2lzdGVyXCJdKTtcclxuICAgIH1cclxuXHJcbiAgICBsb2dpbnRlYWNoZXIoKVxyXG4gICAge1xyXG4gICAgICAgIGFsZXJ0KFwiQmllbnZlbmlkb29cIik7XHJcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi90ZWFjaGVyXCJdKTtcclxuICAgIH1cclxuXHJcbiAgICBzaWduVXAoKSB7XHJcbiAgICAgICAgdGhpcy5hdXRoZW50aWNhdGVTZXJ2aWNlLnJlZ2lzdGVyKHRoaXMudXNlcilcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGVydChcIllvdXIgYWNjb3VudCB3YXMgc3VjY2Vzc2Z1bGx5IGNyZWF0ZWQuXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlRGlzcGxheSgpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChleGNlcHRpb24pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZXhjZXB0aW9uLmVycm9yICYmIGV4Y2VwdGlvbi5lcnJvci5kZXNjcmlwdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGVydChleGNlcHRpb24uZXJyb3IuZGVzY3JpcHRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KGV4Y2VwdGlvbilcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yZ290UGFzc3dvcmQoKVxyXG4gICAge1xyXG4gICAgICAgIGlmKHRoaXMudXNlci51c2VybmFtZSlcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIC8vYWxlcnQoXCJTZSBoYSBtYW5kYWRvIHVuIGVubGFjZSBhIHN1IGNvcnJlbyBwYXJhIHJlc3RhdXJhciBsYSBjb250cmFzZcOxYVwiKTsgICAgICAgICBcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tb2RlbC51c2VybmFtZSA9IHRoaXMudXNlci51c2VybmFtZTtcclxuICAgICAgICAgICAgICAgIC8vdGhpcy5tb2RlbC5wYXNzd29yZCA9XCJob2xhXCI7XHJcbiAgICAgICAgICAgICAgICAvL3RoaXMubW9kZWwudG9rZW4gPVwiaG9sYVwiO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkodGhpcy5tb2RlbCkpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMudXNlclNlcnZpY2UucmVzZXRfcGFzc3dvcmRfZW1haWwodGhpcy5tb2RlbClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiU2UgaGEgbWFuZGFkbyB1biBlbmxhY2UgYSBzdSBjb3JyZW8gcGFyYSByZXN0YXVyYXIgbGEgY29udHJhc2XDsWFcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvcmVzZXRfcGFzc3dvcmRcIl0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKGV4Y2VwdGlvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXhjZXB0aW9uLmVycm9yICYmIGV4Y2VwdGlvbi5lcnJvci5kZXNjcmlwdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXhjZXB0aW9uLmVycm9yLmRlc2NyaXB0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KGV4Y2VwdGlvbi5lcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICBhbGVydChcIlBvciBmYXZvciBpbnRyb2R1emNhIHVuIGNvcnJlbyBwYXJhIHJlc3RhdXJhciBsYSBjb250cmFzZcOxYVwiKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVEaXNwbGF5KCkge1xyXG4gICAgICAgIHRoaXMuaXNMb2dnaW5nSW4gPSAhdGhpcy5pc0xvZ2dpbmdJbjtcclxuICAgIH1cclxuXHJcblxyXG59XHJcblxyXG4iLCJpbXBvcnQgeyBwbGF0Zm9ybU5hdGl2ZVNjcmlwdER5bmFtaWMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcGxhdGZvcm1cIjtcbmltcG9ydCB7IGVuYWJsZVByb2RNb2RlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuaW1wb3J0IHsgQXBwTW9kdWxlIH0gZnJvbSBcIi4vYXBwLm1vZHVsZVwiO1xuXG5lbmFibGVQcm9kTW9kZSgpO1xucGxhdGZvcm1OYXRpdmVTY3JpcHREeW5hbWljKCkuYm9vdHN0cmFwTW9kdWxlKEFwcE1vZHVsZSk7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IFwiXFxuLmhlYWRlciB7XFxuXFxuICBmb250LXNpemU6IDI1O1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIG1hcmdpbi1ib3R0b206IDIwO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgY29sb3I6IHJnYigzOCwgOCwgOTkpO1xcbn1cXG5cXG4uaG9tZS1wYW5lbHtcXG4gICAgdmVydGljYWwtYWxpZ246IGNlbnRlcjsgXFxuICAgIGZvbnQtc2l6ZTogMjA7XFxuICAgIG1hcmdpbjogMTU7XFxufVxcblxcbi5sb2dvIHtcXG4gICAgbWFyZ2luLWJvdHRvbTogMzI7XFxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcbiAgfVxcbiAgXFxuLmRlc2NyaXB0aW9uLWxhYmVse1xcbiAgICBtYXJnaW4tYm90dG9tOiAxNTtcXG59XFxuXFxuLmZvcm0ge1xcbiAgICBtYXJnaW46IDIwO1xcbn1cXG5cXG4ubGFiZWwge1xcbiAgICBtYXJnaW4tcmlnaHQ6IDU7XFxuICAgIHZlcnRpY2FsLWFsaWduOiBjZW50ZXI7XFxufVxcblxcbi5pbnB1dCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDEwO1xcbn1cXG5cIiIsIm1vZHVsZS5leHBvcnRzID0gXCI8RmxleGJveExheW91dD5cXG4gICAgPFNjcm9sbFZpZXc+XFxuICAgICAgICA8U3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwibG9nb1xcXCIgc3JjPVxcXCJ+L2ltYWdlcy9Mb2dvX3BvbGl0ZWNuaWNhLnBuZ1xcXCI+PC9JbWFnZT5cXG4gICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIkZvcm11bGFyaW8gZGUgUmVnaXN0cm9cXFwiXFxuICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgY2xhc3M9XFxcImhlYWRlclxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICA8R3JpZExheW91dCByb3dzPVxcXCIqLCAqLCAqLCAqLCAqXFxcIiBjb2x1bW5zPVxcXCI4MywgKlxcXCIgY2xhc3M9XFxcImZvcm1cXFwiPlxcblxcbiAgICAgICAgICAgICAgICA8TGFiZWwgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjBcXFwiIHRleHQ9XFxcIk5vbWJyZTpcXFwiXFxuICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJyaWdodFxcXCIgY2xhc3M9XFxcImxhYmVsXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICA8VGV4dEZpZWxkIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiBbKG5nTW9kZWwpXT1cXFwibW9kZWwuZmlyc3ROYW1lXFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaGludD1cXFwiRXNjcmliYSBzdSBub21icmVcXFwiIGNsYXNzPVxcXCJpbnB1dCBpbnB1dC1ib3JkZXJcXFwiIG5hbWU9XFxcImxhc3ROYW1lXFxcIiByZXF1aXJlZD5cXG4gICAgICAgICAgICAgICAgPC9UZXh0RmllbGQ+XFxuXFxuICAgICAgICAgICAgICAgIDxMYWJlbCByb3c9XFxcIjFcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwiQXBlbGxpZG9zXFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwicmlnaHRcXFwiIGNsYXNzPVxcXCJsYWJlbFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgPFRleHRGaWVsZCByb3c9XFxcIjFcXFwiIGNvbD1cXFwiMVxcXCIgWyhuZ01vZGVsKV09XFxcIm1vZGVsLmxhc3ROYW1lXFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaGludD1cXFwiRXNjcmliYSBzdXMgYXBlbGxpZG9zXFxcIiBjbGFzcz1cXFwiaW5wdXQgaW5wdXQtYm9yZGVyXFxcIlxcbiAgICAgICAgICAgICAgICAgICAga2V5Ym9hcmRUeXBlPVxcXCJlbWFpbFxcXCI+PC9UZXh0RmllbGQ+XFxuXFxuICAgICAgICAgICAgICAgIDxMYWJlbCByb3c9XFxcIjJcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwiRW1haWw6XFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwicmlnaHRcXFwiIGNsYXNzPVxcXCJsYWJlbFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgPFRleHRGaWVsZCByb3c9XFxcIjJcXFwiIGNvbD1cXFwiMVxcXCIgWyhuZ01vZGVsKV09XFxcIm1vZGVsLnVzZXJuYW1lXFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaGludD1cXFwiRXNjcmliYSBzdSBlbWFpbFxcXCIgY2xhc3M9XFxcImlucHV0IGlucHV0LWJvcmRlclxcXCIgXFxuICAgICAgICAgICAgICAgICAgICBrZXlib2FyZFR5cGU9XFxcImVtYWlsXFxcIiBuYW1lPVxcXCJ1c2VybmFtZVxcXCIgcmVxdWlyZWQgKm5nSWY9XFxcImVtYWlsLmludmFsaWRcXFwiPjwvVGV4dEZpZWxkPlxcblxcbiAgICAgICAgICAgICAgICA8TGFiZWwgcm93PVxcXCIzXFxcIiBjb2w9XFxcIjBcXFwiIHRleHQ9XFxcIk1hdHLDrWN1bGE6XFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwicmlnaHRcXFwiIGNsYXNzPVxcXCJsYWJlbFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgPFRleHRGaWVsZCByb3c9XFxcIjNcXFwiIGNvbD1cXFwiMVxcXCIgWyhuZ01vZGVsKV09XFxcIm1vZGVsLm51bWF0XFxcIlxcbiAgICAgICAgICAgICAgICAgICAgaGludD1cXFwiRXNjcmliYSBzdSBuw7ptZXJvIGRlIG1hdHLDrWN1bGFcXFwiXFxuICAgICAgICAgICAgICAgICAgICBtYXhMZW5ndGg9XFxcIjVcXFwiXFxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cXFwiaW5wdXQgaW5wdXQtYm9yZGVyXFxcIiBuYW1lPVxcXCJudW1hdFxcXCIgcmVxdWlyZWQ+XFxuICAgICAgICAgICAgICAgIDwvVGV4dEZpZWxkPlxcbiAgICAgICAgICAgICAgICA8TGFiZWwgcm93PVxcXCI0XFxcIiBjb2w9XFxcIjBcXFwiIHRleHQ9XFxcIkNvbnRyYXNlw7FhOlxcXCJcXG4gICAgICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcInJpZ2h0XFxcIiBjbGFzcz1cXFwibGFiZWxcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgIDxUZXh0RmllbGQgcm93PVxcXCI0XFxcIiBjb2w9XFxcIjFcXFwiIFt0eXBlXT1cXFwiaGlkZSA/ICdwYXNzd29yZCcgOiAndGV4dCdcXFwiIFsobmdNb2RlbCldPVxcXCJtb2RlbC5wYXNzd29yZFxcXCJcXG4gICAgICAgICAgICAgICAgICAgIGhpbnQ9XFxcIkVzY3JpYmEgc3UgY29udHJhc2XDsWFcXFwiIGNsYXNzPVxcXCJpbnB1dCBpbnB1dC1ib3JkZXJcXFwiIG5hbWU9XFxcInBhc3N3b3JkXFxcIiByZXF1aXJlZCBzZWN1cmU9XFxcInRydWVcXFwiPlxcbiAgICAgICAgICAgICAgICA8L1RleHRGaWVsZD5cXG4gICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuXFxuICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJTZWxlY2Npb25hciBJbWFnZW5cXFwiICBiYWNrZ3JvdW5kY29sb3I9XFxcImdyZWVuXFxcIiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeVxcXCJcXG4gICAgICAgICAgICAodGFwKT1cXFwiaW1hZ2VfcGlja2VyKClcXFwiPjwvQnV0dG9uPlxcblxcbiAgICAgICAgICAgIDxHcmlkTGF5b3V0IHJvd3M9XFxcIipcXFwiIGNvbHVtbnM9XFxcIiosKlxcXCIgd2lkdGg9XFxcIjMxMFxcXCIgaGVpZ2h0PVxcXCI4MFxcXCI+XFxuICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiUmVnaXN0cm9cXFwiIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIlxcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeVxcXCIgKHRhcCk9XFxcInJlZ2lzdGVyMigpXFxcIj5cXG4gICAgICAgICAgICAgICAgPC9CdXR0b24+XFxuXFxuICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiVm9sdmVyXFxcIiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeVxcXCJcXG4gICAgICAgICAgICAgICAgICAgICh0YXApPVxcXCJiYWNrKClcXFwiPlxcbiAgICAgICAgICAgICAgICA8L0J1dHRvbj5cXG4gICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuXFxuICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICA8L1Njcm9sbFZpZXc+XFxuPC9GbGV4Ym94TGF5b3V0PlwiIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEVsZW1lbnRSZWYsIFZpZXdDaGlsZH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IGFsZXJ0LCBwcm9tcHQgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE1hdFNuYWNrQmFyLCBNYXRCb3R0b21TaGVldCwgTWF0Qm90dG9tU2hlZXRSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIFZhbGlkYXRvcnMsIEZvcm1Db250cm9sLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgVXNlciB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLm1vZGVsXCI7XG5pbXBvcnQgeyAgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgKiBhcyBpbWFnZXBpY2tlciBmcm9tIFwibmF0aXZlc2NyaXB0LWltYWdlcGlja2VyXCI7XG5pbXBvcnQgKiBhcyBJbWFnZVNvdXJjZU1vZHVsZSBmcm9tIFwiaW1hZ2Utc291cmNlXCI7XG52YXIgZnMgPSByZXF1aXJlKFwiZmlsZS1zeXN0ZW1cIik7XG5pbXBvcnQgeyBJbWFnZSB9IGZyb20gXCJ1aS9pbWFnZVwiO1xuaW1wb3J0ICogYXMgaW1hZ2VTY291cmUgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvaW1hZ2Utc291cmNlXCI7XG5pbXBvcnQgeyBJbWFnZUFzc2V0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvaW1hZ2UtYXNzZXQvaW1hZ2UtYXNzZXRcIjtcbi8vaW1wb3J0IHsgQWxlcnRTZXJ2aWNlLCB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci9hbGVydC5zZXJ2aWNlXCI7XG5cblxuQENvbXBvbmVudCh7XG5cblx0c2VsZWN0b3I6IFwiUmVnaXN0ZXJcIixcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcblx0dGVtcGxhdGVVcmw6IFwiLi9yZWdpc3Rlci5jb21wb25lbnQuaHRtbFwiLFxuXHRzdHlsZVVybHM6IFsnLi9yZWdpc3Rlci5jb21wb25lbnQuY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgUmVnaXN0ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG5cdG1vZGVsOiBhbnkgPSB7fTtcbiAgICBsb2FkaW5nID0gZmFsc2U7XG5cbiAgICAvL0NhbXBvIGVtYWlsXG4gICAgZW1haWwgPSBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLmVtYWlsXSk7XG5cbiAgICAvL0NhbXBvIENvbnRyYXNlw7FhXG4gICAgaGlkZSA9IHRydWU7XG5cblx0aW1hZ2VBc3NldHMgPSBbXTtcblx0aW1hZ2VTcmM6IGFueTtcblx0XG4gICAgLy9GT1RPXG5cdHNlbGVjdGVkRmlsZSA6IEZpbGUgPSBudWxsO1xuXHRcblx0dXNlcjogVXNlcjtcblx0dG9FbWFpbDtcblx0Y2NFbWFpbDtcblx0YmNjRW1haWw7XG5cdHN1YmplY3Q7XG5cdG1lc3NhZ2U7XG5cblx0Y29uc3RydWN0b3IoXHRcblx0XHRwcml2YXRlIHBhZ2U6IFBhZ2UsIFxuXHRcdHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBcblx0XHRwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsIFxuXHRcdC8vcHJpdmF0ZSBhbGVydFNlcnZpY2U6IEFsZXJ0U2VydmljZSwgICAgICAgIFxuXHRcdC8vcHVibGljIHNuYWNrQmFyOiBNYXRTbmFja0JhclxuXHRcdCkgXG5cdFx0e1xuXHRcdFxuXHRcdHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuXHRcdHRoaXMudXNlciA9IG5ldyBVc2VyKCk7XG5cdFx0dGhpcy51c2VyLnVzZXJuYW1lID0gXCJcIjtcblx0XHR0aGlzLnVzZXIucGFzc3dvcmQgPSBcIlwiO1xuXHRcdH1cblxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcblx0fVxuXG5cdC8qcHVibGljIGltYWdlX3BpY2tlcjMoKXtcblxuXHRcdFx0bGV0IHRoYXQgPSB0aGlzXG5cdFx0XHRsZXQgY29udGV4dCA9IGltYWdlcGlja2VyLmNyZWF0ZSh7IG1vZGU6IFwic2luZ2xlXCIgfSk7XG5cdFx0XHRjb250ZXh0XG5cdFx0XHRcdC5hdXRob3JpemUoKVxuXHRcdFx0XHQudGhlbigoKSA9PiB7XG5cdFx0XHRcdFx0cmV0dXJuIGNvbnRleHQucHJlc2VudCgpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQudGhlbihzZWxlY3Rpb24gPT4ge1xuXHRcdFx0XHRcdGNvbnN0IGltYWdlQXNzZXQgPSBzZWxlY3Rpb24ubGVuZ3RoID4gMCA/IHNlbGVjdGlvblswXSA6IG51bGw7XG5cdFx0XHRcdFx0SW1hZ2VTb3VyY2VNb2R1bGUuZnJvbUFzc2V0KGltYWdlQXNzZXQpLnRoZW4oXG5cdFx0XHRcdFx0XHRzYXZlZEltYWdlID0+IHtcblx0XHRcdFx0XHRcdFx0bGV0IGZpbGVuYW1lID0gXCJpbWFnZVwiICsgXCItXCIgKyBuZXcgRGF0ZSgpLmdldFRpbWUoKSArIFwiLnBuZ1wiO1xuXHRcdFx0XHRcdFx0XHRsZXQgZm9sZGVyID0gZnMua25vd25Gb2xkZXJzLmRvY3VtZW50cygpO1xuXHRcdFx0XHRcdFx0XHRsZXQgcGF0aCA9IGZzLnBhdGguam9pbihmb2xkZXIucGF0aCwgZmlsZW5hbWUpO1xuXHRcdFx0XHRcdFx0XHRzYXZlZEltYWdlLnNhdmVUb0ZpbGUocGF0aCwgXCJwbmdcIik7XG5cdFx0XHRcdFx0XHRcdHZhciBsb2FkZWRJbWFnZSA9IEltYWdlU291cmNlTW9kdWxlLmZyb21GaWxlKHBhdGgpO1xuXHRcdFx0XHRcdFx0XHQvL2xvYWRlZEltYWdlLmZpbGVuYW1lID0gZmlsZW5hbWU7XG5cdFx0XHRcdFx0XHRcdC8vbG9hZGVkSW1hZ2Uubm90ZSA9IFwiXCI7XG5cdFx0XHRcdFx0XHRcdC8vdGhhdC5hcnJheVBpY3R1cmVzLnVuc2hpZnQobG9hZGVkSW1hZ2UpO1xuXHRcdFx0XHRcdFx0XHQvL3RoYXQuc3RvcmVEYXRhKCk7XG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKGxvYWRlZEltYWdlKTtcblxuXHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdGVyciA9PiB7XG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKFwiRmFpbGVkIHRvIGxvYWQgZnJvbSBhc3NldFwiKTtcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2coZXJyKVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdC5jYXRjaChlcnIgPT4ge1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGVycik7XG5cdFx0XHRcdH0pO1xuXHRcblx0fSovXG5cblx0cHVibGljIGltYWdlX3BpY2tlcigpe1xuXG5cdFx0bGV0IGNvbnRleHQgPSBpbWFnZXBpY2tlci5jcmVhdGUoe1xuXHRcdFx0bW9kZTogXCJzaW5nbGVcIiAvLyB1c2UgXCJtdWx0aXBsZVwiIGZvciBtdWx0aXBsZSBzZWxlY3Rpb25cblx0XHR9KTtcblxuXHRcdGNvbnRleHQuYXV0aG9yaXplKCkudGhlbigoKSA9PiB7XG5cdFx0XHRcdHJldHVybiBjb250ZXh0LnByZXNlbnQoKTtcblx0XHRcdH0pLnRoZW4oKHNlbGVjdGlvbikgPT4ge1xuXG5cdFx0XHRcdHRoaXMuaW1hZ2VTcmM9SlNPTi5zdHJpbmdpZnkoc2VsZWN0aW9uWzBdKTtcblxuXHRcdFx0XHR2YXIganNvbiA9IEpTT04ucGFyc2UodGhpcy5pbWFnZVNyYyk7XG5cdFx0XHRcdHRoaXMuaW1hZ2VTcmMgPSBKU09OLnN0cmluZ2lmeShqc29uLl9hbmRyb2lkKTtcblx0XHRcdFx0dGhpcy5pbWFnZVNyYyA9IHRoaXMuaW1hZ2VTcmMucmVwbGFjZSgvXCIvZ2ksXCJcIik7XG5cdFx0XHRcdHRoaXMubW9kZWwuc3JjID0gdGhpcy5pbWFnZVNyYy5zdWJzdHIodGhpcy5pbWFnZVNyYy5sYXN0SW5kZXhPZihcIi9cIikgKyAxKTtcblx0XHRcdFx0Y29uc29sZS5sb2coXCJSdXRhIGRlbCBhcmNoaXZvIHNlbGVjY2lvbmFkbzogXCIgKyBKU09OLnN0cmluZ2lmeShqc29uLl9hbmRyb2lkKSk7XG5cblx0XHRcdH0pLmNhdGNoKGZ1bmN0aW9uIChlKSB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKGUpO1xuXHRcdH0pO1xuXHR9XG5cblx0Z2V0RXJyb3JNZXNzYWdlKCkge1xuXHRcdHJldHVybiB0aGlzLmVtYWlsLmhhc0Vycm9yKCdyZXF1aXJlZCcpID8gJ0RlYmVzIHJlbGxlbmFyIGVsIGNhbXBvJyA6XG5cdFx0XHR0aGlzLmVtYWlsLmhhc0Vycm9yKCdlbWFpbCcpID8gJ0VtYWlsIGludmFsaWRvJyA6XG5cdFx0XHRcdCcnO1xuXHQgIH1cblxuXHRyZWdpc3RlcjIoKVxuXHR7XG5cblx0XHRpZih0aGlzLm1vZGVsLmZpcnN0TmFtZSAmJiB0aGlzLm1vZGVsLmxhc3ROYW1lICYmIHRoaXMubW9kZWwudXNlcm5hbWUgJiYgdGhpcy5tb2RlbC5udW1hdCAmJiB0aGlzLm1vZGVsLnBhc3N3b3JkICYmIHRoaXMuaW1hZ2VTcmMpXG5cdFx0e1xuXHRcdFx0Ly9jb25zb2xlLmxvZyhcImhvbGFcIiArIChKU09OLnN0cmluZ2lmeSh0aGlzLm1vZGVsKSkpO1xuXHRcdFx0YWxlcnQoXCJob2xhXCIpO1xuXHRcdFx0aWYodGhpcy51c2VyU2VydmljZS5jcmVhdGVwaG90bzIodGhpcy5pbWFnZVNyYykpXG5cdFx0XHR7XG5cdFx0XHRcdGNvbnNvbGUubG9nKChKU09OLnN0cmluZ2lmeSh0aGlzLm1vZGVsKSkpO1xuXHRcdFx0XHR0aGlzLnVzZXJTZXJ2aWNlLmNyZWF0ZSh0aGlzLm1vZGVsKVxuXHRcdFx0XHQuc3Vic2NyaWJlKFxuXHRcdFx0XHRcdGRhdGEgPT4ge1xuXHRcdFx0XHRcdFx0YWxlcnQoXCJSZWdpc3RyYWRvIGNvbiDDqXhpdG9cIik7XG5cdFx0XHRcdFx0XHR0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRlcnJvciA9PiB7XG5cdFx0XHRcdFx0XHRhbGVydChlcnJvcik7XG5cdFx0XHRcdFx0XHR0aGlzLmxvYWRpbmcgPSBmYWxzZTtcblx0XHRcdFx0XHR9KTtcblxuXHRcdFx0fVxuXHRcdFx0ZWxzZXtcblx0XHRcdFx0YWxlcnQoXCJObyBzZSBoYSBwb2RpZG8gc3ViaXIgc3UgZm90b1wiKTtcblx0XHRcdH1cblx0XHR9XG5cdFx0ZWxzZXtcblx0XHRcdGFsZXJ0KFwiQ29tcGxldGUgdG9kb3MgbG9zIGNhbXBvcyByZXF1ZXJpZG9zXCIpO1xuXHRcdH1cblxuXHRcdC8vdmFyIGZpbGUgPSAgXCIvc3RvcmFnZS9lbXVsYXRlZC8wL1doYXRzQXBwL01lZGlhL1doYXRzQXBwIEltYWdlcy9JTUctMjAyMDAyMjUtV0EwMDAyLmpwZ1wiO1xuXHRcdC8vdmFyIHVybCA9IFwiaHR0cHM6Ly9zb21lLnJlbW90ZS5zZXJ2aWNlLmNvbS9wYXRoXCI7XG5cdFx0Ly92YXIgbmFtZSA9IGZpbGUuc3Vic3RyKGZpbGUubGFzdEluZGV4T2YoXCIvXCIpICsgMSk7XG5cdFx0Ly9jb25zb2xlLmxvZyhuYW1lKTtcblx0XHQvL2FsZXJ0KFwiRnVuY2nDs24gbm8gZGlzcG9uaWJsZSBhY3R1YWxtZW50ZVwiKVxuXHR9XG5cbiAgIHJlZ2lzdGVyKCkge1xuXHQvL3RoaXMubG9hZGluZyA9IHRydWU7XG5cdC8vdGhpcy5tb2RlbC5zcmMgPSB0aGlzLnNlbGVjdGVkRmlsZS5uYW1lO1xuXHR0aGlzLnVzZXJTZXJ2aWNlLmNyZWF0ZSh0aGlzLm1vZGVsKVxuXHRcdC5zdWJzY3JpYmUoXG5cdFx0XHRkYXRhID0+IHtcblx0XHRcdFx0YWxlcnQoXCJSZWdpc3RyYWRvIGNvbiDDqXhpdG8gXCIpO1xuXHRcdFx0XHR0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XG5cdFx0XHRcdGNvbnNvbGUubG9nKGRhdGEpO1xuXHRcdFx0fSxcblx0XHRcdChleGNlcHRpb24pID0+IHtcblx0XHRcdFx0aWYgKGV4Y2VwdGlvbi5lcnJvciAmJiBleGNlcHRpb24uZXJyb3IuZGVzY3JpcHRpb24pIHtcblx0XHRcdFx0XHRhbGVydChleGNlcHRpb24uZXJyb3IuZGVzY3JpcHRpb24pO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdGFsZXJ0KGV4Y2VwdGlvbi5lcnJvcik7XG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0XG5cdH1cblxuXG5cdFxuXHRiYWNrKCkge1xuXHRcdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9sb2dpblwiXSk7XG5cdH1cblx0Lyp1cGxvYWQoYXR0YWNobWVudCkge1xuXHRcdGNvbnNvbGUubG9nKGF0dGFjaG1lbnQpO1xuXHRcdCAgdGhpcy51c2VyU2VydmljZS5jcmVhdGVwaG90byhhdHRhY2htZW50KVxuXHRcdFx0ICAuc3Vic2NyaWJlKFxuXHRcdFx0XHQgIGRhdGEgPT4ge1xuXHRcdFx0XHRcdCAgYWxlcnQoXCJTdWJpZG8gY29uIMOpeGl0byBcIik7XG5cdFx0XHRcdFx0ICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XG5cdFx0XHRcdCAgfSxcblx0XHRcdFx0ICBlcnJvciA9PiB7XG5cdFx0XHRcdFx0ICBhbGVydChlcnJvcik7XG5cdFx0XHRcdCAgfSk7XG5cdCAgfSovXG5cdCAgb25GaWxlU2VsZWN0ZWQoZXZlbnQpe1xuXHRcdHRoaXMuc2VsZWN0ZWRGaWxlID0gZXZlbnQudGFyZ2V0LmZpbGVzWzBdO1xuXHRcdGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRGaWxlKTtcblx0ICB9XG5cdCAgb25VcGxvYWQoKXtcblx0XHR0cnl7XG5cdFx0XHRjb25zb2xlLmxvZyhcIlBydWViYVwiKTtcblx0XHRcdHZhciBmaWxlID0gIFwiL3N0b3JhZ2UvZW11bGF0ZWQvMC9XaGF0c0FwcC9NZWRpYS9XaGF0c0FwcCBJbWFnZXMvSU1HLTIwMjAwMzAxLVdBMDAwMS5qcGdcIjtcblx0XHRcdHZhciBuYW1lID0gZmlsZS5zdWJzdHIoZmlsZS5sYXN0SW5kZXhPZihcIi9cIikgKyAxKTtcblx0XHRcdGNvbnNvbGUubG9nKG5hbWUpO1xuXHRcdFx0Y29uc3QgZmQgPSBuZXcgRm9ybURhdGEoKTtcblx0XHRcdC8vZmQuYXBwZW5kKCdpbWFnZScsIHRoaXMuc2VsZWN0ZWRGaWxlLCB0aGlzLnNlbGVjdGVkRmlsZS5uYW1lKTtcblx0XHRcdGZkLmFwcGVuZCgnaW1hZ2VuJywgZmlsZSwgbmFtZSk7XG5cdFx0XHRjb25zb2xlLmxvZygnKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqJyk7XG5cdFx0XHRjb25zb2xlLmxvZyhmZCk7XG5cdFx0XHR0aGlzLnVzZXJTZXJ2aWNlLmNyZWF0ZXBob3RvKGZkKVxuXHRcdFx0ICAuc3Vic2NyaWJlKFxuXHRcdFx0XHRkYXRhID0+IHtcblx0XHRcdFx0XHRhbGVydChcIlN1YmlkYSBmb3RvIGNvbiDDqXhpdG8gXCIpO1xuXHRcdFx0XHRcdHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcblx0XHRcdFx0fSxcblx0XHRcdFx0ZXJyb3IgPT4ge1xuXHRcdFx0XHRcdC8vdGhpcy5hbGVydFNlcnZpY2UuZXJyb3IoZXJyb3IpO1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGVycm9yKTtcblx0XHRcdFx0XHRhbGVydChcIkVycm9yIGRlIHN1YmlkYSBlbiBsYSBpbWFnZW5cIik7XG5cdFx0XHRcdH0pO1xuXG5cdFx0fVxuXHRcdGNhdGNoKGUpe1xuXHRcdFx0Y29uc29sZS5sb2coZSk7XG5cdFx0fVxuXHR9XG5cblx0ICBcblx0Ly9vcGVuU25hY2tCYXIoKSB7XG5cdFx0Ly90aGlzLnNuYWNrQmFyLm9wZW4oXCJSZWdpc3RyYWRvIGNvbiBleGl0b1wiLCBcIkNlcnJhclwiLCB7XG5cdFx0ICAvL2R1cmF0aW9uOiAyMDAwXG5cdFx0Ly99KTtcblx0IC8vIH1cbn0iLCJtb2R1bGUuZXhwb3J0cyA9IFwiXFxyXFxuXFxyXFxuLmhlYWRlciB7XFxyXFxuICAgIGhvcml6b250YWwtYWxpZ246IGNlbnRlcjtcXHJcXG4gICAgZm9udC1zaXplOiAyNTtcXHJcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXHJcXG4gICAgbWFyZ2luLWJvdHRvbTogMjA7XFxyXFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXHJcXG4gICAgY29sb3I6IHJnYigzOCwgOCwgOTkpO1xcclxcbiAgfVxcclxcbiAgXFxyXFxuICAuaG9tZS1wYW5lbHtcXHJcXG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogY2VudGVyOyBcXHJcXG4gICAgICBmb250LXNpemU6IDIwO1xcclxcbiAgICAgIG1hcmdpbjogMTU7XFxyXFxuICB9XFxyXFxuICBcXHJcXG4gIC5sb2dvIHtcXHJcXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMjtcXHJcXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcXHJcXG4gICAgfVxcclxcbiAgICBcXHJcXG4gIC5kZXNjcmlwdGlvbi1sYWJlbHtcXHJcXG4gICAgICBtYXJnaW4tYm90dG9tOiAxNTtcXHJcXG4gIH1cXHJcXG4gIFxcclxcbiAgLmZvcm0ge1xcclxcbiAgICAgIG1hcmdpbjogMjA7XFxyXFxuICB9XCIiLCJtb2R1bGUuZXhwb3J0cyA9IFwiPExhYmVsIHRleHQ9XFxcIkhlbGxvXFxcIlxcclxcbiAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiIGNsYXNzPVxcXCJoZWFkZXJcXFwiPjwvTGFiZWw+XCIiLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgRWxlbWVudFJlZiwgVmlld0NoaWxkfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBhbGVydCwgcHJvbXB0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG4vL2ltcG9ydCB7IE1hdFNuYWNrQmFyLCBNYXRCb3R0b21TaGVldCwgTWF0Qm90dG9tU2hlZXRSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5pbXBvcnQgeyBVc2VyIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3VzZXIubW9kZWxcIjtcclxuaW1wb3J0IHsgIFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3VzZXIuc2VydmljZVwiO1xyXG4vL2ltcG9ydCB7IEFsZXJ0U2VydmljZSwgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvYWxlcnQuc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcblx0c2VsZWN0b3I6IFwiUmVnaXN0ZXIyXCIsXHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHR0ZW1wbGF0ZVVybDogXCIuL3JlZ2lzdGVyLmNvbXBvbmVudC5odG1sXCIsXHJcblx0c3R5bGVVcmxzOiBbJy4vcmVnaXN0ZXIuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSZWdpc3RlcjJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuXHRtb2RlbDogYW55ID0ge307XHJcbiAgICBsb2FkaW5nID0gZmFsc2U7XHJcblxyXG4gICAgLy9DYW1wbyBlbWFpbFxyXG4gICAgLy9lbWFpbCA9IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMuZW1haWxdKTtcclxuXHJcbiAgICAvL0NhbXBvIENvbnRyYXNlw7FhXHJcbiAgICBoaWRlID0gdHJ1ZTtcclxuXHJcbiAgICAvL0ZPVE9cclxuXHRzZWxlY3RlZEZpbGUgOiBGaWxlID0gbnVsbDtcclxuXHRcclxuXHR1c2VyOiBVc2VyO1xyXG5cdHRvRW1haWw7XHJcblx0Y2NFbWFpbDtcclxuXHRiY2NFbWFpbDtcclxuXHRzdWJqZWN0O1xyXG5cdG1lc3NhZ2U7XHJcblxyXG5cdGNvbnN0cnVjdG9yKFx0XHJcblx0XHRwcml2YXRlIHBhZ2U6IFBhZ2UsIFxyXG5cdFx0cHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIFxyXG5cdFx0cHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBcclxuXHRcdC8vcHJpdmF0ZSBhbGVydFNlcnZpY2U6IEFsZXJ0U2VydmljZSwgICAgICAgIFxyXG4gICAgICAgIC8vcHVibGljIHNuYWNrQmFyOiBNYXRTbmFja0JhclxyXG4gICAgICAgICkgXHJcblx0XHR7XHJcblxyXG5cdFx0dGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XHJcblx0XHR0aGlzLnVzZXIgPSBuZXcgVXNlcigpO1xyXG5cdFx0dGhpcy51c2VyLnVzZXJuYW1lID0gXCJcIjtcclxuXHRcdHRoaXMudXNlci5wYXNzd29yZCA9IFwiXCI7XHJcblx0XHR9XHJcblxyXG5cdG5nT25Jbml0KCk6IHZvaWQge1xyXG5cdH1cclxuXHJcblx0cmVnaXN0ZXIyKClcclxuXHR7XHJcblx0XHRhbGVydChcIkZ1bmNpw7NuIG5vIGRpc3BvbmlibGUgYWN0dWFsbWVudGVcIilcclxuXHR9XHJcblxyXG5cclxufSIsIm1vZHVsZS5leHBvcnRzID0gXCJcXHJcXG4uaGVhZGVyIHtcXHJcXG5cXHJcXG4gICAgZm9udC1zaXplOiAyNTtcXHJcXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcXHJcXG4gICAgbWFyZ2luLWJvdHRvbTogMjA7XFxyXFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXHJcXG4gICAgY29sb3I6IHJnYigzOCwgOCwgOTkpO1xcclxcbiAgfVxcclxcbiAgXFxyXFxuICAuaG9tZS1wYW5lbHtcXHJcXG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogY2VudGVyOyBcXHJcXG4gICAgICBmb250LXNpemU6IDIwO1xcclxcbiAgICAgIG1hcmdpbjogMTU7XFxyXFxuICB9XFxyXFxuICBcXHJcXG4gIC5sb2dvIHtcXHJcXG4gICAgICBtYXJnaW4tYm90dG9tOiAzMjtcXHJcXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcXHJcXG4gICAgfVxcclxcbiAgICBcXHJcXG4gIC5kZXNjcmlwdGlvbi1sYWJlbHtcXHJcXG4gICAgICBtYXJnaW4tYm90dG9tOiAxNTtcXHJcXG4gIH1cXHJcXG4gIFxcclxcbiAgLmZvcm0ge1xcclxcbiAgICAgIG1hcmdpbjogMjA7XFxyXFxuICB9XFxyXFxuICBcXHJcXG4gIC5sYWJlbCB7XFxyXFxuICAgICAgbWFyZ2luLXJpZ2h0OiA1O1xcclxcbiAgICAgIHZlcnRpY2FsLWFsaWduOiBjZW50ZXI7XFxyXFxuICB9XFxyXFxuICBcXHJcXG4gIC5pbnB1dCB7XFxyXFxuICAgICAgbWFyZ2luLWJvdHRvbTogMTA7XFxyXFxuICB9XFxyXFxuICBcIiIsIm1vZHVsZS5leHBvcnRzID0gXCI8RmxleGJveExheW91dD5cXHJcXG4gICAgPFNjcm9sbFZpZXc+XFxyXFxuICAgICAgICA8U3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwibG9nb1xcXCIgc3JjPVxcXCJ+L2ltYWdlcy9Mb2dvX3BvbGl0ZWNuaWNhLnBuZ1xcXCI+PC9JbWFnZT5cXHJcXG4gICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlJlc2V0ZW8gZGUgY29udHJhc2XDsWFcXFwiXFxyXFxuICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgY2xhc3M9XFxcImhlYWRlclxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICA8R3JpZExheW91dCByb3dzPVxcXCIqLCAqLCAqLCAqIFxcXCIgY29sdW1ucz1cXFwiOTAsICpcXFwiIGNsYXNzPVxcXCJmb3JtXFxcIj5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIiB0ZXh0PVxcXCJFbWFpbDpcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJyaWdodFxcXCIgY2xhc3M9XFxcImxhYmVsXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICA8VGV4dEZpZWxkIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiBbKG5nTW9kZWwpXT1cXFwibW9kZWwudXNlcm5hbWVcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBoaW50PVxcXCJFc2NyaWJhIHN1IG5vbWJyZVxcXCIgY2xhc3M9XFxcImlucHV0IGlucHV0LWJvcmRlclxcXCIgbmFtZT1cXFwibGFzdE5hbWVcXFwiIHJlcXVpcmVkPlxcclxcbiAgICAgICAgICAgICAgICA8L1RleHRGaWVsZD5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMVxcXCIgY29sPVxcXCIwXFxcIiB0ZXh0PVxcXCJDb250cmFzZcOxYTpcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJyaWdodFxcXCIgY2xhc3M9XFxcImxhYmVsXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICA8VGV4dEZpZWxkIHJvdz1cXFwiMVxcXCIgY29sPVxcXCIxXFxcIiBbdHlwZV09XFxcImhpZGUgPyAncGFzc3dvcmQnIDogJ3RleHQnXFxcIiBbKG5nTW9kZWwpXT1cXFwibW9kZWwyLnBhc3N3b3JkXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgaGludD1cXFwiRXNjcmliYSBzdSBjb250cmFzZcOxYVxcXCIgY2xhc3M9XFxcImlucHV0IGlucHV0LWJvcmRlclxcXCIgbmFtZT1cXFwicGFzc3dvcmRcXFwiIHJlcXVpcmVkIHNlY3VyZT1cXFwidHJ1ZVxcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDwvVGV4dEZpZWxkPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICA8TGFiZWwgcm93PVxcXCIyXFxcIiBjb2w9XFxcIjBcXFwiIHRleHQ9XFxcIkNvbnRyYXNlw7FhOlxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcInJpZ2h0XFxcIiBjbGFzcz1cXFwibGFiZWxcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgIDxUZXh0RmllbGQgcm93PVxcXCIyXFxcIiBjb2w9XFxcIjFcXFwiIFt0eXBlXT1cXFwiaGlkZSA/ICdwYXNzd29yZCcgOiAndGV4dCdcXFwiIFsobmdNb2RlbCldPVxcXCJtb2RlbDIucGFzc3dvcmQxXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgaGludD1cXFwiUmVlc2NyaWJhIHN1IGNvbnRyYXNlw7FhXFxcIiBjbGFzcz1cXFwiaW5wdXQgaW5wdXQtYm9yZGVyXFxcIiBuYW1lPVxcXCJwYXNzd29yZFxcXCIgcmVxdWlyZWQgc2VjdXJlPVxcXCJ0cnVlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPC9UZXh0RmllbGQ+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgIDxMYWJlbCByb3c9XFxcIjNcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwiQ8OzZGlnbzpcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJyaWdodFxcXCIgY2xhc3M9XFxcImxhYmVsXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICA8VGV4dEZpZWxkIHJvdz1cXFwiM1xcXCIgY29sPVxcXCIxXFxcIiBbKG5nTW9kZWwpXT1cXFwibW9kZWwudG9rZW5cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICBoaW50PVxcXCJFc2NyaWJhIHN1IGPDs2RpZ28gZGUgcmVzZXRlb1xcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJpbnB1dCBpbnB1dC1ib3JkZXJcXFwiIG5hbWU9XFxcIm51bWF0XFxcIiByZXF1aXJlZD5cXHJcXG4gICAgICAgICAgICAgICAgPC9UZXh0RmllbGQ+XFxyXFxuXFxyXFxuICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcclxcblxcclxcblxcclxcbiAgICAgICAgICAgIDxHcmlkTGF5b3V0IHJvd3M9XFxcIipcXFwiIGNvbHVtbnM9XFxcIiosKlxcXCIgd2lkdGg9XFxcIjMxMFxcXCIgaGVpZ2h0PVxcXCI4MFxcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiUmVzZXRlYXJcXFwiIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeVxcXCIgKHRhcCk9XFxcInJlc2V0ZWFyKClcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8L0J1dHRvbj5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJWb2x2ZXJcXFwiIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5XFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgKHRhcCk9XFxcImJhY2soKVxcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgIDwvR3JpZExheW91dD5cXHJcXG5cXHJcXG4gICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuICAgIDwvU2Nyb2xsVmlldz5cXHJcXG48L0ZsZXhib3hMYXlvdXQ+XCIiLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgRWxlbWVudFJlZiwgVmlld0NoaWxkfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBhbGVydCwgcHJvbXB0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBNYXRTbmFja0JhciwgTWF0Qm90dG9tU2hlZXQsIE1hdEJvdHRvbVNoZWV0UmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuaW1wb3J0IHsgVXNlciB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci91c2VyLm1vZGVsXCI7XHJcbi8vaW1wb3J0IHsgVXNlcjIgfSBmcm9tIFwiLi9zaGFyZWQvdXNlci91c2VyLm1vZGVsIGNvcHlcIjtcclxuaW1wb3J0IHsgIFVzZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3VzZXIuc2VydmljZVwiO1xyXG5pbXBvcnQgKiBhcyBpbWFnZXBpY2tlciBmcm9tIFwibmF0aXZlc2NyaXB0LWltYWdlcGlja2VyXCI7XHJcbmltcG9ydCAqIGFzIEltYWdlU291cmNlTW9kdWxlIGZyb20gXCJpbWFnZS1zb3VyY2VcIjtcclxudmFyIGZzID0gcmVxdWlyZShcImZpbGUtc3lzdGVtXCIpO1xyXG5pbXBvcnQgeyBJbWFnZSB9IGZyb20gXCJ1aS9pbWFnZVwiO1xyXG5pbXBvcnQgKiBhcyBpbWFnZVNjb3VyZSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9pbWFnZS1zb3VyY2VcIjtcclxuaW1wb3J0IHsgSW1hZ2VBc3NldCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2ltYWdlLWFzc2V0L2ltYWdlLWFzc2V0XCI7XHJcbi8vaW1wb3J0IHsgQWxlcnRTZXJ2aWNlLCB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci9hbGVydC5zZXJ2aWNlXCI7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcblxyXG5cdHNlbGVjdG9yOiBcInJlc2V0X3Bhc3N3b3JkXCIsXHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHR0ZW1wbGF0ZVVybDogXCIuL3Jlc2V0X3Bhc3N3b3JkLmNvbXBvbmVudC5odG1sXCIsXHJcblx0c3R5bGVVcmxzOiBbJy4vcmVzZXRfcGFzc3dvcmQuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSZXNldF9QYXNzd29yZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgbW9kZWw6IGFueSA9IHt9O1xyXG4gICAgbW9kZWwyOiBhbnkgPSB7fTtcclxuICAgIGxvYWRpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAvL0NhbXBvIGVtYWlsXHJcbiAgICAvL2VtYWlsID0gbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5lbWFpbF0pO1xyXG5cclxuICAgIC8vQ2FtcG8gQ29udHJhc2XDsWFcclxuICAgIGhpZGUgPSB0cnVlO1xyXG5cclxuICAgIHBhc3N3b3JkOiBhbnk7XHJcbiAgICBwYXNzd29yZDE6IGFueTtcclxuXHRpbWFnZUFzc2V0cyA9IFtdO1xyXG5cdGltYWdlU3JjOiBhbnk7XHJcblx0XHJcbiAgICAvL0ZPVE9cclxuXHRzZWxlY3RlZEZpbGUgOiBGaWxlID0gbnVsbDtcclxuXHRcclxuXHR1c2VyOiBVc2VyO1xyXG5cdHRvRW1haWw7XHJcblx0Y2NFbWFpbDtcclxuXHRiY2NFbWFpbDtcclxuXHRzdWJqZWN0O1xyXG5cdG1lc3NhZ2U7XHJcblxyXG5cdGNvbnN0cnVjdG9yKFx0XHJcblx0XHRwcml2YXRlIHBhZ2U6IFBhZ2UsIFxyXG5cdFx0cHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIFxyXG5cdFx0cHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBcclxuXHRcdC8vcHJpdmF0ZSBhbGVydFNlcnZpY2U6IEFsZXJ0U2VydmljZSwgICAgICAgIFxyXG5cdFx0Ly9wdWJsaWMgc25hY2tCYXI6IE1hdFNuYWNrQmFyXHJcblx0XHQpIFxyXG5cdFx0e1xyXG5cdFx0XHJcblx0XHR0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcclxuXHRcdHRoaXMudXNlciA9IG5ldyBVc2VyKCk7XHJcblx0XHR0aGlzLnVzZXIudXNlcm5hbWUgPSBcIlwiO1xyXG5cdFx0dGhpcy51c2VyLnBhc3N3b3JkID0gXCJcIjtcclxuXHRcdH1cclxuXHJcblx0bmdPbkluaXQoKTogdm9pZCB7XHJcblx0fVxyXG5cclxuXHQvKnB1YmxpYyBpbWFnZV9waWNrZXIzKCl7XHJcblxyXG5cdFx0XHRsZXQgdGhhdCA9IHRoaXNcclxuXHRcdFx0bGV0IGNvbnRleHQgPSBpbWFnZXBpY2tlci5jcmVhdGUoeyBtb2RlOiBcInNpbmdsZVwiIH0pO1xyXG5cdFx0XHRjb250ZXh0XHJcblx0XHRcdFx0LmF1dGhvcml6ZSgpXHJcblx0XHRcdFx0LnRoZW4oKCkgPT4ge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGNvbnRleHQucHJlc2VudCgpO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LnRoZW4oc2VsZWN0aW9uID0+IHtcclxuXHRcdFx0XHRcdGNvbnN0IGltYWdlQXNzZXQgPSBzZWxlY3Rpb24ubGVuZ3RoID4gMCA/IHNlbGVjdGlvblswXSA6IG51bGw7XHJcblx0XHRcdFx0XHRJbWFnZVNvdXJjZU1vZHVsZS5mcm9tQXNzZXQoaW1hZ2VBc3NldCkudGhlbihcclxuXHRcdFx0XHRcdFx0c2F2ZWRJbWFnZSA9PiB7XHJcblx0XHRcdFx0XHRcdFx0bGV0IGZpbGVuYW1lID0gXCJpbWFnZVwiICsgXCItXCIgKyBuZXcgRGF0ZSgpLmdldFRpbWUoKSArIFwiLnBuZ1wiO1xyXG5cdFx0XHRcdFx0XHRcdGxldCBmb2xkZXIgPSBmcy5rbm93bkZvbGRlcnMuZG9jdW1lbnRzKCk7XHJcblx0XHRcdFx0XHRcdFx0bGV0IHBhdGggPSBmcy5wYXRoLmpvaW4oZm9sZGVyLnBhdGgsIGZpbGVuYW1lKTtcclxuXHRcdFx0XHRcdFx0XHRzYXZlZEltYWdlLnNhdmVUb0ZpbGUocGF0aCwgXCJwbmdcIik7XHJcblx0XHRcdFx0XHRcdFx0dmFyIGxvYWRlZEltYWdlID0gSW1hZ2VTb3VyY2VNb2R1bGUuZnJvbUZpbGUocGF0aCk7XHJcblx0XHRcdFx0XHRcdFx0Ly9sb2FkZWRJbWFnZS5maWxlbmFtZSA9IGZpbGVuYW1lO1xyXG5cdFx0XHRcdFx0XHRcdC8vbG9hZGVkSW1hZ2Uubm90ZSA9IFwiXCI7XHJcblx0XHRcdFx0XHRcdFx0Ly90aGF0LmFycmF5UGljdHVyZXMudW5zaGlmdChsb2FkZWRJbWFnZSk7XHJcblx0XHRcdFx0XHRcdFx0Ly90aGF0LnN0b3JlRGF0YSgpO1xyXG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKGxvYWRlZEltYWdlKTtcclxuXHJcblx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdGVyciA9PiB7XHJcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2coXCJGYWlsZWQgdG8gbG9hZCBmcm9tIGFzc2V0XCIpO1xyXG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKGVycilcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0KTtcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdC5jYXRjaChlcnIgPT4ge1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coZXJyKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcclxuXHR9Ki9cclxuXHJcblx0cHVibGljIGltYWdlX3BpY2tlcigpe1xyXG5cclxuXHRcdGxldCBjb250ZXh0ID0gaW1hZ2VwaWNrZXIuY3JlYXRlKHtcclxuXHRcdFx0bW9kZTogXCJzaW5nbGVcIiAvLyB1c2UgXCJtdWx0aXBsZVwiIGZvciBtdWx0aXBsZSBzZWxlY3Rpb25cclxuXHRcdH0pO1xyXG5cclxuXHRcdGNvbnRleHQuYXV0aG9yaXplKCkudGhlbigoKSA9PiB7XHJcblx0XHRcdFx0cmV0dXJuIGNvbnRleHQucHJlc2VudCgpO1xyXG5cdFx0XHR9KS50aGVuKChzZWxlY3Rpb24pID0+IHtcclxuXHJcblx0XHRcdFx0dGhpcy5pbWFnZVNyYz1KU09OLnN0cmluZ2lmeShzZWxlY3Rpb25bMF0pO1xyXG5cclxuXHRcdFx0XHR2YXIganNvbiA9IEpTT04ucGFyc2UodGhpcy5pbWFnZVNyYyk7XHJcblx0XHRcdFx0dGhpcy5pbWFnZVNyYyA9IEpTT04uc3RyaW5naWZ5KGpzb24uX2FuZHJvaWQpO1xyXG5cdFx0XHRcdHRoaXMuaW1hZ2VTcmMgPSB0aGlzLmltYWdlU3JjLnJlcGxhY2UoL1wiL2dpLFwiXCIpO1xyXG5cdFx0XHRcdHRoaXMubW9kZWwuc3JjID0gdGhpcy5pbWFnZVNyYy5zdWJzdHIodGhpcy5pbWFnZVNyYy5sYXN0SW5kZXhPZihcIi9cIikgKyAxKTtcclxuXHRcdFx0XHRjb25zb2xlLmxvZyhcIlJ1dGEgZGVsIGFyY2hpdm8gc2VsZWNjaW9uYWRvOiBcIiArIEpTT04uc3RyaW5naWZ5KGpzb24uX2FuZHJvaWQpKTtcclxuXHJcblx0XHRcdH0pLmNhdGNoKGZ1bmN0aW9uIChlKSB7XHJcblx0XHRcdFx0Y29uc29sZS5sb2coZSk7XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdHJlZ2lzdGVyMigpXHJcblx0e1xyXG5cdFx0Y29uc29sZS5sb2coXCJob2xhNTVcIik7XHJcblx0XHRpZih0aGlzLm1vZGVsLmZpcnN0TmFtZSAmJiB0aGlzLm1vZGVsLmxhc3ROYW1lICYmIHRoaXMubW9kZWwudXNlcm5hbWUgJiYgdGhpcy5tb2RlbC5udW1hdCAmJiB0aGlzLm1vZGVsLnBhc3N3b3JkICYmIHRoaXMuaW1hZ2VTcmMpXHJcblx0XHR7XHJcblx0XHRcdGNvbnNvbGUubG9nKFwiaG9sYTIzNFwiKTtcclxuXHRcdFx0Y29uc29sZS5sb2coXCJob2xhXCIgKyAoSlNPTi5zdHJpbmdpZnkodGhpcy5tb2RlbCkpKTtcclxuXHRcdFx0aWYodGhpcy51c2VyU2VydmljZS5jcmVhdGVwaG90bzIodGhpcy5pbWFnZVNyYykpXHJcblx0XHRcdHtcclxuXHRcdFx0XHRjb25zb2xlLmxvZygoSlNPTi5zdHJpbmdpZnkodGhpcy5tb2RlbCkpKTtcclxuXHRcdFx0XHR0aGlzLnVzZXJTZXJ2aWNlLmNyZWF0ZSh0aGlzLm1vZGVsKVxyXG5cdFx0XHRcdC5zdWJzY3JpYmUoXHJcblx0XHRcdFx0XHRkYXRhID0+IHtcclxuXHRcdFx0XHRcdFx0YWxlcnQoXCJSZWdpc3RyYWRvIGNvbiDDqXhpdG9cIik7XHJcblx0XHRcdFx0XHRcdHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRlcnJvciA9PiB7XHJcblx0XHRcdFx0XHRcdGFsZXJ0KGVycm9yKTtcclxuXHRcdFx0XHRcdFx0dGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcblx0XHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdH1cclxuXHRcdFx0ZWxzZXtcclxuXHRcdFx0XHRhbGVydChcIk5vIHNlIGhhIHBvZGlkbyBzdWJpciBzdSBmb3RvXCIpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRlbHNle1xyXG5cdFx0XHRhbGVydChcIkNvbXBsZXRlIHRvZG9zIGxvcyBjYW1wb3MgcmVxdWVyaWRvc1wiKTtcclxuXHRcdH1cclxuXHJcblx0XHQvL3ZhciBmaWxlID0gIFwiL3N0b3JhZ2UvZW11bGF0ZWQvMC9XaGF0c0FwcC9NZWRpYS9XaGF0c0FwcCBJbWFnZXMvSU1HLTIwMjAwMjI1LVdBMDAwMi5qcGdcIjtcclxuXHRcdC8vdmFyIHVybCA9IFwiaHR0cHM6Ly9zb21lLnJlbW90ZS5zZXJ2aWNlLmNvbS9wYXRoXCI7XHJcblx0XHQvL3ZhciBuYW1lID0gZmlsZS5zdWJzdHIoZmlsZS5sYXN0SW5kZXhPZihcIi9cIikgKyAxKTtcclxuXHRcdC8vY29uc29sZS5sb2cobmFtZSk7XHJcblx0XHQvL2FsZXJ0KFwiRnVuY2nDs24gbm8gZGlzcG9uaWJsZSBhY3R1YWxtZW50ZVwiKVxyXG5cdH1cclxuXHJcbiAgIHJlZ2lzdGVyKCkge1xyXG5cdC8vdGhpcy5sb2FkaW5nID0gdHJ1ZTtcclxuXHQvL3RoaXMubW9kZWwuc3JjID0gdGhpcy5zZWxlY3RlZEZpbGUubmFtZTtcclxuXHR0aGlzLnVzZXJTZXJ2aWNlLmNyZWF0ZSh0aGlzLm1vZGVsKVxyXG5cdFx0LnN1YnNjcmliZShcclxuXHRcdFx0ZGF0YSA9PiB7XHJcblx0XHRcdFx0YWxlcnQoXCJSZWdpc3RyYWRvIGNvbiDDqXhpdG9cIik7XHJcblx0XHRcdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xyXG5cdFx0XHRcdGNvbnNvbGUubG9nKGRhdGEpO1xyXG5cdFx0XHR9LFxyXG5cdFx0XHQoZXhjZXB0aW9uKSA9PiB7XHJcblx0XHRcdFx0aWYgKGV4Y2VwdGlvbi5lcnJvciAmJiBleGNlcHRpb24uZXJyb3IuZGVzY3JpcHRpb24pIHtcclxuXHRcdFx0XHRcdGFsZXJ0KGV4Y2VwdGlvbi5lcnJvci5kZXNjcmlwdGlvbik7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdGFsZXJ0KGV4Y2VwdGlvbi5lcnJvcik7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdFx0XHJcblx0fVxyXG5cclxuICAgIGNvbXByb2Jhcl9jb250cmFzZcOxYSgpe1xyXG4gICAgICAgIGlmKHRoaXMubW9kZWwyLnBhc3N3b3JkICYmIHRoaXMubW9kZWwyLnBhc3N3b3JkMSAmJiB0aGlzLnBhc3N3b3JkPT10aGlzLnBhc3N3b3JkMSlcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblx0cmVzZXRlYXIoKXtcclxuICAgICAgICBpZih0aGlzLm1vZGVsMi5wYXNzd29yZCAmJiB0aGlzLm1vZGVsMi5wYXNzd29yZDEgJiYgdGhpcy5tb2RlbC50b2tlbiAmJiB0aGlzLm1vZGVsLnVzZXJuYW1lKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGhpcy5tb2RlbC5wYXNzd29yZCA9IHRoaXMubW9kZWwyLnBhc3N3b3JkO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLm1vZGVsKSk7XHJcbiAgICAgICAgICAgIHRoaXMudXNlclNlcnZpY2UucmVzZXRQYXNzd29yZCh0aGlzLm1vZGVsKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnQoXCJTdSBjb250cmFzZcOxYSBzZSBoYSByZXNldGVhZG9cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGV4Y2VwdGlvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChleGNlcHRpb24uZXJyb3IgJiYgZXhjZXB0aW9uLmVycm9yLmRlc2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KGV4Y2VwdGlvbi5lcnJvci5kZXNjcmlwdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoZXhjZXB0aW9uLmVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgYWxlcnQoXCJSZWxsZW5lIHRvZG9zIGxvcyBjYW1wb3NcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cdGJhY2soKSB7XHJcblx0XHRcdHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvbG9naW5cIl0pO1xyXG5cdH1cclxuXHQvKnVwbG9hZChhdHRhY2htZW50KSB7XHJcblx0XHRjb25zb2xlLmxvZyhhdHRhY2htZW50KTtcclxuXHRcdCAgdGhpcy51c2VyU2VydmljZS5jcmVhdGVwaG90byhhdHRhY2htZW50KVxyXG5cdFx0XHQgIC5zdWJzY3JpYmUoXHJcblx0XHRcdFx0ICBkYXRhID0+IHtcclxuXHRcdFx0XHRcdCAgYWxlcnQoXCJTdWJpZG8gY29uIMOpeGl0byBcIik7XHJcblx0XHRcdFx0XHQgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcclxuXHRcdFx0XHQgIH0sXHJcblx0XHRcdFx0ICBlcnJvciA9PiB7XHJcblx0XHRcdFx0XHQgIGFsZXJ0KGVycm9yKTtcclxuXHRcdFx0XHQgIH0pO1xyXG5cdCAgfSovXHJcblx0ICBvbkZpbGVTZWxlY3RlZChldmVudCl7XHJcblx0XHR0aGlzLnNlbGVjdGVkRmlsZSA9IGV2ZW50LnRhcmdldC5maWxlc1swXTtcclxuXHRcdGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRGaWxlKTtcclxuXHQgIH1cclxuXHQgIG9uVXBsb2FkKCl7XHJcblx0XHR0cnl7XHJcblx0XHRcdGNvbnNvbGUubG9nKFwiUHJ1ZWJhXCIpO1xyXG5cdFx0XHR2YXIgZmlsZSA9ICBcIi9zdG9yYWdlL2VtdWxhdGVkLzAvV2hhdHNBcHAvTWVkaWEvV2hhdHNBcHAgSW1hZ2VzL0lNRy0yMDIwMDMwMS1XQTAwMDEuanBnXCI7XHJcblx0XHRcdHZhciBuYW1lID0gZmlsZS5zdWJzdHIoZmlsZS5sYXN0SW5kZXhPZihcIi9cIikgKyAxKTtcclxuXHRcdFx0Y29uc29sZS5sb2cobmFtZSk7XHJcblx0XHRcdGNvbnN0IGZkID0gbmV3IEZvcm1EYXRhKCk7XHJcblx0XHRcdC8vZmQuYXBwZW5kKCdpbWFnZScsIHRoaXMuc2VsZWN0ZWRGaWxlLCB0aGlzLnNlbGVjdGVkRmlsZS5uYW1lKTtcclxuXHRcdFx0ZmQuYXBwZW5kKCdpbWFnZW4nLCBmaWxlLCBuYW1lKTtcclxuXHRcdFx0Y29uc29sZS5sb2coJyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKicpO1xyXG5cdFx0XHRjb25zb2xlLmxvZyhmZCk7XHJcblx0XHRcdHRoaXMudXNlclNlcnZpY2UuY3JlYXRlcGhvdG8oZmQpXHJcblx0XHRcdCAgLnN1YnNjcmliZShcclxuXHRcdFx0XHRkYXRhID0+IHtcclxuXHRcdFx0XHRcdGFsZXJ0KFwiU3ViaWRhIGZvdG8gY29uIMOpeGl0byBcIik7XHJcblx0XHRcdFx0XHR0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoWycvbG9naW4nXSk7XHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHRlcnJvciA9PiB7XHJcblx0XHRcdFx0XHQvL3RoaXMuYWxlcnRTZXJ2aWNlLmVycm9yKGVycm9yKTtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGVycm9yKTtcclxuXHRcdFx0XHRcdGFsZXJ0KFwiRXJyb3IgZGUgc3ViaWRhIGVuIGxhIGltYWdlblwiKTtcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHR9XHJcblx0XHRjYXRjaChlKXtcclxuXHRcdFx0Y29uc29sZS5sb2coZSk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQgIFxyXG5cdC8vb3BlblNuYWNrQmFyKCkge1xyXG5cdFx0Ly90aGlzLnNuYWNrQmFyLm9wZW4oXCJSZWdpc3RyYWRvIGNvbiBleGl0b1wiLCBcIkNlcnJhclwiLCB7XHJcblx0XHQgIC8vZHVyYXRpb246IDIwMDBcclxuXHRcdC8vfSk7XHJcblx0IC8vIH1cclxufSIsIm1vZHVsZS5leHBvcnRzID0gXCIuaG9tZS1wYW5lbHtcXHJcXG4gICAgdmVydGljYWwtYWxpZ246IGNlbnRlcjsgXFxyXFxuICAgIGZvbnQtc2l6ZTogMjA7XFxyXFxuICAgIG1hcmdpbjogMTU7XFxyXFxufVxcclxcblxcclxcbi5kZXNjcmlwdGlvbi1sYWJlbHtcXHJcXG4gICAgbWFyZ2luLWJvdHRvbTogMTU7XFxyXFxufVxcclxcblxcclxcbi5wcnVlYmEge1xcclxcbiAgZm9udC1zaXplOiAyNTtcXHJcXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XFxyXFxuICBtYXJnaW4tYm90dG9tOiAyMDtcXHJcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXHJcXG4gIGNvbG9yOndoaXRlO1xcclxcbn1cXHJcXG5cXHJcXG4ucmVjdWFkcm8ge1xcclxcbiAgZm9udC1zaXplOiAzNTtcXHJcXG4gIGZvbnQtd2VpZ2h0OiA4MDA7XFxyXFxuICBtYXJnaW4tYm90dG9tOiAyMDtcXHJcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXHJcXG59XCIiLCJtb2R1bGUuZXhwb3J0cyA9IFwiPEFjdGlvbkJhciB0aXRsZT1cXFwiSG9tZVxcXCI+XFxyXFxuPC9BY3Rpb25CYXI+XFxyXFxuXFxyXFxuPEdyaWRMYXlvdXQgYmFja2dyb3VuZENvbG9yPVxcXCIjMDM3NGRkXFxcIj5cXHJcXG4gICAgPFNjcm9sbFZpZXc+XFxyXFxuICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImhvbWUtcGFuZWxcXFwiPlxcclxcbiAgICAgICAgICAgIDwhLS1BZGQgeW91ciBwYWdlIGNvbnRlbnQgaGVyZS0tPlxcclxcbiAgICAgICAgICAgIDxMYWJlbCB0ZXh0V3JhcD1cXFwidHJ1ZVxcXCIgdGV4dD1cXFwiRXNjcmliYSBzdSBjw7NkaWdvIGRlIHNlc2nDs25cXFwiXFxyXFxuICAgICAgICAgICAgICAgIHRleHRBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgY2xhc3M9XFxcInBydWViYVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgdGV4dEZpZWxkQmFja2dyb3VuZENvbG9yPVxcXCJ3aGl0ZVxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICA8VGV4dEZpZWxkIGhlaWdodD1cXFwiMzBcXFwiIG1heExlbmd0aD1cXFwiNFxcXCIgcm93PVxcXCIzXFxcIiBjb2w9XFxcIjFcXFwiXFxyXFxuICAgICAgICAgICAgICAgIFsobmdNb2RlbCldPVxcXCJtb2RlbC5jb2RlX3Nlc3Npb25cXFwiIGNsYXNzPVxcXCJyZWN1YWRyb1xcXCJcXHJcXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yPVxcXCJ3aGl0ZVxcXCIgaGVpZ2h0PVxcXCI4MFxcXCIgd2lkdGg9XFxcIjIwMFxcXCI+XFxyXFxuICAgICAgICAgICAgPC9UZXh0RmllbGQ+XFxyXFxuICAgICAgICAgICAgPEJ1dHRvbiB3aWR0aD1cXFwiOTAlXFxcIiB0ZXh0PVxcXCJDb250aW51YXJcXFwiICh0YXApPVxcXCJpbnNlcnRfY29kZV9zZXNzaW9uKClcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgPC9TdGFja0xheW91dD5cXHJcXG4gICAgPC9TY3JvbGxWaWV3PlxcclxcbjwvR3JpZExheW91dD5cIiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBFbGVtZW50UmVmLCBWaWV3Q2hpbGR9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IGFsZXJ0LCBwcm9tcHQgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbi8vaW1wb3J0IHsgTWF0U25hY2tCYXIsIE1hdEJvdHRvbVNoZWV0LCBNYXRCb3R0b21TaGVldFJlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5tb2RlbFwiO1xyXG5pbXBvcnQgeyAgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlXCI7XHJcbi8vaW1wb3J0IHsgQWxlcnRTZXJ2aWNlLCB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci9hbGVydC5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogXCJTZXNzaW9uX2NvZGVcIixcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG5cdHRlbXBsYXRlVXJsOiBcIi4vc2Vzc2lvbl9jb2RlLmNvbXBvbmVudC5odG1sXCIsXHJcblx0c3R5bGVVcmxzOiBbJy4vc2Vzc2lvbl9jb2RlLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2Vzc2lvbl9jb2RlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBcclxuICAgIGN1cnJlbnRVc2VyOiBVc2VyO1xyXG4gICAgbW9kZWw6IGFueSA9IHt9O1xyXG4gICAgbG9hZGluZyA9IGZhbHNlO1xyXG4gICAgbWVuc2FqZTogYW55O1xyXG5cclxuICAgIGJvbDogYm9vbGVhblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXIgPSBuZXcgVXNlcigpO1xyXG4gICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xyXG4gICAgICAgIC8vdGhpcy5jdXJyZW50VXNlciA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRVc2VyJykpO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXI9dGhpcy51c2VyU2VydmljZS5jdXJyZW50VXNlcigpXHJcbiAgICB9XHJcblxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHR9XHJcblxyXG5cdGluc2VydF9jb2RlX3Nlc3Npb24oKVxyXG4gICAgeyAgIGlmKHRoaXMubW9kZWwuY29kZV9zZXNzaW9uPT1cIlwiKVxyXG4gICAgICAgICAgICBhbGVydChcIkludHJvZHV6Y2EgdW4gY8OzZGlnbyBkZSBzZXNpw7NuIHbDoWxpZG9cIik7XHJcbiAgICAgICAgZWxzZVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlci5jb2RlX3Nlc3Npb24gPSBwYXJzZUludCh0aGlzLm1vZGVsLmNvZGVfc2Vzc2lvbik7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudFVzZXJcIixKU09OLnN0cmluZ2lmeSh0aGlzLmN1cnJlbnRVc2VyKSk7XHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2codGhpcy5tb2RlbC5jb2RlX3Nlc3Npb24pO1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKHRoaXMuY3VycmVudFVzZXIuc2Vzc2lvbl9jb2RlKTtcclxuICAgICAgICAgICAgLy9hbGVydCh0aGlzLmN1cnJlbnRVc2VyLnNlc3Npb25fY29kZSk7XHJcbiAgICAgICAgICAgIC8vdGhpcy51c2VyU2VydmljZS5zZXRVc2VyKHRoaXMuY3VycmVudFVzZXIpO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZSh0aGlzLmN1cnJlbnRVc2VyKTtcclxuICAgICAgICAgICAgLy9hbGVydCh0aGlzLmN1cnJlbnRVc2VyLnNlc3Npb25fY29kZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICB1cGRhdGUoY3VycmVudFVzZXI6IFVzZXIpIHtcclxuICAgICAgICAvL3RoaXMudXNlclNlcnZpY2Uuc2V0VXNlcih0aGlzLmN1cnJlbnRVc2VyKTtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRVc2VyXCIsSlNPTi5zdHJpbmdpZnkoY3VycmVudFVzZXIpKTtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKHRoaXMuY3VycmVudFVzZXIuc2VhdCk7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLmN1cnJlbnRVc2VyKSk7XHJcbiAgICAgICAgdGhpcy51c2VyU2VydmljZS51cGRhdGUodGhpcy5jdXJyZW50VXNlcikuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9pZGVudGlmaWNhdGlvblwiXSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgYWwgYWN0dWFsaXphciBlbCBjw7NkaWdvIGRlIHNlc3Npw7NuXCIpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iLCJleHBvcnQgY2xhc3MgQ29uZmlnIHtcbiAgLy8gY2FzYSAvL3N0YXRpYyBhcGlVcmwgPSBcImh0dHA6Ly8xOTIuMTY4LjEuMzY6NDAwMFwiO1xuICAvLyBlbXVsYWRvciAvL3N0YXRpYyBhcGlVcmwgPSBcImh0dHA6Ly8xMC4wLjMuMjo0MDAwXCI7XG4gIC8vIGFlIGF2YW5hIHdpZmkgLy9zdGF0aWMgYXBpVXJsID0gXCJodHRwOi8vMTkyLjE2OC4zNS4yOjQwMDBcIjtcbiAgLy8gbW92aWwgLy8gc3RhdGljIGFwaVVybCA9IFwiaHR0cDovLzE5Mi4xNjguNDMuMjI2OjQwMDBcIjtcbiAgLy8gZWR1cm9hbSAvLyBzdGF0aWMgYXBpVXJsID0gXCJodHRwOi8vMTAuMTUwLjIzMi4yMzE6NDAwMFwiO1xuICAvLyBhdmFuYSAvLyBzdGF0aWMgYXBpVXJsID0gXCJodHRwOi8vMTAuOTkuMTUzLjEyODo0MDAwXCI7XG4gIHN0YXRpYyBhcGlVcmwgPSBcImh0dHA6Ly8xOTIuMTY4LjEuMzY6NDAwMFwiO1xuICBzdGF0aWMgYXBwS2V5ID0gXCJraWRfSHlIb1RfUkVmXCI7XG4gIHN0YXRpYyBhdXRoSGVhZGVyID0gXCJCYXNpYyBhMmxrWDBoNVNHOVVYMUpGWmpvMU1Ua3hNREpsWldGaE16UTBNek15T0RGak4yTXlPRE0zTUdRNU9USXpNUVwiO1xuICBzdGF0aWMgdG9rZW4gPSBcIlwiO1xufSIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cFJlc3BvbnNlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIHRocm93RXJyb3IgfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAsIHRhcCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5cclxuaW1wb3J0ICogYXMgbG9jYWxTdG9yYWdlIGZyb20gXCJuYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlXCI7XHJcblxyXG5pbXBvcnQgeyBVc2VyIH0gZnJvbSBcIi4vdXNlci5tb2RlbFwiO1xyXG5pbXBvcnQgeyBDb25maWcgfSBmcm9tIFwiLi4vY29uZmlnXCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBdXRoZW50aWNhdGVTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XHJcblxyXG4gICAgcmVnaXN0ZXIodXNlcjogVXNlcikge1xyXG4gICAgICAgIGlmICghdXNlci51c2VybmFtZSB8fCAhdXNlci5wYXNzd29yZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihcIlBsZWFzZSBwcm92aWRlIGJvdGggYW4gZW1haWwgYWRkcmVzcyBhbmQgcGFzc3dvcmQuXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KFxyXG4gICAgICAgICAgICBDb25maWcuYXBpVXJsICsgXCJ1c2VyL1wiICsgQ29uZmlnLmFwcEtleSxcclxuICAgICAgICAgICAgSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXIudXNlcm5hbWUsXHJcbiAgICAgICAgICAgICAgICBlbWFpbDogdXNlci51c2VybmFtZSxcclxuICAgICAgICAgICAgICAgIHBhc3N3b3JkOiB1c2VyLnBhc3N3b3JkXHJcbiAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICB7IGhlYWRlcnM6IHRoaXMuZ2V0Q29tbW9uSGVhZGVycygpIH1cclxuICAgICAgICApLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IodGhpcy5oYW5kbGVFcnJvcnMpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBsb2dpbih1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KFxyXG4gICAgICAgICAgICBDb25maWcuYXBpVXJsICsgJy91c2Vycy9hdXRoZW50aWNhdGUnLFxyXG4gICAgICAgICAgICAoe1xyXG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXIudXNlcm5hbWUsXHJcbiAgICAgICAgICAgICAgICBwYXNzd29yZDogdXNlci5wYXNzd29yZFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICkucGlwZShcclxuICAgICAgICAgICAgbWFwKHJlc3BvbnNlID0+IHJlc3BvbnNlKSxcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcih0aGlzLmhhbmRsZUVycm9ycylcclxuICAgICAgICApO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBsb2dpbjUodXNlcjogVXNlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChDb25maWcuYXBpVXJsICsgJy91c2Vycy9hdXRoZW50aWNhdGUnLCB7IHVzZXJuYW1lOiB1c2VyLnVzZXJuYW1lLCBwYXNzd29yZDogdXNlci5wYXNzd29yZCB9KVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHVzZXIzMiA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBsb2dpbiBzdWNjZXNzZnVsIGlmIHRoZXJlJ3MgYSBqd3QgdG9rZW4gaW4gdGhlIHJlc3BvbnNlXHJcbiAgICAgICAgICAgICAgICBpZiAodXNlcjMyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc3RvcmUgdXNlciBkZXRhaWxzIGFuZCBqd3QgdG9rZW4gaW4gbG9jYWwgc3RvcmFnZSB0byBrZWVwIHVzZXIgbG9nZ2VkIGluIGJldHdlZW4gcGFnZSByZWZyZXNoZXNcclxuICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY3VycmVudFVzZXInLCBKU09OLnN0cmluZ2lmeSh1c2VyKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHVzZXIzMjtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlKHVzZXI6IFVzZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvcmVnaXN0ZXInLCB1c2VyKTtcclxuICAgIH1cclxuICAgIGNyZWF0ZXBob3RvKGZpbGU6IEZvcm1EYXRhKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCIqKiEhRklMRSEhKipcIik7XHJcbiAgICAgICAgY29uc29sZS5sb2coZmlsZSk7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoQ29uZmlnLmFwaVVybCArICcvdXBsb2FkJywgZmlsZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICBcclxuICAgIGdldENvbW1vbkhlYWRlcnMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgICAgIFwiQXV0aG9yaXphdGlvblwiOiBDb25maWcuYXV0aEhlYWRlclxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvL2xvZ291dChfaWQ6IHN0cmluZywgdXNlcjogVXNlcikge1xyXG4gICAgICAgLy8gcmV0dXJuIHRoaXMuaHR0cC5wdXQoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvbG9nb3V0LycgKyBfaWQsIHVzZXIpO1xyXG4gICAgLy99XHJcblxyXG4gICAgaGFuZGxlRXJyb3JzKGVycm9yOiBSZXNwb25zZSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KGVycm9yKSk7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vSGVtb3MgYcOxYWRpZG8gZWwge3Jlc3BvbnNlVHlwZTogJ3RleHQnfSBwb3JxdWUgZGFiYSB1biBlcnJvciBkZSBhcHJzaW5nIGN1YW5kbyBsZcOtYSBlbCB2YWxvciByZXRvcm5hZG9cclxuICAgIC8vIEVsIG3DqXRvZG8gZW4gcmVhbGlkYWQgZXMgIHJldHVybiB0aGlzLmh0dHAucHV0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzLycgKyB1c2VyLl9pZCwgdXNlcilcclxuICAgIHVwZGF0ZSh1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wdXQoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvJyArIHVzZXIuX2lkLCB1c2VyLHtyZXNwb25zZVR5cGU6ICd0ZXh0J30pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dChfaWQ6IHN0cmluZywgdXNlcjogVXNlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucHV0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2xvZ291dC8nICsgX2lkLCB1c2VyLHtyZXNwb25zZVR5cGU6ICd0ZXh0J30pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRVc2VyKGRhdGE6YW55KXtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRVc2VyXCIsSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjdXJyZW50VXNlcigpe1xyXG4gICAgICAgIHJldHVybiBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudFVzZXJcIikpOyAvL3x8IG51bGw7XHJcbiAgICB9XHJcbn0iLCJleHBvcnQgY2xhc3MgVXNlciB7XHJcbiAgICBfaWQ6IHN0cmluZztcclxuICAgIHVzZXJuYW1lOiBzdHJpbmc7XHJcbiAgICBwYXNzd29yZDogc3RyaW5nO1xyXG4gICAgZmlyc3ROYW1lOiBzdHJpbmc7XHJcbiAgICBsYXN0TmFtZTogc3RyaW5nO1xyXG4gICAgY2xhc3M6IHN0cmluZztcclxuICAgIHNlYXQ6IHN0cmluZztcclxuICAgIG51bWF0OiBzdHJpbmc7XHJcbiAgICBxcnN0cmluZzogc3RyaW5nO1xyXG4gICAgYW5zd2VyOiBzdHJpbmc7XHJcbiAgICBjb2RlX3Nlc3Npb246IG51bWJlcjtcclxuICAgIGlkX2NsYXNzX3Nlc3Npb246IHN0cmluZztcclxuICAgIHNyYzogc3RyaW5nO1xyXG4gIH0iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBSZXNwb25zZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCB0aHJvd0Vycm9yIH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCB0YXAgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuXHJcbmltcG9ydCAqIGFzIGxvY2FsU3RvcmFnZSBmcm9tIFwibmF0aXZlc2NyaXB0LWxvY2Fsc3RvcmFnZVwiO1xyXG5cclxuaW1wb3J0IHsgVXNlciB9IGZyb20gXCIuL3VzZXIubW9kZWxcIjtcclxuaW1wb3J0IHsgVXNlcjIgfSBmcm9tIFwiLi91c2VyLm1vZGVsIGNvcHlcIjtcclxuaW1wb3J0IHsgQ29uZmlnIH0gZnJvbSBcIi4uL2NvbmZpZ1wiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7IH1cclxuXHJcbiAgICByZWdpc3Rlcih1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgaWYgKCF1c2VyLnVzZXJuYW1lIHx8ICF1c2VyLnBhc3N3b3JkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKFwiUGxlYXNlIHByb3ZpZGUgYm90aCBhbiBlbWFpbCBhZGRyZXNzIGFuZCBwYXNzd29yZC5cIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoXHJcbiAgICAgICAgICAgIENvbmZpZy5hcGlVcmwgKyBcInVzZXIvXCIgKyBDb25maWcuYXBwS2V5LFxyXG4gICAgICAgICAgICBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgICAgICAgICB1c2VybmFtZTogdXNlci51c2VybmFtZSxcclxuICAgICAgICAgICAgICAgIGVtYWlsOiB1c2VyLnVzZXJuYW1lLFxyXG4gICAgICAgICAgICAgICAgcGFzc3dvcmQ6IHVzZXIucGFzc3dvcmRcclxuICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgIHsgaGVhZGVyczogdGhpcy5nZXRDb21tb25IZWFkZXJzKCkgfVxyXG4gICAgICAgICkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcih0aGlzLmhhbmRsZUVycm9ycylcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ2luKHVzZXI6IFVzZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoXHJcbiAgICAgICAgICAgIENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2F1dGhlbnRpY2F0ZScsXHJcbiAgICAgICAgICAgICh7XHJcbiAgICAgICAgICAgICAgICB1c2VybmFtZTogdXNlci51c2VybmFtZSxcclxuICAgICAgICAgICAgICAgIHBhc3N3b3JkOiB1c2VyLnBhc3N3b3JkXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKS5waXBlKFxyXG4gICAgICAgICAgICBtYXAocmVzcG9uc2UgPT4gcmVzcG9uc2UpLFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKHRoaXMuaGFuZGxlRXJyb3JzKVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGxvZ2luNSh1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2F1dGhlbnRpY2F0ZScsIHsgdXNlcm5hbWU6IHVzZXIudXNlcm5hbWUsIHBhc3N3b3JkOiB1c2VyLnBhc3N3b3JkIH0pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUodXNlcjMyID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIGxvZ2luIHN1Y2Nlc3NmdWwgaWYgdGhlcmUncyBhIGp3dCB0b2tlbiBpbiB0aGUgcmVzcG9uc2VcclxuICAgICAgICAgICAgICAgIGlmICh1c2VyMzIpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBzdG9yZSB1c2VyIGRldGFpbHMgYW5kIGp3dCB0b2tlbiBpbiBsb2NhbCBzdG9yYWdlIHRvIGtlZXAgdXNlciBsb2dnZWQgaW4gYmV0d2VlbiBwYWdlIHJlZnJlc2hlc1xyXG4gICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50VXNlcicsIEpTT04uc3RyaW5naWZ5KHVzZXIpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdXNlcjMyO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXNldFBhc3N3b3JkKHVzZXI6IFVzZXIyKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkodXNlcikpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChcclxuICAgICAgICAgICAgQ29uZmlnLmFwaVVybCArICcvdXNlcnMvcmVzZXRwYXNzd29yZCcsIHVzZXJcclxuICAgICAgICAgICAgLyooe1xyXG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXIudXNlcm5hbWUsXHJcbiAgICAgICAgICAgICAgICBwYXNzd29yZDogdXNlci5wYXNzd29yZCxcclxuICAgICAgICAgICAgICAgIHRva2VuOiB1c2VyLnRva2VuXHJcbiAgICAgICAgICAgIH0pKi8se3Jlc3BvbnNlVHlwZTogJ3RleHQnfVxyXG4gICAgICAgICkucGlwZShcclxuICAgICAgICAgICAgbWFwKHJlc3BvbnNlID0+IHJlc3BvbnNlKSxcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcih0aGlzLmhhbmRsZUVycm9ycylcclxuICAgICAgICApO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGUodXNlcjogVXNlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChDb25maWcuYXBpVXJsICsgJy91c2Vycy9yZWdpc3RlcicsIHVzZXIse3Jlc3BvbnNlVHlwZTogJ3RleHQnfSkucGlwZShcclxuICAgICAgICAgICAgbWFwKHJlc3BvbnNlID0+IHJlc3BvbnNlKSxcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcih0aGlzLmhhbmRsZUVycm9ycylcclxuICAgICAgICApOztcclxuICAgIH1cclxuXHJcbiAgICByZXNldF9wYXNzd29yZF9lbWFpbCh1c2VyOiBhbnkgPSB7fSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChDb25maWcuYXBpVXJsICsgJy9tYWlscmVzZXRwYXNzd29yZDInLCB1c2VyLHtyZXNwb25zZVR5cGU6ICd0ZXh0J30pLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcChyZXNwb25zZSA9PiByZXNwb25zZSksXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IodGhpcy5oYW5kbGVFcnJvcnMpXHJcbiAgICAgICAgKTs7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlcGhvdG8oZmlsZTogRm9ybURhdGEpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIioqISFGSUxFISEqKlwiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhmaWxlKTtcclxuICAgICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChDb25maWcuYXBpVXJsICsgJy91cGxvYWQnLCBmaWxlKTtcclxuICAgICAgfVxyXG5cclxuICAgIGNyZWF0ZXBob3RvMihmaWxlOnN0cmluZykge1xyXG4gICAgICAgIFxyXG4gICAgICAgIHZhciBuYW1lID0gZmlsZS5zdWJzdHIoZmlsZS5sYXN0SW5kZXhPZihcIi9cIikgKyAxKTtcclxuICAgICAgICB2YXIgYmdodHRwID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1iYWNrZ3JvdW5kLWh0dHBcIik7XHJcbiAgICAgICAgdmFyIHNlc3Npb24gPSBiZ2h0dHAuc2Vzc2lvbihcImltYWdlLXVwbG9hZFwiKTtcclxuXHJcbiAgICAgICAgdmFyIHJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgIHVybDogQ29uZmlnLmFwaVVybCArICcvdXBsb2FkMicsXHJcbiAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vb2N0ZXQtc3RyZWFtXCIsXHJcbiAgICAgICAgICAgICAgICBcIkZpbGUtTmFtZVwiOiBuYW1lXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIlVwbG9hZGluZyBcIiArIG5hbWUsXHJcbiAgICAgICAgICAgIC8vYW5kcm9pZERpc3BsYXlOb3RpZmljYXRpb25Qcm9ncmVzczogZmFsc2UsXHJcbiAgICAgICAgICAgIGFuZHJvaWRBdXRvQ2xlYXJOb3RpZmljYXRpb246IHRydWUsXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIjRcIik7XHJcblxyXG4gICAgICAgIHZhciB0YXNrID1zZXNzaW9uLnVwbG9hZEZpbGUoZmlsZSwgcmVxdWVzdCk7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJJbWFnZW4gc3ViaWRhIGNvcnJlY3RhbWVudGVcIik7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29tbW9uSGVhZGVyczIoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9vY3RldC1zdHJlYW1cIixcclxuICAgICAgICAgICAgXCJGaWxlLU5hbWVcIjogbmFtZVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGdldENvbW1vbkhlYWRlcnMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgICAgIFwiQXV0aG9yaXphdGlvblwiOiBDb25maWcuYXV0aEhlYWRlclxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvL2xvZ291dChfaWQ6IHN0cmluZywgdXNlcjogVXNlcikge1xyXG4gICAgICAgLy8gcmV0dXJuIHRoaXMuaHR0cC5wdXQoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvbG9nb3V0LycgKyBfaWQsIHVzZXIpO1xyXG4gICAgLy99XHJcblxyXG4gICAgaGFuZGxlRXJyb3JzKGVycm9yOiBSZXNwb25zZSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KGVycm9yKSk7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vSGVtb3MgYcOxYWRpZG8gZWwge3Jlc3BvbnNlVHlwZTogJ3RleHQnfSBwb3JxdWUgZGFiYSB1biBlcnJvciBkZSBhcHJzaW5nIGN1YW5kbyBsZcOtYSBlbCB2YWxvciByZXRvcm5hZG9cclxuICAgIC8vIEVsIG3DqXRvZG8gZW4gcmVhbGlkYWQgZXMgIHJldHVybiB0aGlzLmh0dHAucHV0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzLycgKyB1c2VyLl9pZCwgdXNlcilcclxuICAgIHVwZGF0ZSh1c2VyOiBVc2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wdXQoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvJyArIHVzZXIuX2lkLCB1c2VyLHtyZXNwb25zZVR5cGU6ICd0ZXh0J30pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qdXBkYXRlSUQodXNlcjogVXNlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucHV0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzLycgKyB1c2VyLl9pZCwgdXNlcix7cmVzcG9uc2VUeXBlOiAndGV4dCd9KTtcclxuICAgIH0qL1xyXG5cclxuICAgIGdldFRlc3RCeUNsYXNzKF9pZHNlc3Npb246IHN0cmluZykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueT4oQ29uZmlnLmFwaVVybCArICcvdXNlcnMvZ2V0c2Vzc2lvbnRlYWNoZXIvJyArIF9pZHNlc3Npb24pO1xyXG4gICAgICAgIC8vcmV0dXJuIHRoaXMuaHR0cC5nZXQoYXBwQ29uZmlnLmFwaVVybCArICcvdXNlcnMvZ2V0c2Vzc2lvbi8nICsgX2lkKTtcclxuICAgIH1cclxuXHJcbiAgICAvKmNvbm5lY3RVc2VydG9TZXNzaW9uKHVzZXI6IFVzZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnB1dChDb25maWcuYXBpVXJsICsgJy91c2Vycy9pZHNlc3Npb24vJyArIHVzZXIuX2lkLCB1c2VyLHtyZXNwb25zZVR5cGU6ICd0ZXh0J30pLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcChyZXNwb25zZSA9PiByZXNwb25zZSksXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IodGhpcy5oYW5kbGVFcnJvcnMpXHJcbiAgICAgICAgKTs7XHJcbiAgICB9Ki9cclxuXHJcbiAgICBnZXRBbGxDbGFzc3Jvb21zKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueVtdPihDb25maWcuYXBpVXJsICsgJy91c2Vycy9nZXRhdWxhcycpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbm5lY3RTdHVkZW50VG9TZXNzaW9uKHVzZXI6IFVzZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoQ29uZmlnLmFwaVVybCArICcvdXNlcnMvY29ubmVjdHN0dWRlbnR0b3Nlc3Npb24nLCB1c2VyKS5waXBlKFxyXG4gICAgICAgICAgICBtYXAocmVzcG9uc2UgPT4gcmVzcG9uc2UpLFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKHRoaXMuaGFuZGxlRXJyb3JzKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLy9UZWFjaGVyXHJcblxyXG4gICAgZ2V0VGVhY2hlcihfdGVhY2hlcjogc3RyaW5nKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2dldHRlYWNoZXIvJyArIF90ZWFjaGVyKTtcclxuICAgIH1cclxuXHJcbiAgICAvL0N1cnNvc1xyXG5cclxuICAgIGdldEN1cnNvKF9jdXJzbzogc3RyaW5nKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2dldGN1cnNvLycgKyBfY3Vyc28pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dChfaWQ6IHN0cmluZywgdXNlcjogVXNlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucHV0KENvbmZpZy5hcGlVcmwgKyAnL3VzZXJzL2xvZ291dC8nICsgX2lkLCB1c2VyLHtyZXNwb25zZVR5cGU6ICd0ZXh0J30pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRVc2VyKGRhdGE6YW55KXtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRVc2VyXCIsSlNPTi5zdHJpbmdpZnkoZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjdXJyZW50VXNlcigpe1xyXG4gICAgICAgIHJldHVybiBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudFVzZXJcIikpOyAvL3x8IG51bGw7XHJcbiAgICB9XHJcblxyXG59IiwibW9kdWxlLmV4cG9ydHMgPSBcIi5ob21lLXBhbmVse1xcclxcblxcdHZlcnRpY2FsLWFsaWduOiBjZW50ZXI7XFxyXFxuXFx0Zm9udC1zaXplOiAyMDtcXHJcXG5cXHRtYXJnaW46IDE1O1xcclxcbn1cXHJcXG5cXHJcXG4uY2FyZCB7XFxyXFxuXFx0Ym9yZGVyLXJhZGl1czogMjA7XFxyXFxuXFx0bWFyZ2luOiAxMjtcXHJcXG4gICAgcGFkZGluZzogMTA7XFxyXFxuXFx0YmFja2dyb3VuZC1jb2xvcjogcmdiKDE3OSwgMTI3LCAyNCk7XFxyXFxuICAgIGNvbG9yOiAjMzYyNzEzO1xcclxcbiAgICBoZWlnaHQ6IDU2JTtcXHJcXG59XFxyXFxuXFxyXFxuLmJ1dHRvbi1uby1jbGlja2VkXFxyXFxue1xcclxcblxcdGJhY2tncm91bmQtY29sb3I6IHJnYigxNjgsIDE5NCwgMjA2KTtcXHJcXG59XFxyXFxuXFxyXFxuLmJ1dHRvbi1jbGlja2VkXFxyXFxue1xcclxcblxcdGJhY2tncm91bmQtY29sb3I6IHJnYig4MSwgMTU2LCAxOTApO1xcclxcbn1cXHJcXG5cIiIsIm1vZHVsZS5leHBvcnRzID0gXCI8QWN0aW9uQmFyIHRpdGxlPVxcXCJTdHVkZW50IE1haW4gUGFnZVxcXCIgY2xhc3M9XFxcImFjdGlvbi1iYXJcXFwiPlxcclxcbjwvQWN0aW9uQmFyPlxcclxcblxcclxcbjxTdGFja0xheW91dD5cXHJcXG4gICAgPEltYWdlIGNsYXNzPVxcXCJsb2dvXFxcIiBzcmM9XFxcIn4vaW1hZ2VzL0xvZ29fcG9saXRlY25pY2EucG5nXFxcIj48L0ltYWdlPlxcclxcblxcclxcbiAgICA8U3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICA8VGFicyA+XFxyXFxuICAgICAgICAgICAgPFRhYlN0cmlwPlxcclxcbiAgICAgICAgICAgICAgICA8VGFiU3RyaXBJdGVtPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIkNsYXNlXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPEltYWdlIHNyYz1cXFwicmVzOi8vaG9tZVxcXCI+PC9JbWFnZT5cXHJcXG4gICAgICAgICAgICAgICAgPC9UYWJTdHJpcEl0ZW0+XFxyXFxuICAgICAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiVGVzdHNcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8SW1hZ2UgY2xhc3M9XFxcImxvZ29cXFwiIHNyYz1cXFwifi9pbWFnZXMvdGVzdC5wbmdcXFwiPjwvSW1hZ2U+XFxyXFxuICAgICAgICAgICAgICAgIDwvVGFiU3RyaXBJdGVtPlxcclxcbiAgICAgICAgICAgICAgICA8VGFiU3RyaXBJdGVtPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIkFqdXN0ZXNcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJyZXM6Ly9zZXR0aW5nc1xcXCI+PC9JbWFnZT5cXHJcXG4gICAgICAgICAgICAgICAgPC9UYWJTdHJpcEl0ZW0+XFxyXFxuICAgICAgICAgICAgPC9UYWJTdHJpcD5cXHJcXG4gICAgICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxyXFxuICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IHdpZHRoPVxcXCI4MCVcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwiaDJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDMgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJEYXRvcyBkZSBsYSBzZXNpw7NuXFxcIiAgZm9udFdlaWdodD1cXFwiYm9sZFxcXCIgY2xhc3M9XFxcImgxIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDMgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtYXR0ZWRTdHJpbmc+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNwYW4gdGV4dD1cXFwiQWx1bW5vOiBcXFwiICBmb250V2VpZ2h0PVxcXCJib2xkXFxcIj48L1NwYW4+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNwYW4gdGV4dD1cXFwie3tjdXJyZW50VXNlci5maXJzdE5hbWV9fSB7e2N1cnJlbnRVc2VyLmxhc3ROYW1lfX1cXFwiPjwvU3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybWF0dGVkU3RyaW5nPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtYXR0ZWRTdHJpbmc+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNwYW4gdGV4dD1cXFwiUHJvZmVzb3I6IFxcXCIgIGZvbnRXZWlnaHQ9XFxcImJvbGRcXFwiPjwvU3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3BhbiB0ZXh0PVxcXCJ7e2N1cnJlbnRUZWFjaGVyLmZpcnN0TmFtZX19IHt7Y3VycmVudFRlYWNoZXIubGFzdE5hbWV9fVxcXCI+PC9TcGFuPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtYXR0ZWRTdHJpbmc+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtYXR0ZWRTdHJpbmc+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNwYW4gdGV4dD1cXFwiRW1haWw6IFxcXCIgIGZvbnRXZWlnaHQ9XFxcImJvbGRcXFwiIGNsYXNzPVxcXCJoM1xcXCI+PC9TcGFuPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTcGFuIHRleHQ9XFxcInt7Y3VycmVudFRlYWNoZXIudXNlcm5hbWV9fVxcXCIgY2xhc3M9XFxcImgzXFxcIj48L1NwYW4+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm1hdHRlZFN0cmluZz5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPCEtLVxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJFbWFpbCBwcm9mZXNvcjoge3tjdXJyZW50VGVhY2hlci51c2VybmFtZX19XFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAtLT5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgzIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybWF0dGVkU3RyaW5nPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3BhbiB0ZXh0PVxcXCJBdWxhOiBcXFwiICBmb250V2VpZ2h0PVxcXCJib2xkXFxcIj48L1NwYW4+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTcGFuIHRleHQ9XFxcInt7Y3VycmVudFVzZXIuY2xhc3N9fVxcXCI+PC9TcGFuPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtYXR0ZWRTdHJpbmc+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm1hdHRlZFN0cmluZz5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3BhbiB0ZXh0PVxcXCJBc2llbnRvIG9jdXBhZG86IFxcXCIgIGZvbnRXZWlnaHQ9XFxcImJvbGRcXFwiPjwvU3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3BhbiB0ZXh0PVxcXCJ7e2N1cnJlbnRVc2VyLnNlYXR9fVxcXCI+PC9TcGFuPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtYXR0ZWRTdHJpbmc+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTGFiZWw+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJoMiB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm1hdHRlZFN0cmluZz5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3BhbiB0ZXh0PVxcXCJBc2lnbmF0dXJhOiBcXFwiICBmb250V2VpZ2h0PVxcXCJib2xkXFxcIj48L1NwYW4+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNwYW4gdGV4dD1cXFwie3thc2lnbmF0dXJhfX1cXFwiPjwvU3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybWF0dGVkU3RyaW5nPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtYXR0ZWRTdHJpbmc+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNwYW4gdGV4dD1cXFwiR3J1cG86IFxcXCIgIGZvbnRXZWlnaHQ9XFxcImJvbGRcXFwiPjwvU3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3BhbiB0ZXh0PVxcXCJ7e2dydXBvfX1cXFwiPjwvU3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybWF0dGVkU3RyaW5nPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcclxcbiAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgPC9UYWJDb250ZW50SXRlbT5cXHJcXG4gICAgICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxyXFxuICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0PlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJoMiB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiVGVzdHMgRGlzcG9uaWJsZXNcXFwiICBmb250V2VpZ2h0PVxcXCJib2xkXFxcIiBjbGFzcz1cXFwiaDEgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgyIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgPCEtLVxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgKm5nRm9yPVxcXCJsZXQgaXRlbSBvZiBxdWVzdGlvbnM7IGxldCBpID0gaW5kZXhcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd3M9XFxcIiosKiwqLCpcXFwiIGNvbHVtbnM9XFxcIiosIGF1dG9cXFwiIGhlaWdodD1cXFwiMTQwXFxcIiAodGFwKT1cXFwibmF2aWdhdGVUb1F1aXooaSlcXFwiID5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwiVGVzdC0tLVByZWd1bnRhOiB7e2l0ZW0ucXVlc3Rpb259fVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yPVxcXCIjMzVBNkU4XFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcj1cXFwid2hpdGVzbW9rZVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gcm93PVxcXCIxXFxcIiBjb2w9XFxcIjBcXFwiIHRleHQ9XFxcIkE6IHt7aXRlbS5hbnN3ZXIxfX1cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcj1cXFwicmVkXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcj1cXFwid2hpdGVzbW9rZVxcXCIgKHRhcCk9XFxcIm5hdmlnYXRlVG9RdWl6KGkpXFxcIj48L0J1dHRvbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiByb3c9XFxcIjJcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwiQjoge3tpdGVtLmFuc3dlcjJ9fVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yPVxcXCJyZWRcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiAodGFwKT1cXFwibmF2aWdhdGVUb1F1aXooaSlcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIHJvdz1cXFwiM1xcXCIgY29sPVxcXCIwXFxcIiB0ZXh0PVxcXCJDOiB7e2l0ZW0uYW5zd2VyM319XFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I9XFxcInJlZFxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I9XFxcIndoaXRlc21va2VcXFwiICh0YXApPVxcXCJuYXZpZ2F0ZVRvUXVpeihpKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuICAgICAgICAgICAgICAgICAgICAtLT5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCAqbmdGb3I9XFxcImxldCBpdGVtIG9mIHF1ZXN0aW9uczsgbGV0IGkgPSBpbmRleFxcXCI+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwie3tpdGVtLnF1ZXN0aW9ufX1cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yPVxcXCIjMzVBNkU4XFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiAodGFwKT1cXFwidG9vZ2xlU2VsZWN0aW9uMihpKVxcXCIgd2lkdGg9IFxcXCI4MCVcXFwiPjwvQnV0dG9uPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0ICpuZ0lmPVxcXCJpdGVtLm9wZW5cXFwiIHdpZHRoPVxcXCI2MCVcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uICB0ZXh0PVxcXCJhKSB7e2l0ZW0uYW5zd2VyMX19XFxcIiBbbmdDbGFzc109XFxcInsnYnV0dG9uLWNsaWNrZWQnOiBpdGVtLm9wZW5BLCAnYnV0dG9uLW5vLWNsaWNrZWQnOiAhaXRlbS5vcGVuQX1cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiAodGFwKT1cXFwic3dpdGNoQ29sb3IzKGksMClcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uICB0ZXh0PVxcXCJiKSB7e2l0ZW0uYW5zd2VyMn19XFxcIiBbbmdDbGFzc109XFxcInsnYnV0dG9uLWNsaWNrZWQnOiBpdGVtLm9wZW5CLCAnYnV0dG9uLW5vLWNsaWNrZWQnOiAhaXRlbS5vcGVuQn1cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiAodGFwKT1cXFwic3dpdGNoQ29sb3IzKGksMSlcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uICB0ZXh0PVxcXCJDKSB7e2l0ZW0uYW5zd2VyM319XFxcIiBbbmdDbGFzc109XFxcInsnYnV0dG9uLWNsaWNrZWQnOiBpdGVtLm9wZW5DLCAnYnV0dG9uLW5vLWNsaWNrZWQnOiAhaXRlbS5vcGVuQ31cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiAodGFwKT1cXFwic3dpdGNoQ29sb3IzKGksMilcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uICB0ZXh0PVxcXCJFbnZpYXJcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiZ3JlZW5cXFwiICh0YXApPVxcXCJzZW5kUmVzcG9uc2UyKGkpXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcj1cXFwid2hpdGVzbW9rZVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD4gXFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcImgzIHRleHQtY2VudGVyXFxcIj48L0xhYmVsPlxcclxcblxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgKm5nRm9yPVxcXCJsZXQgaXRlbSBvZiBpbmZvcm1hdGlvbjsgbGV0IGkgPSBpbmRleFxcXCI+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gcm93PVxcXCIwXFxcIiBjb2w9XFxcIjBcXFwiIHRleHQ9XFxcInt7aXRlbS5xdWVzdGlvbn19XFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I9XFxcIiMzNUE2RThcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiAodGFwKT1cXFwidG9vZ2xlU2VsZWN0aW9uKGkpXFxcIiB3aWR0aD0gXFxcIjgwJVxcXCI+PC9CdXR0b24+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCAqbmdJZj1cXFwiaXRlbS5vcGVuXFxcIiB3aWR0aD1cXFwiNjAlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gIHRleHQ9XFxcImEpIHt7aXRlbS5hbnN3ZXJzLmF9fVxcXCIgW25nQ2xhc3NdPVxcXCJ7J2J1dHRvbi1jbGlja2VkJzogaXRlbS5hbnN3ZXJzLm9wZW5BLCAnYnV0dG9uLW5vLWNsaWNrZWQnOiAhaXRlbS5hbnN3ZXJzLm9wZW5BfVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiAodGFwKT1cXFwic3dpdGNoQ29sb3IoaSwwKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uICB0ZXh0PVxcXCJiKSB7e2l0ZW0uYW5zd2Vycy5ifX1cXFwiIFtuZ0NsYXNzXT1cXFwieydidXR0b24tY2xpY2tlZCc6IGl0ZW0uYW5zd2Vycy5vcGVuQiwgJ2J1dHRvbi1uby1jbGlja2VkJzogIWl0ZW0uYW5zd2Vycy5vcGVuQn1cXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcj1cXFwid2hpdGVzbW9rZVxcXCIgKHRhcCk9XFxcInN3aXRjaENvbG9yKGksMSlcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiAgdGV4dD1cXFwiQykge3tpdGVtLmFuc3dlcnMuY319XFxcIiBbbmdDbGFzc109XFxcInsnYnV0dG9uLWNsaWNrZWQnOiBpdGVtLmFuc3dlcnMub3BlbkMsICdidXR0b24tbm8tY2xpY2tlZCc6ICFpdGVtLmFuc3dlcnMub3BlbkN9XFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I9XFxcIndoaXRlc21va2VcXFwiICh0YXApPVxcXCJzd2l0Y2hDb2xvcihpLDIpXFxcIj48L0J1dHRvbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gIHRleHQ9XFxcIkVudmlhclxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJncmVlblxcXCIgKHRhcCk9XFxcInNlbmRSZXNwb25zZShpKVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIj48L0J1dHRvbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD4gXFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJoMyB0ZXh0LWNlbnRlclxcXCI+PC9MYWJlbD5cXHJcXG5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgXFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiVmVyIHRlc3RzIGRpc3BvbmlibGVzL0FjdHVhbGl6YXJcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiYmx1ZVxcXCIgd2lkdGg9XFxcIjc1JVxcXCJcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I9XFxcIndoaXRlc21va2VcXFwiICh0YXApPVxcXCJnZXRUZXN0KClcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXHJcXG4gICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcclxcbiAgICAgICAgICAgIDwvVGFiQ29udGVudEl0ZW0+XFxyXFxuICAgICAgICAgICAgPFRhYkNvbnRlbnRJdGVtPlxcclxcbiAgICAgICAgICAgICAgICA8R3JpZExheW91dD5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCB3aWR0aD1cXFwiODAlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiXFxcIiBjbGFzcz1cXFwiaDIgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIkFqdXN0ZXNcXFwiICBmb250V2VpZ2h0PVxcXCJib2xkXFxcIiBjbGFzcz1cXFwiaDEgdGV4dC1jZW50ZXJcXFwiPjwvTGFiZWw+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiTW9kaWZpY2FyIGRhdG9zXFxcIlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I9XFxcIiMzNUE2RThcXFwiIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiA+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlxcXCIgY2xhc3M9XFxcInRleHQtY2VudGVyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xhYmVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiU2FsaXIgZGUgbGEgY2xhc2VcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcj1cXFwiIzM1QTZFOFxcXCIgY29sb3I9XFxcIndoaXRlc21va2VcXFwiICh0YXApPVxcXCJzYWxpcmNsYXNlKClcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJcXFwiIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9MYWJlbD5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIHRleHQ9XFxcIkNlcnJhciBzZXNpw7NuXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiMzNUE2RThcXFwiXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVxcXCJ3aGl0ZXNtb2tlXFxcIiAodGFwKT1cXFwibG9nb3V0KClcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXHJcXG4gICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcclxcbiAgICAgICAgICAgIDwvVGFiQ29udGVudEl0ZW0+XFxyXFxuICAgICAgICA8L1RhYnM+XFxyXFxuICAgIDwvU3RhY2tMYXlvdXQ+XFxyXFxuICAgIDxCdXR0b24gdGV4dD1cXFwiQ2VycmFyIFNlc2nDs25cXFwiICh0YXApPVxcXCJsb2dvdXQoKVxcXCI+PC9CdXR0b24+XFxyXFxuPC9TdGFja0xheW91dD5cIiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uRW50cnkgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9mcmFtZVwiXHJcbmltcG9ydCB7IEl0ZW1FdmVudERhdGEgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9saXN0LXZpZXdcIlxyXG5cclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2VcIjtcclxuXHJcbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5tb2RlbFwiO1xyXG5pbXBvcnQgeyBDdXJzbyB9IGZyb20gXCIuLi9zaGFyZWQvdXNlci9jdXJzby5tb2RlbFwiO1xyXG5pbXBvcnQgeyBTZXNzaW9uIH0gZnJvbSBcIi4uL3NoYXJlZC91c2VyL3Nlc3Npb24ubW9kZWxcIjtcclxuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2hhcmVkL3VzZXIvdXNlci5zZXJ2aWNlXCI7XHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiBcIlN0dWRlbnRcIixcclxuXHRtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG5cdHRlbXBsYXRlVXJsOiBcIi4vc3R1ZGVudC5jb21wb25lbnQuaHRtbFwiLFxyXG5cdHN0eWxlVXJsczogWycuL3N0dWRlbnQuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTdHVkZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBjdXJyZW50VXNlcjogVXNlcjtcclxuICAgIGN1cnJlbnRUZWFjaGVyOiBVc2VyO1xyXG4gICAgY3VycmVudFNlc3Npb246IFNlc3Npb247XHJcbiAgICBjdXJyZW50Q3Vyc286IEN1cnNvO1xyXG5cclxuICAgIHByb2Zlc29yOiBzdHJpbmc7XHJcbiAgICBjbGFzZTogc3RyaW5nO1xyXG4gICAgYXNpZ25hdHVyYTogc3RyaW5nO1xyXG4gICAgZ3J1cG86IHN0cmluZztcclxuICAgIGFzaWVudG9fb2N1cGFkbzogc3RyaW5nO1xyXG5cclxuICAgIHF1ZXN0aW9uczogYW55W10gPSBbXTtcclxuXHJcbiAgICBpbmZvcm1hdGlvbjogYW55W10gPVtcclxuICAgICAgICB7XHJcbiAgICAgICAgICBcInF1ZXN0aW9uXCI6IFwiwr9DdcOhbCBlcyBlbCBzw61tYm9sbyBxdcOtbWljbyBkZWwgUG90YXNpbz9cIixcclxuICAgICAgICAgIFwiYW5zd2Vyc1wiOlxyXG4gICAgICAgICAgeyBcclxuICAgICAgICAgICAgXCJhXCI6IFwiQ2FcIixcclxuICAgICAgICAgICAgXCJiXCI6IFwiS1wiLFxyXG4gICAgICAgICAgICBcImNcIjogXCJQdFwiXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgXCJxdWVzdGlvblwiOiBcIsK/Q3XDoWwgZXMgZWwgc8OtbWJvbG8gcXXDrW1pY28gZGVsIENhcmJvbm8/XCIsXHJcbiAgICAgICAgXCJhbnN3ZXJzXCI6XHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgXCJhXCI6IFwiQ2FcIixcclxuICAgICAgICAgICAgXCJiXCI6IFwiS1wiLFxyXG4gICAgICAgICAgICBcImNcIjogXCJDXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgfVxyXG4gICAgXTtcclxuXHJcblxyXG4gICAgY29uc3RydWN0b3IoIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxwcml2YXRlIHBhZ2U6IFBhZ2UpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRVc2VyID0gbmV3IFVzZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5wcm9mZXNvciA9IFwiQWxiZXJ0byBCcnVuZXRlXCI7XHJcbiAgICAgICAgdGhpcy5jbGFzZSA9IFwiQi0xMlwiO1xyXG4gICAgICAgIHRoaXMuYXNpZ25hdHVyYSA9IFwiRWxlY3Ryw7NuaWNhXCI7XHJcbiAgICAgICAgdGhpcy5ncnVwbyA9IFwiQS0yMDRcIjtcclxuICAgICAgICB0aGlzLmFzaWVudG9fb2N1cGFkbyA9IFwiMlwiO1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vdGhpcy5jdXJyZW50VXNlciA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRVc2VyJykpO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXI9dGhpcy51c2VyU2VydmljZS5jdXJyZW50VXNlcigpXHJcbiAgICAgICAgdGhpcy5jdXJyZW50U2Vzc2lvbiA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRTZXNzaW9uJykpO1xyXG4gICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuaW5mb3JtYXRpb25bMF0ub3BlbiA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuaW5mb3JtYXRpb25bMF0uYW5zd2Vycy5vcGVuQSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuaW5mb3JtYXRpb25bMF0uYW5zd2Vycy5vcGVuQiA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuaW5mb3JtYXRpb25bMF0uYW5zd2Vycy5vcGVuQyA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuXHJcblxyXG5cdG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZ2V0Q3Vyc28oKVxyXG4gICAgfVxyXG4gICAgXHJcblxyXG4gICAgcHJpdmF0ZSBnZXRDdXJzbygpIHtcclxuICAgICAgICB0aGlzLnVzZXJTZXJ2aWNlLmdldEN1cnNvKHRoaXMuY3VycmVudFNlc3Npb24uY3Vyc28pLnN1YnNjcmliZShjdXJzbyA9PiB7XHJcbiAgICAgICAgICB0aGlzLmN1cnJlbnRDdXJzbyA9IGN1cnNvWzBdO1xyXG4gICAgICAgICAgdGhpcy51c2VyU2VydmljZS5nZXRUZWFjaGVyKHRoaXMuY3VycmVudEN1cnNvLnRlYWNoZXIpLnN1YnNjcmliZSh0ZWFjaGVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VGVhY2hlciA9IHRlYWNoZXJbMF07XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICBhbGVydChcIk5vIHNlIGhhIHBvZGlkbyBvYnRlbmVyIGxhIGluZm9ybWFjacOzbiBkZWwgcHJvZmVzb3IgYWwgcXVlIHBlcnRlbmVjZSBsYSBzZXNpw7NuLCBFcnJvcjogXCIgKyBlcnJvcik7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgIGFsZXJ0KFwiTm8gc2UgaGEgcG9kaWRvIG9idGVuZXIgbGEgaW5mb3JtYWNpw7NuIGRlbCBjdXJzbyBhbCBxdWUgcGVydGVuZWNlIGxhIHNlc2nDs24sIEVycm9yOiBcIiArIGVycm9yKTtcclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgb25JdGVtVGFwKGFyZ3M6IEl0ZW1FdmVudERhdGEpOiB2b2lkIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnSXRlbSB3aXRoIGluZGV4OiAnICsgYXJncy5pbmRleCArICcgdGFwcGVkJyk7XHJcbiAgICB9XHJcblxyXG4gICAgbmF2aWdhdGVUb1F1aXooaW5kZXg6IG51bWJlcikge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiSGFzIHB1bHNhZG8gZWxsIGluZGV4XCIgKyBpbmRleCk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9vZ2xlU2VsZWN0aW9uKGluZGV4KSB7XHJcbiAgICAgICAgdGhpcy5pbmZvcm1hdGlvbltpbmRleF0ub3BlbiA9ICF0aGlzLmluZm9ybWF0aW9uW2luZGV4XS5vcGVuO1xyXG4gICAgfVxyXG5cclxuICAgIHRvb2dsZVNlbGVjdGlvbjIoaW5kZXgpIHtcclxuICAgICAgICB0aGlzLnF1ZXN0aW9uc1tpbmRleF0ub3BlbiA9ICF0aGlzLnF1ZXN0aW9uc1tpbmRleF0ub3BlbjtcclxuICAgIH1cclxuXHJcbiAgICBpZGVudGlmaWNhdGlvbigpXHJcbiAgICB7XHJcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9pZGVudGlmaWNhdGlvblwiXSk7XHJcbiAgICB9XHJcbiAgICBzYWxpcmNsYXNlKCl7XHJcbiAgICAgICAgYWxlcnQoXCJGdW5jacOzbiBubyBkaXNwb25pYmxlIGFjdHVhbG1lbnRlXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICBzd2l0Y2hDb2xvcjIoaW5kZXgsIGluZGV4QW5zd2VyKXtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCAzOyBpKyspIFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgaWYgKGkgPT0gaW5kZXhBbnN3ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW5mb3JtYXRpb25baW5kZXhdLmFuc3dlcnNbaW5kZXhBbnN3ZXJdLm9wZW4gPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vyc1tpbmRleEFuc3dlcl0ub3BlbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSovXHJcblxyXG4gICAgcHJpdmF0ZSBzd2l0Y2hDb2xvcihpbmRleCwgaW5kZXhBbnN3ZXIpe1xyXG5cclxuICAgICAgICBpZiAoaW5kZXhBbnN3ZXIgPT0gXCIwXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQSA9ICAhdGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQTtcclxuICAgICAgICAgICAgdGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQiA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmluZm9ybWF0aW9uW2luZGV4XS5hbnN3ZXJzLm9wZW5DID0gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpbmRleEFuc3dlciA9PSBcIjFcIikge1xyXG4gICAgICAgICAgICB0aGlzLmluZm9ybWF0aW9uW2luZGV4XS5hbnN3ZXJzLm9wZW5BID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuaW5mb3JtYXRpb25baW5kZXhdLmFuc3dlcnMub3BlbkIgPSAhdGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQjtcclxuICAgICAgICAgICAgdGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQyA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaW5kZXhBbnN3ZXIgPT0gXCIyXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmluZm9ybWF0aW9uW2luZGV4XS5hbnN3ZXJzLm9wZW5CID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuaW5mb3JtYXRpb25baW5kZXhdLmFuc3dlcnMub3BlbkMgPSAhdGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2VuZFJlc3BvbnNlKGluZGV4KXtcclxuICAgICAgICBpZiAodGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQSkge1xyXG4gICAgICAgICAgICBhbGVydChcIkxhIHJlc3B1ZXN0YSBlbGVnaWRhIGVzIGxhIGEpIFwiKVxyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQikge1xyXG4gICAgICAgICAgICBhbGVydChcIkxhIHJlc3B1ZXN0YSBlbGVnaWRhIGVzIGxhIGIpIFwiKVxyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5pbmZvcm1hdGlvbltpbmRleF0uYW5zd2Vycy5vcGVuQykge1xyXG4gICAgICAgICAgICBhbGVydChcIkxhIHJlc3B1ZXN0YSBlbGVnaWRhIGVzIGxhIGMpIFwiKVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICBhbGVydChcIlBvciBmYXZvciBzZWxlY2Npb25lIHVuYSByZXNwdWVzdGFcIilcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2VuZFJlc3BvbnNlMihpbmRleCl7XHJcbiAgICAgICAgaWYgKHRoaXMucXVlc3Rpb25zW2luZGV4XS5vcGVuQSkge1xyXG4gICAgICAgICAgICBhbGVydChcIkxhIHJlc3B1ZXN0YSBlbGVnaWRhIGVzIGxhIGEpIFwiKVxyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5xdWVzdGlvbnNbaW5kZXhdLm9wZW5CKSB7XHJcbiAgICAgICAgICAgIGFsZXJ0KFwiTGEgcmVzcHVlc3RhIGVsZWdpZGEgZXMgbGEgYikgXCIpXHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnF1ZXN0aW9uc1tpbmRleF0ub3BlbkMpIHtcclxuICAgICAgICAgICAgYWxlcnQoXCJMYSByZXNwdWVzdGEgZWxlZ2lkYSBlcyBsYSBjKSBcIilcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgYWxlcnQoXCJQb3IgZmF2b3Igc2VsZWNjaW9uZSB1bmEgcmVzcHVlc3RhXCIpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc3dpdGNoQ29sb3IzKGluZGV4LCBpbmRleEFuc3dlcil7XHJcblxyXG4gICAgICAgIGlmIChpbmRleEFuc3dlciA9PSBcIjBcIikge1xyXG4gICAgICAgICAgICB0aGlzLnF1ZXN0aW9uc1tpbmRleF0ub3BlbkEgPSAgIXRoaXMucXVlc3Rpb25zW2luZGV4XS5vcGVuQTtcclxuICAgICAgICAgICAgdGhpcy5xdWVzdGlvbnNbaW5kZXhdLm9wZW5CID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMucXVlc3Rpb25zW2luZGV4XS5vcGVuQyA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaW5kZXhBbnN3ZXIgPT0gXCIxXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5xdWVzdGlvbnNbaW5kZXhdLm9wZW5BID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMucXVlc3Rpb25zW2luZGV4XS5vcGVuQiA9ICF0aGlzLnF1ZXN0aW9uc1tpbmRleF0ub3BlbkI7XHJcbiAgICAgICAgICAgIHRoaXMucXVlc3Rpb25zW2luZGV4XS5vcGVuQyA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaW5kZXhBbnN3ZXIgPT0gXCIyXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5xdWVzdGlvbnNbaW5kZXhdLm9wZW5BID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMucXVlc3Rpb25zW2luZGV4XS5vcGVuQiA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLnF1ZXN0aW9uc1tpbmRleF0ub3BlbkMgPSAhdGhpcy5xdWVzdGlvbnNbaW5kZXhdLm9wZW5DO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG5cclxuICAgIGdldFRlc3QoKXtcclxuICAgICAgICB0aGlzLnVzZXJTZXJ2aWNlLmdldFRlc3RCeUNsYXNzKHRoaXMuY3VycmVudFNlc3Npb24uX2lkKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIHRlc3RzID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMucXVlc3Rpb25zID0gdGVzdHNbMF0ucXVlc3Rpb25zO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5xdWVzdGlvbnNbMF0ub3BlbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5xdWVzdGlvbnNbMF0ub3BlbkEgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMucXVlc3Rpb25zWzBdLm9wZW5CID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnF1ZXN0aW9uc1swXS5vcGVuQyA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0Vycm9yIGFsIG9idGVuZXIgbG9zIHRlc3RzOiAnICsgSlNPTi5zdHJpbmdpZnkoZXJyKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgbG9nb3V0KCkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXIgPSB0aGlzLnVzZXJTZXJ2aWNlLmN1cnJlbnRVc2VyKCk7XHJcbiAgICAgICAgdGhpcy51c2VyU2VydmljZS5sb2dvdXQodGhpcy5jdXJyZW50VXNlci5faWQsIHRoaXMuY3VycmVudFVzZXIpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9sb2dpbiddLHtjbGVhckhpc3Rvcnk6IHRydWV9KTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnQoXCJFcnJvciBhbCBjZXJyYXIgc2VzacOzblwiKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgXHJcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IFwiLmhvbWUtcGFuZWx7XFxyXFxuXFx0dmVydGljYWwtYWxpZ246IGNlbnRlcjtcXHJcXG5cXHRmb250LXNpemU6IDIwO1xcclxcblxcdG1hcmdpbjogMTU7XFxyXFxufVwiIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxBY3Rpb25CYXIgdGl0bGU9XFxcIlRlYWNoZXJcXFwiIGNsYXNzPVxcXCJhY3Rpb24tYmFyXFxcIj5cXHJcXG4gICAgPC9BY3Rpb25CYXI+XFxyXFxuICAgIFxcclxcbiAgICA8U2Nyb2xsVmlldyBjbGFzcz1cXFwicGFnZVxcXCI+XFxyXFxuICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImhvbWUtcGFuZWxcXFwiPlxcclxcbiAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiSWRlbnRpZmljYXIgY2xhc2VcXFwiICBiYWNrZ3JvdW5kLWNvbG9yOnJnYig3NiwgOTEsIDIxMyk+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJDcmVhciB0ZXN0XFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5IG0tdC0yMFxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICAgICAgPEJ1dHRvbiB0ZXh0PVxcXCJDcmVhclBydWViYVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeSBtLXQtMjBcXFwiPjwvQnV0dG9uPlxcclxcbiAgICAgICAgICAgIDxCdXR0b24gdGV4dD1cXFwiQ2VycmFyIFNlc2nDs25cXFwiICh0YXApPVxcXCJsb2dvdXQoKVxcXCI+PC9CdXR0b24+XFxyXFxuICAgICAgICA8L1N0YWNrTGF5b3V0PlxcclxcbiAgICA8L1Njcm9sbFZpZXc+XCIiLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogXCJUZWFjaGVyXCIsXHJcblx0bW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuXHR0ZW1wbGF0ZVVybDogXCIuL3RlYWNoZXIuY29tcG9uZW50Lmh0bWxcIixcclxuXHRzdHlsZVVybHM6IFsnLi90ZWFjaGVyLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGVhY2hlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xyXG4gICAgfVxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIH1cclxuICAgIFxyXG4gICAgbG9nb3V0KClcclxuICAgIHtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2xvZ2luXCJdKTtcclxuICAgIH1cclxufSJdLCJzb3VyY2VSb290IjoiIn0=