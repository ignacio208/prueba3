import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { LoginComponent } from './login/login.component';

import { RegisterComponent } from './register/register.component';
import { Register2Component } from './register2/register.component';
import { StudentComponent } from './student/student.component';
import { TeacherComponent } from './teacher/teacher.component';
import { Session_codeComponent } from './session_code/session_code.component';
import { IdentificationComponent } from './identification/identification.component';
import { Reset_PasswordComponent } from './reset_password/reset_password.component';

const routes: Routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "register2", component: Register2Component },
    { path: "student", component: StudentComponent },
    { path: "teacher", component: TeacherComponent },
    { path: "session_code", component: Session_codeComponent },
    { path:"identification", component: IdentificationComponent },
    { path:"reset_password", component: Reset_PasswordComponent }

];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }