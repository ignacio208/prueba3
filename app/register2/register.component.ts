import { Component, OnInit, ElementRef, ViewChild} from "@angular/core";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
//import { MatSnackBar, MatBottomSheet, MatBottomSheetRef } from '@angular/material';

import { User } from "../shared/user/user.model";
import {  UserService } from "../shared/user/user.service";
//import { AlertService, } from "../shared/user/alert.service";

@Component({
	selector: "Register2",
	moduleId: module.id,
	templateUrl: "./register.component.html",
	styleUrls: ['./register.component.css']
})
export class Register2Component implements OnInit {

	model: any = {};
    loading = false;

    //Campo email
    //email = new FormControl('', [Validators.required, Validators.email]);

    //Campo Contraseña
    hide = true;

    //FOTO
	selectedFile : File = null;
	
	user: User;
	toEmail;
	ccEmail;
	bccEmail;
	subject;
	message;

	constructor(	
		private page: Page, 
		private userService: UserService, 
		private routerExtensions: RouterExtensions, 
		//private alertService: AlertService,        
        //public snackBar: MatSnackBar
        ) 
		{

		this.page.actionBarHidden = true;
		this.user = new User();
		this.user.username = "";
		this.user.password = "";
		}

	ngOnInit(): void {
	}

	register2()
	{
		alert("Función no disponible actualmente")
	}


}