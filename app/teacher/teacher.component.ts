import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
	selector: "Teacher",
	moduleId: module.id,
	templateUrl: "./teacher.component.html",
	styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {


    constructor(private routerExtensions: RouterExtensions) {
    }

	ngOnInit(): void {
    }
    
    logout()
    {
        this.routerExtensions.navigate(["/login"]);
    }
}