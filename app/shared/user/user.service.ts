import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";

import * as localStorage from "nativescript-localstorage";

import { User } from "./user.model";
import { User2 } from "./user.model copy";
import { Config } from "../config";

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    register(user: User) {
        if (!user.username || !user.password) {
            return throwError("Please provide both an email address and password.");
        }

        return this.http.post(
            Config.apiUrl + "user/" + Config.appKey,
            JSON.stringify({
                username: user.username,
                email: user.username,
                password: user.password
            }),
            { headers: this.getCommonHeaders() }
        ).pipe(
            catchError(this.handleErrors)
        );
    }

    login(user: User) {
        return this.http.post(
            Config.apiUrl + '/users/authenticate',
            ({
                username: user.username,
                password: user.password
            })
        ).pipe(
            map(response => response),
            catchError(this.handleErrors)
        );

    }

    login5(user: User) {
        return this.http.post(Config.apiUrl + '/users/authenticate', { username: user.username, password: user.password })
            .subscribe(user32 => {
                // login successful if there's a jwt token in the response
                if (user32) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user32;
            });
    }

    resetPassword(user: User2) {
        console.log(JSON.stringify(user));
        return this.http.post(
            Config.apiUrl + '/users/resetpassword', user
            /*({
                username: user.username,
                password: user.password,
                token: user.token
            })*/,{responseType: 'text'}
        ).pipe(
            map(response => response),
            catchError(this.handleErrors)
        );

    }

    create(user: User) {
        return this.http.post(Config.apiUrl + '/users/register', user,{responseType: 'text'}).pipe(
            map(response => response),
            catchError(this.handleErrors)
        );;
    }

    reset_password_email(user: any = {}) {
        return this.http.post(Config.apiUrl + '/mailresetpassword2', user,{responseType: 'text'}).pipe(
            map(response => response),
            catchError(this.handleErrors)
        );;
    }

    createphoto(file: FormData) {
        console.log("**!!FILE!!**");
        console.log(file);
          return this.http.post(Config.apiUrl + '/upload', file);
      }

    createphoto2(file:string) {
        
        var name = file.substr(file.lastIndexOf("/") + 1);
        var bghttp = require("nativescript-background-http");
        var session = bghttp.session("image-upload");

        var request = {
            url: Config.apiUrl + '/upload2',
            method: "POST",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": name
            },
            description: "Uploading " + name,
            //androidDisplayNotificationProgress: false,
            androidAutoClearNotification: true,
        };
        console.log("4");

        var task =session.uploadFile(file, request);
        console.log("Imagen subida correctamente");
        return true;
    }

    getCommonHeaders2() {
        return {
            "Content-Type": "application/octet-stream",
            "File-Name": name
        }
    }
    getCommonHeaders() {
        return {
            "Content-Type": "application/json",
            "Authorization": Config.authHeader
        }
    }

    //logout(_id: string, user: User) {
       // return this.http.put(Config.apiUrl + '/users/logout/' + _id, user);
    //}

    handleErrors(error: Response) {
        console.log(JSON.stringify(error));
        return throwError(error);
    }

    //Hemos añadido el {responseType: 'text'} porque daba un error de aprsing cuando leía el valor retornado
    // El método en realidad es  return this.http.put(Config.apiUrl + '/users/' + user._id, user)
    update(user: User) {
        return this.http.put(Config.apiUrl + '/users/' + user._id, user,{responseType: 'text'});
    }

    /*updateID(user: User) {
        return this.http.put(Config.apiUrl + '/users/' + user._id, user,{responseType: 'text'});
    }*/

    getTestByClass(_idsession: string) {
        return this.http.get<any>(Config.apiUrl + '/users/getsessionteacher/' + _idsession);
        //return this.http.get(appConfig.apiUrl + '/users/getsession/' + _id);
    }

    /*connectUsertoSession(user: User) {
        return this.http.put(Config.apiUrl + '/users/idsession/' + user._id, user,{responseType: 'text'}).pipe(
            map(response => response),
            catchError(this.handleErrors)
        );;
    }*/

    getAllClassrooms() {
        return this.http.get<any[]>(Config.apiUrl + '/users/getaulas');
    }

    connectStudentToSession(user: User) {
        return this.http.post(Config.apiUrl + '/users/connectstudenttosession', user).pipe(
            map(response => response),
            catchError(this.handleErrors)
        );
    }

    //Teacher

    getTeacher(_teacher: string){
        return this.http.get<any>(Config.apiUrl + '/users/getteacher/' + _teacher);
    }

    //Cursos

    getCurso(_curso: string){
        return this.http.get<any>(Config.apiUrl + '/users/getcurso/' + _curso);
    }

    logout(_id: string, user: User) {
        return this.http.put(Config.apiUrl + '/users/logout/' + _id, user,{responseType: 'text'});
    }

    public setUser(data:any){
        localStorage.setItem("currentUser",JSON.stringify(data));
    }

    public currentUser(){
        return JSON.parse(localStorage.getItem("currentUser")); //|| null;
    }

}