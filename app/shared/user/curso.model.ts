export class Curso {
    _id: string;
    teacher: string;
	subject: string;
	group: string;
    year_curso: string;
    date: string;
}