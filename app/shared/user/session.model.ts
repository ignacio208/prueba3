export class Session {
    _id: string;
    classroom: string;
	  group: string;
	  subject: string;
    date: string;
    available: string;
    answers: string;
    curso:string;
    code_session: string;

}