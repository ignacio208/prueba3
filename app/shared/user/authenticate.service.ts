import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";

import * as localStorage from "nativescript-localstorage";

import { User } from "./user.model";
import { Config } from "../config";

@Injectable()
export class AuthenticateService {
    constructor(private http: HttpClient) { }

    register(user: User) {
        if (!user.username || !user.password) {
            return throwError("Please provide both an email address and password.");
        }

        return this.http.post(
            Config.apiUrl + "user/" + Config.appKey,
            JSON.stringify({
                username: user.username,
                email: user.username,
                password: user.password
            }),
            { headers: this.getCommonHeaders() }
        ).pipe(
            catchError(this.handleErrors)
        );
    }

    login(user: User) {
        return this.http.post(
            Config.apiUrl + '/users/authenticate',
            ({
                username: user.username,
                password: user.password
            })
        ).pipe(
            map(response => response),
            catchError(this.handleErrors)
        );

    }

    login5(user: User) {
        return this.http.post(Config.apiUrl + '/users/authenticate', { username: user.username, password: user.password })
            .subscribe(user32 => {
                // login successful if there's a jwt token in the response
                if (user32) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user32;
            });
    }

    create(user: User) {
        return this.http.post(Config.apiUrl + '/users/register', user);
    }
    createphoto(file: FormData) {
        console.log("**!!FILE!!**");
        console.log(file);
          return this.http.post(Config.apiUrl + '/upload', file);
      }

    
    getCommonHeaders() {
        return {
            "Content-Type": "application/json",
            "Authorization": Config.authHeader
        }
    }

    //logout(_id: string, user: User) {
       // return this.http.put(Config.apiUrl + '/users/logout/' + _id, user);
    //}

    handleErrors(error: Response) {
        console.log(JSON.stringify(error));
        return throwError(error);
    }

    //Hemos añadido el {responseType: 'text'} porque daba un error de aprsing cuando leía el valor retornado
    // El método en realidad es  return this.http.put(Config.apiUrl + '/users/' + user._id, user)
    update(user: User) {
        return this.http.put(Config.apiUrl + '/users/' + user._id, user,{responseType: 'text'});
    }

    logout(_id: string, user: User) {
        return this.http.put(Config.apiUrl + '/users/logout/' + _id, user,{responseType: 'text'});
    }

    public setUser(data:any){
        localStorage.setItem("currentUser",JSON.stringify(data));
    }

    public currentUser(){
        return JSON.parse(localStorage.getItem("currentUser")); //|| null;
    }
}