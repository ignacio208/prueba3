export class User {
    _id: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    class: string;
    seat: string;
    numat: string;
    qrstring: string;
    answer: string;
    code_session: number;
    id_class_session: string;
    src: string;
  }