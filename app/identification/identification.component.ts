import { Component, OnInit, ValueProvider } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

import { User } from "../shared/user/user.model";
import { UserService } from "../shared/user/user.service";

import { Page, horizontalAlignmentProperty } from "tns-core-modules/ui/page";

import { BarcodeScanner } from 'nativescript-barcodescanner';
import { Nfc } from "nativescript-nfc";
import { NfcNdefData } from "nativescript-nfc";

@Component({
	selector: "identification",
    moduleId: module.id,
    providers: [UserService],
	templateUrl: "./identification.component.html",
	styleUrls: ['./identification.component.css']
})
export class IdentificationComponent implements OnInit {
    currentUser: User;
    mensaje: any;
    nfc = new Nfc();
    bol: boolean;
    valid_QR: boolean;

    classes : any[] =[];

    enableAulas = false;
    enableSeats = false;

    seats = [
        {value: '1'},{value: '2'},{value: '3'},{value: '4'},{value: '5'},{value: '6'},{value: '7'},{value: '8'},{value: '9'},{value: '10'},{value: '11'},{value: '12'},{value: '13'},{value: '14'},{value: '15'},{value: '16'},{value: '17'},
        {value: '18'},{value: '19'},{value: '20'},{value: '21'},{value: '22'},{value: '23'},{value: '24'},{value: '25'},{value: '26'},{value: '27'},{value: '28'},{value: '29'},{value: '30'},{value: '30'},{value: '31'},{value: '32'},{value: '33'}
      ];

    constructor( private page: Page, private userService: UserService,private routerExtensions: RouterExtensions) {
        this.currentUser = new User();
        this.page.actionBarHidden = true;

        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //console.log(JSON.stringify(this.currentUser));
        //this.currentUser=this.userService.currentUser()
    }

	ngOnInit(): void {
        this.loadAllClassrooms();
    }
    
    private loadAllClassrooms() {
        this.userService.getAllClassrooms().subscribe(
          classes => { this.classes = classes; });
  }
    back()
    {
        this.currentUser.code_session=null;
        //this.userService.setUser(this.currentUser);
        localStorage.setItem("currentUser",JSON.stringify(this.currentUser));
            //console.log(this.currentUser.seat);
            //console.log(JSON.stringify(this.currentUser));
        this.userService.update(this.currentUser).subscribe(
            data => {
                    //alert("Registrado en la clase con éxito");
                    this.routerExtensions.back();
            },
            error => {
                alert("Error al volver a seleccionar código de sessión");
            });
    }
    

    public onItemTapClasses() {
        this.enableAulas = true;
    }

    public onItemTapSeats() {
        this.enableSeats = true;
    }

    public scanBarcode() {
        new BarcodeScanner().scan({
            formats: "QR_CODE, EAN_13",
            cancelLabel: "EXIT. Also, try the volume buttons!", // iOS only, default 'Close'
            cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
            message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
            showFlipCameraButton: true,   // default false
            preferFrontCamera: false,     // default false
            showTorchButton: true,        // default false
            beepOnScan: true,             // Play or Suppress beep on scan (default true)
            fullScreen: true,             // Currently only used on iOS; with iOS 13 modals are no longer shown fullScreen by default, which may be actually preferred. But to use the old fullScreen appearance, set this to 'true'. Default 'false'.
            torchOn: false,               // launch with the flashlight on (default false)
            closeCallback: () => { console.log("Scanner closed")}, // invoked when the scanner was closed (success or abort)
            resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text

            openSettingsIfPermissionWasPreviouslyDenied: true, // On iOS you can send the user to the settings app if access was previously denied
            presentInRootViewController: true // iOS-only; If you're sure you're not presenting the (non embedded) scanner in a modal, or are experiencing issues with fi. the navigationbar, set this to 'true' and see if it works better for your app (default false).
          }).then(
            (result) => {
                this.mensaje = result.text, 
                this.mensaje = this.mensaje.replace(/&#34;/gi,"\""),

                this.function1()

                //alert({
                    //title: "Scan result",
                    //message: "Format: " + result.format + ",\nValue: " + result.text,
                    //okButtonText: "OK"
                  //});
             },
            (errorMessage) => {
              console.log("No scan. " + errorMessage);
            },
          );
          //if(this.valid_QR==false)
            alert("QR no válido");
        }        

    public function1 () {
        if(this.saveJSON())
            this.update(this.currentUser)
        else
            alert("QR no válido")        
    }


    public mostrarmensaje(){
        alert("Función no habilitada actualmente");
    }   
        
    public saveJSON(){

    // /mensaje/gi sirve para que reemplace todos los valores encontrados, porque sin esto solo reemplaza el primero
        try{
            var json = JSON.parse(this.mensaje);
            if(json.clase && json.asiento)
            {
                this.currentUser.class = json.clase;
                this.currentUser.seat = json.asiento;
                //localStorage.setItem("currentUser",JSON.stringify(this.currentUser));
                console.log("Mensaje de Prueba")
                return true;
            }
            else
            {
                this.valid_QR = false;
                alert("QR no válido");
                console.log("QR no válido 1");
                return false;
            }
        }
        catch
        {
                this.valid_QR = false;
                alert("QR no válido");
                console.log("QR no válido 1");
                return false;
        }
    }


    public mostrarValores(){
            console.log(JSON.stringify(this.currentUser));
            alert(JSON.stringify(this.currentUser));
            //alert("La clase leída del qr es: " + this.currentUser.class + "\n El asiento leído del qr es: " +  this.currentUser.seat
            //+ "\n El id de la sesión  es: " +  this.currentUser._id + this.currentUser.username );

        }

        

    update(currentUser: User) {
        if(this.currentUser.seat!="" && this.currentUser.class!="")
        {
            this.userService.setUser(this.currentUser);
            //localStorage.setItem("currentUser",JSON.stringify(currentUser));
            console.log(this.currentUser.seat);
            console.log(JSON.stringify(this.currentUser));
            //this.userService.update(this.currentUser).subscribe(
            this.userService.connectStudentToSession(this.currentUser).subscribe(
                    data => {
                        alert("Registrado en la clase con éxito");
                        this.routerExtensions.navigate(["/student"]);
                    },
                    error => {
                        alert("Error de registro en clase");
                    });
        }
        else{
            alert("Asiento y clase vacíos");
        }
    }


    update2() {
        this.currentUser.seat="4";
        this.currentUser.class="A-10";
        if(this.currentUser.seat!="" && this.currentUser.class!="")
        {

            //console.log(JSON.stringify(this.currentUser));
            localStorage.setItem("currentUser",JSON.stringify(this.currentUser));

            this.userService.connectStudentToSession(this.currentUser).subscribe(
                    session => {
                        console.log(JSON.stringify(session));
                        localStorage.setItem('currentSession', JSON.stringify(session));
                        //alert("Registrado en la clase con éxito");
                        this.routerExtensions.navigate(["/student"]);
                    },
                    error => {
                        alert("Error de registro en clase");
                    });
        }
        else{
            alert("Por favor rellene los campos de clase y asiento");
        }
    }



    public mensajeReadNFC(){
        alert("Por favor acerque un chip NFC")
    }
    public ReadNFC()
    {
      this.nfc.setOnNdefDiscoveredListener((data: NfcNdefData) => {
        // data.message is an array of records, so:

        if (data.message) {
          //for (let m in data.message) {
            let record = data.message[0];
            this.mensaje = record.payloadAsString; 
            this.mensaje = this.mensaje.replace(/&#34;/gi,"\""),

            this.saveJSON(),
            this.update(this.currentUser);
            //alert(
                //"\n data.message: " + record.payloadAsString
            //)         
        }
      }, {
        // iOS-specific options
        stopAfterFirstRead: true,
        scanHint: "Scan a tag"
      }).then(() => {
          console.log("OnNdefDiscovered listener added");
      });
    }

    logout() {
            this.currentUser = this.userService.currentUser();
            this.userService.logout(this.currentUser._id, this.currentUser).subscribe(
                    data => {
                        this.routerExtensions.navigate(['/login'],{clearHistory: true});
                    },
                    error => {
                        alert("Error al cerrar sesión");
                    });
      }

}