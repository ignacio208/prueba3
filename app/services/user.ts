import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { User } from "../shared/user/user.model";
import { Config } from "../shared/config";


@Injectable()
export class UserServiceRegister {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get(Config.apiUrl + '/users');
    }

    getAllByClass(_id: string) {
        return this.http.get(Config.apiUrl + '/users/getstudentsclass/' + _id);
    }

    getById(_id: string) {
        return this.http.get(Config.apiUrl + '/users/' + _id);
    }

    create(user: User) {
        return this.http.post(Config.apiUrl + '/users/register', user);
    }

    createphoto(file: FormData) {
        console.log("**!!FILE!!**");
        console.log(file);
        return this.http.post(Config.apiUrl + '/upload', file);
    }



    logout(_id: string, user: User) {
        return this.http.put(Config.apiUrl + '/users/logout/' + _id, user);
    }

    
}