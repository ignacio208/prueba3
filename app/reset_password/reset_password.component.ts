import { Component, OnInit, ElementRef, ViewChild} from "@angular/core";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { MatSnackBar, MatBottomSheet, MatBottomSheetRef } from '@angular/material';

import { User } from "../shared/user/user.model";
//import { User2 } from "./shared/user/user.model copy";
import {  UserService } from "../shared/user/user.service";
import * as imagepicker from "nativescript-imagepicker";
import * as ImageSourceModule from "image-source";
var fs = require("file-system");
import { Image } from "ui/image";
import * as imageScoure from "tns-core-modules/image-source";
import { ImageAsset } from "tns-core-modules/image-asset/image-asset";
//import { AlertService, } from "../shared/user/alert.service";


@Component({

	selector: "reset_password",
	moduleId: module.id,
	templateUrl: "./reset_password.component.html",
	styleUrls: ['./reset_password.component.css']
})
export class Reset_PasswordComponent implements OnInit {

    model: any = {};
    model2: any = {};
    loading = false;

    //Campo email
    //email = new FormControl('', [Validators.required, Validators.email]);

    //Campo Contraseña
    hide = true;

    password: any;
    password1: any;
	imageAssets = [];
	imageSrc: any;
	
    //FOTO
	selectedFile : File = null;
	
	user: User;
	toEmail;
	ccEmail;
	bccEmail;
	subject;
	message;

	constructor(	
		private page: Page, 
		private userService: UserService, 
		private routerExtensions: RouterExtensions, 
		//private alertService: AlertService,        
		//public snackBar: MatSnackBar
		) 
		{
		
		this.page.actionBarHidden = true;
		this.user = new User();
		this.user.username = "";
		this.user.password = "";
		}

	ngOnInit(): void {
	}

	/*public image_picker3(){

			let that = this
			let context = imagepicker.create({ mode: "single" });
			context
				.authorize()
				.then(() => {
					return context.present();
				})
				.then(selection => {
					const imageAsset = selection.length > 0 ? selection[0] : null;
					ImageSourceModule.fromAsset(imageAsset).then(
						savedImage => {
							let filename = "image" + "-" + new Date().getTime() + ".png";
							let folder = fs.knownFolders.documents();
							let path = fs.path.join(folder.path, filename);
							savedImage.saveToFile(path, "png");
							var loadedImage = ImageSourceModule.fromFile(path);
							//loadedImage.filename = filename;
							//loadedImage.note = "";
							//that.arrayPictures.unshift(loadedImage);
							//that.storeData();
							console.log(loadedImage);

						},
						err => {
							console.log("Failed to load from asset");
							console.log(err)
						}
					);
				})
				.catch(err => {
					console.log(err);
				});
	
	}*/

	public image_picker(){

		let context = imagepicker.create({
			mode: "single" // use "multiple" for multiple selection
		});

		context.authorize().then(() => {
				return context.present();
			}).then((selection) => {

				this.imageSrc=JSON.stringify(selection[0]);

				var json = JSON.parse(this.imageSrc);
				this.imageSrc = JSON.stringify(json._android);
				this.imageSrc = this.imageSrc.replace(/"/gi,"");
				this.model.src = this.imageSrc.substr(this.imageSrc.lastIndexOf("/") + 1);
				console.log("Ruta del archivo seleccionado: " + JSON.stringify(json._android));

			}).catch(function (e) {
				console.log(e);
		});
	}

	register2()
	{
		console.log("hola55");
		if(this.model.firstName && this.model.lastName && this.model.username && this.model.numat && this.model.password && this.imageSrc)
		{
			console.log("hola234");
			console.log("hola" + (JSON.stringify(this.model)));
			if(this.userService.createphoto2(this.imageSrc))
			{
				console.log((JSON.stringify(this.model)));
				this.userService.create(this.model)
				.subscribe(
					data => {
						alert("Registrado con éxito");
						this.routerExtensions.navigate(['/login']);
					},
					error => {
						alert(error);
						this.loading = false;
					});

			}
			else{
				alert("No se ha podido subir su foto");
			}
		}
		else{
			alert("Complete todos los campos requeridos");
		}

		//var file =  "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200225-WA0002.jpg";
		//var url = "https://some.remote.service.com/path";
		//var name = file.substr(file.lastIndexOf("/") + 1);
		//console.log(name);
		//alert("Función no disponible actualmente")
	}

   register() {
	//this.loading = true;
	//this.model.src = this.selectedFile.name;
	this.userService.create(this.model)
		.subscribe(
			data => {
				alert("Registrado con éxito");
				this.routerExtensions.navigate(['/login']);
				console.log(data);
			},
			(exception) => {
				if (exception.error && exception.error.description) {
					alert(exception.error.description);
				} else {
					alert(exception.error);
				}
			})
		
	}

    comprobar_contraseña(){
        if(this.model2.password && this.model2.password1 && this.password==this.password1)
            return true;
        else
            return false;
    }
	resetear(){
        if(this.model2.password && this.model2.password1 && this.model.token && this.model.username)
        {
            this.model.password = this.model2.password;
            console.log(JSON.stringify(this.model));
            this.userService.resetPassword(this.model)
            .subscribe(
                data => {
                    alert("Su contraseña se ha reseteado");
                    this.routerExtensions.navigate(['/login']);
                    //console.log(data);
                },
                (exception) => {
                    if (exception.error && exception.error.description) {
                        alert(exception.error.description);
                    } else {
                        alert(exception.error);
                    }
                })
            
        }
        else{
            alert("Rellene todos los campos");
        }
    }
	back() {
			this.routerExtensions.navigate(["/login"]);
	}
	/*upload(attachment) {
		console.log(attachment);
		  this.userService.createphoto(attachment)
			  .subscribe(
				  data => {
					  alert("Subido con éxito ");
					  this.routerExtensions.navigate(['/login']);
				  },
				  error => {
					  alert(error);
				  });
	  }*/
	  onFileSelected(event){
		this.selectedFile = event.target.files[0];
		console.log(this.selectedFile);
	  }
	  onUpload(){
		try{
			console.log("Prueba");
			var file =  "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20200301-WA0001.jpg";
			var name = file.substr(file.lastIndexOf("/") + 1);
			console.log(name);
			const fd = new FormData();
			//fd.append('image', this.selectedFile, this.selectedFile.name);
			fd.append('imagen', file, name);
			console.log('******************************');
			console.log(fd);
			this.userService.createphoto(fd)
			  .subscribe(
				data => {
					alert("Subida foto con éxito ");
					this.routerExtensions.navigate(['/login']);
				},
				error => {
					//this.alertService.error(error);
					console.log(error);
					alert("Error de subida en la imagen");
				});

		}
		catch(e){
			console.log(e);
		}
	}

	  
	//openSnackBar() {
		//this.snackBar.open("Registrado con exito", "Cerrar", {
		  //duration: 2000
		//});
	 // }
}