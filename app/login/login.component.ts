import { Component} from "@angular/core";
import { Router } from "@angular/router";
import { User } from "../shared/user/user.model";
import { UserService } from "../shared/user/user.service";
import { AuthenticateService } from "../shared/user/authenticate.service";

import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { SelectMultipleControlValueAccessor } from "@angular/forms";

@Component({
    selector: "gr-login",
    providers: [UserService],
    moduleId: module.id,
    styleUrls: ["./login.component.css"],
    templateUrl: "./login.component.html"
})

export class LoginComponent {
    user: User;

    isLoggingIn = true;
    selection = true;

    currentUser: User;
    model: any = {};

    

    constructor(private page: Page, private authenticateService: AuthenticateService, private routerExtensions: RouterExtensions,private userService: UserService) {
        this.user = new User();
        this.page.actionBarHidden = true;


        this.currentUser = new User();

        //localStorage.setItem('currentUser', "a")
        //this.user.username="prueba21@gmail.com";
        //this.user.password="prueba21";
    }

    select() {
        this.selection = true;
    }

    select2() {
        this.selection = false;
    }
    
    submit() {
        if (this.isLoggingIn) {
            this.login();
        } else {
            this.signUp();
        }
    }

    login() {
        //console.log(JSON.stringify(this.user));
        this.authenticateService.login(this.user)
            .subscribe(
                response =>{ 
                        //console.log("111");
                        //this.authenticateService.setUser(response);
                       //this.authenticateService.setUser(response);
                       console.log("1" + response);
                       console.log("1" + JSON.stringify(response));

                       localStorage.setItem('currentUser', JSON.stringify(response));               
                       this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
                       console.log("Class: " + this.currentUser.class);
                       console.log("Code session: " + this.currentUser.code_session );
                       if(this.currentUser.class!=null && this.currentUser.code_session!=null)
                       {
                            this.routerExtensions.navigate(["/student"]);
                       }
                       else{
                        this.routerExtensions.navigate(["/session_code"]);
                        //console.log("222");
                        //console.log(this.authenticateService.currentUser());
                        console.log(response);
                       }
                     },
                (exception) => {
                    //console.log("Error de logueo");
                    if (exception.error && exception.error.description) {
                        alert(exception.error.description);
                    } else {
                        alert(exception.error)
                    }
                }
            );
    }
    register2() {
        this.routerExtensions.navigate(["/register"]);
    }

    loginteacher()
    {
        alert("Bienvenidoo");
        this.routerExtensions.navigate(["/teacher"]);
    }

    signUp() {
        this.authenticateService.register(this.user)
            .subscribe(
                () => {
                    alert("Your account was successfully created.");
                    this.toggleDisplay();
                },
                (exception) => {
                    if (exception.error && exception.error.description) {
                        alert(exception.error.description);
                    } else {
                        alert(exception)
                    }
                }
            );
    }

    forgotPassword()
    {
        if(this.user.username)
        {
            //alert("Se ha mandado un enlace a su correo para restaurar la contraseña");         
            {
                this.model.username = this.user.username;
                //this.model.password ="hola";
                //this.model.token ="hola";
                console.log(JSON.stringify(this.model));

                this.userService.reset_password_email(this.model)
                .subscribe(
                    data => {
                        alert("Se ha mandado un enlace a su correo para restaurar la contraseña");
                        this.routerExtensions.navigate(["/reset_password"]);
                        //console.log(data);
                    },
                    (exception) => {
                        if (exception.error && exception.error.description) {
                            alert(exception.error.description);
                        } else {
                            alert(exception.error);
                        }
                    })
                }
        }
        else{
            alert("Por favor introduzca un correo para restaurar la contraseña")
        }
    }

    toggleDisplay() {
        this.isLoggingIn = !this.isLoggingIn;
    }


}

