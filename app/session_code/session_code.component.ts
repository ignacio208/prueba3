import { Component, OnInit, ElementRef, ViewChild} from "@angular/core";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
//import { MatSnackBar, MatBottomSheet, MatBottomSheetRef } from '@angular/material';

import { User } from "../shared/user/user.model";
import {  UserService } from "../shared/user/user.service";
//import { AlertService, } from "../shared/user/alert.service";

@Component({
	selector: "Session_code",
	moduleId: module.id,
	templateUrl: "./session_code.component.html",
	styleUrls: ['./session_code.component.css']
})
export class Session_codeComponent implements OnInit {
  
    currentUser: User;
    model: any = {};
    loading = false;
    mensaje: any;

    bol: boolean

    constructor( private page: Page, private userService: UserService,private routerExtensions: RouterExtensions) {
        this.currentUser = new User();
        this.page.actionBarHidden = true;
        //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser=this.userService.currentUser()
    }


	ngOnInit(): void {
	}

	insert_code_session()
    {   if(this.model.code_session=="")
            alert("Introduzca un código de sesión válido");
        else
        {
            this.currentUser.code_session = parseInt(this.model.code_session);
            localStorage.setItem("currentUser",JSON.stringify(this.currentUser));
            //console.log(this.model.code_session);
            //console.log(this.currentUser.session_code);
            //alert(this.currentUser.session_code);
            //this.userService.setUser(this.currentUser);
            this.update(this.currentUser);
            //alert(this.currentUser.session_code);
        }
    }
    
    update(currentUser: User) {
        //this.userService.setUser(this.currentUser);
        localStorage.setItem("currentUser",JSON.stringify(currentUser));
        //console.log(this.currentUser.seat);
        //console.log(JSON.stringify(this.currentUser));
        this.userService.update(this.currentUser).subscribe(
                data => {
                    this.routerExtensions.navigate(["/identification"]);
                },
                error => {
                    alert("Error al actualizar el código de sessión");
                });
    }
}