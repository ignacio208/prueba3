import { NgModule } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from './register/register.component';
import { Register2Component } from './register2/register.component';
import { StudentComponent } from './student/student.component';
import { TeacherComponent } from './teacher/teacher.component';
import { Session_codeComponent } from './session_code/session_code.component';
import { IdentificationComponent } from './identification/identification.component';
import { Reset_PasswordComponent } from './reset_password/reset_password.component';


import { UserService } from "./shared/user/user.service";
import { AuthenticateService } from "./shared/user/authenticate.service";

import { JwtInterceptorProvider, ErrorInterceptorProvider } from './helpers/index';

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
    NativeScriptRouterModule,
    NativeScriptModule,
    NativeScriptFormsModule,
    AppRoutingModule,
    NativeScriptHttpClientModule
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    Register2Component,
    StudentComponent,
    TeacherComponent,
    Session_codeComponent,
    IdentificationComponent,
    Reset_PasswordComponent
  ],
  providers: [
    UserService,
    AuthenticateService,
    JwtInterceptorProvider
    //ErrorInterceptorProvider
],
  bootstrap: [AppComponent]
})
export class AppModule { }
