import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationEntry } from "tns-core-modules/ui/frame"
import { ItemEventData } from "tns-core-modules/ui/list-view"

import { Page } from "tns-core-modules/ui/page";

import { User } from "../shared/user/user.model";
import { Curso } from "../shared/user/curso.model";
import { Session } from "../shared/user/session.model";
import { UserService } from "../shared/user/user.service";
@Component({
	selector: "Student",
	moduleId: module.id,
	templateUrl: "./student.component.html",
	styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

    currentUser: User;
    currentTeacher: User;
    currentSession: Session;
    currentCurso: Curso;

    profesor: string;
    clase: string;
    asignatura: string;
    grupo: string;
    asiento_ocupado: string;

    questions: any[] = [];

    information: any[] =[
        {
          "question": "¿Cuál es el símbolo químico del Potasio?",
          "answers":
          { 
            "a": "Ca",
            "b": "K",
            "c": "Pt"
          }
        },
        {
        "question": "¿Cuál es el símbolo químico del Carbono?",
        "answers":
            {
            "a": "Ca",
            "b": "K",
            "c": "C"
            }
       }
    ];


    constructor( private userService: UserService,private routerExtensions: RouterExtensions,private page: Page) {
        this.currentUser = new User();

        this.profesor = "Alberto Brunete";
        this.clase = "B-12";
        this.asignatura = "Electrónica";
        this.grupo = "A-204";
        this.asiento_ocupado = "2";
        
        //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser=this.userService.currentUser()
        this.currentSession = JSON.parse(localStorage.getItem('currentSession'));
        this.page.actionBarHidden = true;
        this.information[0].open = false;
        this.information[0].answers.openA = false;
        this.information[0].answers.openB = false;
        this.information[0].answers.openC = false;
    }



	ngOnInit(): void {
        this.getCurso()
    }
    

    private getCurso() {
        this.userService.getCurso(this.currentSession.curso).subscribe(curso => {
          this.currentCurso = curso[0];
          this.userService.getTeacher(this.currentCurso.teacher).subscribe(teacher => {
            this.currentTeacher = teacher[0];
          },
          error => {
            alert("No se ha podido obtener la información del profesor al que pertenece la sesión, Error: " + error);
          });
        },
        error => {
          alert("No se ha podido obtener la información del curso al que pertenece la sesión, Error: " + error);
        }
      );
    }

    onItemTap(args: ItemEventData): void {
        console.log('Item with index: ' + args.index + ' tapped');
    }

    navigateToQuiz(index: number) {
        console.log("Has pulsado ell index" + index);
    }

    toogleSelection(index) {
        this.information[index].open = !this.information[index].open;
    }

    toogleSelection2(index) {
        this.questions[index].open = !this.questions[index].open;
    }

    identification()
    {
        this.routerExtensions.navigate(["/identification"]);
    }
    salirclase(){
        alert("Función no disponible actualmente");
    }

    /*
    switchColor2(index, indexAnswer){

        for (let i = 0; i < 3; i++) 
        {
            if (i == indexAnswer) {
                this.information[index].answers[indexAnswer].open = true;
            }
            else
            {
                this.information[index].answers[indexAnswer].open = false;
            }
        }
    }*/

    private switchColor(index, indexAnswer){

        if (indexAnswer == "0") {
            this.information[index].answers.openA =  !this.information[index].answers.openA;
            this.information[index].answers.openB = false;
            this.information[index].answers.openC = false;
        } else if (indexAnswer == "1") {
            this.information[index].answers.openA = false;
            this.information[index].answers.openB = !this.information[index].answers.openB;
            this.information[index].answers.openC = false;
        } else if (indexAnswer == "2") {
            this.information[index].answers.openA = false;
            this.information[index].answers.openB = false;
            this.information[index].answers.openC = !this.information[index].answers.openC;
        }
    }

    sendResponse(index){
        if (this.information[index].answers.openA) {
            alert("La respuesta elegida es la a) ")
        } else if (this.information[index].answers.openB) {
            alert("La respuesta elegida es la b) ")
        } else if (this.information[index].answers.openC) {
            alert("La respuesta elegida es la c) ")
        }
        else{
            alert("Por favor seleccione una respuesta")
        }
    }

    sendResponse2(index){
        if (this.questions[index].openA) {
            alert("La respuesta elegida es la a) ")
        } else if (this.questions[index].openB) {
            alert("La respuesta elegida es la b) ")
        } else if (this.questions[index].openC) {
            alert("La respuesta elegida es la c) ")
        }
        else{
            alert("Por favor seleccione una respuesta")
        }
    }

    private switchColor3(index, indexAnswer){

        if (indexAnswer == "0") {
            this.questions[index].openA =  !this.questions[index].openA;
            this.questions[index].openB = false;
            this.questions[index].openC = false;
        } else if (indexAnswer == "1") {
            this.questions[index].openA = false;
            this.questions[index].openB = !this.questions[index].openB;
            this.questions[index].openC = false;
        } else if (indexAnswer == "2") {
            this.questions[index].openA = false;
            this.questions[index].openB = false;
            this.questions[index].openC = !this.questions[index].openC;
        }
    }
    

    getTest(){
        this.userService.getTestByClass(this.currentSession._id).subscribe(
            tests => {
                this.questions = tests[0].questions;
                this.questions[0].open = false;
                this.questions[0].openA = false;
                this.questions[0].openB = false;
                this.questions[0].openC = false;
            },
            err => {
                console.log('Error al obtener los tests: ' + JSON.stringify(err));
            });
    }
    logout() {
        this.currentUser = this.userService.currentUser();
        this.userService.logout(this.currentUser._id, this.currentUser).subscribe(
                data => {
                    this.routerExtensions.navigate(['/login'],{clearHistory: true});
                },
                error => {
                    alert("Error al cerrar sesión");
                });
  }

  
}